/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_SDC_H_
#define IIT_SDC_H_

#include "include.h"
#include "lib.h"
namespace iit{

class SdcParser {

  ifstream  is ;                                           ///< Input file stream for reading sdc file
  double64  _capScalingFac;                                ///< Capacitance scaling factor, base unit is 'fF'
  double64  _timeScalingFac;                               ///< Time scaling factor, base unit is 'ps'

  std::tr1::unordered_map <char,bool> _specCharSdcDir;     ///< Set of delimiters used during sdc file parsing

  bool  read_line_as_tokens (istream& is, deque<string>& tokens, bool includeSpecialChars = false );

public:

  void init_sdc_parser(CompleteTimingInfo& tinfo);

  /**
   * Constructor for the class- opens the given 'filename' file
   */
  SdcParser (string filename): is(filename.c_str())
	{
	  static const char _specialChars[]= {'(', ')', ',', ':', ';', '/', '#', '[', ']', '{', '}', '*', '\"', '\\'};

	  for(int i = 0; i < (int) sizeof(_specialChars); ++i)
	  {
		  _specCharSdcDir[_specialChars[i]] = true;
	  }
	}

} ;

}

#endif /* IIT_SDC_H_*/
