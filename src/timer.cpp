/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#include "include.h"
#include "timer.h"
#include "forward_propagate.h"
#include "backward_propagate.h"
#include "cppr.h"
#include "report.h"
#include "operations.h"
#include "utility.h"
#include "globals.h"
#include "spef.h"
#include "sanity.h"

namespace iit
{

/* Global variables declaration --------------------------------------- */

extern CelllibStorage      celllibstorage_e;
extern CelllibStorage      celllibstorage_l;
extern LibFileInfo         libinfo_e;
extern LibFileInfo         libinfo_l;
//extern CompleteTimingInfo  timing;

extern Instance* CLKSRC;
extern int       numInc;
extern int       Verbose;
extern int64     MaxNumPaths;
extern int64     PathFlag_G;
extern string    BenchmarkDir;
extern bool     _isStaticDone;
extern int64     SkipCnt;
extern int64     NoSkipCnt;

//extern deque   < Instance >       InstanceList;
extern vector  < Hash_Instance >  InstanceHashTable;
//extern deque   < SpefNet >        SpefNetList;
extern vector  < Hash_Instance >  spefNetHashTable;
extern vector  < SpefNet* >       Sort;
//extern vector  < SpefNet* >       IncNetList;
//extern deque   < SpefNet >        WireList;
//extern queue   < SpefNet* >       InitNetList;
//extern vector  < Instance* >      PIList;
//extern vector  < Instance* >      POList;
extern vector  < InstanceIpin* >  ConeEndPoints;
//extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
//extern deque   < Slack_Instance > pathLUTSlackTable_Adv;

extern std::tr1::unordered_map <char,bool> specCharLibDir;

/* End - global variables declaration -----------------------------------*/

/**
 * Constructor for class Timer for initialization of its attributes
 */
Timer::Timer()
{                                                                            // Constructor.
 	_isStaticDone = false;
 	CLKSRC        = NULL ;
 	PathFlag_G    = 1    ;
 	numInc        = 1    ;
 	MaxNumPaths   = 0    ;
 	SkipCnt       = 0    ;
 	NoSkipCnt     = 0    ;
 	_forwardPropagate_ptr  = new ForwardPropagate() ;
 	_backwardPropagate_ptr = new BackwardPropagate();
 	_cppr_ptr              = new CPPR()             ;
  	_report_ptr            = new Report()           ;
 	_operations_ptr        = new Operations()       ;
 	_spef_ptr              = new SpefParser()       ;

	_operations_ptr->forwardPropagate_ptr        = _forwardPropagate_ptr ;
	_operations_ptr->spef_ptr                    = _spef_ptr;
	_forwardPropagate_ptr->backwardPropagate_ptr = _backwardPropagate_ptr ;
 	_cppr_ptr->backwardPropagate_ptr             = _backwardPropagate_ptr;
 	_report_ptr->cppr_ptr                        = _cppr_ptr;

 	// Calling certain initialization functions.
 	init_enum_to_function_name();
}

/**
 * Destructor for class Timer for freeing pointers
 */
Timer::~Timer()
 {
 	delete _forwardPropagate_ptr ;
    delete _backwardPropagate_ptr;
    delete _cppr_ptr             ;
    delete _report_ptr           ;
    delete _operations_ptr       ;
    delete _spef_ptr             ;
 }

/**
 *  Main timing engine that reads input file names, calls BuildCircuit and RunCircuit
 *  Performs complete timing update, CPPR incremental changes and reports final results
 */
void Timer::Engine(int args, char**argv)
{
	/* License Declaration */
	cout <<
	"/*************************************************************************************" << endl <<
	 "* ********************************************************************************* *" << endl <<
	 "* *                                                                               * *" << endl <<
	 "* *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *" << endl <<
	 "* *                                                                               * *" << endl <<
	 "* *  All Rights Reserved.                                                         * *" << endl <<
	 "* *                                                                               * *" << endl <<
	 "* *  This program is free software. You can redistribute and/or modify            * *" << endl <<
	 "* *  it in accordance with the terms of the accompanying license agreement.       * *" << endl <<
	 "* *  See LICENSE in the top-level directory for details.                          * *" << endl <<
	 "* *                                                                               * *" << endl <<
	 "* ********************************************************************************* *" << endl <<
	 "*************************************************************************************/" << endl << endl << endl;

	if (args < 5)
	{
		    cout << "Usage example: ./iitRACE <.tau2015> <.timing> <.ops> <output> [verbose]"<< endl ;
		    exit(0) ;
	}

	if ((args > 5) && (strcmp(argv[5], "verbose")==0))
	{
		Verbose = 1;
	}

	debug_time("*Let the iitRACE begin...*");

	PathFlag_G = 1;

	static const char specialChars[] = {'(', ')', ',', ':', ';', '/', '#', '[', ']', '*', '\"', '\\'};

	for (int i=0; i < (int)sizeof(specialChars); ++i)
		specCharLibDir[specialChars[i]]=true;

	string tau2015File  = argv[1];
	string timingFile   = argv[2];
	string opsFile      = argv[3];
	string outFile      = argv[4];

	string spefFile,verilogFile;
	string dummy,dummy2;

	TimeIt totalTime;
	timeit_start(&totalTime);
	ofstream myfile;
	string benchmark;
	deque <string> splits;
	splits= split(tau2015File,'/');

	if(splits.size() >= 2)
		benchmark = splits[splits.size()-2];
	else
		benchmark= "Run";

	_tau2015File = tau2015File;
	_timingFile = timingFile;
	_opsFile    = opsFile;
	_outFile    = outFile;

	debug_t_start (BuildCircuit_enum);
	BuildCircuit  ();
	debug_t_stop  (BuildCircuit_enum);

	RunCircuit();

	debug_time(benchmark + " Session completed.");
	cout << endl << "---  " << benchmark << " Session completed ! ---" << endl;
	return ;
}

/**
 *  Function to call parsers for each input file and build the underlying data structures for the circuit
 */
void Timer::BuildCircuit (void)
{
	string tau2015File = _tau2015File;
	string timingFile  = _timingFile;
	string opsFile     = _opsFile;
	string outFile     = _outFile;
    string verilogFile = _verilogFile;
    string spefFile    = _spefFile;
    string sdcFile     = _sdcFile;

  if ((tau2015File).size() > 8 && (tau2015File).compare((tau2015File).size() - 8, 8, ".tau2015") == 0)
  {
	std::ifstream is;
	is.open(tau2015File.c_str());

	deque<string> tau2015;
	read_tau2015_as_tokens(is,tau2015);
	string benchmark;
	deque <string> splits;
	splits= split(tau2015File,'/');

	if(splits.size() >= 2)
		benchmark = splits[splits.size()-2];
	else
		benchmark= "Run";

	string tauFilePath(tau2015File);
	string tauFile(splits[splits.size()-1]);
	string dummy,dummy2;
	dummy = tauFilePath.substr(0,tauFilePath.length()-tauFile.length());
	debug_time("Benchmark: " + benchmark);
	debug_time("Output File: " + outFile);

	BenchmarkDir = dummy;
	string tempDir = BenchmarkDir;
	tempDir = tempDir.append("globalOut.txt");
	string tempDir2 = BenchmarkDir;
	tempDir2 = tempDir2.append("timeInfo.txt");

	for(int i=0;i<4;i++)
	{
		dummy2=dummy;
		tau2015[i]=dummy2.append(tau2015[i]);

	}
	is.close();

	for(int i=0;i<(int)tau2015.size();i++)
	{
		if(tau2015[i].size()>9 && tau2015[i].compare(tau2015[i].size()-9,9,"Early.lib")==0)
		{
			cout << "parsing " << tau2015[i] << endl;
			test_lib_parser(tau2015[i],&celllibstorage_e,&libinfo_e, EARLY);
		}
		else if(tau2015[i].size()>8 && tau2015[i].compare(tau2015[i].size()-8,8,"Late.lib")==0)
		{
			cout << "parsing " << tau2015[i] << endl;
			test_lib_parser(tau2015[i],&celllibstorage_l,&libinfo_l, LATE);
		}
		else if(tau2015[i].size()>5 && tau2015[i].compare(tau2015[i].size()-5,5,".spef")==0)
			spefFile=tau2015[i];
		else if(tau2015[i].size()>2 && tau2015[i].compare(tau2015[i].size()-2,2,".v")==0)
			verilogFile=tau2015[i];
	}

  }
  else // Arguments given accordingly for Sizer, not the timer.
  {
	  cout << "sizer:: parsing " << _libFile << endl;
	    string lFile = _libFile;
		string eFile = _libFile;

		// Parsing the same file into early and late to avoid segfaults in timer
		test_lib_parser(lFile.append(".late"),&celllibstorage_l,&libinfo_l, LATE);
		test_lib_parser(eFile.append(".early"),&celllibstorage_e,&libinfo_e, EARLY);

  }

	if(timingFile.size() > 7 && (timingFile).compare((timingFile).size()-7,7,".timing") == 0)
	{

		cout << "parsing " << timingFile << endl;
		test_timing_parser(timingFile);
	}
	else if(sdcFile.size() > 4 && (sdcFile).compare((sdcFile).size()-4,4,".sdc") == 0)
	{
		test_sdc_parser(sdcFile);
	}
	else
		assert(0);

	cout << "parsing " << verilogFile << endl;
	test_verilog_parser(verilogFile) ;

	sort(InstanceHashTable.begin(),InstanceHashTable.end(),CompareByHashnum<Hash_Instance>());

	if (opsFile.size() > 4 && (opsFile).compare((opsFile).size() - 4, 4,".ops") == 0)
	{
		cout << "parsing " << opsFile << endl;
		test_ops_parser(opsFile);
	}

	cout << "parsing " << spefFile << endl;
	test_spef_parser(spefFile) ;

	cout << "Done Building Circuit"<< endl;

	return;
}

/**
 *  Runs timing analysis on complete circuit and performs complete timing update, CPPR incremental changes and reports final results
 */
void Timer:: RunCircuit(void)
{
	string opsFile = _opsFile;
	string outFile = _outFile;

	ofstream myfile;
	string tempDir3 = BenchmarkDir;
	std::ifstream is;

	tempDir3 = tempDir3.append("results.txt");
	myfile.open(outFile.c_str());

	myfile<<std::fixed;
	myfile.precision(3);
	_isStaticDone  = false;

////////////////////////////////////////////////////////////////////
//
//
//    Running Static Part for once
//
////////////////////////////////////////////////////////////////////

	_forwardPropagate_ptr->  TopSort();
	_forwardPropagate_ptr->  ATSlew(BOTH);
	_backwardPropagate_ptr-> ComputeRATNetBased();
	_backwardPropagate_ptr-> ComputePreCPPRSlack();

	_cppr_ptr->              RunCPPR(numInc);
	ConeEndPoints.           clear();
	_isStaticDone = true;
	debug_time("Static: DONE");

	is.open(opsFile.c_str());
	std::streampos filePos;
	deque <string> commands;
	deque < string > reports;

	while(1)
	{

		string tempBenchmarkDir=BenchmarkDir;
		bool valid;

		_operations_ptr->InitOperations(commands, is, filePos, tempBenchmarkDir, valid);

		// It's time to clear the isChangeSpefRead flag, and keep it ready for next set of incremental commands
		// It is very important to make sure the flags are cleared

		int64 spefEraseStart, instEraseStart;
		sort (InstanceHashTable.begin(), InstanceHashTable.end(), CompareByIndex<Hash_Instance>());
		sort (spefNetHashTable.begin(), spefNetHashTable.end(), CompareByIndex<Hash_Instance>());

		for(int a = InstanceHashTable.size()-1 ; a >= 0  ; a--)
		{
			if(InstanceHashTable[a].index != LLONG_MAX)
			{
				instEraseStart = a+1;
				break;
			}
		}

		for(int a = spefNetHashTable.size()-1 ; a >= 0  ; a--)
		{
			if(spefNetHashTable[a].index != LLONG_MAX)
			{
				spefEraseStart = a+1;
				break;
			}
		}

		InstanceHashTable.erase(InstanceHashTable.begin() + instEraseStart, InstanceHashTable.end());
		spefNetHashTable.erase (spefNetHashTable.begin()   + spefEraseStart, spefNetHashTable.end());

		sort (InstanceHashTable.begin(), InstanceHashTable.end(), CompareByHashnum<Hash_Instance>());
		sort (spefNetHashTable.begin(), spefNetHashTable.end(), CompareByHashnum<Hash_Instance>());

		// Finished Processing commands when no change.spef is given for the current(last) set of commands

		_forwardPropagate_ptr->  InitForwardPropagate(BOTH);
		_backwardPropagate_ptr-> ComputeRATInc();

#if TESTING == 1
		cout << "Testing mode: on - running static CPPR all over again" << endl;
		// for testing purposes -  running static CPPR again.
		ConeEndPoints.clear();
		for (int64 i = 0; i < InstanceList.size(); i++)
		{
			if(InstanceList[i].isIncluded == false) continue;
			if(InstanceList[i].cellLibIndex_e == -3 || (InstanceList[i].cellLibIndex_e >=0 && InstanceList[i].info_early->isSequential))
			{
				for(int j=0; j< InstanceList[i].iipList.size(); j++)
				{
					if(InstanceList[i].iipList[j].isConnected == true)
						ConeEndPoints.push_back(&InstanceList[i].iipList[j]);
				}
			}
		}
		sort (ConeEndPoints.begin(), ConeEndPoints.end(), compareEndPoints2);
#endif

		_cppr_ptr-> RunCPPR(++numInc);  //This is incremental run, make sure to update FFlist

		Sort.clear();
		ConeEndPoints.clear();
		_operations_ptr->ReportOperations(_report_ptr, reports, is, filePos, myfile, valid);

		if(!valid)
			break;
	}
//	cout << "Prune Ratio = " << SkipCnt << " / " << (SkipCnt + NoSkipCnt) <<
//			" = " << ((double64)SkipCnt/(SkipCnt + NoSkipCnt)) << endl;
	myfile.close();
	is.close();
}

/**
 * \param is Input .tau2015 file stream
 * \param tokens Set of words in a line (separated by specified delimiters)
 * \result Returns true if tokens is not empty
 *
 * Reads from the input stream and splits a line into a set of words separated by delimiters
 */
bool Timer::read_tau2015_as_tokens (istream& is, deque<string>& tokens)
{
	tokens.clear();
	bool includeSpecialChars=false;
	string line ;
	std::getline (is, line);

	while (is && tokens.empty())
	{
		string token = "" ;
		for (int i = 0; i < (int)line.size(); ++i)
		{
			char currChar = line[i];
			bool isSpecialChar = is_special_char_tau2015(currChar);
			if (std::isspace (currChar) || isSpecialChar)
			{
				if (!token.empty())
				{
					// Add the current token to the list of tokens
					tokens.push_back(token);
					token.clear();
				}

			if (includeSpecialChars && isSpecialChar)
				tokens.push_back(string(1, currChar));
			}
			else
			{
				// Add the char to the current token
				token.push_back(currChar);
			}
		}

		if (!token.empty())
			tokens.push_back(token);

		// Previous line read was empty. Read the next one.
		if (tokens.empty())
			std::getline (is, line);
	}
	return !tokens.empty();
}

/**
 * \param c Input character to check if its a delimiter
 * \result bool True if c is a special character (i.e. delimiter), else false
 *
 * Checks if a given character c is a special character (i.e. delimiter) or not
 */
bool Timer::is_special_char_tau2015 (char c)
{
	static const char specialChars[] = {'(', ')', ',', ':', ';', '#', '[', ']', '{', '}', '*', '\"', '\\','\''} ;

	for (int i=0; i < (int)sizeof(specialChars); ++i)
	{
		if (c == specialChars[i])
			return true ;
	}
	return false ;
}

};






