/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_INCLUDE_H_
#define IIT_INCLUDE_H_

/**
 * \file include.h
 *
 * This file includes all the necessary libraries in the project such as C++ STL, TBB and so on.
 */
#include <iostream>
#include <fstream>
#include <deque>
#include "tbb/tbb.h"
#include "tbb/concurrent_vector.h"
#include "tbb/concurrent_priority_queue.h"
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <limits>
#include <cassert>
#include <cctype>
#include <algorithm>
#include <typeinfo>
#include <sstream>
#include <queue>
#include <list>
#include <map>
#include <omp.h>
#include <tr1/unordered_map>
#include <time.h>
#include <math.h>
#include <vector>
#include "timeit.h"
#include <iomanip>
#include "typedef.h"
#include "enumerate.h"
#include "macrodef.h"


using namespace std;


#endif /* IIT_INCLUDE_H_*/
