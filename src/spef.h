/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_SPEF_H_
#define IIT_SPEF_H_

#include "include.h"
#include "classdecs.h"
#include "net.h"

namespace iit
{

/**
 * Class for supporting parsing of .spef, finding net delays and moments and linking to cell pins
 * Note - SpefParser assumes the following units - Time (1 PS), Capacitance (1 FF), Resistance (1 KOhm)
 */
class SpefParser
{

public:
	ifstream    is;                           ///< Input .spef file stream

	inline bool read_line_as_tokens_spef      (istream& is, deque<string>& tokens);
	inline bool read_line_as_tokens_spef_res  (istream& is, deque<string>& tokens);
	void        init_spef_parser              (string filename);
	bool        read_net_dataInc              (vector <SpefNet*> & linkWireList) ;
	void        close                         (void);

	void FastTapdelaybeta                     (vector <Cnode>& otherNode, vector <Redge>& rList,
			                                    SpefNet& spefNet, bool isInc = false);
	void NFSTapdelaybeta                      (SpefNet& spefNet, int tIdx, double64 capChange);

private:

	bool read_connections                     (Cnode& portNode, vector <Cnode>& tapNode,
			                                    SpefNet& spefNet,bool& saveRC) ;
	void read_resistances                     (vector <Cnode>& otherNode, Cnode& portNode,
			                                    vector <Cnode>& tapNode, vector <Redge>& rList) ;
	void read_capacitances                    (vector <Cnode>& otherNode, Cnode& portNode,
			                                    vector <Cnode>& tapNode, SpefNet& spefNet);
	bool read_connectionsInc                  (Cnode& portNode, vector <Cnode>& tapNode,
			                                    SpefNet& spefNet, bool& saveRC) ;
	void read_resistancesInc                  (vector <Cnode>& otherNode, Cnode& portNode,
			                                    vector <Cnode>& tapNode, vector <Redge>& rList) ;
	void read_capacitancesInc                 (vector <Cnode>& otherNode, Cnode& portNode,
			                                    vector <Cnode>& tapNode, SpefNet& spefNet);
	bool read_net_data                        (SpefNet& spefNet);
	bool read_net_data_ispd2012               (int64 SpefNetListsz) ;

	void linker                               (void);
	void UpdateParasitics                     (SpefTap* tap);

};

};

#endif
