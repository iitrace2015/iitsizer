/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/


#ifndef IIT_VERILOG_H_
#define IIT_VERILOG_H_

#include "include.h"
#include "classdecs.h"
#include "instance.h"
#include "container.h"
#include "forward_propagate.h"

namespace iit{

class Verilog

{

public:

	ForwardPropagate*              forwardPropagate_ptr;      ///< Pointer to an instance of ForwardPropagate class

	void  ProcessPI                (void);
	void  ProcessPO                (void);
	void  ProcessNet               (void);
	void  Canonicalize             (void);
	void  createNetHash            (void);
	void  fix_checkpoints          (void);
	bool  is_special_char          (char c);
	bool  is_special_char3         (char c);
	bool  read_wire                (string& wire) ;
	void  init_verilog_parser      (string buffer);
	void  SetRelatedPins           (Instance* cell);
	bool  read_module              (string& moduleName) ;
	bool  read_primary_input       (deque<string>& primaryInput) ;
	bool  read_primary_output      (deque<string>& primaryInput) ;
	bool  read_line_as_tokens2     (istream& is, deque<string>& tokens, bool includeSpecialChars=false);
	bool  read_line_as_tokens3     (istream& is, deque<string>& tokens, bool includeSpecialChars=false);
	bool  read_cell_inst           (string& cellType, string& cellInstName, deque<pair<string, string> >& pinNetPairs) ;
	bool  read_cell_inst2          (string& cellType, string& cellInstName, deque<pair<string, string> >& pinNetPairs) ;

	/**
	 * Constructor for the class- Opens 'filename' file and resets the class members to the desired initial values
	 */
	Verilog(string filename): is(filename.c_str())
	{

		_bracket_cnt2  =  0;
		_PIStart       = -2;
		_POStart       = -2;
		_wireStart     = -2;
		_cellStart     = -2;
		_moduleStart   = -2;
		_nextStroke    = -1;
		_isModuleDone  = false;
		_isPIDone      = false;
		_isPODone      = false;
		_isNetDone     = false;
		_isCellDone    = false;
		_isPIFixed     = false;
		_isPOFixed     = false;
		_isWireFixed   = false;

	}

private:

	ifstream                  is           ;       ///< Input file stream for verilog file parsing
	int                       _nextStroke  ;       ///< Next stroke among PIs/POs/Nets in verilog
	int                       _bracket_cnt2;       ///< Bracket count, helpful in parsing
	bool                      _isModuleDone;       ///< Flag for module declaration reading
	bool                      _isPIDone    ;       ///< Flag for PI information reading from PIs listing
	bool                      _isPODone    ;       ///< Flag for PO information reading from POs listing
	bool                      _isNetDone   ;       ///< Flag for net information reading from wire listing
	bool                      _isCellDone  ;       ///< Flag for gate-net connection reading
	bool                      _isPIFixed   ;       ///< Flag for whether location of PI-listing starting point is located
	bool                      _isPOFixed   ;       ///< Flag for whether location of PO-listing starting point is located
	bool                      _isWireFixed ;       ///< Flag for whether location of wire-listing starting point is located
	streampos                 _PIStart     ;       ///< Stream position storing beginning of PI-listing in verilog file
    streampos                 _POStart     ;       ///< Stream position storing beginning of PO-listing in verilog file
    streampos                 _wireStart   ;       ///< Stream position storing beginning of wire-listing in verilog file
    streampos                 _cellStart   ;       ///< Stream position storing beginning of connections-listing in verilog file
    streampos                 _moduleStart ;       ///< Stream position storing beginning of module-declaration in verilog file
	streampos                 _cursorPos   ;       ///< Current cursor position (stream pos) in file reading
	streampos                 _cursorPos1  ;       ///< Another cursor position (stream pos) in file reading
	deque  <Instance      >   CellList     ;       ///< Rudimentary cell list for storing list of all the cells
	vector <Hash_Instance >   wireHashTable;       ///< Temporary has table for nets list - letting SpefParser know about all wires
	vector <Hash_Instance >   HashBank     ;       ///< Hash bank for wires
	vector <Hash_Instance >   PIPONets     ;       ///< Set of nets connected to primary inputs and primary outputs

};

/**
 * A passing call to the SetRelatedPins function from ForwardPropagate class
 */
inline void Verilog::SetRelatedPins(Instance* cell)
{

	forwardPropagate_ptr->SetRelatedPins (cell);

	return;
}

};


#endif /* IIT_VERILOG_H_*/
