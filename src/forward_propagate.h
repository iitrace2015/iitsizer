/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_FORWARD_PROPAGATE_H_
#define IIT_FORWARD_PROPAGATE_H_

#include "include.h"
#include "classdecs.h"
#include "net.h"
#include "backward_propagate.h"

namespace iit{

/**
 * Class to mainly support forward propagation of AT values
 */
class ForwardPropagate
{

public:
	BackwardPropagate* backwardPropagate_ptr;                        ///< Pointer to class BackwardPropagate

	void                       TopSort              (void);
	void                       Init_L               (void);
	void                       ReClear              (void);
	void                       TopSortInc           (void);
	void                       ATSlew               (int mode = BOTH);
	void                       BFSInc               (void);
	void                       setInitNetSort       (void);
	void                       ReachableNets        (void);
	void                       InitForwardPropagate (int mode = BOTH);
	void                       SetRelatedPins       (Instance* cell);
	long int                   FindMaxLevel         (SpefNet* wirePtr);

	vector <int64>             GetRelatedPins       (int pinHash,int cellIndex,int direction);
	double64                   ComputeSlew          (double64 si, double64 m1, double64 m2);
	inline double64            ComputeSlew          (double64 si, double64 dt);
	inline double64            EvaluateLUT          (bool isEarly,double64 slew , double64 cap,
			                                           LibParserLUT& sample);
	pair<SLEW_DATA,DELAY_DATA> ComputePinTiming     (NODE_TIMING_DATA& nodeTimingData, int64 iPinHash,
			                                           int64 oPinHash, int cellIndexEarly,int cellIndexLate,
													   double64 opcapEarly, double64 opcapLate, int mode = BOTH);
	double64                   ComputeSlew          (Instance* currGate, LibParserCellInfo* cellGate,
			                                           double64 output_load, LibParserPinInfo* oPin,
													   bool DefaultSlewMode = true);

	void                       SetPortAT             (SpefNet* currNet);
	void                       SetPI_AT              (SpefNet* currNet);
	
	void                       FindcPi               (InstanceOpin* oPin);

 protected:

	queue  < SpefNet*  >       L;                                 ///< Queue of nets needed for TopSort
	queue  < SpefNet*  >       InitNetSort;                       ///< Queue to store starting points for TopSort
//	deque  < Instance* >       IncCellList;

};

/**
 * Linking function to connect to EvaluateLUT in class BackwardPropagate
 */
inline double64 ForwardPropagate::EvaluateLUT (bool isEarly,double64 slew , double64 cap, LibParserLUT& sample)
{
	return (backwardPropagate_ptr->EvaluateLUT (isEarly, slew , cap, sample));
}

};

#endif
