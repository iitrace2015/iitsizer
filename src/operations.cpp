/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#include "include.h"
#include "operations.h"
#include "timer.h"
#include "spef.h"
#include "globals.h"

namespace iit{

/* Global variables declaration --------------------------------------- */

extern CelllibStorage      celllibstorage_e;
extern CelllibStorage      celllibstorage_l;
//extern LibFileInfo         libinfo_e;
//extern LibFileInfo         libinfo_l;
extern CompleteTimingInfo  timing;

//extern Instance* CLKSRC;
//extern int       numInc;
//extern int       Verbose;
extern int64     MaxNumPaths;
//extern int64     PathFlag_G;
extern string    BenchmarkDir;
//extern bool     _isStaticDone;

extern deque   < Instance >       InstanceList;
extern vector  < Hash_Instance >  InstanceHashTable;
extern deque   < SpefNet >        SpefNetList;
extern vector  < Hash_Instance >  spefNetHashTable;
//extern vector  < SpefNet* >       Sort;
extern vector  < SpefNet* >       IncNetList;
//extern deque   < SpefNet >        WireList;
extern queue   < SpefNet* >       InitNetList;
//extern vector  < Instance* >      PIList;
//extern vector  < Instance* >      POList;
extern vector  < InstanceIpin* >  ConeEndPoints;
//extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
//extern deque   < Slack_Instance > pathLUTSlackTable_Adv;

//extern std::tr1::unordered_map <char,bool> specCharLibDir;

/* End - global variables declaration -----------------------------------*/

/**
 * \param is Input file stream
 * \param tokens Set of tokens read and split from a line
 * \return Flag for line reading was successful or failed
 *
 * Reads a line from the current position of input file stream
 */
bool Operations:: read_ops_as_tokens (istream& is, deque<string>& tokens)
{
	tokens.clear() ;
	string line ;
	std::getline (is,line) ;

	while (is && tokens.empty())
	{

		char *temp=strdup(line.c_str());

		char*tokenPtr;

		tokenPtr = strtok(temp, " -");
		while(tokenPtr != NULL)
		{
			tokens.push_back(tokenPtr);
			tokenPtr = strtok(NULL, " -");
		}

		free(temp);

		if (tokens.empty())
			std::getline (is, line) ;
	}
	return !tokens.empty();
}

/**
 * \param filename Name of the operations file (.ops)
 *
 * Function of the timer class initiates the call to ops parser
 */
void Timer::test_ops_parser (string filename)
{
	_operations_ptr->init_ops_parser(filename);
}

/**
 * \param filename Name of the operations file (.ops)
 *
 * Reads the ops file to find two things- (i) Set of gates being repowered (ii) Maximum count of number of
 * paths being asked to report
 */
void Operations:: init_ops_parser(string filename)
{

	std::ifstream is;
	is.open(filename.c_str());
	deque<string>tokens;
	bool valid = read_ops_as_tokens(is,tokens);
	int64 instInd;
	MaxNumPaths=1;
		while(valid)
		{
				if(tokens[0]=="repower_gate")
				{
					instInd=Bsearch(InstanceHashTable,ComputeHash(tokens[1]),0,InstanceHashTable.size()-1);
					if(instInd!=-1)
						InstanceList[InstanceHashTable[instInd].index].repowerFlag=true;


				}
				else if(tokens[0] == "report_worst_paths")
				{
					for(int i = 0;i < (int)tokens.size();i++)
					{
						if(tokens[i] == "numPaths")
						{
							MaxNumPaths = MaxNumPaths < atoi(tokens[i+1].c_str()) ? atoi(tokens[i+1].c_str()) : MaxNumPaths;
							break;
						}
					}
				}


			valid=read_ops_as_tokens(is,tokens);
		}

	is.close();

	MaxNumPaths = MaxNumPaths + Threshold(MaxNumPaths);

	return;
}

/**
 * \param commands tokens read from a line in .ops file
 * \param is Input file stream
 * \param filePos Stream position type for knowing position of is in the file
 * \param tempBenchmarkDir Path to the benchmark directory
 * \param valid Success flag for line reading
 *
 * Reads all the incremental design change operations and performs changes to the circuit in order, re-computes
 * delay and slew of affected nets
 */
void Operations::InitOperations(deque <string>& commands, ifstream& is, streampos& filePos, string& tempBenchmarkDir, bool& valid)
{
	filePos= is.tellg();
	valid=read_ops_as_tokens(is,commands);
	while(valid)
	{

		if( commands[0]== "insert_gate")
			InsertGate(commands[1],commands[2]);
		else if(commands[0]=="insert_net" || commands[0]=="create_net")
			CreateNet(commands[1]);
		else if(commands[0].compare(0,6,"report")==0)
		{
			is.seekg(filePos);
			break;
		}

		valid=read_ops_as_tokens(is,commands);
	}
	//CHECKPOINT: Now that all the gates are inserted, only now go ahead and sort the InstanceHashTable now.
	sort (InstanceHashTable.begin(), InstanceHashTable.end(), CompareByHashnum<Hash_Instance>());
	//WARNING: In all incremental functions, make sure to check first for null pointers, like in disconnect pin, check if the
	//pin is already disconnected, then only try to disconnect it, check for such cases in other functions as well.

	//Have to sort SpefNetHashTable Now
	sort (spefNetHashTable.begin(), spefNetHashTable.end(), CompareByHashnum<Hash_Instance>());

	filePos=is.tellg();

	valid=read_ops_as_tokens(is,commands);

	while(valid)
	{
			if(commands[0]=="repower_gate")
			{
				RepowerGate(commands[1],commands[2]);
			}
			else if(commands[0]=="remove_net")
			{
				RemoveNet(commands[1]);
			}
			else if(commands[0]=="remove_gate")
			{
				RemoveGate(commands[1]);
			}
			else if(commands[0]=="connect_pin")
			{
				ConnectPin(commands[1],commands[2]);
			}
			else if(commands[0]=="disconnect_pin")
			{
				DisconnectPin(commands[1]);
			}
			else if(commands[0]=="read_spef")
			{
				ClearIncWireListDB();
				ComputeDBInc();

				/*---------------------------------------------------------------------------------------*
				 * Sending IncWireList to the function to fill the nets that were not there in the list
				 * but their parasitics were changed in change.spef, so connections are lost, have to
				 * link them again.
				 */

				ReadSpef(tempBenchmarkDir.append(commands[1]),IncWireList);
				tempBenchmarkDir=BenchmarkDir;

				for(int64 j=0;j < (int64)IncWireList.size();j++)
				{
					IncWireList[j]->isIncforRAT   = false;
					if(!IncWireList[j]->isIncluded)
						continue;

					linkerInc(*IncWireList[j]);
				}

				ComputeDB  .clear();
				IncWireList.clear();

			}
			else if(commands[0].compare(0,6,"report")==0)
			{
				is.seekg(filePos);
				break;
			}

			filePos=is.tellg();
			valid=read_ops_as_tokens(is,commands);
	}


	//Strategy: Instances are not going to be removed from InstanceList, just gonna be disabled for the use, while they will be
	//erased from InstanceHashTable

	ClearIncWireListDB();

	ComputeDBInc();

	for(int64 j = 0;j < (int64)IncWireList.size(); j++)
		{
			IncWireList[j]->isIncforRAT   = false;
			if(!IncWireList[j]->isIncluded)
				continue;

			linkerInc(*IncWireList[j]);
		}

	IncWireList.clear();
	ComputeDB  .clear();

	return;
}

/**
 * \param report_ptr Pointer to an instance of report class
 * \param reports tokens read from a line in .ops file- especially from lines of report commands
 * \param is Input file stream
 * \param filePos Stream position for 'is' stream
 * \param myfile Output file stream for dumping results
 * \param valid Success flag for reading a line
 *
 * Interprets the report commands from .ops file and dumps the results accordingly- mainly reporting AT/RAT/Slack values and reporting
 * worst paths
 */
void Operations::ReportOperations(Report* report_ptr, deque <string>& reports, ifstream& is, streampos& filePos,ofstream& myfile, bool& valid)
{

	ComputeDB  .clear();
	IncWireList.clear();

	filePos = is.tellg();

	myfile.flush();

	valid=read_ops_as_tokens(is,reports);

	while(valid)
	{
		if(reports[0].compare(0,6,"report")==0)
		{
			report_ptr->DumpResults(myfile,reports);  // Dump results for every report directive
		}
		else if(reports[0].compare(0,6,"report")!=0)
		{
			is.seekg(filePos);
			break;
		}

		filePos=is.tellg();

		valid= read_ops_as_tokens(is,reports);
	}

	myfile.flush();

	return;
}


/**
 * \param hashTable Hash table to be set in order
 *
 * This Function is for inserting an element to already sorted table in O(N) time,
 * rather than blindly sorting ,which would otherwise take O(NlogN).
 * Only one element is off from its position and by iterative swapping, it can be
 * brought back to the correct position such that Hash table retains its property of having elements in sorted
 * order of hashes
 */
void Operations:: InplaceMerge(deque<Hash_Instance>& hashTable)
{
	Hash_Instance temp;
	int64 size = hashTable.size();

	for(int64 i = size-1 ; i > 0 ; i--)
	{
		if(hashTable[i].hash > hashTable[i-1].hash)
		{
			return;
		}
		else
		{
			temp            = hashTable[i];
			hashTable[i]    = hashTable[i-1];
			hashTable[i-1]  = temp;
		}
	}

	return ;
}

/**
 * Clears otherNode and rList of incremental nets, resets their moment values as well. It also sorts the
 * taps of each net in order of their hashes for Bsearch access ahead
 */
void Operations::ClearIncWireListDB(void)
{
	for(int64 j=0;j< (int64)IncWireList.size();j++)
	{
		if(!IncWireList[j]->isIncluded) continue;

		//If Connect/Disconnect Pin has happened on this net, that means it has to be specified in change.spef, hence clearing its otherNode
		//and rList dequeues.
		sort(IncWireList[j]->taps.begin(),IncWireList[j]->taps.end(),CompareByHashnum<SpefTap>());

		IncWireList[j]->otherNode.clear();
		IncWireList[j]->rList.clear();

		IncWireList[j]->netLumpedCap = 0;

		for(int64 k=0;k< (int64)IncWireList[j]->taps.size();k++)
		{
			IncWireList[j]->taps[k].betaEarly = 0;
			IncWireList[j]->taps[k].delayLate = 0;
			IncWireList[j]->taps[k].betaLate  = 0;
			IncWireList[j]->taps[k].delayLate = 0;
		}
	}
	return;
}

/**
 * Recomputes delay and slew for all those nets affected by re-powering and not given in change.spef.
 *
 */
void Operations:: ComputeDBInc(void)
{

	//Computing fastdelaybeta for all the nets changed due to repowering.

	int64 sizeNets = ComputeDB.size();

	for(int64 itr=0;itr<sizeNets;itr++)
	{

		SpefNet* spefNet     = ComputeDB[itr];
		spefNet->isRepowered = false;     //This flag was set true when nets were pushed in ComputeDB, now clearing it !

		if (spefNet->otherNode.size() != 0)
		{
			for (int64 jtr = 0; jtr < (int64)spefNet->taps.size(); jtr++)
			{
				int64 temp = Bsearch(spefNet->otherNode, spefNet->taps[jtr].hash, 0, spefNet->otherNode.size() - 2);

				if(temp==-1)
				{
					continue;
					assert (0);
				}
				else
				{

					spefNet->otherNode[temp].capacitanceEarly = spefNet->taps[jtr].capacitanceEarly;
					spefNet->otherNode[temp].capacitanceLate  = spefNet->taps[jtr].capacitanceLate;
				}
			}
			spef_ptr->FastTapdelaybeta(spefNet->otherNode, spefNet->rList, *spefNet, true);
		}
	}
}

/**
 * \param cellInst Name of the gate to be inserted in the circuit newly
 * \param cellType Type of the cell
 *
 * Inserts a gate in the design, resolves all the dependencies lying around it and getting back the
 * circuit to a well connected graph without any discrepancies/inconsistencies
 */
void Operations:: InsertGate(string cellInst,string cellType)
{

    // This instance will be added in Instance list and InstanceHashTable will be modified accordingly.
	//WARNING: Need not sort InstanceHashTable here, we will be sorting it, once all the new gate instances are added.

	int64 key = ComputeHash(cellInst);
	Instance cell;
	int64 key1   = ComputeHash(cellType);
	int64 index1 = Bsearch(celllibstorage_e.cellhashtable,key1,0,celllibstorage_e.cellhashtable.size()-1);
	int64 index2 = Bsearch(celllibstorage_l.cellhashtable,key1,0,celllibstorage_l.cellhashtable.size()-1);

	if(index1==-1 || index2==-1)
	  return ;


	int64 index_e       = celllibstorage_e.cellhashtable[index1].index; //to get the index in cell lib
	int64 index_l       = celllibstorage_l.cellhashtable[index2].index;
	cell.info_early     = &(celllibstorage_e.celllib[index_e]);
	cell.info_late      = &(celllibstorage_l.celllib[index_l]);
	cell.isIncluded     = true;
	cell.cellLibIndex_e = index_e;cell.cellLibIndex_l = index_l;
	cell.instanceName   = cellInst;
	cell.numVisOut      = 0;
	cell.hash           = key;

	InstanceList.push_back(cell);

	if(cell.info_early->isSequential)
	{
		Instance* cell1 = &(InstanceList.back());
		for (int64 i = 0; i <(int64) cell1->iipList.size(); i++)
		{
			ConeEndPoints.push_back(&(cell1->iipList[i]));     //all the pins at this stage will be disconnected only
		}
	}

	//	Creating iipList,iopList,ckList for newly added gate just like we do in VerilogParser.cpp

	InstanceList.back().iipList.reserve(cell.info_early->ipinhashtable.size());

	for(int z = 0;z< (int)cell.info_early->ipinhashtable.size();z++)
	{
		InstanceIpin ip;
		ip.name.cellid    = cell.hash;
		ip.name.icellName = cell.instanceName;
		ip.info_early     = &cell.info_early->ipins [cell.info_early->ipinhashtable[z].index];
		ip.name.pinName   = ip.info_early->name;
		ip.hash           = cell.info_early->ipinhashtable[z].hash;
		int ind           = Bsearch(cell.info_late->ipinhashtable,ip.hash,0,cell.info_late->ipinhashtable.size()-1);
		ip.info_late      = &cell.info_late->ipins[cell.info_late->ipinhashtable[ind].index];
		InstanceList.back().iipList.push_back(ip);
	}

	InstanceList.back().iopList.reserve(cell.info_early->opinhashtable.size());
	for(int z = 0;z< (int)cell.info_early->opinhashtable.size();z++)
	{
		InstanceOpin op;
		op.name.cellid      = cell.hash;
		op.name.icellName   = cell.instanceName;
		op.name.pinName     = cell.info_early->opins[cell.info_early->opinhashtable[z].index].name;
		op.hash             = cell.info_early->opinhashtable[z].hash;
		SpefNet* wirePtr    = new SpefNet();
		op.info_early       = &cell.info_early->opins[cell.info_early->opinhashtable[z].index];
		op.capacitanceEarly = op.info_early->capacitance;
		int ind             = Bsearch(cell.info_late->opinhashtable,op.hash,0,cell.info_late->opinhashtable.size()-1);
		op.info_late        = &cell.info_late->opins[cell.info_late->opinhashtable[ind].index];
		op.capacitanceLate  = op.info_late->capacitance;
		InstanceList.back().iopList.push_back(op);


		wirePtr->port.pinPtr  = &InstanceList.back().iopList.back();
		wirePtr->port.cellPtr = &InstanceList.back();

		InstanceList.back().iopList.back().wire = wirePtr;
	}

	if(cell.info_early->isSequential)
	{
		InstanceIpin ip;
		ip.name.cellid    = cell.hash;
		ip.name.icellName = cell.instanceName;
		ip.name.pinName   = cell.info_early->clkpin[0].name;
		ip.hash           = cell.info_early->clkpin[0].hash;
		ip.info_early     = &cell.info_early->clkpin[0];
		ip.info_late      = &cell.info_late->clkpin[0];
		InstanceList.back().ckList.push_back(ip);
	}

	SetRelatedPins(&InstanceList.back());

	// Done creating iipList,iopList,ckList for newly added gate

	Hash_Instance h;                    //making the indexTable of InstanceList
	h.hash  = key;
	h.index = InstanceList.size()-1;

	InstanceHashTable.push_back(h);

	return;

}

/**
 * \param spefNet Input net
 *
 * Links the driving and sink pins to the given net- sets appropriate pointers, connections etc.
 */
void Operations::linkerInc (SpefNet& spefNet)
{

	int64  k, pinIndex, cellIndex;
	int64 temp;

	bool clkFlag   = false;
	bool overWrite = false;

	int64 tempHash;

	temp = Bsearch(InstanceHashTable,spefNet.port.portName.pinid, 0, InstanceHashTable.size() - 1);
	if (temp == -1)
		return;

	cellIndex      = InstanceHashTable[temp].index;
	Instance* cell = &(InstanceList[cellIndex]); 							//identified instance of cell connected to this port

	if(!cell->isIncluded)
		cell->isIncluded = true;

	InstanceOpin* op = NULL;
	if (cell->cellLibIndex_e != -2 && cell->cellLibIndex_e != -4)
	{
		tempHash = ComputeHash(spefNet.port.portName.n2);
		temp     = Bsearch(cell->iopList,tempHash, 0, cell->iopList.size() - 1);

		if (temp == -1)
			return;
		else
		{
			op = &(cell->iopList[temp]);

			if(op->isConnected)
				overWrite = true;

			op->isConnected = true;

			if(op->wire->isVirtualNet)
				delete op->wire; //deleting the virtual net

			vector<int> ipinIndexList = op->relPinIndex;

			if(cell->ckList.size()!=0 && cell->info_early->isSequential)
			{
				if(!(cell->ckList[0].wire->isIncremental))
				{
					cell->ckList[0].wire->isIncremental = true;
					IncNetList.push_back(cell->ckList[0].wire);
					cell->ckList[0].wire->isDiscovered  = true;
					InitNetList.push(cell->ckList[0].wire);
				}
			}
			else // for combinational elements
			{
				for(int m =0;m < (int)ipinIndexList.size();m++) // m - related input pin control
				{
					if(cell->iipList[ipinIndexList[m]].isConnected && !(cell->iipList[ipinIndexList[m]].wire->isIncremental))
					{
						/*BEWARE: there can be non-included nets in IncNetList, hence explicit check reqd */
						cell->iipList[ipinIndexList[m]].wire->isIncremental = true;
						IncNetList.push_back(cell->iipList[ipinIndexList[m]].wire);
						cell->iipList[m].wire->isDiscovered = true;
						InitNetList.push(cell->iipList[ipinIndexList[m]].wire);
					}
				}

			}
		}

		if (temp == -1)
			return;

		if(cell->info_early == NULL)
			return;

		pinIndex         = cell->info_early->opinhashtable[temp].index; 							//got correct input pin index in ipins
		op->name.pinName = spefNet.port.portName.n2;

		if (!(cell->info_early!=NULL && cell->info_late!=NULL))
			return;

		op->info_early = &(cell->info_early->opins[pinIndex]);
		op->info_late  = &(cell->info_late->opins[pinIndex]);    			 // setting all params of op correctly
	}
	else
	{
		/* take care whether the opin is new or not */
		if(cell->iopList.size()!=0)
		{
			op               = &(cell->iopList[0]);
			op->hash         = ComputeHash(spefNet.port.portName.n1);
			overWrite        = true;
			op->isConnected  = true;
		}

		else
		{

			op->hash         = ComputeHash(spefNet.port.portName.n1);
			overWrite        = false;
			op->isConnected  = true;
			cell->iopList.push_back(*op);

		}
	}

	op->port               = &(spefNet.port);
	op->name.cellid        = spefNet.port.portName.pinid;
	op->name.icellName     = spefNet.port.portName.n1;
	op->wire               = &(spefNet);
	op->wire->isVirtualNet = false;


	if((cell->cellLibIndex_e == -2 || cell->cellLibIndex_e == -4))
	{
		if(!(op->wire->isIncremental))
		{
			op->wire->isIncremental = true;
			IncNetList.push_back(op->wire);
			InitNetList.push(op->wire);
		}
	}

	if(!overWrite)
		cell->outDegree++;

	op->capacitanceEarly = spefNet.port.capacitanceEarly;
	op->capacitanceLate  = spefNet.port.capacitanceLate;
	op->cellPtr          = cell;
	op->ceffEarly        = spefNet.netLumpedCap;
	op->ceffLate         = spefNet.netLumpedCap;
	spefNet.port.pinPtr  = op;
	spefNet.port.cellPtr = cell;                // setting the backlink from spefport to pin/instance

	/* assigning input pins of the cell*/

	deque<SpefTap> taps;

	for(k = 0; k < (int64)spefNet.taps.size(); k++) //for each tap of the net
	{
		clkFlag   = false;
		overWrite = false;
	 	temp      = Bsearch(InstanceHashTable, spefNet.taps[k].tapName.pinid, 0, InstanceHashTable.size() - 1);

	 	if (temp == -1)
		{
			continue;
		}

	 	cellIndex      = InstanceHashTable[temp].index;
		Instance* cell = &(InstanceList[cellIndex]);

		if(!cell->isIncluded)
			cell->isIncluded = true;

		InstanceIpin* ip = NULL;

		if (cell->cellLibIndex_e != -3)
		{
			tempHash = ComputeHash(spefNet.taps[k].tapName.n2);
 			temp     = Bsearch(cell->iipList,tempHash, 0, cell->iipList.size() - 1);

 			if (temp == -1)
			{
				if (cell->ckList.size() != 0 && tempHash == cell->ckList[0].hash)
				{
					ip = &(cell->ckList[0]);

					if(ip->isConnected)
						overWrite = true;

					ip->isConnected = true;
					pinIndex        = 0;
					clkFlag         = true;
				}
				else
				{
					continue;
				}
			}
			else
			{
				ip = &(cell->iipList[temp]);

				if(ip->isConnected)
					overWrite = true;

				ip->isConnected = true;
				pinIndex        = temp;
			}

			ip->name.pinName     = spefNet.taps[k].tapName.n2;
			ip->name.icellName   = spefNet.taps[k].tapName.n1;
			ip->name.cellid      = spefNet.taps[k].tapName.pinid;
			ip->wire             = &spefNet;
			ip->tap              = &(spefNet.taps[k]);
			ip->capacitanceEarly = spefNet.taps[k].capacitanceEarly;
			ip->capacitanceLate  = spefNet.taps[k].capacitanceLate;

			if(clkFlag == false)
			{
				ip->info_early   = &(cell->info_early->ipins[pinIndex]);
				ip->info_late    = &(cell->info_late->ipins[pinIndex]);
				op->ceffEarly   += ip->info_early->capacitance;
				op->ceffLate    += ip->info_late->capacitance;
				ip->isClk        = false;
				ip->cellPtr      = cell;

				spefNet.taps[k].pinPtr = ip;
			}
			else
			{

				ip->info_early   = &(cell->info_early->clkpin[pinIndex]);
				ip->info_late    = &(cell->info_late->clkpin[pinIndex]);
				op->ceffEarly   += ip->info_early->capacitance;
				op->ceffLate    += ip->info_late->capacitance;
				ip->isClk        = true;
				ip->cellPtr      = cell;
				ip->isConnected  = true;
				spefNet.taps[k].pinPtr = ip;
			}
		}
		else   // for primary outputs
		{
			if(cell->iipList.size()!= 0)
			{
				ip              = &(cell->iipList[0]);
				overWrite       = true;
				ip->isConnected = true;

			}
			else
			{
				overWrite       = false;
				ip->isConnected = true;
				cell->iipList.push_back(*ip);
			}

			ip->name.icellName   = spefNet.taps[k].tapName.n1;
			ip->name.cellid      = spefNet.taps[k].tapName.pinid;
			ip->wire             = &spefNet;
			ip->tap              = &(spefNet.taps[k]);
			ip->capacitanceEarly = spefNet.taps[k].capacitanceEarly;
			ip->capacitanceLate  = spefNet.taps[k].capacitanceLate;
			ip->cellPtr          = cell;
			ip->hash             = ComputeHash(spefNet.taps[k].tapName.n1);
			int64 temp1          = Bsearch(timing.loadcap, ip->hash, 0, timing.loadcap.size()-1);

			if(temp1 != -1)
			{
				op->ceffEarly += timing.loadcap[temp1].earlycap;
				op->ceffLate  += timing.loadcap[temp1].latecap;
			}

			spefNet.taps[k].pinPtr = ip;
		}

		spefNet.taps[k].cellPtr = cell;
	}

}

/**
 * \param cellInst Gate name for removal
 *
 * Remove a given gate from the circuit and resets the connections around it. Resolves all the dependencies lying
 * around it and getting back the
 * circuit to a well connected graph without any discrepancies/inconsistencies
 */
void Operations:: RemoveGate(string cellInst)
{
	int64 key     = ComputeHash(cellInst);
	int64 instInd = Bsearch(InstanceHashTable,key,0,InstanceHashTable.size()-1);

	if(instInd ==-1)
		return;

	InstanceList[InstanceHashTable[instInd].index].isIncluded  = false;
	InstanceList[InstanceHashTable[instInd].index].iipList.clear();
	InstanceList[InstanceHashTable[instInd].index].instanceName = "";
	InstanceList[InstanceHashTable[instInd].index].iopList.clear();
	InstanceList[InstanceHashTable[instInd].index].ckList.clear();

	//Entry will be removed later after all the incremental commands are over
	InstanceHashTable[instInd].index = LLONG_MAX;

   /* InstanceHashTable will be sorted once all the remove gate commands have been processed*/
}

/**
 * \param netName New net-name to be inserted
 *
 * Inserts a new net in the circuit and sets the connections around it. Resolves all the dependencies lying
 * around it and getting back the
 * circuit to a well connected graph without any discrepancies/inconsistencies. The net is added to SpefNetList and
 * spefNetHashTable is updated
 */
void Operations::CreateNet(string netName)
{

	SpefNet newNet;

	newNet.hash           = ComputeHash(netName);
	newNet.netName        = netName;
	newNet.isIncluded     = true;
	newNet.isVirtualNet   = false;
	newNet.netLumpedCap   = 0;

	SpefNetList.push_back(newNet);

	Hash_Instance hashInst;
	hashInst.hash         = newNet.hash;
	hashInst.index        = SpefNetList.size()-1;

	spefNetHashTable.push_back(hashInst);

	return;
}


/**
 * \param netName Net-name to be removed
 *
 * Removes the net from circuit and resets the connections around it. Resolves all the dependencies lying
 * around it and getting back the
 * circuit to a well connected graph without any discrepancies/inconsistencies. The net is flagged as
 * disconnected in SpefNetList and
 * is removed from spefNetHashTable
 */
void Operations::RemoveNet(string netName)
{

	int64 hash       = ComputeHash(netName);
	int64 netInd;
	int64 netHashInd = Bsearch(spefNetHashTable,hash,0,spefNetHashTable.size()-1);

	if(netHashInd == -1)
		return;

	netInd = spefNetHashTable[netHashInd].index;

	SpefNetList[netInd].isIncluded = false;

	// We will erase the entry later.
	spefNetHashTable[netHashInd].index = LLONG_MAX;

	// Disconnecting Port and Opin.
	if(SpefNetList[netInd].port.cellPtr!=NULL && SpefNetList[netInd].port.pinPtr!=NULL)
	{
		int64 oPinInd=Bsearch(SpefNetList[netInd].port.cellPtr->iopList,SpefNetList[netInd].port.pinPtr->hash,
				0,SpefNetList[netInd].port.cellPtr->iopList.size()-1);

		SpefNetList[netInd].port.cellPtr->iopList[oPinInd].isConnected = false;

		Instance* cell                   = SpefNetList[netInd].port.cellPtr;
		cell->outDegree                 -= 1;
		SpefNet* wirePtr                 = new SpefNet();
		wirePtr->port.pinPtr             = &(cell->iopList[oPinInd]);
		wirePtr->port.cellPtr            = SpefNetList[netInd].port.cellPtr;
		cell->iopList[oPinInd].wire      = wirePtr;
		SpefNetList[netInd].port.cellPtr = NULL;
		SpefNetList[netInd].port.pinPtr  = NULL;
	}

	//Disconnecting all the taps of this net from IPins.

	int64 numTaps = SpefNetList[netInd].taps.size();

	for(int64 j=0;j<numTaps;j++)
	{
		if(SpefNetList[netInd].taps[j].cellPtr!= NULL && SpefNetList[netInd].taps[j].pinPtr!= NULL)
		{
			int64 IPinInd=Bsearch(SpefNetList[netInd].taps[j].cellPtr->iipList,SpefNetList[netInd].taps[j].pinPtr->hash,
					0,SpefNetList[netInd].taps[j].cellPtr->iipList.size()-1);
			if(IPinInd!=-1)
			{
				SpefNetList[netInd].taps[j].cellPtr->iipList[IPinInd].isConnected = false;
			}
			else        // means it's a clock pin
				SpefNetList[netInd].taps[j].cellPtr->ckList[0].isConnected        = false;

			SpefNetList[netInd].taps[j].pinPtr->tap  = NULL;
			SpefNetList[netInd].taps[j].pinPtr->wire = NULL;
			SpefNetList[netInd].taps[j].cellPtr      = NULL;
			SpefNetList[netInd].taps[j].pinPtr       = NULL;
		}
	}

	SpefNetList[netInd].otherNode.clear();
	SpefNetList[netInd].rList.clear();
	SpefNetList[netInd].taps.clear();
	SpefNetList[netInd].netName = "";

	return;
}

/**
 * \param filename Changed parasitics file name
 * \param linkWireList List of wires to be linked
 *
 * Reads in the changed .spef which provides parasitics information for newly inserted nets.
 * Rest of the operations are same as the regular .spef parsing operations
 */
void Operations::ReadSpef(string filename, vector<SpefNet*>& linkWireList)
{
	SpefParser sp;

	sp.is.open(filename.c_str());

	SpefNet spefNet;

	bool valid = sp.read_net_dataInc (linkWireList);

	while (valid)
	{
	  valid = sp.read_net_dataInc (linkWireList) ;  // true represents incremental operation
	}

	  sp.is.close();
}


/**
 * \param instName Name of the gate that is being repowered
 * \param cellType New cell type of the gate
 *
 * Changes the cell version of the gate (among lot of its variants from cell library). Necessary attribute
 * changes in cell and pins are done by taking information from newly changed cell version
 */
void Operations:: RepowerGate(string instName, string cellType)
{
	InstanceIpin* ip;
	InstanceOpin* op;
	int64         instHash     = ComputeHash(instName);
	int64 		  cellHash     = ComputeHash(cellType);
	int64         instanceInd  = Bsearch(InstanceHashTable,instHash,0,InstanceHashTable.size()-1);

	if(instanceInd == -1)
		return;

	instanceInd                 = InstanceHashTable[instanceInd].index;
	Instance* cell              = &InstanceList[instanceInd];
	int64 index_e               = Bsearch(celllibstorage_e.cellhashtable,cellHash,0,celllibstorage_e.cellhashtable.size()-1);
	int64 index_l               = Bsearch(celllibstorage_l.cellhashtable,cellHash,0,celllibstorage_l.cellhashtable.size()-1);

	if(index_e == -1 || index_l == -1)
		return;

	int64 celllibindex_e        = celllibstorage_e.cellhashtable[index_e].index;
	InstanceList[instanceInd].cellLibIndex_e = celllibindex_e;
	int64 celllibindex_l        = celllibstorage_l.cellhashtable[index_l].index;
	cell->cellLibIndex_l        = celllibindex_l;
	cell->info_early            = &(celllibstorage_e.celllib[celllibindex_e]);
	cell->info_late             = &(celllibstorage_l.celllib[celllibindex_l]);
	cell->isIncluded            = true;
	cell->instanceName          = instName;
	cell->hash                  = instHash;

	int64 IpinHashInd,opinHashInd;

	vector <InstanceIpin> tempiipList;
	vector <InstanceOpin> tempiopList;

	for(int64 i=0;i < (int64)cell->iipList.size();i++)
	{
		//CHECKPOINT:Have to restore D pin pointer from PathLUT
		ip          = &(cell->iipList[i]);
		string temp = ip->name.icellName;
		temp        = temp.append(":");
		temp        = temp.append(ip->name.pinName);

		//Erasing the D pin from pathLUT that got repowered, because it is anyways gonna be inserted again.
		if(ip->isConnected == true)
		{
			ip->tap->capacitanceEarly -= ip->info_early->capacitance;
			ip->tap->capacitanceLate  -= ip->info_late->capacitance;

			if(ip->wire->port.pinPtr != NULL)
			{
				ip->wire->port.pinPtr->ceffEarly -= ip->info_early->capacitance;
				ip->wire->port.pinPtr->ceffLate -= ip->info_late->capacitance;
			}

			IpinHashInd                = Bsearch(cell->info_early->ipinhashtable,ip->hash,0,cell->info_early->ipinhashtable.size()-1);
			ip->info_early             = &(cell->info_early->ipins[cell->info_early->ipinhashtable[IpinHashInd].index]);
			IpinHashInd                = Bsearch(cell->info_late->ipinhashtable,ip->hash,0,cell->info_late->ipinhashtable.size()-1);
			ip->info_late              = &(cell->info_late->ipins[cell->info_late->ipinhashtable[IpinHashInd].index]);
			ip->tap->capacitanceEarly += ip->info_early->capacitance;
			ip->tap->capacitanceLate  += ip->info_late->capacitance;

			if(ip->wire->port.pinPtr  != NULL)
			{
				ip->wire->port.pinPtr->ceffEarly += ip->info_early->capacitance;
				ip->wire->port.pinPtr->ceffLate += ip->info_late->capacitance;
			}

			if(ip->wire->isRepowered==false)
			{

				ComputeDB.push_back(ip->wire);
				ip->wire->isRepowered=true;
			}
			tempiipList.push_back(cell->iipList[i]);
		}
		else
		{
			IpinHashInd    = Bsearch(cell->info_early->ipinhashtable,ip->hash,0,cell->info_early->ipinhashtable.size()-1);
			ip->info_early = &(cell->info_early->ipins[cell->info_early->ipinhashtable[IpinHashInd].index]);
			IpinHashInd    = Bsearch(cell->info_late->ipinhashtable,ip->hash,0,cell->info_late->ipinhashtable.size()-1);
			ip->info_late  = &(cell->info_late->ipins[cell->info_late->ipinhashtable[IpinHashInd].index]);
			tempiipList.push_back(cell->iipList[i]);
		}

	}
	if(cell->info_early->isSequential==true)
	{

		ip=&(cell->ckList[0]);
		if(ip->isConnected)
		{
			if(ip->wire->port.pinPtr != NULL)
			{
				ip->wire->port.pinPtr->ceffEarly -= ip->info_early->capacitance;
				ip->wire->port.pinPtr->ceffLate  -= ip->info_late->capacitance;
			}

			ip->tap->capacitanceEarly -= ip->info_early->capacitance;
			ip->tap->capacitanceLate  -= ip->info_late->capacitance;
			ip->info_early             = &(cell->info_early->clkpin[0]);
			ip->info_late              = &(cell->info_late->clkpin[0]);

			if(ip->wire->port.pinPtr != NULL)
			{
				ip->wire->port.pinPtr->ceffEarly += ip->info_early->capacitance;
				ip->wire->port.pinPtr->ceffLate  += ip->info_late->capacitance;
			}

			ip->tap->capacitanceEarly += ip->info_early->capacitance;
			ip->tap->capacitanceLate  += ip->info_late->capacitance;

			if(ip->wire->isRepowered == false)
			{
				ComputeDB.push_back(ip->wire);
				ip->wire->isRepowered=true;    //TODO:CHECKPOINT: clear this flag later in FastTapDBInc().
			}
		}

		else
		{
			ip->info_early = &(cell->info_early->clkpin[0]);
			ip->info_late  = &(cell->info_late->clkpin[0]);
		}

	}
	for(int64 i=0;i < (int64)cell->iopList.size();i++)
		{
			op=&(cell->iopList[i]);
			if(op->isConnected==true)
			{
				op->port->capacitanceEarly -= op->info_early->capacitance;
				op->port->capacitanceLate  -= op->info_late->capacitance;
				opinHashInd                 = Bsearch(cell->info_early->opinhashtable,op->hash,0,cell->info_early->opinhashtable.size()-1);
				op->info_early              = &(cell->info_early->opins[cell->info_early->opinhashtable[opinHashInd].index]);
				opinHashInd                 = Bsearch(cell->info_late->opinhashtable,op->hash,0,cell->info_late->opinhashtable.size()-1);
				op->info_late               = &(cell->info_late->opins[cell->info_late->opinhashtable[opinHashInd].index]);
				op->port->capacitanceEarly += op->info_early->capacitance;
				op->port->capacitanceLate  += op->info_late->capacitance;

				if(op->wire->isRepowered == false)
				{
					ComputeDB.push_back(op->wire);
					op->wire->isRepowered=true;  //TODO:CHECKPOINT: clear this flag later in FastTapDBInc().
				}

				tempiopList.push_back(cell->iopList[i]);

			}
			else
			{
				opinHashInd    = Bsearch(cell->info_early->opinhashtable,op->hash,0,cell->info_early->opinhashtable.size()-1);
				op->info_early = &(cell->info_early->opins[cell->info_early->opinhashtable[opinHashInd].index]);
				opinHashInd    = Bsearch(cell->info_late->opinhashtable,op->hash,0,cell->info_late->opinhashtable.size()-1);
				op->info_late  = &(cell->info_late->opins[cell->info_late->opinhashtable[opinHashInd].index]);
				tempiopList.push_back(cell->iopList[i]);

			}
		}


	//	Creating iipList,iopList,ckList for repowered gate just like we do in VerilogParser.cpp

	  cell->iipList.clear();
	  cell->iopList.clear();
	  cell->iipList.reserve(cell->info_early->ipinhashtable.size());

	  for(int z = 0;z < (int)cell->info_early->ipinhashtable.size();z++)
  	  {
		  InstanceIpin ip;

		  ip.hash       = cell->info_early->ipinhashtable[z].hash;
  		  int64 iPinInd = Bsearch(tempiipList,ip.hash,0,tempiipList.size()-1);

  		  if(iPinInd!=-1)
  		  {
  			  cell->iipList.push_back(tempiipList[iPinInd]);
  			  InstanceIpin* tempip = &(cell->iipList.back());

  			  if(tempip->isConnected)
  			  {
				  tempip->tap->cellPtr = cell;
				  tempip->tap->pinPtr  = tempip;
  			  }
  		  }
  		  else{ // means it's a new disconnected pin

  			ip.name.cellid      = cell->hash;
  			ip.name.icellName   = cell->instanceName;
  			ip.name.pinName     = cell->info_early->ipins[cell->info_early->ipinhashtable[z].index].name;
  			ip.info_early       = &cell->info_early->ipins[cell->info_early->ipinhashtable[z].index];
  			int64 ind           = Bsearch(cell->info_late->ipinhashtable,ip.hash,0,cell->info_late->ipinhashtable.size()-1);
  			ip.info_late        = &cell->info_late->ipins[cell->info_late->ipinhashtable[ind].index];
  			ip.capacitanceEarly = ip.info_early->capacitance;
  			ip.capacitanceLate  = ip.info_late->capacitance;
  			ip.isConnected      = false;
  			cell->iipList.push_back(ip);

  		  }
  	  }
	  cell->iopList.reserve(cell->info_early->opinhashtable.size());

  	  for(int z = 0; z < (int)cell->info_early->opinhashtable.size(); z++)

  	  {
  		  InstanceOpin op;
  		  op.name.cellid    = cell->hash;
		  op.name.icellName = cell->instanceName;
		  op.name.pinName   = cell->info_early->opins[cell->info_early->opinhashtable[z].index].name;
  		  op.hash           = cell->info_early->opinhashtable[z].hash;

  		  int64 oPinInd=Bsearch(tempiopList,op.hash,0,tempiopList.size()-1);

  		  if(oPinInd!=-1)
  		  {
			  cell->iopList.push_back(tempiopList[oPinInd]);

			  InstanceOpin* tempop = &(cell->iopList.back());
			  if(tempop->isConnected == true)
			  {
				  tempop->port->pinPtr      = tempop;
				  tempop->wire->port.pinPtr = tempop;
			  }
  		  }
  		  else
  		  {
				// means it's a new and disconnected pin
				SpefNet* wirePtr     = new SpefNet();
				op.info_early        = &cell->info_early->opins[cell->info_early->opinhashtable[z].index];
				int oPinLibInd       = Bsearch(cell->info_late->opinhashtable,op.hash,0,cell->info_late->opinhashtable.size()-1);
				op.info_late         = &cell->info_late->opins[cell->info_late->opinhashtable[oPinLibInd].index];
				op.capacitanceEarly  = op.info_early->capacitance ;
				op.capacitanceLate   = op.info_late->capacitance ;
				op.isConnected       = false;
				op.ceffEarly         = 0;
				op.ceffLate          = 0;
				cell->iopList.push_back(op);


				wirePtr->port.pinPtr      = &(cell->iopList.back());
				wirePtr->port.cellPtr     = cell;
				cell->iopList.back().wire = wirePtr;

  		  }
   	  }

  	  if(cell->info_early->isSequential)
  	  {
  		  InstanceIpin* tempip = &(cell->ckList[0]);

  		  if(tempip->isConnected == true)
  		  {
  			  tempip->tap->cellPtr = cell;
  			  tempip->tap->pinPtr  = tempip;
  		  }


  	  }

  	  SetRelatedPins(cell);
	  ColorParentNets(cell);

	return;
}

/**
 * \param cell Gate being repowered
 *
 * Colors the parents of the driving nets of the current gate 'cell'
 */
void Operations::ColorParentNets(Instance* cell)
{

	if(cell->cellLibIndex_l == -2 || cell->cellLibIndex_l == -4) return;

	if(cell->info_late->isSequential)      // extra consideration for ck pins reqd
	{
		if(cell->ckList[0].isConnected && cell->ckList[0].wire->port.pinPtr!= NULL &&
				cell->ckList[0].wire->port.pinPtr->cellPtr!=NULL)
		{

			InstanceOpin* opinPtr = cell->ckList[0].wire->port.pinPtr;
			Instance* parentPtr   = opinPtr->cellPtr;


			if(parentPtr->cellLibIndex_l == -2 || parentPtr->cellLibIndex_l == -4)
			{
				if(!cell->ckList[0].wire->isIncremental)
				{
					cell->ckList[0].wire->isIncremental = true;
					IncNetList.push_back(cell->ckList[0].wire);
					cell->ckList[0].wire->isDiscovered  = true;
					InitNetList.push(cell->ckList[0].wire);
				}
			}


			vector<int> ipinIndexList = opinPtr->relPinIndex;

			for(int m =0 ; m < (int)ipinIndexList.size(); m++) // m - related input pin control of parent
			{
				if(parentPtr->iipList[ipinIndexList[m]].isConnected &&
						!(parentPtr->iipList[ipinIndexList[m]].wire->isIncremental))
				{
					/*BEWARE: there can be non-included nets in IncNetList, hence explicit check reqd */
					parentPtr->iipList[ipinIndexList[m]].wire->isIncremental = true;
					IncNetList.push_back(parentPtr->iipList[ipinIndexList[m]].wire);
					parentPtr->iipList[ipinIndexList[m]].wire->isDiscovered  = true;
					InitNetList.push(parentPtr->iipList[ipinIndexList[m]].wire);
				}
			}
		}
	}

	for(int i = 0; i < (int)cell->iipList.size(); i++)  // i - input pin control
	{

		if(cell->iipList[i].isConnected && cell->iipList[i].wire->port.pinPtr!=NULL &&
				cell->iipList[i].wire->port.pinPtr->cellPtr!=NULL)
		{
			InstanceOpin* opinPtr = cell->iipList[i].wire->port.pinPtr;

			Instance* parentPtr   = opinPtr->cellPtr;

			if(parentPtr->cellLibIndex_e == -2 || parentPtr->cellLibIndex_e == -4)
			{

				if(!cell->iipList[i].wire->isIncremental)
				{
					cell->iipList[i].wire->isIncremental = true;
					IncNetList.push_back(cell->iipList[i].wire);
					cell->iipList[i].wire->isDiscovered = true;
					InitNetList.push(cell->iipList[i].wire);
				}
			}
			else if(parentPtr->info_late->isSequential)
			{
				if(parentPtr->ckList[0].isConnected && !(parentPtr->ckList[0].wire->isIncremental))
				{
					parentPtr->ckList[0].wire->isIncremental = true;
					IncNetList.push_back(parentPtr->ckList[0].wire);
					parentPtr->ckList[0].wire->isDiscovered = true;
					InitNetList.push(parentPtr->ckList[0].wire);
				}
			}
			else
			{
				vector<int> ipinIndexList = opinPtr->relPinIndex;

				for(int m =0;m < (int)ipinIndexList.size();m++) // m - related input pin control of parent
				{
					if(parentPtr->iipList[ipinIndexList[m]].isConnected &&
							!(parentPtr->iipList[ipinIndexList[m]].wire->isIncremental))
					{
						parentPtr->iipList[ipinIndexList[m]].wire->isIncremental = true;
						IncNetList.push_back(parentPtr->iipList[ipinIndexList[m]].wire);
						parentPtr->iipList[ipinIndexList[m]].wire->isDiscovered = true;
						InitNetList.push(parentPtr->iipList[ipinIndexList[m]].wire);
					}
				}
			}
		}
	}
}

/**
 * \param pinName Pin name
 * \param netName Net name
 *
 * Connects the given pin to the given net
 */
void Operations:: ConnectPin(string pinName,string netName)
{

	deque<string> tokens  = split(pinName, ':');
	int tokensz           = tokens.size();

	int64 netHash     = ComputeHash(netName);
	int64 instHash    = ComputeHash(tokens[0]);
	int64 pinHash,completePinHash;
	completePinHash   = ComputeHash(pinName);
	if(tokensz==2)
		pinHash= ComputeHash(tokens[1]);
	else
		pinHash= ComputeHash(tokens[0]);

	int64 i;
	int64 temp;

		//finding position of the current net in spefnetlist
		int64 netHashTableInd = Bsearch(spefNetHashTable,netHash,0,spefNetHashTable.size()-1);

		if(netHashTableInd == -1)
			return;

		i=spefNetHashTable[netHashTableInd].index;

		if(SpefNetList[i].isIncforRAT==false)
		{
			IncWireList.push_back(&SpefNetList[i]);
			SpefNetList[i].isIncforRAT=true;
		}

		//finding whether the pin is an Opin or Ipin
		int64 instInd = Bsearch(InstanceHashTable,instHash,0,InstanceHashTable.size()-1);

		if(instInd == -1)
			return;

		bool isOPin;
		int64 pinCellLibIndex = 0;
		bool isClkPin = false;
		instInd       = InstanceHashTable[instInd].index;

		if(InstanceList[instInd].cellLibIndex_e>=0)
		{
			if(InstanceList[instInd].info_early->clkpin.size()>0 &&
					InstanceList[instInd].info_early->clkpin[0].name==tokens[1])
			{
				isOPin=false; isClkPin=true;
			}
			else
			{
				int64 hashInd=Bsearch(InstanceList[instInd].info_early->opinhashtable,
						pinHash,0,InstanceList[instInd].info_early->opinhashtable.size()-1);

				if(hashInd!=-1)
				{
					isOPin          = true;
					pinCellLibIndex = InstanceList[instInd].info_early->opinhashtable[hashInd].index;
				}
				else
				{
					isOPin=false;
					int64 hashInd   = Bsearch(InstanceList[instInd].info_early->ipinhashtable,
									  pinHash,0,InstanceList[instInd].info_early->ipinhashtable.size()-1);
					pinCellLibIndex = InstanceList[instInd].info_early->ipinhashtable[hashInd].index;

				}
			}
		}
		else if(InstanceList[instInd].cellLibIndex_e==-2 || InstanceList[instInd].cellLibIndex_e==-4)
			isOPin = true;
		else
		{
			isOPin = false; // it's a PO, i.e. InstanceList[instInd].cellLibIndex_e=-3
		}

		// till here, we know whether the pin is Op/In/clk and it's index in the celllib pin info., mind you, this is not the index
		// in iipList/iopList

		if(isOPin)    //means it's an output pin i.e. a port
		{
			if(InstanceList[instInd].cellLibIndex_e == -2 || InstanceList[instInd].cellLibIndex_e == -4)
			{
				SpefNetList[i].port.portName.n1      = tokens[0];
				SpefNetList[i].port.portName.n2      = "";
				SpefNetList[i].port.hash             = pinHash;
				SpefNetList[i].port.portName.pinid   = pinHash;
				SpefNetList[i].port.portType         = 'P';
				SpefNetList[i].port.capacitanceEarly = 0.0;
				SpefNetList[i].port.capacitanceLate  = 0.0;
			}
			else
			{
				SpefNetList[i].port.portName.n1      = tokens[0];
				SpefNetList[i].port.portName.n2      = tokens[1];
				SpefNetList[i].port.hash             = completePinHash;
				SpefNetList[i].port.portName.pinid   = ComputeHash(tokens[0]);
				SpefNetList[i].port.portType         = 'I';
				SpefNetList[i].port.capacitanceEarly = InstanceList[instInd].info_early->opins[pinCellLibIndex].capacitance;
				SpefNetList[i].port.capacitanceLate  = InstanceList[instInd].info_late->opins[pinCellLibIndex].capacitance;

			}
		}
		else          //means it's an input pin,i.e. a tap
		{
			if(InstanceList[instInd].cellLibIndex_e == -3)       //means it'a PO
			{
				SpefTap itap;
				itap.tapName.n1       = tokens[0];
				itap.tapName.pinid    = pinHash;
				itap.tapName.n2       = "";
				itap.hash             = pinHash;
				itap.tapType          = 'P';
				temp                  = Bsearch(timing.loadcap, itap.hash, 0, timing.loadcap.size()-1);

				if (temp == -1)
				{
					itap.capacitanceEarly = 0;
					itap.capacitanceLate  = 0;
				}
				else
				{
					itap.capacitanceEarly = timing.loadcap[temp].earlycap;  //Assumed late and early caps of PO are same, take any one
					itap.capacitanceLate  = timing.loadcap[temp].latecap;
				}
				SpefNetList[i].port.ceffEarly += itap.capacitanceEarly;
				SpefNetList[i].port.ceffLate  += itap.capacitanceLate;

				bool present = false;

				for (int j =0 ; j< (int)SpefNetList[i].taps.size();j++)
				{
					if(SpefNetList[i].taps[j].tapName.n1 == itap.tapName.n1)
					{
						SpefNetList[i].taps[j] = itap;
						present                = true;
						break;
					}
				}
				if(present == false)
					SpefNetList[i].taps.push_back(itap);

				InstanceList[instInd].iipList[0].wire = &SpefNetList[i];
				InstanceList[instInd].iipList[0].tap  = NULL;
			}
			else
			{
				SpefTap itap;
				itap.tapName.n1       = tokens[0];
				itap.tapName.pinid    = ComputeHash(tokens[0]);
				itap.tapName.n2       = tokens[1];
				itap.hash             = completePinHash;
				itap.tapType          = 'I';

				if(isClkPin==true)
				{
					itap.capacitanceEarly = InstanceList[instInd].info_early->clkpin[0].capacitance;
					itap.capacitanceLate  = InstanceList[instInd].info_late->clkpin[0].capacitance;
				}
				else
				{
					itap.capacitanceEarly = InstanceList[instInd].info_early->ipins[pinCellLibIndex].capacitance;
					itap.capacitanceLate  = InstanceList[instInd].info_late->ipins[pinCellLibIndex].capacitance;
				}

				SpefNetList[i].port.ceffEarly += itap.capacitanceEarly;
				SpefNetList[i].port.ceffLate  += itap.capacitanceLate;

				bool present = false;

				for (int j =0 ; j< (int)SpefNetList[i].taps.size();j++)
				{
					if(SpefNetList[i].taps[j].tapName.n1 == itap.tapName.n1 && SpefNetList[i].taps[j].tapName.n2 == itap.tapName.n2)
					{
						SpefNetList[i].taps[j] = itap;
						present                = true;
						break;
					}
				}

				if(present == false)
					SpefNetList[i].taps.push_back(itap);

				InstanceList[instInd].iipList[pinCellLibIndex].wire = &SpefNetList[i];
				InstanceList[instInd].iipList[pinCellLibIndex].tap  = NULL;
			}
		}

	return;
}

/**
 * \param pinName Pin name
 *
 * Disconnects the given pin from all of its connected components
 */

void Operations:: DisconnectPin(string pinName)
{

	deque<string> tokens = split(pinName, ':');
	int tokensz          = tokens.size();

	int64 instHash  = ComputeHash(tokens[0]);
	int64 pinHash = 0,completePinHash = 0;
	completePinHash = ComputeHash(pinName);

	if(tokensz==2)
		pinHash=ComputeHash(tokens[1]);
	else if(tokensz==1)
		pinHash=ComputeHash(tokens[0]);

	int64 pinIndex;

	//if tokensz==1 implies it either a PI or a PO.
	//finding whether the pin exists in the instance, and is it an Opin or Ipin

	int64 instInd=Bsearch(InstanceHashTable,instHash,0,InstanceHashTable.size()-1);

	if(instInd == -1)
		return;

	bool isOPin;

	bool isClkPin = false;
	bool isPI     = false;
	bool isPO     = false;
	instInd       = InstanceHashTable[instInd].index;

	if(InstanceList[instInd].cellLibIndex_e>=0)
	{
		if(InstanceList[instInd].info_early->clkpin.size()>0 && InstanceList[instInd].info_early->clkpin[0].name==tokens[1])
		{
			isOPin   = false;
			isClkPin = true;
		}
		else
		{
			pinIndex = Bsearch(InstanceList[instInd].iopList,
					pinHash,0,InstanceList[instInd].iopList.size()-1);
			if(pinIndex!=-1)
			{
				isOPin = true;
			}
			else
			{
				isOPin   = false;
				pinIndex = Bsearch(InstanceList[instInd].iipList,
								pinHash,0,InstanceList[instInd].iipList.size()-1);
			}
		}
	}
	else if(InstanceList[instInd].cellLibIndex_e == -2 || InstanceList[instInd].cellLibIndex_e == -4)
	{    // means it's a PI
		isOPin   = true;
		isPI     = true;
		pinIndex = 0;
	}
	else
	{
		isOPin    = false;
		isPO      = true;
		pinIndex  = 0;  // it's a PO, i.e. InstanceList[instInd].cellLibIndex_e=-3
	}

	// till here, we know whether the pin is Op/In/clk and it's index in the celllib pin info., mind you, this is not the index
	// in iipList/iopList

	Instance* cell;
	if(isOPin==true)       //concerned pin is an output pin
	{
		cell = &(InstanceList[instInd]); 		//identified instance of cell connected to this port
		InstanceOpin *op;

		op = &(cell->iopList[pinIndex]);

		if(op->isConnected==false || op->port ==NULL)
			return;
		if(op->wire->isIncforRAT==false)
		{
			IncWireList.push_back(op->wire);
			op->wire->isIncforRAT=true;
		}

		op->isConnected=false;

		if(!isPI)
			op->tData.clear();
		else
			op->tData.clearPI();  //Do not clear AT values of PI.

		op->wire->port.pinPtr   = NULL;
		op->port->pinPtr        = NULL;
		op->port->cellPtr       = NULL;
		op->wire->port.cellPtr  = NULL;
		SpefNet* wirePtr        = new SpefNet();
		wirePtr->port.pinPtr    = op;
		wirePtr->port.cellPtr   = cell;
		op->wire                = wirePtr;
		cell->outDegree        -= 1;

	}

	else
	{
		/* means either an input or a clock pin*/
		deque<SpefTap> taps;
		//index of the tap to which the pin is connected.

		int64 IPinInd;
		cell = &(InstanceList[instInd]);

		InstanceIpin newIPin,*ip;
		if(isClkPin==true)
		{
				ip = &(cell->ckList[0]);
		}
		else  // implies the given input pin is not a clock
		{
			IPinInd = Bsearch(cell->iipList,pinHash,0,cell->iipList.size()-1);

			ip = &(cell->iipList[IPinInd]);



			//CHECKPOINT:WARNING: In same set of commands, If pin was disconnected, then connected again, now asked to disconnect again,
			//then isConnected flag will already be true (this
			//will help in deleting tap from the net) but its tap pointer will be null since connections haven't been made yet, but wire
			//pointer will purposefully be set to correct wire so that we can go to that wire and delete corresponding tap.
			//Deleting D pin from pathLUT if it got disconnected.

			if(ip->tap!=NULL){
			}
		}

		if(ip->wire != NULL && ip->wire->isIncforRAT == false)
		{
			IncWireList.push_back(ip->wire);
			ip->wire->isIncforRAT = true;
		}

		if(!isPO)
			ip->tData.clear();
		else
			ip->tData.clearPO();

		if(ip->tap == NULL && ip->wire != NULL)     //Implies that this pin was earlier connected using connect Pin function, for which
												//we purposefully set the right wire pointer but NULL tap pointer, so as to delete the tap.
		{
			for(int64 k = 0; k < (int64)ip->wire->taps.size(); k++)
			{
				if(completePinHash==ip->wire->taps[k].hash)
				{
					ip->wire->taps.erase(ip->wire->taps.begin()+k);
					break;
				}
			}
			ip->wire = NULL;
			return;
		}

		if(ip->isConnected==false)
			return;

		ip->isConnected=false;

		for(int64 k=0;k < (int64)ip->wire->taps.size();k++)
		{
			if(ip->wire->taps[k].hash == ip->tap->hash)
			{
				ip->wire->taps.erase(ip->wire->taps.begin()+k);
				break;
			}
		}

		ip->wire=NULL;
		ip->tap=NULL;

	}
return;

}

/**
 * \param cell Input cell pointer
 *
 * Initiates the call to SetRelatedPins from ForwardPropagate class
 */
void Operations::SetRelatedPins    (Instance* cell)
{
	forwardPropagate_ptr->SetRelatedPins (cell);
	return;
}

};

