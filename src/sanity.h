/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_SANITY_H_
#define IIT_SANITY_H_

#include "include.h"
#include "classdecs.h"

/**
 * \file sanity.h
 *
 * This file contains sanity checking functions used especially after certain complex operations in the program
 */
namespace iit {

void  hash_tables          (void);
void  unordered_maps       (void);
bool  check_violations     (void);
bool  check_KKTConditions  (void);

};

#endif /* IIT_SANITY_H_*/
