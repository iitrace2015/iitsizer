/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_TIMER_H_
#define IIT_TIMER_H_

#include "include.h"
#include "classdecs.h"
#include "cppr.h"
#include "forward_propagate.h"
#include "report.h"
#include "spef.h"
#include "backward_propagate.h"
#include "operations.h"

namespace iit
{

/**
 * Main class for Timer to allow complete running of iitRACE
 */
class Timer
{

public:

	void Engine                  (int args, char** argv);

    Timer();
    ~Timer();

protected:
	string              _outFile;                         ///< Name of output file
	string              _tau2015File;                     ///< Name of .tau2015 file
	string              _opsFile;                         ///< Name of operations (.ops) file
	string              _timingFile;                      ///< Name of timing (.timing) file (when running only iitRACE)
	string              _libFile;                         ///< Name of liberty (.lib) file
	string              _verilogFile;                     ///< Name of verilog (.v) file
	string              _spefFile;                        ///< Name of parasitics (.spef) file
	string              _sdcFile;                         ///< Name of timing constraints (.sdc) file (when running iitSizer)
	string              _sizeFile;                        ///< Name of size file (when running iitSizer to read gate versions from a file if need be)

	ForwardPropagate *  _forwardPropagate_ptr;            ///< Pointer to class ForwardPropagate
	BackwardPropagate * _backwardPropagate_ptr;           ///< Pointer to class BackwardPropagate
	CPPR *              _cppr_ptr;                        ///< Pointer to class CPPR
	Report *            _report_ptr;                      ///< Pointer to class Report
	Operations *        _operations_ptr;                  ///< Pointer to class Operations
	SpefParser *        _spef_ptr;                        ///< Pointer to class SpefParser

	void BuildCircuit             (void);
	void RunCircuit               (void);
	void test_lib_parser          (string filename, CelllibStorage* tempcelllib,
			                        LibFileInfo* libinfo, int mode);
	void test_timing_parser       (string filename);
	void test_ops_parser          (string filename);
	void test_spef_parser         (string filename);
	void test_sdc_parser          (string filename);
	void test_verilog_parser      (string buffer);
	bool read_tau2015_as_tokens   (istream& is, deque<string>& tokens);
	bool is_special_char_tau2015  (char c);

};

};

#endif
