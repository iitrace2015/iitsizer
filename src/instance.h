/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_INSTANCE_H_
#define IIT_INSTANCE_H_

#include "lib.h"
#include "parallel.h"
#include "timing_info.h"
#include "classdecs.h"

namespace iit
{

class Instance;

/**
 * Stores current stage solution attributes for iitSizer in DGS
 */
class SolutionState
{

public:
	double64 lkg;                                ///< Total leakage (sum of leakage of each cell in design)
	double64 tns;                                ///< Total negative slack (sum of negative slack of cone-end points)
	double64 worstSlack;                         ///< Worst negative slack among all cone-end points
	double64 totalLD;                            ///< Sum of Lambda times delay of each timing arc

	SolutionState()
	{
		lkg        = 0;
		tns        = 0;
		worstSlack = 0;
		totalLD    = 0;
	}
};

/**
 * Stores path flag information for an input pin used for coloring mechanism while path extraction after CPPR
 */
class PATH_FLAG
{

public:
	int64 flagRE [NUM_THREADS];                   ///< Rise Early flag array for each thread
	int64 flagRL [NUM_THREADS];                   ///< Rise Late flag array for each thread
	int64 flagFE [NUM_THREADS];                   ///< Fall Early flag array for each thread
	int64 flagFL [NUM_THREADS];                   ///< Fall Late flag array for each thread

};

/**
 * Stores name and id associated with a pin
 */
class NodeName
{

public:
	string icellName ;                           ///< Name of associated cell
	string pinName ;                             ///< Pin name
	int64  cellid;                               ///< Hash of associated cell name

	void clear()
	{
		icellName = "";
		cellid    = 0;
		pinName   = "";
	}
} ;

/**
 * Structure of an input pin with all its attributes
 */
class InstanceIpin
{

public:
	NodeName          name;					///< Contains name of pin
	                                        ///< name.cellid has hash of cell Instance only
	                                        ///< e.g. u1:b will have hash(u1)
	int64             hash;					///< Hash of pin name only e.g. u1:b will have hash(b)
	LibParserPinInfo* info_early;           ///< Link to pin information from .lib (Early mode)
	LibParserPinInfo* info_late;            ///< Link to pin information from .lib (Late mode)
	SpefTap*          tap;					///< Pointer to tap to which the pin is connected
	SpefNet*          wire;                 ///< Pointer to wire to which the pin is connected
	bool              isClk;                ///< true if the input pin is a clock pin
	double64          capacitanceEarly;     ///< Early mode pin capacitance (pin cap from .lib + pin cap from .spef)
	double64          capacitanceLate;      ///< Late mode pin capacitance (pin cap from .lib + pin cap from .spef)
	NODE_TIMING_DATA  tData;                ///< Timing information of the pin
	vector <int>      relPinIndex;          ///< Array to store indexes of related output pins in ioplist of cell
	Instance*         cellPtr;              ///< Pointer to associated cell
	PARALLEL_DATA_IN  pData[NUM_THREADS];   ///< Stores timing information to run multithreaded CPPR
	PATH_FLAG         pathFlag;             ///< Stores coloring flag while extracting paths after CPPR
	bool              isConnected;          ///< Checks if pin is connected to a net
	int64             pinColor;             ///< Used to limit scope when extracting worst paths through a pin
	double64	      capChange;            ///< Change in pin capacitance when changing gate version in iitSizer during DGS

	InstanceIpin()
	{
		isClk            = false;
		capacitanceEarly = 0.0;
		capacitanceLate  = 0.0;
		info_early       = NULL;
		info_late        = NULL;
		tap              = NULL;
		wire             = NULL;
		cellPtr          = NULL;
		isConnected      = false;
		pinColor         = -1;
		capChange        = 0;
	}
} ;

/**
 * Structure of an output pin with all its attributes
 */
class InstanceOpin
{
public:
	NodeName            name;					///< Contains name of pin
                                                ///< name.cellid has hash of cell Instance only
                                                ///< e.g. u1:b will have hash(u1)
	int64               hash;					///< Hash of pin name only e.g. u1:b will have hash(b)
	LibParserPinInfo*   info_early;             ///< Link to pin information from .lib (Early mode)
	LibParserPinInfo*   info_late;              ///< Link to pin information from .lib (Late mode)
	SpefPort*           port;					///< Pointer to port to which the pin is connected
	SpefNet*            wire;                   ///< Pointer to wire to which the pin is connected
	double64            capacitanceEarly;       ///< Early mode pin capacitance (pin cap from .lib + pin cap from .spef)
	double64            capacitanceLate;        ///< Late mode pin capacitance (pin cap from .lib + pin cap from .spef)
	double64            ceffEarly;              ///< Early mode effective load capacitance (lumped or ECM)
	double64            ceffLate;               ///< Late mode effective load capacitance (lumped or ECM)
	NODE_TIMING_DATA    tData;                  ///< Timing information of the pin
	vector <DELAY_DATA> dData;                  ///< Delay information of associated timing arcs
	PARALLEL_DATA_OUT   pData[NUM_THREADS];     ///< Stores timing information to run multithreaded CPPR
	vector <int>        relPinIndex;            ///< Array to store indexes of related input pins in iiplist of cell
	Instance*           cellPtr;                ///< Pointer to associated cell
	int                 visitCount;             ///< # of times gate pin is visited during BFS during TopSort
	bool                isConnected;            ///< Checks if pin is connected to a net
	bool                isIncRelatedSet;        ///< Checks if relPinIndex set during incremental changes
	size_t              incRelatedCount;        ///< # of related input pins during incremental changes

    // Added for New Effective Capacitance
	double64 	        cPi;                    ///< Temporary storage for effective capacitance for ECM model
	double64 	        tD;                     ///< td + tt*0.5 (td -> timing arc delay, tt -> timing arc input slew)
	double64 	        tX;                     ///< td + tt*0.5 - trf*0.5 (td -> timing arc delay,
	                                            ///< tt -> timing arc input slew, trf -> corresponding output pin slew)

	InstanceOpin()
	{
		capacitanceEarly = 0.0;
		capacitanceLate  = 0.0;
		visitCount       = 0;
		info_early       = NULL;
		info_late        = NULL;
		port             = NULL;
		wire             = NULL;
		ceffEarly        = 0;
		ceffLate         = 0;
		cellPtr          = NULL;
		isConnected      = false;
		isIncRelatedSet  = false;
		incRelatedCount  = 0;

		cPi              = 0;
		tD               = 0;
		tX               = 0;
	}
};

/**
 * Structure of a cell with all its attributes
 */
class Instance{
public:

	string instanceName;                     ///< Name of the cell instance
	int64  hash;                             ///< Hash value of cell-instance name
	bool   isIncluded;	                     ///< Checks if cell is included or not for incremental purposes
	int    cellLibIndex_e;                   ///< Early mode index of cell in list of cell types from .lib
	int    cellLibIndex_l;                   ///< Late mode index of cell in list of cell types from .lib
	int    outDegree; 		                 ///< Initialised to iopList.size() and used in TopSort
	bool   isCkTreeMember;                   ///< True if cell is in clock tree
	int    numVisOut;                        ///< Counter for # of visits to cell during TopSort
	bool   repowerFlag;                      ///< Flag to check if cell is repowered during incremental changes

	LibParserCellInfo*      info_early;      ///< Link to cell information from .lib (Early mode)
	LibParserCellInfo*      info_late;       ///< Link to cell information from .lib (Late mode)
	vector < InstanceIpin > iipList;         ///< List containing all input pins
	vector < InstanceOpin > iopList;         ///< List containing all output pins (assumed
	                                         ///< only 1 output pin for iitSizer, else iitSizer won't work)
	vector < InstanceIpin > ckList;          ///< List containing all clock pins (only 1 element assumed)

	double64           lambdaFL;             ///< Stores Fall Late Lagrangian Multiplier for Primary Output only (for iitSizer)
	double64           lambdaRL;             ///< Stores Rise Late Lagrangian Multiplier for Primary Output only (for iitSizer)
	LibParserCellInfo* bestSoln;

	Instance()
	{	info_early     = NULL;
		info_late      = NULL;
		outDegree      = 0;
		isCkTreeMember = false;
		numVisOut      = 0;
		repowerFlag    = false;

		lambdaFL       = DEFAULT_LAMBDA;
		lambdaRL       = DEFAULT_LAMBDA;
	}
};

};

#endif
