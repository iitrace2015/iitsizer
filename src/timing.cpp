/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#include "include.h"
#include "timing.h"
#include "backward_propagate.h"
#include "lib.h"
#include "timer.h"
#include "globals.h"
namespace iit{

/* Global variables declaration --------------------------------------- */

//extern CelllibStorage      celllibstorage_e;
//extern CelllibStorage      celllibstorage_l;
//extern LibFileInfo         libinfo_e;
//extern LibFileInfo         libinfo_l;
extern CompleteTimingInfo  timing;

//extern Instance* CLKSRC;
//extern int       numInc;
//extern int       Verbose;
//extern int64     MaxNumPaths;
//extern int64     PathFlag_G;
//extern string    BenchmarkDir;
//extern bool     _isStaticDone;

//extern deque   < Instance >       InstanceList;
//extern vector  < Hash_Instance >  InstanceHashTable;
//extern deque   < SpefNet >        SpefNetList;
//extern vector  < Hash_Instance >  spefNetHashTable;
//extern vector  < SpefNet* >       Sort;
//extern vector  < SpefNet* >       IncNetList;
//extern deque   < SpefNet >        WireList;
//extern queue   < SpefNet* >       InitNetList;
//extern vector  < Instance* >      PIList;
//extern vector  < Instance* >      POList;
//extern vector  < InstanceIpin* >  ConeEndPoints;
//extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
//extern deque   < Slack_Instance > pathLUTSlackTable_Adv;

extern std::tr1::unordered_map <char,bool> specCharLibDir;

/* End - global variables declaration -----------------------------------*/

/**
 * Function to close and free input file stream
 */
void TimingParser::_close(void)
{
	free(is);
	is.close();
	return;
}

/**
 * \param is Input .timing file stream
 * \param tokens Set of words in a line (separated by specified delimiters)
 * \param includeSpecialChars Flag to indicate if special characters to be included
 * \result Returns true if tokens is not empty
 *
 * Reads from the input stream and splits a line into a set of words separated by delimiters
 */
bool TimingParser::read_line_as_tokens (istream& is, deque<string>& tokens, bool includeSpecialChars )
{
	tokens.clear();
	string line;
	std::getline (is, line);

	while (is && tokens.empty())
	{
		string token = "" ;
		for (int i = 0; i < (int)line.size(); ++i)
		{
			char currChar = line[i];
			bool isSpecialChar = specCharLibDir[currChar];
			if (std::isspace (currChar) || isSpecialChar)
			{
				if (!token.empty())
				{
					// Add the current token to the list of tokens
					tokens.push_back(token);
					token.clear();
				}
				if (includeSpecialChars && isSpecialChar)
				{
					tokens.push_back(string(1, currChar));
				}
			}
			else
			{
				// Add the char to the current token
				token.push_back(currChar);
			}
		}
		if (!token.empty())
			tokens.push_back(token);

		// Previous line read was empty. Read the next one.
		if (tokens.empty())
			std::getline (is, line);
	}
	return !tokens.empty();
}

/**
 * \param tinfo Stores complete timing information from .timing file (by reference)
 *
 * Reads .timing file and stores all information in CompleteTimingInfo
 */
bool TimingParser::read_timing_line (CompleteTimingInfo& tinfo)
{
	PIPOTimingInfo pipotiming;
	POLoadCapTimingInfo loadtiming;

	deque<string> tokens ;
	bool valid = read_line_as_tokens (is, tokens);

	while(valid)
	{
		if(tokens[0]== "at")
		{
			pipotiming.name=tokens[1];
			pipotiming.earlyrise=std::atof(tokens[2].c_str());
			pipotiming.earlyfall=std::atof(tokens[3].c_str());
			pipotiming.laterise=std::atof(tokens[4].c_str());
			pipotiming.latefall=std::atof(tokens[5].c_str());
			pipotiming.hash=ComputeHash(tokens[1]);
			tinfo.at.push_back(pipotiming);
		}
		else if(tokens[0]== "slew")
		{
			pipotiming.name=tokens[1];
			pipotiming.earlyrise=std::atof(tokens[2].c_str());
			pipotiming.earlyfall=std::atof(tokens[3].c_str());
			pipotiming.laterise=std::atof(tokens[4].c_str());
			pipotiming.latefall=std::atof(tokens[5].c_str());
			pipotiming.hash=ComputeHash(tokens[1]);
			tinfo.slew.push_back(pipotiming);
		}
		else if(tokens[0]== "rat")
		{
			pipotiming.name=tokens[1];
			pipotiming.earlyrise=std::atof(tokens[2].c_str());
			pipotiming.earlyfall=std::atof(tokens[3].c_str());
			pipotiming.laterise=std::atof(tokens[4].c_str());
			pipotiming.latefall=std::atof(tokens[5].c_str());
			pipotiming.hash=ComputeHash(tokens[1]);
			tinfo.rat.push_back(pipotiming);
		}
		else if(tokens[0]== "load")
		{
			loadtiming.poname=tokens[1];
			loadtiming.earlycap=std::atof(tokens[2].c_str());
			if(tokens.size()>=4)
			  loadtiming.latecap=std::atof(tokens[3].c_str());
			else
			  loadtiming.latecap=loadtiming.earlycap;
			loadtiming.hash=ComputeHash(tokens[1]);
			tinfo.loadcap.push_back(loadtiming);
		}
		else if(tokens[0]=="clock")
		{
			tinfo.clock.name=tokens[1];
			tinfo.clock.period=std::atof(tokens[2].c_str());
			tinfo.clock.low=std::atof(tokens[3].c_str());
		}
		valid=read_line_as_tokens (is, tokens) ;
	}
	_close();
	return true;
}

/**
 * \param filename Name of .timing file
 *
 * Timing parser to parse and store information from .timing file
 */
void Timer::test_timing_parser (string filename)
{
	TimingParser tp (filename) ;

	tp.read_timing_line(timing) ;

	// Sorting various hashtables
	std::sort (timing.at.begin()     , timing.at.end()     , CompareByHashnum < PIPOTimingInfo >());
	std::sort (timing.rat.begin()    , timing.rat.end()    , CompareByHashnum < PIPOTimingInfo >());
	std::sort (timing.slew.begin()   , timing.slew.end()   , CompareByHashnum < PIPOTimingInfo >());
	std::sort (timing.loadcap.begin(), timing.loadcap.end(), CompareByHashnum < POLoadCapTimingInfo>());
}

};



