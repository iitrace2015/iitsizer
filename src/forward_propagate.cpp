/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#include "include.h"
#include "instance.h"
#include "net.h"
#include "forward_propagate.h"
#include "timer.h"
#include "globals.h"

namespace iit{

/* Global variables declaration --------------------------------------- */

extern CelllibStorage      celllibstorage_e;
extern CelllibStorage      celllibstorage_l;
//extern LibFileInfo         libinfo_e;
//extern LibFileInfo         libinfo_l;
extern CompleteTimingInfo  timing;

extern Instance* CLKSRC;
//extern int       numInc;
//extern int       Verbose;
//extern int64     MaxNumPaths;
//extern int64     PathFlag_G;
//extern string    BenchmarkDir;
extern bool     _isStaticDone;

extern deque   < Instance >       InstanceList;
extern vector  < Hash_Instance >  InstanceHashTable;
extern deque   < SpefNet >        SpefNetList;
//extern vector  < Hash_Instance >  spefNetHashTable;
extern vector  < SpefNet* >       Sort;
extern vector  < SpefNet* >       IncNetList;
//extern deque   < SpefNet >        WireList;
extern queue   < SpefNet* >       InitNetList;
extern vector  < Instance* >      PIList;
//extern vector  < Instance* >      POList;
extern vector  < InstanceIpin* >  ConeEndPoints;
//extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
//extern deque   < Slack_Instance > pathLUTSlackTable_Adv;

//extern std::tr1::unordered_map <char,bool> specCharLibDir;
extern SolutionState     TrackSoln;

/* End - global variables declaration -----------------------------------*/

/**
 * \param mode Mode (early/late)
 *
 * Performs incremental Arrival Time propagation during incremental changes while running incremental STA
 */
void ForwardPropagate::InitForwardPropagate (int mode)
{
	Sort.clear();
	BFSInc();
	setInitNetSort();
	IncNetList.clear();
	TopSortInc();
	ATSlew(mode);

	return;
}

/**
 * Resolves dependencies in incrementally modified nets
 * Identifies a set of independent and non-reachable incrementally modified nets to act as start points for incremental AT propagation
 * Identifies relevant cone-end points that are affected by incremental changes
 */
void ForwardPropagate:: BFSInc()
{
	while(!InitNetList.empty())
	{
		SpefNet* wirePtr = InitNetList.front();
		InitNetList.pop();

		if(!wirePtr->isIncluded)
			continue;

		wirePtr->isDiscovered = true;
		wirePtr->isIncremental = true;
		for(int i = 0; i < (int)wirePtr->taps.size(); i++)
		{
			if(wirePtr->taps[i].cellPtr->cellLibIndex_l != -3)
			{
				if(wirePtr->taps[i].cellPtr->info_early->isSequential && !wirePtr->taps[i].pinPtr->isClk)
				{
					ConeEndPoints.push_back(wirePtr->taps[i].pinPtr);
				}

				if(wirePtr->taps[i].pinPtr->isClk)
				{
					for(int y = 0; y < (int)wirePtr->taps[i].cellPtr->iipList.size(); y++)
					{
						if(wirePtr->taps[i].cellPtr->iipList[y].isConnected)
							ConeEndPoints.push_back(&(wirePtr->taps[i].cellPtr->iipList[y]));
					}
					for(int k = 0;k<(int)wirePtr->taps[i].cellPtr->iopList.size();k++)
					{
						if(!wirePtr->taps[i].cellPtr->iopList[k].isConnected)
							continue;
						if(wirePtr->taps[i].cellPtr->iopList[k].wire->isDiscovered)
							wirePtr->taps[i].cellPtr->iopList[k].wire->isReachable = true;
						else
						{
							wirePtr->taps[i].cellPtr->iopList[k].wire->isDiscovered  = true;
							wirePtr->taps[i].cellPtr->iopList[k].wire->isIncremental = true;
							InitNetList.push(wirePtr->taps[i].cellPtr->iopList[k].wire);
						}
					}
				}
				else
				{
					for(int j = 0; j < (int)wirePtr->taps[i].pinPtr->relPinIndex.size(); j++) // j - Related output PinControl
					{
						std::vector<int> buffer = wirePtr->taps[i].pinPtr->relPinIndex;
						if(wirePtr->taps[i].cellPtr->iopList[buffer[j]].isConnected)
						{
							if(wirePtr->taps[i].cellPtr->iopList[buffer[j]].wire->isDiscovered)
								wirePtr->taps[i].cellPtr->iopList[buffer[j]].wire->isReachable = true;
							else
							{
								wirePtr->taps[i].cellPtr->iopList[buffer[j]].wire->isDiscovered  = true;
								wirePtr->taps[i].cellPtr->iopList[buffer[j]].wire->isIncremental = true;
								InitNetList.push(wirePtr->taps[i].cellPtr->iopList[buffer[j]].wire);
							}
						}
					}
				}
			}
			else
			{
				ConeEndPoints.push_back(&(wirePtr->taps[i].cellPtr->iipList[0]));
			}
		}
	}
}

/**
 * Function to properly initialize the InitNetSort
 */
void ForwardPropagate::setInitNetSort()
{
	for(int i = 0; i < (int)IncNetList.size(); i++)
	{
		IncNetList[i]->isDiscovered  = false;
		if(!IncNetList[i]->isIncluded)
		{
			IncNetList[i]->isReachable = false;
			continue;
		}
		if(IncNetList[i]->isReachable) IncNetList[i]->isReachable = false;
		else
		{
				InitNetSort.push(IncNetList[i]);
		}
	}
}

/**
 * \param pinHash Hash of pin whose related pins needs to be found
 * \param cellIndex Index of cell in CelllibStorage
 * \param direction FORWARD for getting related input pins for an output pin, REVERSE for getting related output pins for an input pin
 * \result vector<int64> List List of indexes of related pins
 *
 * Finds and returns a list of indexes of pins related to a given pin
 */
vector <int64> ForwardPropagate:: GetRelatedPins (int pinHash, int cellIndex, int direction)
{
	//*******************************************************************************************************************
	//
	// WARNING: this function won't work for sequential cells, for sequential circuits, refer to cell.clkToIpinhashtable
	// and cell.clkToOPinhashtable, it will give info about list of related pins to the clock.
	//
	//*******************************************************************************************************************
	vector<int64> garb;

	if(direction == FORWARD)
	{
		vector<RelatedPinTable> temptable;
		temptable = celllibstorage_e.celllib[cellIndex].ipinhashtable;
		int64 indPin = Bsearch(temptable,pinHash,0,temptable.size()-1);
		return (temptable[indPin].relPinHash);
	}
	else if(direction == REVERSE)
	{
		vector<RelatedPinTable> temptable;
		temptable = celllibstorage_e.celllib[cellIndex].opinhashtable;
		int64 indPin = Bsearch(temptable,pinHash,0,temptable.size()-1);
		return (temptable[indPin].relPinHash);
	}

	return garb;
}

/**
 * \param cell Pointer to cell whose related pins need to be set
 *
 * Finds and sets lists of indexes of related pins for all pins of a given cell
 */
void ForwardPropagate:: SetRelatedPins(Instance* cell)
{
	if(cell->cellLibIndex_e < 0)
		return;
    int iipsize = cell->iipList.size();
    int iopsize = cell->iopList.size();
    vector<int64> relPinHash;
    int Index;
    int isPresent = false;

    if(celllibstorage_e.celllib[cell->cellLibIndex_e].isSequential == false)
	{
		for(int i=0; i<iipsize;i++)
		{
			cell->iipList[i].relPinIndex.clear();
			relPinHash = GetRelatedPins(cell->iipList[i].hash,cell->cellLibIndex_e,FORWARD);
			for(int j = 0; j < (int)relPinHash.size(); j++)
			{
				Index = Bsearch(cell->iopList,relPinHash[j],0,cell->iopList.size()-1);
				if(Index != -1)
				{
					for(int k = 0; k < (int)cell->iipList[i].relPinIndex.size(); k++)
					{
						if(cell->iipList[i].relPinIndex[k] == Index)
						{
							isPresent = true;
							break;
						}
					}
					if(isPresent == false)
						cell->iipList[i].relPinIndex.push_back(Index);
					isPresent=false;
				}
			}
		}
		for(int i = 0; i < iopsize; i++)
		{
			cell->iopList[i].relPinIndex.clear();
			relPinHash = GetRelatedPins(cell->iopList[i].hash,cell->cellLibIndex_e,REVERSE);
			for(int j = 0; j < (int)relPinHash.size(); j++)
			{
				Index = Bsearch(cell->iipList,relPinHash[j],0,cell->iipList.size()-1);
				if(Index != -1)
				{
					for(int k = 0; k < (int)cell->iopList[i].relPinIndex.size(); k++)
					{
						if(cell->iopList[i].relPinIndex[k] == Index)
						{
							isPresent = true;
							break;
						}
					}
					if(isPresent == false)
						cell->iopList[i].relPinIndex.push_back(Index);
					isPresent=false;
				}
			}
		}
	}
	else
	{
		cell->ckList[0].relPinIndex.clear();
		relPinHash = celllibstorage_e.celllib[cell->cellLibIndex_e].clkToIpinhashtable[0].relPinHash;
		for(int j = 0; j < (int)relPinHash.size(); j++)
		{
			Index = Bsearch(cell->iipList,relPinHash[j],0,cell->iipList.size()-1);
			if(Index != -1)
			{
				for(int k = 0; k < (int)cell->ckList[0].relPinIndex.size(); k++)
				{
					if(cell->ckList[0].relPinIndex[k] == Index)
					{
						isPresent = true;
						break;
					}
				}
				if(isPresent == false)
					cell->ckList[0].relPinIndex.push_back(Index);
				isPresent = false;
			}
		}
	}
	return;
}

/**
 * \param nodeTimingData Timing information of input/clock pin
 * \param iPinHash Hash of input/clock pin
 * \param oPinHash Hash of output pin
 * \param cellIndexEarly Early mode index of cell in CelllibStorage
 * \param cellIndexLate Late mode index of cell in CelllibStorage
 * \param opcapEarly Effective early mode load capacitance at output pin
 * \param opcapLate Effective late mode load capacitance at output pin
 * \param mode Mode (early/late)
 * \result pair<SLEW_DATA,DELAY_DATA> Output slew and timing arc delay information
 *
 *  Computes delay and output slew for a timing arc by using input slew and effective load capacitance at output pin
 *  Uses LUTs from .lib to interpolate and find slew and delay values
 */
std::pair<SLEW_DATA,DELAY_DATA> ForwardPropagate:: ComputePinTiming(NODE_TIMING_DATA& nodeTimingData,
		                           int64 iPinHash,int64 oPinHash, int cellIndexEarly, int cellIndexLate,
								   double64 opcapEarly, double64 opcapLate, int mode)
{
	SLEW_DATA slewData;
	DELAY_DATA delayData;  								// Already initialized to forbidden in constructor.

	vector <LibParserTimingInfo>* tempArcEarly;
	vector <LibParserTimingInfo>* tempArcLate;
	int arcIndexEarly,arcIndexLate;
	arcIndexEarly=arcIndexLate=-1;
	tempArcEarly=&(celllibstorage_e.celllib[cellIndexEarly].timingArcs);
	tempArcLate=&(celllibstorage_l.celllib[cellIndexLate].timingArcs);
	int timingArcSize=celllibstorage_e.celllib[cellIndexEarly].timingArcs.size();

	//TODO: can optimize the next step if you already stored list of pointers to the timing arcs along with the related pin list,
	//TODO: Also make sure that opinhashtable and ipinhashtable of early and late libstorage have pins in same order, else there will
    // different indices for related pins for early and late celllib.
	//TODO: According to Jin, setup and hold tests can be tested against both rising and falling edge of clock, so incorporate this!

	deque <int> indexEarly;
	deque <int> indexLate;
	for(int i = 0;i < timingArcSize; i++)
	{
		if(mode != LATE)
		{
			if(iPinHash == (*tempArcEarly)[i].hashFromPin && oPinHash == (*tempArcEarly)[i].hashToPin)
			{
				indexEarly.push_back(i);
			}
		}
		if(iPinHash == (*tempArcLate)[i].hashFromPin && oPinHash == (*tempArcLate)[i].hashToPin)
		{
			indexLate.push_back(i);
		}
	}

	if(indexLate.size() == 0)
		return std::make_pair<SLEW_DATA,DELAY_DATA>(slewData,delayData);

	else if(indexLate.size() == 2)
	{
		//WARNING: Special case of non_unateness
		int posEarly = 0, posLate = 0, negEarly =0, negLate =0;
		if(mode != LATE)
		{
			if((*tempArcEarly)[indexEarly[0]].timingSense == "positive_unate")
			{
				posEarly = indexEarly[0];
				negEarly = indexEarly[1];
			}
			else if((*tempArcEarly)[indexEarly[0]].timingSense == "negative_unate")
			{
				posEarly = indexEarly[1];
				negEarly = indexEarly[0];
			}
		}

		if((*tempArcLate)[indexLate[0]].timingSense == "positive_unate")
		{
			posLate = indexLate[0];
			negLate = indexLate[1];
		}
		else if((*tempArcLate)[indexLate[0]].timingSense == "negative_unate")
		{
			posLate = indexLate[1];
			negLate = indexLate[0];
		}
		slewData.timeSense = COMBINATIONAL;
		delayData.timeSense = COMBINATIONAL;

		if(mode != LATE)
		{
			double64 a = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[posEarly].fallDelay);
			double64 b = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[negEarly].fallDelay);
			delayData.delayFallEarly    = a;
			delayData.delayFallEarlyNeg = b;

			a = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[negEarly].riseDelay);
			b = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[posEarly].riseDelay);
			delayData.delayRiseEarlyNeg = a;
			delayData.delayRiseEarly    = b;

			a = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[posEarly].fallTransition);
			b = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[negEarly].fallTransition);
			slewData.slewFallEarly =a > b ? b : a ;

			a = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[negEarly].riseTransition);
			b = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[posEarly].riseTransition);
			slewData.slewRiseEarly=a > b ? b : a ;

		}
		if(mode != EARLY)
		{
			double64 a = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[posLate].fallDelay);
			double64 b = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[negLate].fallDelay);
			delayData.delayFallLate    = a;
			delayData.delayFallLateNeg = b;

			a = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[negLate].riseDelay);
			b = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[posLate].riseDelay);
			delayData.delayRiseLateNeg = a;
			delayData.delayRiseLate    = b;

			a = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[posLate].fallTransition);
			b = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[negLate].fallTransition);
			slewData.slewFallLate= a > b ? a : b ;

			a = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[negLate].riseTransition);
			b = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[posLate].riseTransition);
			slewData.slewRiseLate= a > b ? a : b ;
		}
		return std::make_pair<SLEW_DATA,DELAY_DATA>(slewData,delayData);
	}

	arcIndexEarly = indexEarly[0];
	arcIndexLate = indexLate[0];
	if((*tempArcLate)[arcIndexLate].timingSense == "positive_unate")
	{
		slewData.timeSense = POSITIVE_UNATE;
		delayData.timeSense = POSITIVE_UNATE;
		if(mode != LATE)
		{
			delayData.delayFallEarly = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].fallDelay);
			delayData.delayRiseEarly = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].riseDelay);
			slewData.slewFallEarly   = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].fallTransition);
			slewData.slewRiseEarly   = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].riseTransition);
		}
		if(mode != EARLY)
		{
			delayData.delayFallLate = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[arcIndexLate].fallDelay);
			delayData.delayRiseLate = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[arcIndexLate].riseDelay);
			slewData.slewFallLate   = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[arcIndexLate].fallTransition);
			slewData.slewRiseLate   = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[arcIndexLate].riseTransition);
		}
	}
	else if((*tempArcLate)[arcIndexLate].timingSense == "negative_unate")
	{
		slewData.timeSense  = NEGATIVE_UNATE;
		delayData.timeSense = NEGATIVE_UNATE;
		if(mode != LATE)
		{
			delayData.delayFallEarly = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].fallDelay);
			delayData.delayRiseEarly = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].riseDelay);
			slewData.slewFallEarly   = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].fallTransition);
			slewData.slewRiseEarly   = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].riseTransition);
		}
		if(mode != EARLY)
		{
			delayData.delayFallLate = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[arcIndexLate].fallDelay);
			delayData.delayRiseLate = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[arcIndexLate].riseDelay);
			slewData.slewFallLate   = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[arcIndexLate].fallTransition);
			slewData.slewRiseLate   = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[arcIndexLate].riseTransition);
		}
	}
	else
	{
		if((*tempArcLate)[arcIndexLate].timingType == "rising_edge")
		{
			slewData.timeSense  = RISING_EDGE;
			delayData.timeSense = RISING_EDGE;

			if(mode != LATE)
			{
				delayData.delayFallEarly = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].fallDelay);
				delayData.delayRiseEarly = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].riseDelay);
				slewData.slewFallEarly   = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].fallTransition);
				slewData.slewRiseEarly   = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].riseTransition);
			}

			if(mode != EARLY)
			{
				delayData.delayFallLate = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[arcIndexLate].fallDelay);
				delayData.delayRiseLate = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[arcIndexLate].riseDelay);
				slewData.slewFallLate   = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[arcIndexLate].fallTransition);
				slewData.slewRiseLate   = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[arcIndexLate].riseTransition);
			}
		}
		else if((*tempArcLate)[arcIndexLate].timingType == "falling_edge")
		{
			slewData.timeSense  = FALLING_EDGE;
			delayData.timeSense = FALLING_EDGE;
			if(mode != LATE)
			{
				delayData.delayFallEarly = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].fallDelay);
				delayData.delayRiseEarly = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].riseDelay);
				slewData.slewFallEarly   = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].fallTransition);
				slewData.slewRiseEarly   = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].riseTransition);
			}

			if(mode != EARLY)
			{
				delayData.delayFallLate = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[arcIndexLate].fallDelay);
				delayData.delayRiseLate = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[arcIndexLate].riseDelay);
				slewData.slewFallLate   = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[arcIndexLate].fallTransition);
				slewData.slewRiseLate   = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[arcIndexLate].riseTransition);
			}
		}
		else if((*tempArcLate)[arcIndexLate].timingType == "combinational") //TODO: what to do if combinational is not specified in timingType ??
		{
			slewData.timeSense  = COMBINATIONAL;
			delayData.timeSense = COMBINATIONAL;

			if(mode != LATE)
			{
				double64 a = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].fallDelay);
				double64 b = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].fallDelay);
				delayData.delayFallEarly   = a;
				delayData.delayFallEarlyNeg= b;

				a = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].riseDelay);
				b = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].riseDelay);
				delayData.delayRiseEarlyNeg = a;
				delayData.delayRiseEarly    = b;

				a = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].fallTransition);
				b = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].fallTransition);
				slewData.slewFallEarly =a > b ? b : a ;

				a = EvaluateLUT(true,nodeTimingData.slewFallEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].riseTransition);
				b = EvaluateLUT(true,nodeTimingData.slewRiseEarly,opcapEarly,(*tempArcEarly)[arcIndexEarly].riseTransition);
				slewData.slewRiseEarly=a > b ? b : a ;

			}

			if(mode != EARLY)
			{
				double64 a = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[arcIndexLate].fallDelay);
				double64 b = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[arcIndexLate].fallDelay);
				delayData.delayFallLate    = a;
				delayData.delayFallLateNeg = b;

				a = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[arcIndexLate].riseDelay);
				b = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[arcIndexLate].riseDelay);
				delayData.delayRiseLateNeg = a;
				delayData.delayRiseLate    = b;

				a = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[arcIndexLate].fallTransition);
				b = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[arcIndexLate].fallTransition);
				slewData.slewFallLate= a > b ? a : b ;

				a = EvaluateLUT(false,nodeTimingData.slewFallLate,opcapLate,(*tempArcLate)[arcIndexLate].riseTransition);
				b = EvaluateLUT(false,nodeTimingData.slewRiseLate,opcapLate,(*tempArcLate)[arcIndexLate].riseTransition);
				slewData.slewRiseLate= a > b ? a : b ;
			}
		}
	}
	return std::make_pair<SLEW_DATA,DELAY_DATA>(slewData,delayData);
}

/**
 * \param currNet Net whose port pin AT needs to be set
 *
 * Computes and sets the Arrival Time of an output pin
 * Very similar to function ATSlew
 * Used only when ECM is true or in iitSizer
 */
void ForwardPropagate::SetPortAT (SpefNet* currNet)
{
	vector <DELAY_DATA> dData_backup;
	for (int itr = 0; itr < (int)currNet->port.pinPtr->dData.size() ; itr++)
	{
		dData_backup.push_back(currNet->port.pinPtr->dData[itr]);
	}

	currNet->port.pinPtr->visitCount = 0;
	currNet->port.pinPtr->dData.clear();

	double64 atRiseEarly   = 987654;
	double64 atFallEarly   = 987654;
	double64 slewRiseEarly = 987654;
	double64 slewFallEarly = 987654;
	double64 atRiseLate    = -987654;
	double64 atFallLate    = -987654;
	double64 slewRiseLate  = -987654;
	double64 slewFallLate  = -987654;

	int indRiseEarly = DEFAULTINDEX;
	int indFallEarly = DEFAULTINDEX;
	int indRiseLate  = DEFAULTINDEX;
	int indFallLate  = DEFAULTINDEX;

	bool RiseEarlyCause= RISE, RiseLateCause = RISE, FallEarlyCause = FALL, FallLateCause = FALL;

	if(currNet->port.cellPtr->cellLibIndex_l != -2 && currNet->port.cellPtr->cellLibIndex_l!=-4) // Check index for PI or CLOCK
	{
		if(currNet->port.cellPtr->info_late->isSequential) 										 // Check for sequential elements
		{
			 std::pair<SLEW_DATA,DELAY_DATA> info;
			 info = ComputePinTiming(currNet->port.cellPtr->ckList[0].tData,
						currNet->port.cellPtr->ckList[0].hash,
						currNet->port.pinPtr->hash,
						currNet->port.cellPtr->cellLibIndex_e,
						currNet->port.cellPtr->cellLibIndex_l,
						currNet->port.pinPtr->ceffEarly,
						currNet->port.pinPtr->ceffLate,
						LATE
						);

			if(info.first.slewFallEarly < slewFallEarly)
				slewFallEarly = info.first.slewFallEarly;

			if(info.first.slewFallLate > slewFallLate)
				slewFallLate = info.first.slewFallLate;

			if(info.first.slewRiseEarly < slewRiseEarly)
				slewRiseEarly = info.first.slewRiseEarly;

			if(info.first.slewRiseLate > slewRiseLate)
				slewRiseLate = info.first.slewRiseLate;

			info.second.index = 0;
			currNet->port.pinPtr->dData.push_back(info.second);

			switch(info.second.timeSense)
			{
				case 2:
					{
						if(currNet->port.cellPtr->ckList[0].tData.atRiseEarly!=987654 &&
							info.second.delayRiseEarly!=987654 &&
							currNet->port.cellPtr->ckList[0].tData.atRiseEarly +
							info.second.delayRiseEarly < atRiseEarly)
						{
							atRiseEarly = currNet->port.cellPtr->ckList[0].tData.atRiseEarly +
											info.second.delayRiseEarly;
							RiseEarlyCause = RISE;
						}
						if(currNet->port.cellPtr->ckList[0].tData.atRiseEarly!=987654 &&
								info.second.delayFallEarly!=987654 &&
								currNet->port.cellPtr->ckList[0].tData.atRiseEarly +
								info.second.delayFallEarly < atFallEarly)
						{
							atFallEarly = currNet->port.cellPtr->ckList[0].tData.atRiseEarly +
											info.second.delayFallEarly;
							FallEarlyCause = RISE;
						}
						if(currNet->port.cellPtr->ckList[0].tData.atRiseLate!=-987654 &&
								info.second.delayRiseLate!=-987654 &&
								currNet->port.cellPtr->ckList[0].tData.atRiseLate +
								info.second.delayRiseLate > atRiseLate)
						{
							atRiseLate = currNet->port.cellPtr->ckList[0].tData.atRiseLate +
											info.second.delayRiseLate;
							RiseLateCause = RISE;
						}
						if(currNet->port.cellPtr->ckList[0].tData.atRiseLate!=-987654 &&
								info.second.delayFallLate!=-987654 &&
								currNet->port.cellPtr->ckList[0].tData.atRiseLate +
								info.second.delayFallLate > atFallLate)
						{
							atFallLate = currNet->port.cellPtr->ckList[0].tData.atRiseLate +
											info.second.delayFallLate;
							FallLateCause = RISE;
						}
						break;
					}
				case -2:
					{
						if(currNet->port.cellPtr->ckList[0].tData.atFallEarly!=987654 &&
								info.second.delayRiseEarly!=987654 &&
								currNet->port.cellPtr->ckList[0].tData.atFallEarly +
								info.second.delayRiseEarly < atRiseEarly)
						{
							atRiseEarly = currNet->port.cellPtr->ckList[0].tData.atFallEarly +
											info.second.delayRiseEarly;
							RiseEarlyCause = FALL;
						}
						if(currNet->port.cellPtr->ckList[0].tData.atFallEarly!=987654 &&
								info.second.delayFallEarly!=987654 &&
								currNet->port.cellPtr->ckList[0].tData.atFallEarly +
								info.second.delayFallEarly < atFallEarly)
						{
							atFallEarly = currNet->port.cellPtr->ckList[0].tData.atFallEarly +
											info.second.delayFallEarly;
							FallEarlyCause = FALL;
						}
						if(currNet->port.cellPtr->ckList[0].tData.atFallLate!=-987654 &&
								info.second.delayRiseLate!=-987654 &&
								currNet->port.cellPtr->ckList[0].tData.atFallLate +
								info.second.delayRiseLate > atRiseLate)
						{
							atRiseLate = currNet->port.cellPtr->ckList[0].tData.atFallEarly +
											info.second.delayRiseLate;
							RiseLateCause = FALL;
						}
						if(currNet->port.cellPtr->ckList[0].tData.atFallLate!=-987654 &&
								info.second.delayFallLate!=-987654 &&
								currNet->port.cellPtr->ckList[0].tData.atFallLate +
								info.second.delayFallLate > atFallLate)
						{
							atFallLate = currNet->port.cellPtr->ckList[0].tData.atFallLate +
											info.second.delayFallLate;
							FallLateCause = FALL;
						}
						break;
					}

				default: {
							// Do Nothing
						 }
			}
		}

		else 										// For combinational elements
		{
			for(int k = 0; k < (int)currNet->port.pinPtr->relPinIndex.size(); k++)
			{
				vector<int> ipinIndexList = currNet->port.pinPtr->relPinIndex;
				 std::pair<SLEW_DATA,DELAY_DATA> info;

				info = ComputePinTiming(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData,
						                 currNet->port.cellPtr->iipList[ipinIndexList[k]].hash,
										 currNet->port.pinPtr->hash,currNet->port.cellPtr->cellLibIndex_e,
										 currNet->port.cellPtr->cellLibIndex_l,
										 currNet->port.pinPtr->ceffEarly,
										 currNet->port.pinPtr->ceffLate,
										 LATE);

				if(info.first.slewFallEarly < slewFallEarly)
					slewFallEarly = info.first.slewFallEarly;

				if(info.first.slewFallLate > slewFallLate)
					slewFallLate = info.first.slewFallLate;

				if(info.first.slewRiseEarly < slewRiseEarly)
					slewRiseEarly = info.first.slewRiseEarly;

				if(info.first.slewRiseLate > slewRiseLate)
					slewRiseLate = info.first.slewRiseLate;

				info.second.index = ipinIndexList[k];
				currNet->port.pinPtr->dData.push_back(info.second);
				switch(info.second.timeSense)
				{
					case 1: 											// Positive unate
						{
							if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate!=-987654 &&
									info.second.delayRiseLate!=-987654 &&
									currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
									info.second.delayRiseLate > atRiseLate)
							{
								atRiseLate = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
												info.second.delayRiseLate;
								indRiseLate = ipinIndexList[k];
								RiseLateCause = RISE;
							}
							if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly!=987654 &&
									info.second.delayRiseEarly!=987654 &&
									currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
									info.second.delayRiseEarly < atRiseEarly)
							{
								atRiseEarly = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
												info.second.delayRiseEarly;
								indRiseEarly = ipinIndexList[k];
								RiseEarlyCause = RISE;
							}

							if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly!=987654 &&
									info.second.delayFallEarly!=987654 &&
									currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
									info.second.delayFallEarly < atFallEarly)
							{
								atFallEarly = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
												info.second.delayFallEarly;
								indFallEarly = ipinIndexList[k];
								FallEarlyCause = FALL;
							}

							if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate!=-987654 &&
									info.second.delayFallLate!=-987654 &&
									currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
									info.second.delayFallLate > atFallLate)
							{
								atFallLate = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
												info.second.delayFallLate;
								indFallLate = ipinIndexList[k];
								FallLateCause = FALL;
							}

							break;
						}
					case -1: 													// Negative unate
						{
							if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate!=-987654 &&
									info.second.delayRiseLate!=-987654 &&
									(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
											info.second.delayRiseLate) > atRiseLate)
							{
								atRiseLate = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
												info.second.delayRiseLate;
								indRiseLate = ipinIndexList[k];
								RiseLateCause = FALL;
							}

							if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly!=987654 &&
									info.second.delayRiseEarly!=987654 &&
									currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
									info.second.delayRiseEarly < atRiseEarly)
							{
								atRiseEarly = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
												info.second.delayRiseEarly;
								indRiseEarly = ipinIndexList[k];
								RiseEarlyCause = FALL;
							}

							if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly!=987654 &&
									info.second.delayFallEarly!=987654 &&
									currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
									info.second.delayFallEarly < atFallEarly)
							{
								atFallEarly = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
												info.second.delayFallEarly;
								indFallEarly = ipinIndexList[k];
								FallEarlyCause = RISE;
							}

							if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate!=-987654 &&
									info.second.delayFallLate!=-987654 &&
									currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
									info.second.delayFallLate > atFallLate)
							{
								atFallLate = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
												info.second.delayFallLate;
								indFallLate = ipinIndexList[k];
								FallLateCause = RISE;
							}

							break;
						}

					case 2:  								// Rising edge
					{
						if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate!=-987654 &&
								info.second.delayRiseLate!=-987654 &&
								currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
								info.second.delayRiseLate > atRiseLate)
						{
							atRiseLate = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
									info.second.delayRiseLate;
							indRiseLate = ipinIndexList[k];
							RiseLateCause = RISE;
						}

						if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly!=987654 &&
								info.second.delayRiseEarly!=987654 &&
								currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
								info.second.delayRiseEarly < atRiseEarly)
						{
							atRiseEarly = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
									info.second.delayRiseEarly;
							indRiseEarly = ipinIndexList[k];
							RiseEarlyCause = RISE;
						}

						if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly!=987654 &&
								info.second.delayFallEarly!=987654 &&
								currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly + info.second.delayFallEarly < atFallEarly)
						{
							atFallEarly = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly + info.second.delayFallEarly;
							indFallEarly = ipinIndexList[k];
							FallEarlyCause = RISE;
						}

						if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate!=-987654 &&
								info.second.delayFallLate!=-987654 &&
								currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate + info.second.delayFallLate > atFallLate)
						{
							atFallLate = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate + info.second.delayFallLate;
							indFallLate = ipinIndexList[k];
							FallLateCause = RISE;
						}
						break;
					}

				case -2:  													// Falling edge
					{
						if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate!=-987654 &&
								info.second.delayRiseLate!=-987654 &&
								currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate + info.second.delayRiseLate > atRiseLate)
						{
							atRiseLate = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate + info.second.delayRiseLate;
							indRiseLate = ipinIndexList[k];
							RiseLateCause = FALL;
						}

						if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly!=987654 &&
								info.second.delayRiseEarly!=987654 &&
								currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly + info.second.delayRiseEarly < atRiseEarly)
						{
							atRiseEarly = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly + info.second.delayRiseEarly;
							indRiseEarly = ipinIndexList[k];
							RiseEarlyCause = FALL;
						}

						if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly!=987654 &&
								info.second.delayFallEarly!=987654 &&
								currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly + info.second.delayFallEarly < atFallEarly)
						{
							atFallEarly = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly + info.second.delayFallEarly;
							indFallEarly = ipinIndexList[k];
							FallEarlyCause = FALL;
						}

						if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate!=-987654 &&
								info.second.delayFallLate!=-987654 &&
								currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate + info.second.delayFallLate > atFallLate)
						{
							atFallLate = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate + info.second.delayFallLate;
							indFallLate = ipinIndexList[k];
							FallLateCause = FALL;
						}
						break;
					}
				case 3:
					{
						if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate!=-987654)
						{
							if(info.second.delayRiseLate!=-987654)
							{
								double64 dummy;
								if((dummy = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
										info.second.delayRiseLate) > atRiseLate)
								{
									atRiseLate = dummy;
									indRiseLate = ipinIndexList[k];
									RiseLateCause = RISE;

								}
							}

							if(info.second.delayFallLateNeg!=-987654)
							{
								double64 dummy;
								if((dummy = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
										info.second.delayFallLateNeg) > atFallLate)
								{
									atFallLate = dummy;
									indFallLate = ipinIndexList[k];
									FallLateCause = RISE;
								}
							}
						}

						if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate!=-987654)
						{
							if(info.second.delayRiseLateNeg!=-987654)
							{
								double64 dummy;
								if((dummy = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
										info.second.delayRiseLateNeg) > atRiseLate)
								{
									atRiseLate = dummy;
									indRiseLate = ipinIndexList[k];
									RiseLateCause = FALL;

								}
							}

							if(info.second.delayFallLate!=-987654)
							{
								double64 dummy;
								if((dummy = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
										info.second.delayFallLate) > atFallLate)
								{
									atFallLate = dummy;
									indFallLate = ipinIndexList[k];
									FallLateCause = FALL;
								}
							}
						}

						if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly!=987654)
						{
							if(info.second.delayRiseEarly!=987654)
							{
								double64 dummy;
								if((dummy = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
										info.second.delayRiseEarly)<atRiseEarly)
								{
									atRiseEarly = dummy;
									indRiseEarly = ipinIndexList[k];
									RiseEarlyCause = RISE;
								}
							}

							if(info.second.delayFallEarlyNeg!=987654)
							{
								double64 dummy;
								if((dummy = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
										info.second.delayFallEarlyNeg)<atFallEarly)
								{
									atFallEarly = dummy;
									indFallEarly = ipinIndexList[k];
									FallEarlyCause = RISE;
								}
							}
						}

						if(currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly!=987654)
						{
							if(info.second.delayFallEarly!=987654)
							{
								double64 dummy;
								if((dummy = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
										info.second.delayFallEarly)<atFallEarly)
								{
									atFallEarly = dummy;
									indFallEarly = ipinIndexList[k];
									FallEarlyCause = FALL;
								}
							}

							if(info.second.delayRiseEarlyNeg!=987654)
							{
								double64 dummy;
								if((dummy = currNet->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
										info.second.delayRiseEarlyNeg)<atRiseEarly)
								{
									atRiseEarly = dummy;
									indRiseEarly = ipinIndexList[k];
									RiseEarlyCause = FALL;
								}
							}
						}
						break;
					}
				default:
					{
						// Do Nothing
					}
				}
			}
		}

		currNet->port.pinPtr->tData.atFallEarly = atFallEarly;
		currNet->port.pinPtr->tData.atFallLate  = atFallLate;
		currNet->port.pinPtr->tData.atRiseEarly = atRiseEarly;
		currNet->port.pinPtr->tData.atRiseLate  = atRiseLate;

		currNet->port.pinPtr->tData.criticalAT.indexFallEarly = indFallEarly;
		currNet->port.pinPtr->tData.criticalAT.indexFallLate  = indFallLate;
		currNet->port.pinPtr->tData.criticalAT.indexRiseEarly = indRiseEarly;
		currNet->port.pinPtr->tData.criticalAT.indexRiseLate  = indRiseLate;

		currNet->port.pinPtr->tData.criticalAT.transitionRE = RiseEarlyCause;
		currNet->port.pinPtr->tData.criticalAT.transitionRL = RiseLateCause;
		currNet->port.pinPtr->tData.criticalAT.transitionFE = FallEarlyCause;
		currNet->port.pinPtr->tData.criticalAT.transitionFL = FallLateCause;

		currNet->port.pinPtr->tData.slewFallEarly = slewFallEarly;
		currNet->port.pinPtr->tData.slewFallLate = slewFallLate;
		currNet->port.pinPtr->tData.slewRiseEarly = slewRiseEarly;
		currNet->port.pinPtr->tData.slewRiseLate = slewRiseLate;

		// Restoring lambda's
		for (int z = 0; z < (int)dData_backup.size() ; z++)
		{
			for (int l = 0; l < (int)currNet->port.pinPtr->dData.size() ; l++)
			{
				if(dData_backup[z].index == currNet->port.pinPtr->dData[l].index)
				{
					currNet->port.pinPtr->dData[l].lambdaFL = dData_backup[z].lambdaFL;
					currNet->port.pinPtr->dData[l].lambdaRL = dData_backup[z].lambdaRL;
					break;
				}
			}
		}
	}
}

// Added for New Effective Capacitance
/**
 * \param oPin Pointer to output pin whose ECM effective load capacitance needs to be found
 *
 * Finds effective capacitance for an output pin according to ECM
 */
void ForwardPropagate::FindcPi(InstanceOpin* oPin)
{
	if (oPin->cellPtr->cellLibIndex_l == PI || oPin->cellPtr->cellLibIndex_l == CLK_SRC)
	{
		assert(oPin->ceffLate > 0);
		oPin->cPi = oPin->ceffLate;
		return;
	}

	if (oPin->ceffLate < ZERO_VAL)
	{
		cout << oPin->ceffLate << endl;
		assert(0);
	}
	assert (oPin->ceffLate > ZERO_VAL);

	double64 tt, td, trf;

	if (AT_MODE == true)
	{
		if (oPin->tData.atRiseLate > oPin->tData.atFallLate)
		{

			if (oPin->tData.criticalAT.transitionRL == RISE)
			{
				if (oPin->cellPtr->info_late->isSequential)
				{
					tt = oPin->cellPtr->ckList[0].tData.slewRiseLate;
					td = oPin->dData[0].delayRiseLate;

				}
				else
				{
					tt = oPin->cellPtr->iipList[oPin->tData.criticalAT.indexRiseLate].tData.slewRiseLate;
					for (int x = 0; x < (int)oPin->dData.size(); x++)
					{
						if (oPin->dData[x].index == oPin->tData.criticalAT.indexRiseLate)
						{
							td = oPin->dData[x].delayRiseLate;
							assert(oPin->cellPtr->iipList[oPin->dData[x].index].name.pinName ==
									oPin->cellPtr->iipList[oPin->tData.criticalAT.indexRiseLate].name.pinName);
							break;
						}
					}

					pair<SLEW_DATA, DELAY_DATA> sdTemp = ComputePinTiming(oPin->cellPtr->iipList[oPin->tData.criticalAT.indexRiseLate].tData,
							oPin->cellPtr->iipList[oPin->tData.criticalAT.indexRiseLate].hash,
							oPin->hash, oPin->cellPtr->cellLibIndex_e, oPin->cellPtr->cellLibIndex_l, oPin->ceffEarly, oPin->ceffLate, LATE);
					trf = sdTemp.first.slewRiseLate;

				}
			}
			else
			{
				if (oPin->cellPtr->info_late->isSequential)
				{
					tt = oPin->cellPtr->ckList[0].tData.slewFallLate;
					td = oPin->dData[0].delayRiseLate;
				}
				else
				{
					tt = oPin->cellPtr->iipList[oPin->tData.criticalAT.indexRiseLate].tData.slewFallLate;
					for (int x = 0; x < (int)oPin->dData.size(); x++)
					{
						if (oPin->dData[x].index == oPin->tData.criticalAT.indexRiseLate)
						{
							td = oPin->dData[x].delayRiseLate;
							assert(oPin->cellPtr->iipList[oPin->dData[x].index].name.pinName ==
									oPin->cellPtr->iipList[oPin->tData.criticalAT.indexRiseLate].name.pinName);
							break;
						}
					}

					pair<SLEW_DATA, DELAY_DATA> sdTemp = ComputePinTiming(oPin->cellPtr->iipList[oPin->tData.criticalAT.indexRiseLate].tData,
							oPin->cellPtr->iipList[oPin->tData.criticalAT.indexRiseLate].hash,
							oPin->hash, oPin->cellPtr->cellLibIndex_e, oPin->cellPtr->cellLibIndex_l, oPin->ceffEarly, oPin->ceffLate, LATE);
					trf = sdTemp.first.slewRiseLate;
				}
			}
		}
		else
		{
			if (oPin->tData.criticalAT.transitionFL == RISE)
			{
				if (oPin->cellPtr->info_late->isSequential)
				{
					tt = oPin->cellPtr->ckList[0].tData.slewRiseLate;
					td = oPin->dData[0].delayFallLate;
				}
				else
				{
					tt = oPin->cellPtr->iipList[oPin->tData.criticalAT.indexFallLate].tData.slewRiseLate;
					for (int x = 0; x < (int)oPin->dData.size(); x++)
					{
						if (oPin->dData[x].index == oPin->tData.criticalAT.indexFallLate)
						{
							td = oPin->dData[x].delayFallLate;
							assert(oPin->cellPtr->iipList[oPin->dData[x].index].name.pinName ==
									oPin->cellPtr->iipList[oPin->tData.criticalAT.indexFallLate].name.pinName);
							break;
						}
					}
					pair<SLEW_DATA, DELAY_DATA> sdTemp = ComputePinTiming(oPin->cellPtr->iipList[oPin->tData.criticalAT.indexFallLate].tData,
							oPin->cellPtr->iipList[oPin->tData.criticalAT.indexFallLate].hash,
							oPin->hash, oPin->cellPtr->cellLibIndex_e, oPin->cellPtr->cellLibIndex_l, oPin->ceffEarly, oPin->ceffLate, LATE);
					trf = sdTemp.first.slewFallLate;
				}
			}
			else
			{
				if (oPin->cellPtr->info_late->isSequential)
				{
					tt = oPin->cellPtr->ckList[0].tData.slewFallLate;
					td = oPin->dData[0].delayFallLate;
				}
				else
				{
					tt = oPin->cellPtr->iipList[oPin->tData.criticalAT.indexFallLate].tData.slewFallLate;
					for (int x = 0; x < (int)oPin->dData.size(); x++)
					{
						if (oPin->dData[x].index == oPin->tData.criticalAT.indexFallLate)
						{
							td = oPin->dData[x].delayFallLate;
							assert(oPin->cellPtr->iipList[oPin->dData[x].index].name.pinName ==
									oPin->cellPtr->iipList[oPin->tData.criticalAT.indexFallLate].name.pinName);
							break;
						}
					}
					pair<SLEW_DATA, DELAY_DATA> sdTemp = ComputePinTiming(oPin->cellPtr->iipList[oPin->tData.criticalAT.indexFallLate].tData,
							oPin->cellPtr->iipList[oPin->tData.criticalAT.indexFallLate].hash,
							oPin->hash, oPin->cellPtr->cellLibIndex_e, oPin->cellPtr->cellLibIndex_l, oPin->ceffEarly, oPin->ceffLate, LATE);
					trf = sdTemp.first.slewFallLate;
				}
			}
		}
	}

	else if (DELAY_MODE == true)
	{
		if (oPin->cellPtr->info_late->isSequential)
		{
			if (oPin->dData[0].timeSense == RISING_EDGE)
			{
				tt = oPin->cellPtr->ckList[0].tData.slewRiseLate;

				if (oPin->dData[0].delayRiseLate > oPin->dData[0].delayFallLate)
				{
					td = oPin->dData[0].delayRiseLate;
					trf = oPin->tData.slewRiseLate;
				}
				else
				{
					td = oPin->dData[0].delayFallLate;
					trf = oPin->tData.slewFallLate;
				}
			}
			else
			{
				tt = oPin->cellPtr->ckList[0].tData.slewFallLate;

				if (oPin->dData[0].delayRiseLate > oPin->dData[0].delayFallLate)
				{
					td = oPin->dData[0].delayRiseLate;
					trf = oPin->tData.slewRiseLate;
				}
				else
				{
					td = oPin->dData[0].delayFallLate;
					trf = oPin->tData.slewFallLate;
				}
			}

		}
		else
		{
			double64 maxDelay = -987654;
			int maxDelayIdx = 0;
			int maxDelaySense = oPin->dData[0].timeSense;
			int maxDelayTrans = RISE;

			for (int x = 0; x < (int)oPin->dData.size(); x++)
			{
				if (oPin->dData[x].delayRiseLate > maxDelay)
				{
					maxDelay = oPin->dData[x].delayRiseLate;
					maxDelayIdx = oPin->dData[x].index;
					maxDelaySense = oPin->dData[x].timeSense;
					maxDelayTrans = RISE;
				}
				if (oPin->dData[x].delayFallLate > maxDelay)
				{
					maxDelay = oPin->dData[x].delayFallLate;
					maxDelayIdx = oPin->dData[x].index;
					maxDelaySense = oPin->dData[x].timeSense;
					maxDelayTrans = FALL;
				}
			}

			td = maxDelay;
			pair<SLEW_DATA, DELAY_DATA> sdTemp = ComputePinTiming(oPin->cellPtr->iipList[maxDelayIdx].tData,
					oPin->cellPtr->iipList[maxDelayIdx].hash,
					oPin->hash, oPin->cellPtr->cellLibIndex_e, oPin->cellPtr->cellLibIndex_l,
					oPin->ceffEarly, oPin->ceffLate, LATE);

			if (maxDelaySense == POSITIVE_UNATE)
			{
				if (maxDelayTrans == RISE)
				{
					tt = oPin->cellPtr->iipList[maxDelayIdx].tData.slewRiseLate;
					trf = sdTemp.first.slewRiseLate;
				}
				else
				{
					tt = oPin->cellPtr->iipList[maxDelayIdx].tData.slewFallLate;
					trf = sdTemp.first.slewFallLate;
				}
			}
			else if (maxDelaySense == NEGATIVE_UNATE)
			{
				if (maxDelayTrans == RISE)
				{
					tt = oPin->cellPtr->iipList[maxDelayIdx].tData.slewFallLate;
					trf = sdTemp.first.slewRiseLate;
				}
				else
				{
					tt = oPin->cellPtr->iipList[maxDelayIdx].tData.slewRiseLate;
					trf = sdTemp.first.slewFallLate;
				}
			}
			else
			{
					tt = max(oPin->cellPtr->iipList[maxDelayIdx].tData.slewRiseLate,
							oPin->cellPtr->iipList[maxDelayIdx].tData.slewFallLate);
					trf = max(sdTemp.first.slewRiseLate, sdTemp.first.slewFallLate);
			}


		}

	}

	else	//SLEW_MODE == true
	{

	}


	oPin->tD = td + tt*0.5;
	oPin->tX = td + tt*0.5 - trf*0.5;


	if ((oPin->tD - oPin->tX) <= 0 || oPin->tX <= 0)
	{
		oPin->cPi = oPin->ceffLate;
		return;
	}

	oPin->cPi = oPin->wire->c2 + oPin->wire->c1*(1
				- (oPin->wire->r*oPin->wire->c1)/(oPin->tD - oPin->tX*0.5)
				+ (pow(oPin->wire->r*oPin->wire->c1, 2))/(oPin->tX*(oPin->tD - oPin->tX*0.5))
				*exp((oPin->tX - oPin->tD)/(oPin->wire->r*oPin->wire->c1))
				*(1-exp(((-1)*oPin->tX)/(oPin->wire->r*oPin->wire->c1))));


	if(oPin->tX < ZERO_VAL)
	{
		cout << oPin->name.icellName << ":" << oPin->name.pinName << "\t" << oPin->cellPtr->info_late->name << "\t" << oPin->ceffLate << "\t" << oPin->cPi << "\t";
		cout << "C1 = " << oPin->wire->c1 << "\tC2 = " << oPin->wire->c2 << "\tR = " << oPin->wire->r << "\t";
		cout << "tD = " << oPin->tD << "\ttX = " << oPin->tX << "\t";
		cout << "td = " << td << "\ttt = " << tt << "\ttrf = " << trf << endl;
		cout << "true trf = " << oPin->tData.slewRiseLate << endl;

		pair<SLEW_DATA, DELAY_DATA> sdTemp = ComputePinTiming(oPin->cellPtr->iipList[0].tData,
				oPin->cellPtr->iipList[0].hash,
				oPin->hash, oPin->cellPtr->cellLibIndex_e, oPin->cellPtr->cellLibIndex_l, oPin->ceffEarly, oPin->ceffLate, LATE);
		cout << "fake trfRL = " << sdTemp.first.slewRiseLate << endl;
		cout << "fake trfFL = " << sdTemp.first.slewFallLate << endl;

		assert(0);
	}
	if (oPin->cPi < 0)
	{
		cout << oPin->name.icellName << ":" << oPin->name.pinName << "\t" << oPin->cellPtr->info_late->name << "\t" << oPin->ceffLate << "\t" << oPin->cPi << "\t";
		cout << "C1 = " << oPin->wire->c1 << "\tC2 = " << oPin->wire->c2 << "\tR = " << oPin->wire->r << "\t";
		cout << "tD = " << oPin->tD << "\ttX = " << oPin->tX << "\t";
		cout << "td = " << td << "\ttt = " << tt << "\ttrf = " << trf << endl;

		assert(0);
	}
	assert (oPin->cPi >= 0);


	if (tt == FORBIDDEN_LATE)
	{
		cout << oPin->tData.criticalAT.indexRiseLate << endl;
		assert(0);
	}
}

/**
 * \param currNet PI Net whose port pin PI needs to be set
 *
 * Computes and sets the Arrival Time of a PI pin
 * Very similar to function ATSlew
 * Used only in iitSizer
 */
void ForwardPropagate::SetPI_AT (SpefNet* currNet)
{
	InstanceOpin* oPin = currNet->port.pinPtr;

	/* Get the index in timing */
	int64 i = Bsearch(timing.at,oPin->hash,0,timing.at.size()-1);
	assert(i!=-1 && " assertion failed in AT&Slew");
	assert(timing.at[i].name == oPin->name.icellName);
	if(i == -1) return;

	oPin->tData.atFallLate = timing.at[i].latefall;
	oPin->tData.atRiseLate = timing.at[i].laterise;
	oPin->tData.slewFallLate = timing.slew[i].latefall;
	oPin->tData.slewRiseLate = timing.slew[i].laterise;

	// WARNING: AT/Slew has to be propagated if clock tree is present, while in iitSizer ISPD 2013 benchmarks it is not, so skipping for early

	if(timing.slew[i].drivingInd != -1 && oPin->cellPtr->cellLibIndex_l != CLK_SRC)
	{
		 std::pair<SLEW_DATA,DELAY_DATA> info_load,info_load_max,info_noload,info_noload_max;
		 NODE_TIMING_DATA drivingData;
		 drivingData.atFallLate = 0;
		 drivingData.atRiseLate = 0;
		 drivingData.slewRiseLate = timing.slew[i].laterise;
		 drivingData.slewFallLate = timing.slew[i].latefall;
		 if(celllibstorage_l.celllib[timing.slew[i].drivingInd].isSequential)
		 {
				info_load = ComputePinTiming(drivingData,
								celllibstorage_l.celllib[timing.slew[i].drivingInd].clkpin[0].hash,
								celllibstorage_l.celllib[timing.slew[i].drivingInd].opins[0].hash,
								timing.slew[i].drivingInd,
								timing.slew[i].drivingInd,
								oPin->ceffEarly,
								oPin->ceffLate,
								LATE
								);
				info_noload = ComputePinTiming(drivingData,
									celllibstorage_l.celllib[timing.slew[i].drivingInd].clkpin[0].hash,
									celllibstorage_l.celllib[timing.slew[i].drivingInd].opins[0].hash,
									timing.slew[i].drivingInd,
									timing.slew[i].drivingInd,
									0,
									0,
									LATE
									);

					info_load_max = info_load;
					info_noload_max = info_noload;

					oPin->tData.atFallLate +=
							info_load_max.second.delayFallLate - info_noload_max.second.delayFallLate;
					oPin->tData.atRiseLate +=
							info_load_max.second.delayRiseLate - info_noload_max.second.delayRiseLate;

					oPin->tData.slewFallLate =
							info_load_max.first.slewFallLate;
					oPin->tData.slewRiseLate =
							info_load_max.first.slewRiseLate;
		 }
		 else
		 {
			 for(int v = 0;  v < (int)celllibstorage_l.celllib[timing.slew[i].drivingInd].ipins.size(); v++)
			 {
					info_load = ComputePinTiming(drivingData,
							celllibstorage_l.celllib[timing.slew[i].drivingInd].ipins[v].hash,
							celllibstorage_l.celllib[timing.slew[i].drivingInd].opins[0].hash,
							timing.slew[i].drivingInd,
							timing.slew[i].drivingInd,
							oPin->ceffEarly,
							oPin->ceffLate,
							LATE
							);
					info_noload = ComputePinTiming(drivingData,
							celllibstorage_l.celllib[timing.slew[i].drivingInd].ipins[v].hash,
							celllibstorage_l.celllib[timing.slew[i].drivingInd].opins[0].hash,
							timing.slew[i].drivingInd,
							timing.slew[i].drivingInd,
							0,
							0,
							LATE
							);
					if(v == 0)
					{
						info_load_max = info_load;
						info_noload_max = info_noload;
					}
					else
					{
						info_load_max.first.slewFallLate = max(info_load_max.first.slewFallLate,
																info_load.first.slewFallLate);
						info_load_max.first.slewRiseLate = max(info_load_max.first.slewRiseLate,
																info_load.first.slewRiseLate);
						info_load_max.second.delayFallLate = max(info_load_max.second.delayFallLate,
																info_load.second.delayFallLate);
						info_load_max.second.delayRiseLate = max(info_load_max.second.delayRiseLate,
																info_load.second.delayRiseLate);

						info_noload_max.first.slewFallLate = max(info_noload_max.first.slewFallLate,
																info_noload.first.slewFallLate);
						info_noload_max.first.slewRiseLate = max(info_noload_max.first.slewRiseLate,
																info_noload.first.slewRiseLate);
						info_noload_max.second.delayFallLate = max(info_noload_max.second.delayFallLate,
																info_noload.second.delayFallLate);
						info_noload_max.second.delayRiseLate = max(info_noload_max.second.delayRiseLate,
																info_noload.second.delayRiseLate);
					}
			 }
			 oPin->tData.atFallLate +=
					info_load_max.second.delayFallLate - info_noload_max.second.delayFallLate;
			 oPin->tData.atRiseLate +=
					info_load_max.second.delayRiseLate - info_noload_max.second.delayRiseLate;

			 oPin->tData.slewFallLate =
					info_load_max.first.slewFallLate;
			 oPin->tData.slewRiseLate =
					info_load_max.first.slewRiseLate;
		}
	}
}

/**
 * \param mode Mode (early/late)
 *
 * Performs core forward propagation of Arrival Time and Slew for complete design
 * Used both in STA as well as in incremental changes
 */
void ForwardPropagate::ATSlew(int mode)
{
	/* Initialise the PI Virtual sources with the correct timing data */
	if(!_isStaticDone || SIZER)
	{

		for(int i = 0;i < (int)timing.at.size(); i++)
		{
			/* Get the index in InstanceHashTable */
			int64 index = Bsearch(InstanceHashTable,timing.at[i].hash,0,InstanceHashTable.size()-1);

			assert (index != -1 && " assertion failed in AT&Slew");
			if(index == -1)
				continue;

			//WARNING: AT/Slew has to be propagated if clock tree is present, while in case of ISPD 2013 benchmarks it is not, so skipping for early
			if(InstanceList[InstanceHashTable[index].index].cellLibIndex_l == CLK_SRC || mode != LATE)
			{
				InstanceList[InstanceHashTable[index].index].iopList[0].tData.atFallEarly = timing.at[i].earlyfall;
				InstanceList[InstanceHashTable[index].index].iopList[0].tData.atRiseEarly = timing.at[i].earlyrise;
				InstanceList[InstanceHashTable[index].index].iopList[0].tData.slewFallEarly = timing.slew[i].earlyfall;
				InstanceList[InstanceHashTable[index].index].iopList[0].tData.slewRiseEarly = timing.slew[i].earlyrise;
			}
			if(InstanceList[InstanceHashTable[index].index].cellLibIndex_l == CLK_SRC || mode != EARLY)
			{
				InstanceList[InstanceHashTable[index].index].iopList[0].tData.atFallLate = timing.at[i].latefall;
				InstanceList[InstanceHashTable[index].index].iopList[0].tData.atRiseLate = timing.at[i].laterise;
				InstanceList[InstanceHashTable[index].index].iopList[0].tData.slewFallLate = timing.slew[i].latefall;
				InstanceList[InstanceHashTable[index].index].iopList[0].tData.slewRiseLate = timing.slew[i].laterise;

				if(ECM == true)
				{
					InstanceList[InstanceHashTable[index].index].iopList[0].ceffLate =
							InstanceList[InstanceHashTable[index].index].iopList[0].wire->c1 +
							InstanceList[InstanceHashTable[index].index].iopList[0].wire->c2;

					InstanceList[InstanceHashTable[index].index].iopList[0].port->ceffLate =
							InstanceList[InstanceHashTable[index].index].iopList[0].ceffLate;
				}
				if(timing.slew[i].drivingInd != -1 && InstanceList[InstanceHashTable[index].index].cellLibIndex_l != CLK_SRC)
				{
					 std::pair<SLEW_DATA,DELAY_DATA> info_load,info_load_max,info_noload,info_noload_max;
					 NODE_TIMING_DATA drivingData;
					 drivingData.atFallLate = 0;
					 drivingData.atRiseLate = 0;
					 drivingData.slewRiseLate = timing.slew[i].laterise;
					 drivingData.slewFallLate = timing.slew[i].latefall;
					 if(celllibstorage_l.celllib[timing.slew[i].drivingInd].isSequential)
					 {
							info_load = ComputePinTiming(drivingData,
											celllibstorage_l.celllib[timing.slew[i].drivingInd].clkpin[0].hash,
											celllibstorage_l.celllib[timing.slew[i].drivingInd].opins[0].hash,
											timing.slew[i].drivingInd,
											timing.slew[i].drivingInd,
											InstanceList[InstanceHashTable[index].index].iopList[0].ceffEarly,
											InstanceList[InstanceHashTable[index].index].iopList[0].ceffLate,
															mode
															);
									info_noload = ComputePinTiming(drivingData,
											celllibstorage_l.celllib[timing.slew[i].drivingInd].clkpin[0].hash,
											celllibstorage_l.celllib[timing.slew[i].drivingInd].opins[0].hash,
											timing.slew[i].drivingInd,
											timing.slew[i].drivingInd,
											0,
											0,
											mode
															);

								info_load_max = info_load;
								info_noload_max = info_noload;

								InstanceList[InstanceHashTable[index].index].iopList[0].tData.atFallLate +=
										info_load_max.second.delayFallLate - info_noload_max.second.delayFallLate;
								InstanceList[InstanceHashTable[index].index].iopList[0].tData.atRiseLate +=
										info_load_max.second.delayRiseLate - info_noload_max.second.delayRiseLate;

								InstanceList[InstanceHashTable[index].index].iopList[0].tData.slewFallLate =
										info_load_max.first.slewFallLate;
								InstanceList[InstanceHashTable[index].index].iopList[0].tData.slewRiseLate =
										info_load_max.first.slewRiseLate;
					 }
					 else
					 {
						 for(int v = 0;  v < (int)celllibstorage_l.celllib[timing.slew[i].drivingInd].ipins.size(); v++)
						 {
								info_load = ComputePinTiming(drivingData,
										celllibstorage_l.celllib[timing.slew[i].drivingInd].ipins[v].hash,
										celllibstorage_l.celllib[timing.slew[i].drivingInd].opins[0].hash,
										timing.slew[i].drivingInd,
										timing.slew[i].drivingInd,
										InstanceList[InstanceHashTable[index].index].iopList[0].ceffEarly,
										InstanceList[InstanceHashTable[index].index].iopList[0].ceffLate,
														mode
														);
								info_noload = ComputePinTiming(drivingData,
										celllibstorage_l.celllib[timing.slew[i].drivingInd].ipins[v].hash,
										celllibstorage_l.celllib[timing.slew[i].drivingInd].opins[0].hash,
										timing.slew[i].drivingInd,
										timing.slew[i].drivingInd,
										0,
										0,
										mode
														);
								if(v == 0)
								{
									info_load_max = info_load;
									info_noload_max = info_noload;
								}
								else
								{
									info_load_max.first.slewFallLate = max(info_load_max.first.slewFallLate,
																			info_load.first.slewFallLate);
									info_load_max.first.slewRiseLate = max(info_load_max.first.slewRiseLate,
																			info_load.first.slewRiseLate);
									info_load_max.second.delayFallLate = max(info_load_max.second.delayFallLate,
																			info_load.second.delayFallLate);
									info_load_max.second.delayRiseLate = max(info_load_max.second.delayRiseLate,
																			info_load.second.delayRiseLate);

									info_noload_max.first.slewFallLate = max(info_noload_max.first.slewFallLate,
																			info_noload.first.slewFallLate);
									info_noload_max.first.slewRiseLate = max(info_noload_max.first.slewRiseLate,
																			info_noload.first.slewRiseLate);
									info_noload_max.second.delayFallLate = max(info_noload_max.second.delayFallLate,
																			info_noload.second.delayFallLate);
									info_noload_max.second.delayRiseLate = max(info_noload_max.second.delayRiseLate,
																			info_noload.second.delayRiseLate);
								}
						 }

						InstanceList[InstanceHashTable[index].index].iopList[0].tData.atFallLate +=
								info_load_max.second.delayFallLate - info_noload_max.second.delayFallLate;
						InstanceList[InstanceHashTable[index].index].iopList[0].tData.atRiseLate +=
								info_load_max.second.delayRiseLate - info_noload_max.second.delayRiseLate;

						InstanceList[InstanceHashTable[index].index].iopList[0].tData.slewFallLate =
								info_load_max.first.slewFallLate;
						InstanceList[InstanceHashTable[index].index].iopList[0].tData.slewRiseLate =
								info_load_max.first.slewRiseLate;
					}
				}
			}

			for (int j = 0; j < NUM_THREADS; j++)
			{
				InstanceList[InstanceHashTable[index].index].iopList[0].pData[j].atFallEarlyCPPR = timing.at[i].earlyfall;
				InstanceList[InstanceHashTable[index].index].iopList[0].pData[j].atFallLateCPPR = timing.at[i].latefall;
				InstanceList[InstanceHashTable[index].index].iopList[0].pData[j].atRiseEarlyCPPR = timing.at[i].earlyrise;
				InstanceList[InstanceHashTable[index].index].iopList[0].pData[j].atRiseLateCPPR = timing.at[i].laterise;
			}
		}
	}

	/* Processing each element from "Sort" and updating the interconnect values too */

	for(int64 i = 0; i < (int64)Sort.size(); i++)
	{
		vector <DELAY_DATA> dData_backup;
		for (int iter = 0; iter < (int)Sort[i]->port.pinPtr->dData.size() ; iter++)
		{
			dData_backup.push_back(Sort[i]->port.pinPtr->dData[iter]);
			TrackSoln.totalLD -= Sort[i]->port.pinPtr->dData[iter].delayFallLate * Sort[i]->port.pinPtr->dData[iter].lambdaFL ;
			TrackSoln.totalLD -= Sort[i]->port.pinPtr->dData[iter].delayRiseLate * Sort[i]->port.pinPtr->dData[iter].lambdaRL ;
		}

		Sort[i]->isRemoved               = false;
		Sort[i]->isDiscovered            = false;
		Sort[i]->port.pinPtr->visitCount = 0;
		Sort[i]->port.pinPtr->dData.clear();
		Sort[i]->port.pinPtr->dData.clear();
		Sort[i]->isReachable             = false;
		Sort[i]->isIncremental           = false;
		if(Sort[i]->isCktreeMember && Sort[i]->port.cellPtr->cellLibIndex_e == -4)
		{
			Sort[i]->port.parity = EVEN;
		}

		double64 atRiseEarly   = 987654;
		double64 atFallEarly   = 987654;
		double64 slewRiseEarly = 987654;
		double64 slewFallEarly = 987654;
		double64 atRiseLate    = -987654;
		double64 atFallLate    = -987654;
		double64 slewRiseLate  = -987654;
		double64 slewFallLate  = -987654;

		int indRiseEarly = DEFAULTINDEX;
		int indFallEarly = DEFAULTINDEX;
		int indRiseLate  = DEFAULTINDEX;
		int indFallLate  = DEFAULTINDEX;

		bool RiseEarlyCause = RISE, RiseLateCause = RISE, FallEarlyCause = FALL, FallLateCause = FALL;

		if(Sort[i]->port.cellPtr->cellLibIndex_l != -2 && Sort[i]->port.cellPtr->cellLibIndex_l!=-4) // Check index for PI or Clock
		{
			if (ECM == true )
			{
				Sort[i]->port.pinPtr->ceffLate = Sort[i]->c1 + Sort[i]->c2;
				Sort[i]->port.ceffLate = Sort[i]->port.pinPtr->ceffLate;
			}

			if(Sort[i]->port.cellPtr->info_late->isSequential) 				// For sequential elements
			{
				 std::pair<SLEW_DATA,DELAY_DATA> info;

				if(Sort[i]->isVirtualNet)
				{
					info = ComputePinTiming(Sort[i]->port.cellPtr->ckList[0].tData,
							                Sort[i]->port.cellPtr->ckList[0].hash,
											Sort[i]->port.pinPtr->hash,
											Sort[i]->port.cellPtr->cellLibIndex_e,
											Sort[i]->port.cellPtr->cellLibIndex_l,
											0,
											0,
											mode
											);

				}
				else  info = ComputePinTiming(Sort[i]->port.cellPtr->ckList[0].tData,Sort[i]->port.cellPtr->ckList[0].hash,
								Sort[i]->port.pinPtr->hash,
								Sort[i]->port.cellPtr->cellLibIndex_e,
								Sort[i]->port.cellPtr->cellLibIndex_l,
								Sort[i]->port.pinPtr->ceffEarly,
								Sort[i]->port.pinPtr->ceffLate,
								mode
								);


				if(info.first.slewFallEarly < slewFallEarly)
					slewFallEarly = info.first.slewFallEarly;

				if(info.first.slewFallLate > slewFallLate)
					slewFallLate = info.first.slewFallLate;

				if(info.first.slewRiseEarly < slewRiseEarly)
					slewRiseEarly = info.first.slewRiseEarly;

				if(info.first.slewRiseLate > slewRiseLate)
					slewRiseLate = info.first.slewRiseLate;

				info.second.index = 0;
				Sort[i]->port.pinPtr->dData.push_back(info.second);

				switch(info.second.timeSense)
				{
					case 2:
						{
							if(Sort[i]->port.cellPtr->ckList[0].tData.atRiseEarly!=987654 &&
									info.second.delayRiseEarly!=987654 && Sort[i]->port.cellPtr->ckList[0].tData.atRiseEarly +
									info.second.delayRiseEarly < atRiseEarly)
							{
								atRiseEarly = Sort[i]->port.cellPtr->ckList[0].tData.atRiseEarly +
												info.second.delayRiseEarly;
								RiseEarlyCause = RISE;
							}

							if(Sort[i]->port.cellPtr->ckList[0].tData.atRiseEarly!=987654 &&
									info.second.delayFallEarly!=987654 && Sort[i]->port.cellPtr->ckList[0].tData.atRiseEarly +
									info.second.delayFallEarly < atFallEarly)
							{
								atFallEarly = Sort[i]->port.cellPtr->ckList[0].tData.atRiseEarly +
												info.second.delayFallEarly;
								FallEarlyCause = RISE;
							}

							if(Sort[i]->port.cellPtr->ckList[0].tData.atRiseLate!=-987654 &&
									info.second.delayRiseLate!=-987654 && Sort[i]->port.cellPtr->ckList[0].tData.atRiseLate +
									info.second.delayRiseLate > atRiseLate)
							{
								atRiseLate = Sort[i]->port.cellPtr->ckList[0].tData.atRiseLate +
												info.second.delayRiseLate;
								RiseLateCause = RISE;
							}

							if(Sort[i]->port.cellPtr->ckList[0].tData.atRiseLate!=-987654 &&
									info.second.delayFallLate!=-987654 && Sort[i]->port.cellPtr->ckList[0].tData.atRiseLate +
									info.second.delayFallLate > atFallLate)
							{
								atFallLate = Sort[i]->port.cellPtr->ckList[0].tData.atRiseLate +
												info.second.delayFallLate;
								FallLateCause = RISE;
							}

							break;
						}
					case -2:
						{
							if(Sort[i]->port.cellPtr->ckList[0].tData.atFallEarly!=987654 &&
									info.second.delayRiseEarly!=987654 && Sort[i]->port.cellPtr->ckList[0].tData.atFallEarly +
									info.second.delayRiseEarly < atRiseEarly)
							{
								atRiseEarly = Sort[i]->port.cellPtr->ckList[0].tData.atFallEarly +
												info.second.delayRiseEarly;
								RiseEarlyCause = FALL;
							}

							if(Sort[i]->port.cellPtr->ckList[0].tData.atFallEarly!=987654 &&
									info.second.delayFallEarly!=987654 && Sort[i]->port.cellPtr->ckList[0].tData.atFallEarly +
									info.second.delayFallEarly < atFallEarly)
							{
								atFallEarly = Sort[i]->port.cellPtr->ckList[0].tData.atFallEarly +
												info.second.delayFallEarly;
								FallEarlyCause = FALL;
							}

							if(Sort[i]->port.cellPtr->ckList[0].tData.atFallLate!=-987654 &&
									info.second.delayRiseLate!=-987654 && Sort[i]->port.cellPtr->ckList[0].tData.atFallLate +
									info.second.delayRiseLate > atRiseLate)
							{
								atRiseLate = Sort[i]->port.cellPtr->ckList[0].tData.atFallEarly +
												info.second.delayRiseLate;
								RiseLateCause = FALL;
							}

							if(Sort[i]->port.cellPtr->ckList[0].tData.atFallLate!=-987654 &&
									info.second.delayFallLate!=-987654 && Sort[i]->port.cellPtr->ckList[0].tData.atFallLate +
									info.second.delayFallLate > atFallLate)
							{
								atFallLate = Sort[i]->port.cellPtr->ckList[0].tData.atFallLate +
												info.second.delayFallLate;
								FallLateCause = FALL;
							}
							break;
						}
					default: {
								// Do Nothing
							 }
				}
			}
			else //for combinational elements
			{
				for(int k = 0; k < (int)Sort[i]->port.pinPtr->relPinIndex.size(); k++)
				{
					vector<int> ipinIndexList = Sort[i]->port.pinPtr->relPinIndex;
					std::pair<SLEW_DATA,DELAY_DATA> info;

					if(Sort[i]->isVirtualNet)info = ComputePinTiming(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData,
														Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].hash,
														Sort[i]->port.pinPtr->hash,
														Sort[i]->port.cellPtr->cellLibIndex_e,
														Sort[i]->port.cellPtr->cellLibIndex_l,
														0,
														0,
														mode
														);
					else
					{
						info = ComputePinTiming(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData,
									Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].hash,
									Sort[i]->port.pinPtr->hash,
									Sort[i]->port.cellPtr->cellLibIndex_e,
									Sort[i]->port.cellPtr->cellLibIndex_l,
									Sort[i]->port.pinPtr->ceffEarly,
									Sort[i]->port.pinPtr->ceffLate,
									mode
									);
					}
					if(mode == LATE)
					{
						info.first.slewFallEarly = 0;
						info.first.slewRiseEarly = 0;
						info.second.delayFallEarly = 0;
						info.second.delayRiseEarly = 0;
					}

					if(info.first.slewFallEarly < slewFallEarly)
						slewFallEarly = info.first.slewFallEarly;

					if(info.first.slewFallLate > slewFallLate)
						slewFallLate = info.first.slewFallLate;

					if(info.first.slewRiseEarly < slewRiseEarly)
						slewRiseEarly = info.first.slewRiseEarly;

					if(info.first.slewRiseLate > slewRiseLate)
						slewRiseLate = info.first.slewRiseLate;

					info.second.index = ipinIndexList[k];
					Sort[i]->port.pinPtr->dData.push_back(info.second);

					switch(info.second.timeSense)
					{
						case 1: 						// positive unate
							{
								if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate!=-987654 &&
										info.second.delayRiseLate!=-987654 &&
										Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
										info.second.delayRiseLate > atRiseLate)
								{
									atRiseLate = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
													info.second.delayRiseLate;
									indRiseLate = ipinIndexList[k];
									RiseLateCause = RISE;
								}
								if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly!=987654 &&
										info.second.delayRiseEarly!=987654 &&
										Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
										info.second.delayRiseEarly < atRiseEarly)
								{
									atRiseEarly = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
													info.second.delayRiseEarly;
									indRiseEarly = ipinIndexList[k];
									RiseEarlyCause = RISE;
								}

								if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly!=987654 &&
										info.second.delayFallEarly!=987654 &&
										Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
										info.second.delayFallEarly < atFallEarly)
								{
									atFallEarly = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
													info.second.delayFallEarly;
									indFallEarly = ipinIndexList[k];

									FallEarlyCause = FALL;
								}

								if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate!=-987654 &&
										info.second.delayFallLate!=-987654 &&
										Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
										info.second.delayFallLate > atFallLate)
								{
									atFallLate = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
													info.second.delayFallLate;
									indFallLate = ipinIndexList[k];
									FallLateCause = FALL;
								}
								break;
							}
						case -1:  				// negative unate
							{
								if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate!=-987654 &&
										info.second.delayRiseLate!=-987654 &&
										(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
										 info.second.delayRiseLate) > atRiseLate)
								{
									atRiseLate = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
													info.second.delayRiseLate;
									indRiseLate = ipinIndexList[k];
									RiseLateCause = FALL;
								}

								if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly!=987654 &&
										info.second.delayRiseEarly!=987654 &&
										Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
										info.second.delayRiseEarly < atRiseEarly)
								{
									atRiseEarly =Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
													info.second.delayRiseEarly;
									indRiseEarly = ipinIndexList[k];
									RiseEarlyCause = FALL;
								}

								if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly!=987654 &&
										info.second.delayFallEarly!=987654 &&
										Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
										info.second.delayFallEarly < atFallEarly)
								{
									atFallEarly =Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
													info.second.delayFallEarly;
									indFallEarly = ipinIndexList[k];
									FallEarlyCause = RISE;
								}

								if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate!=-987654 &&
										info.second.delayFallLate!=-987654 &&
										Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
										info.second.delayFallLate > atFallLate)
								{
									atFallLate =Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
													info.second.delayFallLate;
									indFallLate = ipinIndexList[k];

									FallLateCause = RISE;
								}
								break;
							}
						case 2:  												// Rising edge
							{
								if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate!=-987654 &&
										info.second.delayRiseLate!=-987654 &&
										Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
										info.second.delayRiseLate > atRiseLate)
								{
									atRiseLate = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
													info.second.delayRiseLate;
									indRiseLate = ipinIndexList[k];
									RiseLateCause = RISE;
								}

								if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly!=987654 &&
										info.second.delayRiseEarly!=987654 &&
										Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
										info.second.delayRiseEarly < atRiseEarly)
								{
									atRiseEarly = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
													info.second.delayRiseEarly;
									indRiseEarly = ipinIndexList[k];
									RiseEarlyCause = RISE;
								}

								if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly!=987654 &&
										info.second.delayFallEarly!=987654 &&
										Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
										info.second.delayFallEarly < atFallEarly)
								{
									atFallEarly =Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
													info.second.delayFallEarly;
									indFallEarly = ipinIndexList[k];
									FallEarlyCause = RISE;
								}

								if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate!=-987654 &&
										info.second.delayFallLate!=-987654 &&
										Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
										info.second.delayFallLate > atFallLate)
								{
									atFallLate =Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
													info.second.delayFallLate;
									indFallLate = ipinIndexList[k];
									FallLateCause = RISE;
								}
								break;
							}
					case -2:  						// Falling edge
						{
							if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate!=-987654 &&
									info.second.delayRiseLate!=-987654 &&
									Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
									info.second.delayRiseLate > atRiseLate)
							{
								atRiseLate = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
												info.second.delayRiseLate;
								indRiseLate = ipinIndexList[k];
								RiseLateCause = FALL;
							}

							if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly!=987654 &&
									info.second.delayRiseEarly!=987654 &&
									Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
									info.second.delayRiseEarly < atRiseEarly)
							{
								atRiseEarly = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
												info.second.delayRiseEarly;
								indRiseEarly = ipinIndexList[k];
								RiseEarlyCause = FALL;
							}

							if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly!=987654 &&
									info.second.delayFallEarly!=987654 &&
									Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
									info.second.delayFallEarly < atFallEarly)
							{
								atFallEarly = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
												info.second.delayFallEarly;
								indFallEarly = ipinIndexList[k];
								FallEarlyCause = FALL;
							}

							if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate!=-987654 &&
									info.second.delayFallLate!=-987654 &&
									Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
									info.second.delayFallLate > atFallLate)
							{
								atFallLate = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
												info.second.delayFallLate;
								indFallLate = ipinIndexList[k];
								FallLateCause = FALL;
							}
							break;
						}
					case 3:
						{
							if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate!=-987654)
							{
								if(info.second.delayRiseLate!=-987654)
								{
									double64 dummy;
									if((dummy = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
											info.second.delayRiseLate) > atRiseLate)
									{
										atRiseLate = dummy;
										indRiseLate = ipinIndexList[k];
										RiseLateCause = RISE;

									}
								}

								if(info.second.delayFallLateNeg!=-987654)
								{
									double64 dummy;
									if((dummy = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseLate +
											info.second.delayFallLateNeg) > atFallLate)
									{
										atFallLate = dummy;
										indFallLate = ipinIndexList[k];
										FallLateCause = RISE;
									}
								}
							}

							if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate!=-987654)
							{
								if(info.second.delayRiseLateNeg!=-987654)
								{
									double64 dummy;
									if((dummy = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
											info.second.delayRiseLateNeg) > atRiseLate)
									{
										atRiseLate = dummy;
										indRiseLate = ipinIndexList[k];
										RiseLateCause = FALL;

									}
								}

								if(info.second.delayFallLate!=-987654)
								{
									double64 dummy;
									if((dummy = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallLate +
											info.second.delayFallLate) > atFallLate)
									{
										atFallLate = dummy;
										indFallLate = ipinIndexList[k];
										FallLateCause = FALL;
									}
								}
							}

							if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly!=987654)
							{
								if(info.second.delayRiseEarly!=987654)
								{
									double64 dummy;
									if((dummy = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
											info.second.delayRiseEarly)<atRiseEarly)
									{
										atRiseEarly = dummy;
										indRiseEarly = ipinIndexList[k];
										RiseEarlyCause = RISE;
									}
								}

								if(info.second.delayFallEarlyNeg!=987654)
								{
									double64 dummy;
									if((dummy = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atRiseEarly +
											info.second.delayFallEarlyNeg)<atFallEarly)
									{
										atFallEarly = dummy;
										indFallEarly = ipinIndexList[k];
										FallEarlyCause = RISE;
									}
								}
							}

							if(Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly!=987654)
							{
								if(info.second.delayFallEarly!=987654)
								{
									double64 dummy;
									if((dummy = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
											info.second.delayFallEarly)<atFallEarly)
									{
										atFallEarly = dummy;
										indFallEarly = ipinIndexList[k];
										FallEarlyCause = FALL;
									}
								}

								if(info.second.delayRiseEarlyNeg!=987654)
								{
									double64 dummy;
									if((dummy = Sort[i]->port.cellPtr->iipList[ipinIndexList[k]].tData.atFallEarly +
											info.second.delayRiseEarlyNeg)<atRiseEarly)
									{
										atRiseEarly = dummy;
										indRiseEarly = ipinIndexList[k];
										RiseEarlyCause = FALL;
									}
								}
							}
							break;
						}
					default:
							{
								// Do Nothing
							}
					}
				}
			}

			/* Updating criticality and timing data of the output pin */

			Sort[i]->port.pinPtr->tData.atFallEarly = atFallEarly;
			Sort[i]->port.pinPtr->tData.atFallLate  = atFallLate;
			Sort[i]->port.pinPtr->tData.atRiseEarly = atRiseEarly;
			Sort[i]->port.pinPtr->tData.atRiseLate  = atRiseLate;

			Sort[i]->port.pinPtr->tData.criticalAT.indexFallEarly = indFallEarly;
			Sort[i]->port.pinPtr->tData.criticalAT.indexFallLate  = indFallLate;
			Sort[i]->port.pinPtr->tData.criticalAT.indexRiseEarly = indRiseEarly;
			Sort[i]->port.pinPtr->tData.criticalAT.indexRiseLate  = indRiseLate;

			Sort[i]->port.pinPtr->tData.criticalAT.transitionRE = RiseEarlyCause;
			Sort[i]->port.pinPtr->tData.criticalAT.transitionRL = RiseLateCause;
			Sort[i]->port.pinPtr->tData.criticalAT.transitionFE = FallEarlyCause;
			Sort[i]->port.pinPtr->tData.criticalAT.transitionFL = FallLateCause;


			PARALLEL_DATA_OUT pdataOut;

			for (int j = 0; j < NUM_THREADS; j++)
			{
				Sort[i]->port.pinPtr->pData[j].atFallEarlyCPPR = atFallEarly;
				Sort[i]->port.pinPtr->pData[j].atFallLateCPPR = atFallLate;
				Sort[i]->port.pinPtr->pData[j].atRiseEarlyCPPR = atRiseEarly;
				Sort[i]->port.pinPtr->pData[j].atRiseLateCPPR = atRiseLate;

				Sort[i]->port.pinPtr->pData[j].criticalAT.indexFallEarly = indFallEarly;
				Sort[i]->port.pinPtr->pData[j].criticalAT.indexFallLate = indFallLate;
				Sort[i]->port.pinPtr->pData[j].criticalAT.indexRiseEarly = indRiseEarly;
				Sort[i]->port.pinPtr->pData[j].criticalAT.indexRiseLate = indRiseLate;

				Sort[i]->port.pinPtr->pData[j].criticalAT.transitionRE = RiseEarlyCause;
				Sort[i]->port.pinPtr->pData[j].criticalAT.transitionRL = RiseLateCause;
				Sort[i]->port.pinPtr->pData[j].criticalAT.transitionFE = FallEarlyCause;
				Sort[i]->port.pinPtr->pData[j].criticalAT.transitionFL = FallLateCause;
			}

			Sort[i]->port.pinPtr->tData.slewFallEarly = slewFallEarly;
			Sort[i]->port.pinPtr->tData.slewFallLate = slewFallLate;
			Sort[i]->port.pinPtr->tData.slewRiseEarly = slewRiseEarly;
			Sort[i]->port.pinPtr->tData.slewRiseLate = slewRiseLate;

			if(Sort[i]->isVirtualNet)
			{
				Sort[i]->port.pinPtr->tData.ratFallEarly = -987654;
				Sort[i]->port.pinPtr->tData.ratFallLate = 987654;
				Sort[i]->port.pinPtr->tData.ratRiseEarly = -987654;
				Sort[i]->port.pinPtr->tData.ratRiseLate = 987654;

				Sort[i]->port.pinPtr->tData.slackFallEarly = (Sort[i]->port.pinPtr->tData.atFallEarly == UNDEFINED_AT_EARLY || Sort[i]->port.pinPtr->tData.ratFallEarly == UNDEFINED_RAT_EARLY ) ?
											UNDEFINED_SLACK : (Sort[i]->port.pinPtr->tData.atFallEarly - Sort[i]->port.pinPtr->tData.ratFallEarly );
				Sort[i]->port.pinPtr->tData.slackRiseEarly = (Sort[i]->port.pinPtr->tData.atRiseEarly == UNDEFINED_AT_EARLY || Sort[i]->port.pinPtr->tData.ratRiseEarly == UNDEFINED_RAT_EARLY ) ?
											UNDEFINED_SLACK : (Sort[i]->port.pinPtr->tData.atRiseEarly - Sort[i]->port.pinPtr->tData.ratRiseEarly );
				Sort[i]->port.pinPtr->tData.slackFallLate = (Sort[i]->port.pinPtr->tData.atFallLate == UNDEFINED_AT_LATE || Sort[i]->port.pinPtr->tData.ratFallLate == UNDEFINED_RAT_LATE ) ?
											UNDEFINED_SLACK : (Sort[i]->port.pinPtr->tData.ratFallLate - Sort[i]->port.pinPtr->tData.atFallLate );
				Sort[i]->port.pinPtr->tData.slackRiseLate = (Sort[i]->port.pinPtr->tData.atRiseLate == UNDEFINED_AT_LATE || Sort[i]->port.pinPtr->tData.ratRiseLate == UNDEFINED_RAT_LATE ) ?
											UNDEFINED_SLACK : (Sort[i]->port.pinPtr->tData.ratRiseLate - Sort[i]->port.pinPtr->tData.atRiseLate );
			}

			if(ECM == true)
			{
				// Added for New Effective Capacitance
				InstanceOpin* oPin = Sort[i]->port.pinPtr;
				for (int x = 0; x < NUM_CEFF_ITR; x++)
				{
					if (oPin->ceffLate < 0)
					{
						cout << oPin->name.icellName << ":" << oPin->name.pinName << "\t" << oPin->ceffLate << endl;
						assert(0);
					}
					FindcPi(oPin);
					oPin->ceffLate = oPin->cPi;
					oPin->ceffEarly = oPin->cPi;
					oPin->port->ceffLate = oPin->cPi;
					oPin->port->ceffEarly = oPin->cPi;
					SetPortAT(Sort[i]);
				}
			}

		}


		for(int k=0;k< (int)Sort[i]->taps.size();k++) // k - tap control
		{
			Sort[i]->taps[k].pinPtr->tData.atFallEarly = Sort[i]->port.pinPtr->tData.atFallEarly!= 987654 ? Sort[i]->port.pinPtr->tData.atFallEarly + Sort[i]->taps[k].delayEarly:987654;
			Sort[i]->taps[k].pinPtr->tData.atFallLate = Sort[i]->port.pinPtr->tData.atFallLate != -987654 ? Sort[i]->port.pinPtr->tData.atFallLate + Sort[i]->taps[k].delayLate:-987654;
			Sort[i]->taps[k].pinPtr->tData.atRiseEarly = Sort[i]->port.pinPtr->tData.atRiseEarly != 987654 ? Sort[i]->port.pinPtr->tData.atRiseEarly + Sort[i]->taps[k].delayEarly:987654;
			Sort[i]->taps[k].pinPtr->tData.atRiseLate = Sort[i]->port.pinPtr->tData.atRiseLate != -987654 ? Sort[i]->port.pinPtr->tData.atRiseLate + Sort[i]->taps[k].delayLate:-987654;

			for (int j = 0; j < NUM_THREADS; j++)
			{
				Sort[i]->taps[k].pinPtr->pData[j].atFallEarlyCPPR = Sort[i]->taps[k].pinPtr->tData.atFallEarly;
				Sort[i]->taps[k].pinPtr->pData[j].atFallLateCPPR = Sort[i]->taps[k].pinPtr->tData.atFallLate;
				Sort[i]->taps[k].pinPtr->pData[j].atRiseEarlyCPPR = Sort[i]->taps[k].pinPtr->tData.atRiseEarly;
				Sort[i]->taps[k].pinPtr->pData[j].atRiseLateCPPR = Sort[i]->taps[k].pinPtr->tData.atRiseLate;
			}

			Sort[i]->taps[k].pinPtr->tData.slewFallEarly = Sort[i]->port.pinPtr->tData.slewFallEarly != 987654 ? ComputeSlew(Sort[i]->port.pinPtr->tData.slewFallEarly,Sort[i]->taps[k].m1_E,Sort[i]->taps[k].m2_E): 987654;
			Sort[i]->taps[k].pinPtr->tData.slewFallLate = Sort[i]->port.pinPtr->tData.slewFallLate != -987654 ? ComputeSlew(Sort[i]->port.pinPtr->tData.slewFallLate,Sort[i]->taps[k].m1_L,Sort[i]->taps[k].m2_L): -987654;
			Sort[i]->taps[k].pinPtr->tData.slewRiseEarly = Sort[i]->port.pinPtr->tData.slewRiseEarly != 987654 ? ComputeSlew(Sort[i]->port.pinPtr->tData.slewRiseEarly,Sort[i]->taps[k].m1_E,Sort[i]->taps[k].m2_E): 987654;
			Sort[i]->taps[k].pinPtr->tData.slewRiseLate = Sort[i]->port.pinPtr->tData.slewRiseLate != -987654 ? ComputeSlew(Sort[i]->port.pinPtr->tData.slewRiseLate,Sort[i]->taps[k].m1_L,Sort[i]->taps[k].m2_L): -987654;
		}
		if(Sort[i]->isCktreeMember && !Sort[i]->isVirtualNet &&Sort[i]->port.cellPtr->cellLibIndex_e!=-4)
		{
			switch(Sort[i]->port.pinPtr->dData[0].timeSense)
			{
				case 1:
				{
					if(Sort[i]->port.cellPtr->iipList[0].wire->port.parity == ODD)
						Sort[i]->port.parity = ODD;
					else if(Sort[i]->port.cellPtr->iipList[0].wire->port.parity == EVEN)
						Sort[i]->port.parity = EVEN;
					break;
				}
				case -1:
				{
					if(Sort[i]->port.cellPtr->iipList[0].wire->port.parity == ODD)
						Sort[i]->port.parity = EVEN;
					else if(Sort[i]->port.cellPtr->iipList[0].wire->port.parity == EVEN)
						Sort[i]->port.parity = ODD;
					break;
				}
				default:
				{
				}

			}
		}

		// Restoring lambda's
		for (int z = 0; z < (int)dData_backup.size() ; z++)
		{
			for (int l = 0; l < (int)Sort[i]->port.pinPtr->dData.size() ; l++)
			{
				if(dData_backup[z].index == Sort[i]->port.pinPtr->dData[l].index)
				{
					Sort[i]->port.pinPtr->dData[l].lambdaFL = dData_backup[z].lambdaFL;
					Sort[i]->port.pinPtr->dData[l].lambdaRL = dData_backup[z].lambdaRL;
					TrackSoln.totalLD += Sort[i]->port.pinPtr->dData[l].delayFallLate * Sort[i]->port.pinPtr->dData[l].lambdaFL ;
					TrackSoln.totalLD += Sort[i]->port.pinPtr->dData[l].delayRiseLate * Sort[i]->port.pinPtr->dData[l].lambdaRL ;
					break;
				}
			}
		}
	}
}

/**
 * \param si Slew at port pin of net
 * \param m1 First RC moment of concerned tap
 * \param m2 Second RC moment of concerned tap
 * \result double64 Slew at concerned tap
 *
 * Computes slew at a tap pin based on port pin slew and RC moments information
 */
double64  ForwardPropagate::ComputeSlew(double64 si, double64 m1, double64 m2)
{
	if (ISPD_2012 == true)
		return si;

	else if (SET_BETA == false)
		return sqrt(pow(si,2) + pow((2.1972245 * m1),2));

	else if (NEW_MI == true)
		return sqrt(pow(si,2)+pow(m2,2));

	else
	{
		if(m2 == 0)
		{
			assert(m1 ==0);
			return si;
		}
		////////////////////////SCALED_S2M///////////////////////
		if (SCALED_S2M == true)
			return sqrt(pow(si,2) + ((m1/sqrt(m2))*4.8278*(2*m2 - pow(m1,2))));

		////////////////////////TAU_SLEW///////////////////////
		else if (TAU_SLEW == true)
		{
			return sqrt(pow(si,2)+(2*m2)-pow(m1,2));
		}

		////////////////////////PERI///////////////////////
		else
			return sqrt(pow(si,2) + pow((2.1972245 * m1),2));
	}
}

/**
 * \param currGate Pointer to concerned gate
 * \param cellGate Pointer to cell information from .lib of concerned gate
 * \param output_load Effective output load capacitance
 * \param oPin Pointer to output pin information from .lib for concerned output pin
 * \param defaultSlewMode Decides whether input slew be taken as input slew at pin or DEFAULT MAX TRANSITION
 * \result double64 Returns max of output slew of the output pin for all modes and transition combinations
 *
 * Computes maximum output slew for an output pin to check for slew violations
 * Used only for iitSizer, not for timer alone
 */
double64 ForwardPropagate::ComputeSlew (Instance * currGate, LibParserCellInfo* cellGate,
										double64 output_load, LibParserPinInfo* oPin,
										bool defaultSlewMode)
{
	double64                      returnSlew = FORBIDDEN_LATE;
	double64                      tempSlew;
	vector  <LibParserTimingInfo>* tempArcLate;

	int arcIndexLate;

	arcIndexLate       = -1;
	tempArcLate        = &(cellGate->timingArcs);
	int timingArcSize  = cellGate->timingArcs.size();

	deque<int> indexLate;

	for(int num = 0; num < (int)cellGate->ipins.size(); num++)
	{
		NODE_TIMING_DATA * iPintData = NULL;

		for(int num2 = 0; num2 < (int)currGate->iipList.size(); num2++)
		{
			if(currGate->iipList[num2].name.pinName == cellGate->ipins[num].name)
			{
				iPintData = &currGate->iipList[num2].tData;
				break;
			}
		}

		for(int i = 0; i < timingArcSize; i++)
		{

			if(cellGate->ipins[num].hash==(*tempArcLate)[i].hashFromPin &&
					oPin->hash==(*tempArcLate)[i].hashToPin)
			{
				indexLate.push_back(i);
			}
		}

		if(/*indexEarly.size()==0 ||*/ indexLate.size() == 0)
			return FORBIDDEN_LATE;

		else if(indexLate.size() == 2)
		{
			// CHECKPOINT: check with this kind of benchmark (cadence) to see whether it works or not !
			// WARNING: Special case of non_unateness
			int posLate = 0, negLate = 0;

			if((*tempArcLate)[indexLate[0]].timingSense=="positive_unate")
			{
				posLate=indexLate[0];
				negLate=indexLate[1];
			}
			else if((*tempArcLate)[indexLate[0]].timingSense=="negative_unate")
			{
				posLate=indexLate[1];
				negLate=indexLate[0];
			}

			if(defaultSlewMode == true)
			{
				double64 a = EvaluateLUT(false,MaxSlew,output_load,(*tempArcLate)[posLate].fallTransition);
				double64 b = EvaluateLUT(false,MaxSlew,output_load,(*tempArcLate)[negLate].fallTransition);
				tempSlew   = a > b ? a : b ;
				returnSlew = std::max(tempSlew,returnSlew);

				a          = EvaluateLUT(false,MaxSlew,output_load,(*tempArcLate)[negLate].riseTransition);
				b          = EvaluateLUT(false,MaxSlew,output_load,(*tempArcLate)[posLate].riseTransition);
				tempSlew   = a > b ? a : b ;
				returnSlew = std::max(tempSlew,returnSlew);
			}
			else
			{
				double64 a = EvaluateLUT(false,iPintData->slewFallLate,output_load,(*tempArcLate)[posLate].fallTransition);
				double64 b = EvaluateLUT(false,iPintData->slewRiseLate,output_load,(*tempArcLate)[negLate].fallTransition);
				tempSlew   = a > b ? a : b ;
				returnSlew = std::max(tempSlew,returnSlew);

				a          = EvaluateLUT(false,iPintData->slewFallLate,output_load,(*tempArcLate)[negLate].riseTransition);
				b          = EvaluateLUT(false,iPintData->slewRiseLate,output_load,(*tempArcLate)[posLate].riseTransition);
				tempSlew   = a > b ? a : b ;
				returnSlew = std::max(tempSlew,returnSlew);
			}
				return returnSlew;
		}

		arcIndexLate= indexLate[0];
		if(defaultSlewMode == false)
		{
			if((*tempArcLate)[arcIndexLate].timingSense=="positive_unate")
			{
				//TODO: check for the tables are not empty , and also check the order of variable_1,variable_2
					returnSlew = std::max(returnSlew, EvaluateLUT (false,iPintData->slewFallLate,output_load,(*tempArcLate)[arcIndexLate].fallTransition));
					returnSlew = std::max(returnSlew, EvaluateLUT (false,iPintData->slewRiseLate,output_load,(*tempArcLate)[arcIndexLate].riseTransition));
			}
			else if((*tempArcLate)[arcIndexLate].timingSense=="negative_unate")
			{
					returnSlew = std::max(returnSlew, EvaluateLUT(false,iPintData->slewRiseLate,output_load,(*tempArcLate)[arcIndexLate].fallTransition));
					returnSlew = std::max(returnSlew, EvaluateLUT(false,iPintData->slewFallLate,output_load,(*tempArcLate)[arcIndexLate].riseTransition));

			}
			else
			{
				if((*tempArcLate)[arcIndexLate].timingType=="rising_edge")
				{
						returnSlew = std::max(returnSlew, EvaluateLUT(false,iPintData->slewRiseLate,output_load,(*tempArcLate)[arcIndexLate].fallTransition));
						returnSlew = std::max(returnSlew, EvaluateLUT(false,iPintData->slewRiseLate,output_load,(*tempArcLate)[arcIndexLate].riseTransition));

				}
				else if((*tempArcLate)[arcIndexLate].timingType=="falling_edge")
				{
						returnSlew = std::max(returnSlew, EvaluateLUT(false,iPintData->slewFallLate,output_load,(*tempArcLate)[arcIndexLate].fallTransition));
						returnSlew = std::max(returnSlew, EvaluateLUT(false,iPintData->slewFallLate,output_load,(*tempArcLate)[arcIndexLate].riseTransition));

				}
				else if((*tempArcLate)[arcIndexLate].timingType=="combinational") //TODO: what to do if combinational is not specified in timingType ??
				{

						double a   = EvaluateLUT(false,iPintData->slewFallLate,output_load, (*tempArcLate)[arcIndexLate].fallTransition);
						double b   = EvaluateLUT(false,iPintData->slewRiseLate,output_load, (*tempArcLate)[arcIndexLate].fallTransition);
						tempSlew   = a > b ? a : b ;
						returnSlew = std::max(tempSlew,returnSlew);

						a          = EvaluateLUT(false,iPintData->slewFallLate,output_load, (*tempArcLate)[arcIndexLate].riseTransition);
						b          = EvaluateLUT(false,iPintData->slewRiseLate,output_load, (*tempArcLate)[arcIndexLate].riseTransition);
						tempSlew   = a > b ? a : b ;
						returnSlew = std::max(tempSlew,returnSlew);
				}
			 }
		}
		else  // default transition mode is on (true) - use MaxSlew
		{
			if((*tempArcLate)[arcIndexLate].timingSense=="positive_unate")
			{
				//TODO: check for the tables are not empty , and also check the order of variable_1,variable_2
				returnSlew = std::max(returnSlew, EvaluateLUT (false,MaxSlew,output_load,(*tempArcLate)[arcIndexLate].fallTransition));
				returnSlew = std::max(returnSlew, EvaluateLUT (false,MaxSlew,output_load,(*tempArcLate)[arcIndexLate].riseTransition));
			}
			else if((*tempArcLate)[arcIndexLate].timingSense=="negative_unate")
			{
				returnSlew = std::max(returnSlew, EvaluateLUT(false,MaxSlew,output_load,(*tempArcLate)[arcIndexLate].fallTransition));
				returnSlew = std::max(returnSlew, EvaluateLUT(false,MaxSlew,output_load,(*tempArcLate)[arcIndexLate].riseTransition));
			}
			else
			{
				if((*tempArcLate)[arcIndexLate].timingType=="rising_edge")
				{
					returnSlew = std::max(returnSlew, EvaluateLUT(false,MaxSlew,output_load,(*tempArcLate)[arcIndexLate].fallTransition));
					returnSlew = std::max(returnSlew, EvaluateLUT(false,MaxSlew,output_load,(*tempArcLate)[arcIndexLate].riseTransition));
				}
				else if((*tempArcLate)[arcIndexLate].timingType=="falling_edge")
				{
					returnSlew = std::max(returnSlew, EvaluateLUT(false,MaxSlew,output_load,(*tempArcLate)[arcIndexLate].fallTransition));
					returnSlew = std::max(returnSlew, EvaluateLUT(false,MaxSlew,output_load,(*tempArcLate)[arcIndexLate].riseTransition));
				}
				else if((*tempArcLate)[arcIndexLate].timingType=="combinational") //TODO: what to do if combinational is not specified in timingType ??
				{
					double a   = EvaluateLUT(false,MaxSlew,output_load, (*tempArcLate)[arcIndexLate].fallTransition);
					double b   = EvaluateLUT(false,MaxSlew,output_load, (*tempArcLate)[arcIndexLate].fallTransition);
					tempSlew   = a > b ? a : b ;
					returnSlew = std::max(tempSlew,returnSlew);

					a          = EvaluateLUT(false,MaxSlew,output_load, (*tempArcLate)[arcIndexLate].riseTransition);
					b          = EvaluateLUT(false,MaxSlew,output_load, (*tempArcLate)[arcIndexLate].riseTransition);
					tempSlew   = a > b ? a : b ;
					returnSlew = std::max(tempSlew,returnSlew);
				}
			 }
		}
	}
	return returnSlew;
}

/**
 * Topologically sorts the nets in the design and stores it in Sort
 */
void ForwardPropagate::TopSort(void)
{
	Init_L();
	while(!(L.empty()))
	{
		SpefNet* wirePtr = L.front();
		L.pop();
		wirePtr->port.pinPtr->visitCount = 0;
		wirePtr->isRemoved = false;
		Sort.push_back(wirePtr);
		for(int64 i=0;i< (int64)wirePtr->taps.size();i++)
		{
			if(wirePtr->taps[i].pinPtr->isClk) // testing for sequential elements
			{
				for(int64 j =0;j< (int64)wirePtr->taps[i].cellPtr->iopList.size();j++)
				{
					wirePtr->taps[i].cellPtr->isCkTreeMember = true;
					if(!wirePtr->taps[i].cellPtr->iopList[j].isConnected) continue;
					if((wirePtr->taps[i].cellPtr->iopList[j].isConnected==true) &&
							!(wirePtr->taps[i].cellPtr->iopList[j].wire->isRemoved))
					{
						wirePtr->taps[i].cellPtr->numVisOut++;
						wirePtr->taps[i].cellPtr->iopList[j].wire->level = (wirePtr->level) + 1;
						wirePtr->taps[i].cellPtr->iopList[j].wire->isRemoved = true;
						L.push(wirePtr->taps[i].cellPtr->iopList[j].wire);

						if(wirePtr->taps[i].cellPtr->numVisOut == wirePtr->taps[i].cellPtr->outDegree)
						{
							wirePtr->taps[i].cellPtr->numVisOut=0;
							for(int64 m =0;m< (int64)wirePtr->taps[i].cellPtr->iopList.size();m++)
							{
								if(wirePtr->taps[i].cellPtr->iopList[m].wire->isVirtualNet)
								{
									wirePtr->taps[i].cellPtr->iopList[m].wire->isRemoved = true;
									Sort.push_back(wirePtr->taps[i].cellPtr->iopList[m].wire);
								}
							}

						}
					}
				}
			}
			else
			{
				if(wirePtr->taps[i].cellPtr->cellLibIndex_l!=-3)
				{
					for(int64 j=0;j<(int64)wirePtr->taps[i].pinPtr->relPinIndex.size();j++)
					{
						if(!(wirePtr->taps[i].cellPtr->info_late->isSequential))
						{
							vector<int> buffer = wirePtr->taps[i].pinPtr->relPinIndex;
							wirePtr->taps[i].cellPtr->iopList[buffer[j]].visitCount++;
							if(wirePtr->taps[i].cellPtr->iopList[buffer[j]].visitCount ==
									(int)wirePtr->taps[i].cellPtr->iopList[buffer[j]].relPinIndex.size())
							{
								if((wirePtr->port.cellPtr->cellLibIndex_l !=-2 &&
										wirePtr->port.cellPtr->cellLibIndex_l !=-4  ))
								{
									if(!(wirePtr->port.cellPtr->info_late->isSequential))
									{
										wirePtr->taps[i].cellPtr->isCkTreeMember =
												          wirePtr->port.cellPtr->isCkTreeMember;
									}
								}

								if((wirePtr->taps[i].cellPtr->iopList[buffer[j]].isConnected==true) &&
										!(wirePtr->taps[i].cellPtr->iopList[buffer[j]].wire->isRemoved))
								{
									wirePtr->taps[i].cellPtr->numVisOut++;
									wirePtr->taps[i].cellPtr->iopList[buffer[j]].wire->level = wirePtr->level + 1;
									wirePtr->taps[i].cellPtr->iopList[buffer[j]].wire->isRemoved = true;
									if (wirePtr->isCktreeMember)
										wirePtr->taps[i].cellPtr->iopList[buffer[j]].wire->isCktreeMember = true;
									L.push(wirePtr->taps[i].cellPtr->iopList[buffer[j]].wire);
									if(wirePtr->taps[i].cellPtr->numVisOut == wirePtr->taps[i].cellPtr->outDegree)
									{
										wirePtr->taps[i].cellPtr->numVisOut=0;
										for(int64 m =0;m< (int64)wirePtr->taps[i].cellPtr->iopList.size();m++)
										{
											if(wirePtr->taps[i].cellPtr->iopList[m].wire->isVirtualNet)
											{
												wirePtr->taps[i].cellPtr->iopList[m].wire->isRemoved = true;
												Sort.push_back(wirePtr->taps[i].cellPtr->iopList[m].wire);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

/**
 * Initializes list L with all nets connected to PI and Clock source
 */
void ForwardPropagate:: Init_L()
{
	if (CLKSRC != NULL)
	{
		if(CLKSRC->iopList.size() != 0 && CLKSRC->iopList[0].isConnected)
		CLKSRC->iopList[0].wire->isCktreeMember = true;
	}
	for(int64 i=0;i< (int64)PIList.size();i++)
		if (PIList[i]->iopList.size() != 0 && PIList[i]->iopList[0].isConnected)
			L.push(PIList[i]->iopList[0].wire);
}

/**
 * Resets all flags and counters related to all nets in the design
 * Clears away list Sort
 */
void ForwardPropagate:: ReClear()
{
	////////////////////////////////////////////////////////////////////////////
	// WARNING:Check if Needed for RAT as well as AT for incremental , must keep it !
	///////////////////////////////////////////////////////////////////////////
	Sort.clear();

	return ;
	for(int64 i = 0; i < (int64)SpefNetList.size(); i++)
	{
		if(SpefNetList[i].isIncluded==false)
			continue;

		SpefNetList[i].isRemoved = false;
		SpefNetList[i].isCktreeMember = false;
		SpefNetList[i].level = 1;
		SpefNetList[i].port.pinPtr->visitCount = 0;
		SpefNetList[i].port.pinPtr->dData.clear();
	}
}

/**
 * Topologically sorts the incrementally modified nets in the design and stores it in Sort
 */
void ForwardPropagate::TopSortInc()
{
	while(!InitNetSort.empty())
	{
		SpefNet* wirePtr = InitNetSort.front();
		InitNetSort.pop();
		Sort.push_back(wirePtr);
		for(int i = 0; i < (int)wirePtr->taps.size(); i++)
		{
			if((wirePtr->taps[i].pinPtr == NULL))
				continue;
			if(!wirePtr->taps[i].pinPtr->isConnected)
				continue;
			if(wirePtr->taps[i].pinPtr->isClk)				 // testing for clk pins
			{
				for(int j = 0; j < (int)wirePtr->taps[i].cellPtr->iopList.size(); j++)
				{
					if(wirePtr->taps[i].cellPtr->iopList[j].wire->isRemoved)
						continue;

					wirePtr->taps[i].cellPtr->iopList[j].wire->isRemoved = true;
					if(!(wirePtr->taps[i].cellPtr->iopList[j].isConnected))
						Sort.push_back(wirePtr->taps[i].cellPtr->iopList[j].wire);
					else
					{
						wirePtr->taps[i].cellPtr->iopList[j].wire->level = wirePtr->level + 1;
						InitNetSort.push(wirePtr->taps[i].cellPtr->iopList[j].wire);
					}
				}
			}

			else
			{
				if(wirePtr->taps[i].cellPtr->cellLibIndex_l != -3)
				{
					vector<int> obuffer = wirePtr->taps[i].pinPtr->relPinIndex;
					for(int j = 0; j < (int)obuffer.size(); j++)
					{
						if(!(wirePtr->taps[i].cellPtr->iopList[obuffer[j]].isIncRelatedSet))
						{
							vector<int> ibuffer = wirePtr->taps[i].cellPtr->iopList[obuffer[j]].relPinIndex;
							for(int m = 0; m < (int)ibuffer.size(); m++)
							{
								if(wirePtr->taps[i].cellPtr->iipList[ibuffer[m]].isConnected &&
										wirePtr->taps[i].cellPtr->iipList[ibuffer[m]].wire->isIncremental) //Implies BFSInc cannot be optimized
								{
									wirePtr->taps[i].cellPtr->iopList[obuffer[j]].incRelatedCount++;
								}
							}
							wirePtr->taps[i].cellPtr->iopList[obuffer[j]].isIncRelatedSet = true;
						}

						wirePtr->taps[i].cellPtr->iopList[obuffer[j]].visitCount++;

						if(wirePtr->taps[i].cellPtr->iopList[obuffer[j]].visitCount ==
								(int)wirePtr->taps[i].cellPtr->iopList[obuffer[j]].incRelatedCount)
						{

							wirePtr->taps[i].cellPtr->iopList[obuffer[j]].wire->isRemoved = true;
							wirePtr->taps[i].cellPtr->iopList[obuffer[j]].visitCount = 0;           //clearing visitcount
							wirePtr->taps[i].cellPtr->iopList[obuffer[j]].isIncRelatedSet = false;
							wirePtr->taps[i].cellPtr->iopList[obuffer[j]].incRelatedCount = 0;      //clearing values

							if(wirePtr->taps[i].cellPtr->iopList[obuffer[j]].wire->isVirtualNet)
							{
								Sort.push_back(wirePtr->taps[i].cellPtr->iopList[obuffer[j]].wire);
							}

							else
							{
								size_t level = FindMaxLevel(wirePtr->taps[i].cellPtr->iopList[obuffer[j]].wire);
								wirePtr->taps[i].cellPtr->iopList[obuffer[j]].wire->level = level+1;
								InitNetSort.push(wirePtr->taps[i].cellPtr->iopList[obuffer[j]].wire);
							}
						}
					}
				}
			}
		}
	}
}

/**
 * \param wirePtr Pointer to net whose port pin cell needs to be checked for max topological level
 * \result long_int Returns the maximum topological level among all nets connected to related input pins of an output port pin
 *
 * Finds the maximum topological level among all nets connected to related input pins of an output port pin
 */
long int ForwardPropagate:: FindMaxLevel(SpefNet* wirePtr)
{
   if(wirePtr->port.cellPtr->cellLibIndex_l==-2 || wirePtr->port.cellPtr->cellLibIndex_l==-4)
	   return 0;

	size_t level = 0;
	vector<int> ibuffer = wirePtr->port.pinPtr->relPinIndex;

	for(int i=0;i< (int)ibuffer.size();i++)
	{
		if(wirePtr->port.cellPtr->iipList[ibuffer[i]].isConnected &&
				(wirePtr->port.cellPtr->iipList[ibuffer[i]].wire->level >= (int64)level))
		{
			level = wirePtr->port.cellPtr->iipList[ibuffer[i]].wire->level;
		}
	}
	return level;
}

};
