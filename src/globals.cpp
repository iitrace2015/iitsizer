/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#include "include.h"
#include "globals.h"

namespace iit{

// List of global variables used
// For Timer come Sizer

CelllibStorage      celllibstorage_e;                  ///< Early mode cell library storage
CelllibStorage      celllibstorage_l;                  ///< Late mode cell library storage
LibFileInfo         libinfo_e;                         ///< Early mode units and LUT template info of library
LibFileInfo         libinfo_l;                         ///< Late mode units and LUT template info of library
CompleteTimingInfo  timing;                            ///< Slew, AT, RAT assertions storage from .sdc/.timing file

Instance*     CLKSRC;                                  ///< Pointer to the clock source instance
int           numInc;                                  ///< Number of incremental operations
int           Verbose;                                 ///< Flag for verbose printing/debugging/logging
TimeIt        g_Timer;                                 ///< Variable to keep global timer
TimeIt        Var_TimeIt[NUM_FUNCTIONS];               ///< Array of runtime-keeping variables for functions
string        Func_Names[NUM_FUNCTIONS];               ///< Array of all function names
double64      Func_Total_T[NUM_FUNCTIONS];             ///< Array of all function-runtimes
int64         Func_Total_C[NUM_FUNCTIONS];             ///< Array of number of calls to functions
int64         MaxNumPaths;                             ///< Max number of paths to report among all the queries
int64         PathFlag_G;                              ///< Global color count for non-repetitive values throughout
string        BenchmarkDir;                            ///< Path to the benchmark directory that is being run
bool         _isStaticDone;                            ///< Flag for whether STA is done

int64         SkipCnt;                                 ///< Count of pins pruned in CPPR back-traversal
int64         NoSkipCnt;                               ///< Count of un-pruned pins in CPPR back-traversal
double64      MaxSlew;                                 ///< Default max slew constraint from cell library


deque   < Instance >       InstanceList;               ///< Set of gate instances in the design
vector  < Hash_Instance >  InstanceHashTable;          ///< Hash-table for gates in InstanceList
deque   < SpefNet >        SpefNetList;                ///< Set of net instances in the design
vector  < Hash_Instance >  spefNetHashTable;           ///< Hash-table for nets in SpefNetList
vector  < SpefNet* >       Sort;                       ///< Set of net-pointers ordered in increasing topological levels
vector  < SpefNet* >       IncNetList;                 ///< Set of incremental nets
deque   < SpefNet >        WireList;                   ///< Set of nets stored with their names initially in Verilog parser
queue   < SpefNet* >       InitNetList;                ///< Set of net-pointers to initiate incremental timing from
vector  < Instance* >      PIList;                     ///< Set of pointers to primary input instances
vector  < Instance* >      POList;                     ///< Set of pointers to primary output instances
vector  < InstanceIpin* >  ConeEndPoints;              ///< Set of pointers to end-point pins
deque   < PATH_TABLE_ADV > pathLUT_Adv;                ///< Set of top 'N' worst paths- a PathTable
deque   < Slack_Instance > pathLUTSlackTable_Adv;      ///< Set of paths form PathTable (pathLUT_Adv) sorted in slack criticality
std::tr1::unordered_map <char,bool> specCharLibDir;    ///< Delimiters for line splitting into tokens

// For sizer
FPLibStorage      FPLibstorage_e;                      ///< Footprint-wise information storage for cells - early mode
FPLibStorage      FPLibstorage_l;                      ///< Footprint-wise information storage for cells - late mode
SolutionState     TrackSoln;                           ///< Global tracker of circuit state- leakage, TNS, worst slack etc.

};


