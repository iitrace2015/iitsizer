/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#include "include.h"
#include "lib.h"
#include "timer.h"
#include "backward_propagate.h"
#include "utility.h"
#include "globals.h"

namespace iit{

/* Global variables declaration --------------------------------------- */

//extern CelllibStorage      celllibstorage_e;
//extern CelllibStorage      celllibstorage_l;
//extern LibFileInfo         libinfo_e;
//extern LibFileInfo         libinfo_l;
//extern CompleteTimingInfo  timing;

//extern Instance* CLKSRC;
//extern int       numInc;
//extern int       Verbose;
//extern int64     MaxNumPaths;
//extern int64     PathFlag_G;
//extern string    BenchmarkDir;
//extern bool     _isStaticDone;
extern double64  MaxSlew;

//extern deque   < Instance >       InstanceList;
//extern vector  < Hash_Instance >  InstanceHashTable;
//extern deque   < SpefNet >        SpefNetList;
//extern vector  < Hash_Instance >  spefNetHashTable;
//extern vector  < SpefNet* >       Sort;
//extern vector  < SpefNet* >       IncNetList;
//extern deque   < SpefNet >        WireList;
//extern queue   < SpefNet* >       InitNetList;
//extern vector  < Instance* >      PIList;
//extern vector  < Instance* >      POList;
//extern vector  < InstanceIpin* >  ConeEndPoints;
//extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
//extern deque   < Slack_Instance > pathLUTSlackTable_Adv;

extern std::tr1::unordered_map <char,bool> specCharLibDir;

/* End - global variables declaration -----------------------------------*/

/**
 * \param lut Instance of 2D array for reading values in
 * \param dimension Table name
 * \param inTokens Tokens from first line of reading LUT
 *
 * Reads in a single complete LUT for a given cell from cell library
 */
void LibParser::_begin_read_lut (LibParserLUT& lut,string dimension, deque <string>& inTokens) {

  std::deque<string> tokens ;
  tokens.clear();
  streampos pos;
  for(int i=2;i< (int)inTokens.size();i++)
	  if(inTokens[i]!="{" && inTokens[i]!="}")
		  tokens.push_back(inTokens[i]);

  pos = is.tellg();

  bool isCorrupt = false;
  int count = 0;

  // To detect whether there are any 3 dimensional LUTs
  while(1)
  {
	  count++;
	  if(tokens.size() > 0 && tokens[0] == "index_3")
	  {
		  isCorrupt = true; // Corrupted ! Can not support 3 dimensional LUT
		  is.seekg(pos);
		  break;
	  }
	  else if((tokens.size() > 0 && tokens[0] == "values") || count > 5)
	  {
		  is.seekg(pos);
		  break;
	  }
	  read_line_as_tokens(is, tokens);

  }

  //CHECKPOINT: make parsing robust so that it can take care of "{" appearing at the beginning of line containing valid data.

  int iteration = 3;
  int size1 = 7,size2 = 8;
  bool var1 = CAPACITANCE,var2 = SLEW;
  lut.tableName = dimension;

  //checking for units:
  if(isCorrupt == false)
  {
	  for(int k=0;k<(int)_libInfo->lut_templates.size();k++)
	  {
		  if(_libInfo->lut_templates[k].name==dimension)
		  {
			 if(_libInfo->lut_templates[k].variable1=="total_output_net_capacitance")
				 var1 = CAPACITANCE;
			 else
				 var1 = SLEW;

			 if(_libInfo->lut_templates[k].variable2=="total_output_net_capacitance")
				 var2 = CAPACITANCE;
			 else
				 var2 = SLEW;
			 break;
		  }

	  }
  }

  if(dimension == "scalar")
  {
	  iteration = 1;
  }

  for(int z = 0; z < iteration; z++)
  {
	  if(z == 0)
	  {
		  if(inTokens.size() <= 3)
			  read_line_as_tokens (is, tokens);
	  }
	  else
		  {
		  	  read_line_as_tokens (is, tokens) ;
		  	  if( tokens[0] == "index_3")
		  		  read_line_as_tokens(is, tokens);
		  }

	  if(tokens[0] == "index_1")
	  {
		  size1 = tokens.size()-1 ;
		  lut.index_1.resize(size1) ;
		  for (int i = 0; i < size1; ++i)
		  {
			  lut.index_1[i] = var1==CAPACITANCE ? _capScalingFac* std::atof(tokens[i+1].c_str()) : _slewScalingFac*std::atof(tokens[i+1].c_str()) ;
		  }
	  }

	  else if(tokens[0]=="index_2")
	  {
		  size2 = tokens.size()-1 ;
		  lut.index_2.resize(size2) ;
		  for (int i=0; i < size2; ++i)
		  {
			lut.index_2[i] = var2==CAPACITANCE ? _capScalingFac* std::atof(tokens[i+1].c_str()) : _slewScalingFac * std::atof(tokens[i+1].c_str()) ;
		  }
	  }
	  else if(tokens[0]=="values")
	  {
		  if(dimension!="scalar")
		  {
			  lut.tableVals.resize(size1) ;
			  for (int i=0 ; i < (int)lut.index_1.size(); ++i)
			  {
				read_line_as_tokens (is, tokens) ;
				lut.tableVals[i].resize(size2) ;
				for (int j=0; j < (int)lut.index_2.size(); ++j)
				{
					if(isCorrupt == false)
						lut.tableVals[i][j] = _timeScalingFac* std::atof(tokens[j].c_str()) ;
					else
						lut.tableVals[i][j] = 0;
				}
			  }
		  }
		  else
		  {
			  size1 = 1;
			  size2 = 1;
			  lut.index_1.resize(size1);
			  lut.index_1[0] = 0;
			  lut.index_2.resize(size2) ;
			  lut.index_2[0] = 0;
			  lut.tableVals.resize(size1);
			  lut.tableVals[0].resize(size2);
			  lut.tableVals[0][0] = _timeScalingFac* std::atof(tokens[1].c_str());
		  }
	  }

  }

}

/**
 * \param toPin Timing arc sink-pin
 * \param toPinHash Hash of the sink pin-name
 * \param timing Complete timing arc information to be written
 *
 * Reads in complete timing information on a timing arc from cell library
 */
bool LibParser::_begin_read_timing_info (string toPin, int64 toPinHash, LibParserTimingInfo& timing)
{
	bool isCorrupt       = false;
	timing.toPin         = toPin ;
	timing.hashToPin     =toPinHash;

	std::deque<string> tokens ;
	while (1)
	{
		read_line_as_tokens (is, tokens) ;

		if (tokens[0] == "cell_fall")
		{
			_begin_read_lut (timing.fallDelay,tokens[1],tokens) ;

		} else if (tokens[0] == "cell_rise")
		{
			_begin_read_lut (timing.riseDelay,tokens[1],tokens) ;

		} else if (tokens[0] == "fall_transition")
		{
			_begin_read_lut (timing.fallTransition,tokens[1],tokens) ;

		} else if (tokens[0] == "rise_transition")
		{
			_begin_read_lut (timing.riseTransition,tokens[1],tokens) ;

		} else if (tokens[0] == "fall_constraint")
		{

			_begin_read_lut (timing.fallConstraint,tokens[1],tokens) ;

		} else if (tokens[0] == "rise_constraint")
		{

			_begin_read_lut(timing.riseConstraint,tokens[1],tokens) ;

		} else if (tokens[0] == "timing_sense")
		{
		  timing.timingSense = tokens[1] ;

		} else if (tokens[0] == "related_pin")
		{

			timing.fromPin     = tokens[1] ;
			timing.hashFromPin = ComputeHash(tokens[1]);

		} else if(tokens[0]=="timing_type")
		{
			timing.timingType=tokens[1];

			if(lib_parsing_mode == LATE)
			{
			  if(timing.timingType == "hold_rising" || timing.timingType == "hold_falling")
				  isCorrupt = true;
			}
			if(lib_parsing_mode == EARLY)
			{
			  if(timing.timingType == "setup_rising" || timing.timingType == "setup_falling")
				  isCorrupt = true;
			}

		}
		if(_bracket_cnt == 3) break;
	}

	return isCorrupt;
}


/**
 * \param pinName Name of the pin for reading its info
 * \param cell Current cell of the pin
 * \param pin Pin-information structure instance for reading multiple attributes of the same
 *
 * Reads in complete information for a pin of cell
 */
void LibParser::_begin_read_pin_info (string pinName, LibParserCellInfo& cell, LibParserPinInfo& pin)
{

	pin.name           = pinName ;
	pin.isClock        = false ;
	pin.maxCapacitance = std::numeric_limits<double64>::max() ;
	pin.minCapacitance = 0 ;
	pin.hash           = ComputeHash(pinName);

	std::deque<string> tokens ;
	while (1)
	{
		read_line_as_tokens (is, tokens) ;

		if (tokens[0] == "direction")
		{

			if (tokens[1] == "input")
			{
				pin.isInput = true ;
				cell.numipins++;
			}
			else if (tokens[1] == "output")
			{
			  pin.isInput = false ;
			  cell.numopins++;
			}

		} else if (tokens[0] == "capacitance")
		{
			pin.capacitance = std::atof(tokens[1].c_str())*_capScalingFac ;
		} else if (tokens[0] == "max_capacitance")
		{
			pin.maxCapacitance = std::atof(tokens[1].c_str())*_capScalingFac ;
		} else if (tokens[0] == "min_capacitance")
		{

			pin.minCapacitance = std::atof(tokens[1].c_str())*_capScalingFac ;

		} else if (tokens[0] == "timing")
		{

			cell.timingArcs.push_back(LibParserTimingInfo()) ; // add an empty TimingInfo object
			bool isCorrupt = _begin_read_timing_info(pinName, pin.hash, cell.timingArcs.back()) ; // pass the empty object to the function to be filled

			if(isCorrupt == true)
				cell.timingArcs.pop_back();
		} else if (tokens[0] == "clock")
		{
			if(tokens[1]=="true")
			{
				pin.isClock       = true ;
				cell.numipins     = cell.numipins-1;
				cell.isSequential = true;
			}

		}

		if(_bracket_cnt==2) break;
	}

	pin.isUsed=true;

	if(pin.isClock == true)
	{
		cell.clkpin.push_back(pin);
		RelatedPinTable temphashtable;
		temphashtable.hash  = pin.hash;
		temphashtable.index = 0;
		cell.clkToIpinhashtable.push_back(temphashtable);
		cell.clkToOpinhashtable.push_back(temphashtable);
	}
	else if(pin.isInput == false)
	{
		cell.opins.push_back(pin);
		RelatedPinTable temphashtable;
		temphashtable.hash  = pin.hash;
		temphashtable.index = cell.opins.size()-1;
		cell.opinhashtable.push_back(temphashtable);
	}
	else
	{
		cell.ipins.push_back(pin);
		RelatedPinTable temphashtable;
		temphashtable.hash  = pin.hash;
		temphashtable.index = cell.ipins.size()-1;
		cell.ipinhashtable.push_back(temphashtable);
	}
return;
}


/**
 * \param cellName Name of the cell
 * \param cell Instance of cell information storage class
 *
 * Starts reading complete cell info for the given cell
 */
void LibParser::_begin_read_cell_info (string cellName, LibParserCellInfo& cell)
{

	cell.name         = cellName ;
	cell.hash         = ComputeHash(cellName);
	cell.isSequential = false ;

	std::deque<string> tokens ;
	bool valid = read_line_as_tokens (is, tokens) ;
	while (valid) {

	if(tokens[0]=="cell_leakage_power")
	{
		cell.leakage = atof(tokens[1].c_str());
	}
	else if(tokens[0]=="area")
	{
		cell.area    = atof(tokens[1].c_str());
	}
	else if(tokens[0]=="cell_footprint")
	{
		cell.footprint = tokens[1];
		cell.fphash    = ComputeHash(tokens[1]);
	}
	if (tokens[0] == "pin")
	{

	  LibParserPinInfo temp;                           // add empty PinInfo object
	  _begin_read_pin_info (tokens[1], cell,temp) ;    // pass the new PinInfo object to be filled

	}
	if(_bracket_cnt==1) break;          //it's a curly bracket count to know when the particular definition is over.
		valid = read_line_as_tokens (is, tokens) ;
	}

	std::sort (cell.ipinhashtable.begin(), cell.ipinhashtable.end(), CompareByHashnum<RelatedPinTable>());
	std::sort (cell.opinhashtable.begin(),cell.opinhashtable.end(), CompareByHashnum<RelatedPinTable>());

	int sizeOfTimingArc = cell.timingArcs.size();
	int64 clkhash;

	if(cell.isSequential==true)
		clkhash=cell.clkpin[0].hash;
	else
		clkhash=0;

	for (int i=0;i<sizeOfTimingArc;i++)
	{
		int64 hashfrompin = cell.timingArcs[i].hashFromPin;
		int64 hashtopin   = cell.timingArcs[i].hashToPin;
		if(clkhash!=hashfrompin && clkhash!=hashtopin)
		{
			int is_frompin_output = Bsearch(cell.opinhashtable,hashfrompin,0,cell.opinhashtable.size()-1);

			if(is_frompin_output == -1) // that means, frompin is an input pin
			{
				int index_hashToPin    = Bsearch(cell.opinhashtable,hashtopin,0,cell.opinhashtable.size()-1);
				int index_hashFromPin  = Bsearch(cell.ipinhashtable,hashfrompin,0,cell.ipinhashtable.size()-1);
				cell.ipinhashtable[index_hashFromPin].relPinHash.push_back(hashtopin);
				cell.opinhashtable[index_hashToPin].relPinHash.push_back(hashfrompin);
			}
			else       // means, frompin is an output pin.
			{
				int index_hashToPin = Bsearch(cell.ipinhashtable,hashtopin,0,cell.ipinhashtable.size()-1);
				cell.ipinhashtable[index_hashToPin].relPinHash.push_back(hashfrompin);
				cell.opinhashtable[is_frompin_output].relPinHash.push_back(hashtopin);
			}

		}
		else if(clkhash == hashfrompin)
		{
			int is_toPin_output = Bsearch(cell.opinhashtable,hashtopin,0,cell.opinhashtable.size()-1);
			if(is_toPin_output == -1)   //topin is an input
			{
				cell.clkToIpinhashtable[0].relPinHash.push_back(hashtopin);
			}
			else  //topin is an output
			{
				cell.clkToOpinhashtable[0].relPinHash.push_back(hashtopin);
			}
		}


	}

	// Storing index of a pin in sorted pinhashtable

	int size1 = cell.ipinhashtable.size();
	int size2 = cell.opinhashtable.size();

	for(int i=0;i<size1;i++)
	{
		//WARNING: make sure in all Bsearch call everywhere, you are sending last argument as ' deque.size()-1 '
		cell.ipins[cell.ipinhashtable[i].index].index = i;
	}
	for(int i=0;i<size2;i++)
	{
		cell.opins[cell.opinhashtable[i].index].index = i;
	}
	if(cell.isSequential)
		cell.clkpin[0].index = CLKPININDEX;


}

/**
 * \param is Input file stream
 * \param tokens Set of tokens read and split from a line
 * \param includeSpecialChars Flag for including special characters/delimiters as a part of the token name
 * \return Flag for line reading was successful or failed
 *
 * Reads a line from the current position of input file stream
 */
bool LibParser::read_line_as_tokens (istream& is, deque<string>& tokens, bool includeSpecialChars ) {

  tokens.clear() ;

  string line ;
  std::getline (is, line) ;

  while (is && tokens.empty())
  {

		string token = "" ;

		for (int i=0; i < (int)line.size(); ++i)
		{
			char currChar = line[i] ;
			if (currChar=='{')
			{
			  _bracket_cnt=_bracket_cnt+1;
			}
			else if(currChar=='}')
			{
			  _bracket_cnt=_bracket_cnt-1;
			}
			bool isSpecialChar = specCharLibDir[currChar] ;

			if (std::isspace (currChar) || isSpecialChar)
			{

				if (!token.empty())
				{
				  tokens.push_back(token) ;
				  token.clear() ;
				}

				if (includeSpecialChars && isSpecialChar)
				{
				  tokens.push_back(string(1, currChar)) ;
				}

			}
			else
			{
				token.push_back(currChar) ;
			}

		}

		if (!token.empty())
		  tokens.push_back(token) ;


		if (tokens.empty())
		  std::getline (is, line) ;
  }

  return !tokens.empty() ;
}

/**
 * \param libinfo Instance of LibFileInfo class for writing liberty file information
 *
 * Reads in time, capacitance and voltage units, operating conditions and so on. This is a generic information on
 * library and not relating to any specific cell as such
 */
void LibParser::_begin_read_library_info( LibFileInfo* libinfo)
{   int count=0;
	std::deque<string> tokens;
	bool finishedReading = false;
	libinfo->cap_unit.resize(2);
	libinfo->cap_unit[0] = string("1");
	libinfo->cap_unit[1] = string("ff");
	libinfo->time_unit   = string("1ps");
	libinfo->res_unit    = string("1kohm");
	libinfo->vol_unit    = string("1V");
	std::streampos linePos;

	while(!finishedReading)
	{
		linePos= is.tellg();
		read_line_as_tokens(is, tokens);

		if(tokens[0]=="time_unit")
		{
			if(tokens.size()>=2)
				libinfo->time_unit = tokens[1];
			count++;

		}
		else if(tokens[0]=="voltage_unit")
		{
			if(tokens.size()>=2)
				libinfo->vol_unit = tokens[1];
			count++;

		}
		else if(tokens[0]=="capacitive_load_unit")
		{
			if(tokens.size()>=3){
				libinfo->cap_unit.resize(2);
				libinfo->cap_unit[0] = tokens[1];
				libinfo->cap_unit[1] = tokens[2];
			}

			count++;

		}
		else if(tokens[0]=="pulling_resistance_unit")
		{
			if(tokens.size()>=2)
				libinfo->res_unit = tokens[1];
			count++;
		}
		else if(tokens[0]=="operating_conditions" || tokens[0]=="default_operating_conditions")
			finishedReading=true;
		else if(tokens[0]=="lu_table_template")
		{
			finishedReading=true;
			is.seekg(linePos);
		}
		if(count==4)
			finishedReading= true;

	}
}

/**
 * \param LutName Name of the look-up table
 * \param libinfo Instance of LibFileInfo class for writing liberty file information
 *
 * Reads in the template definitions of various look-up tables from cell liberty
 */
void LibParser::_begin_read_LUT_template(string LutName,  LibFileInfo* libinfo)
{
	int readcount = 0;
	LibLutTemplate lut;
    lut.name      = LutName;
    deque<string> tokens;

    while(readcount < 2)
    {
    	read_line_as_tokens(is,tokens);

    	if(tokens[0]=="variable_1")
    	{
    		lut.variable1 = tokens[1];
    		readcount++;
    	}
    	else if(tokens[0]=="variable_2")
    	{
    		lut.variable2 = tokens[1];
    		readcount++;
    	}
    }

    	libinfo->lut_templates.push_back(lut);
}

/**
 * \param libinfo Instance of LibFileInfo class for writing liberty file information
 *
 * Begin reading-in the template definitions of various look-up tables from cell liberty
 */
bool LibParser::read_LUT_template( LibFileInfo* libinfo)
{
	int count = 0;
	deque<string> tokens;

	bool valid = read_line_as_tokens(is,tokens);

	while(valid)
	{
		if(tokens.size() == 2 && tokens[0] == "lu_table_template")
		{
			_begin_read_LUT_template(tokens[1], libinfo);
			count++;
		}
		if(_bracket_cnt==1)
			break;

		valid=read_line_as_tokens(is,tokens);

	}
	if(count > 0)
		return true;
	else
		return false;
}

/**
 * \param libinfo Instance of LibFileInfo class for writing liberty file information
 *
 * Begin reading-in in time, capacitance and voltage units, operating conditions and so on. This is a generic information on
 * library and not relating to any specific cell as such. Also reads-in the default max transition allowed
 */
bool LibParser::read_library_info( LibFileInfo* libinfo)
{
	deque<string> tokens ;
	int cnt       = 0;
	int targetCnt = 2;
	bool valid    = read_line_as_tokens (is, tokens) ;

	while(valid)
	{
		if(tokens.size() >= 2 && tokens[0] == "library")
		{
			_begin_read_library_info(libinfo);
			cnt++;
			if (!SIZER)	return true;
		}

		if(tokens.size() >= 2 && tokens[0]=="default_max_transition")
		{
			MaxSlew = std::atof(tokens[1].c_str());
			cnt++;
		}

		if(cnt == targetCnt) return true;
	  valid = read_line_as_tokens(is,tokens);
	}
	return false;
}

/**
 * Closes the liberty file that was opened for parsing using the associated file stream
 */
void LibParser:: _close(void)
{
	free(is);
	is.close();
	return;
}

/**
 * \param tempcelllib Pointer to cell library storage
 * \param libinfo Instance of LibFileInfo class for writing liberty file information
 *
 * Reads complete cell information iteratively and LUT templates
 */
void LibParser::read_cell_info(CelllibStorage* tempcelllib, LibFileInfo* libinfo)
{

	bool valid;

	TempHashTable temphashtable;
	int readCnt = 0 ;

	deque<string> tokens;

	int count = 0;

	valid = read_line_as_tokens(is,tokens);
	while(valid)
	{
		if(tokens.size()>=2 && tokens[0]=="lu_table_template")
		{
			_begin_read_LUT_template(tokens[1], libinfo);
			count++;
		}
		if(tokens.size()>=2 && tokens[0]=="cell")
		{
			break;
		}
		valid = read_line_as_tokens(is,tokens);

		_libInfo = libinfo;
	}

	valid=true;

	do
	{

		LibParserCellInfo cell ;
		  while (valid)
		  {

			if (tokens.size() >= 2 && tokens[0] == "cell")
			{
				_begin_read_cell_info (tokens[1], cell) ;
			   break;
			}
			valid = read_line_as_tokens (is, tokens) ;
		  }

		if (valid)
		{
			  ++readCnt ;
			  (tempcelllib->celllib).push_back(cell);
			  temphashtable.hash  = cell.hash;
			  temphashtable.index = tempcelllib->celllib.size()-1;
			  tempcelllib->cellhashtable.push_back(temphashtable);

		}
		valid = read_line_as_tokens (is, tokens) ;

	} while (valid) ;

	std::sort (tempcelllib->cellhashtable.begin(), tempcelllib->cellhashtable.end(), CompareByHashnum<TempHashTable>());

	_close();

}

/**
 * \param filename Liberty file name
 * \param tempcelllib Pointer to cell library storage
 * \param libinfo Instance of LibFileInfo class for writing liberty file information
 * \param mode Early/Late parsing mode specifier
 *
 * Fires-off liberty file parsing
 */
void Timer:: test_lib_parser(string filename, CelllibStorage* tempcelllib, LibFileInfo* libinfo, int mode)
{
	LibParser lp (filename);
	if(mode == EARLY)
		lp.lib_parsing_mode = EARLY;
	else if(mode == LATE)
		lp.lib_parsing_mode = LATE;

	lp.init_lib_parser(filename,tempcelllib, libinfo);
}

/**
 * \param filename Liberty file name
 * \param tempcelllib Pointer to cell library storage
 * \param libinfo Instance of LibFileInfo class for writing liberty file information
 *
 * Initiates liberty file parsing, sets appropriate capacitance, time and voltage units
 */

void LibParser::init_lib_parser (string filename, CelllibStorage* tempcelllib, LibFileInfo* libinfo)
{

	_bracket_cnt=0;
	read_library_info(libinfo);

	_capScalingFac= 1.0 ;_timeScalingFac=1.0 ;_slewScalingFac=1.0 ;

	_capScalingFac = _capScalingFac* std::atof(libinfo->cap_unit[0].c_str());
	if(libinfo->cap_unit[1]=="pf" || libinfo->cap_unit[1]=="PF" || libinfo->cap_unit[1]=="pF")
	  _capScalingFac = _capScalingFac* 1000;
	else if(libinfo->cap_unit[1]=="nf" || libinfo->cap_unit[1]=="NF" || libinfo->cap_unit[1]=="nF")
	  _capScalingFac = _capScalingFac* 1000000;
	else if(libinfo->cap_unit[1]=="uf" || libinfo->cap_unit[1]=="UF" || libinfo->cap_unit[1]=="uF")
		  _capScalingFac = _capScalingFac* 1000000000;

	if(libinfo->time_unit=="1fs"      || libinfo->time_unit=="1FS" || libinfo->time_unit=="1fS" )
	  _timeScalingFac = 0.001;
	else if(libinfo->time_unit=="1ns" || libinfo->time_unit=="1NS" || libinfo->time_unit=="1nS" )
	  _timeScalingFac = 1000;
	else if(libinfo->time_unit=="1us" || libinfo->time_unit=="1US" || libinfo->time_unit=="1uS" )
	  _timeScalingFac = 1000000;
	else if(libinfo->time_unit=="1ms" || libinfo->time_unit=="1uS" || libinfo->time_unit=="1mS" )
	  _timeScalingFac = 1000000000;

	if(libinfo->vol_unit=="1V"       || libinfo->vol_unit=="1v")
	  _slewScalingFac = 1.0*1/_timeScalingFac;
	else if(libinfo->vol_unit=="1KV" || libinfo->vol_unit=="1kv" || libinfo->vol_unit=="1kV")
	  _slewScalingFac = 1.0*1000/_timeScalingFac;
	else if(libinfo->vol_unit=="1mV" || libinfo->vol_unit=="1mv")
	  _slewScalingFac = 1.0*0.001/_timeScalingFac;
	else if(libinfo->vol_unit=="1uV" || libinfo->vol_unit=="1uv")
	  _slewScalingFac = 1.0/(1000000*_timeScalingFac);

	read_cell_info(tempcelllib,libinfo);

}
};


