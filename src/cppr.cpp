/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#include "include.h"
#include "cppr.h"
#include "container.h"
#include "timer.h"
#include "globals.h"
#include <omp.h>

namespace iit
{

/* Global variables declaration --------------------------------------- */

//extern CelllibStorage      celllibstorage_e;
//extern CelllibStorage      celllibstorage_l;
//extern LibFileInfo         libinfo_e;
//extern LibFileInfo         libinfo_l;
//extern CompleteTimingInfo  timing;

extern Instance* CLKSRC;
//extern int       numInc;
//extern int       Verbose;
extern int64     MaxNumPaths;
extern int64     PathFlag_G;
//extern string    BenchmarkDir;
//extern bool     _isStaticDone;

//extern deque   < Instance >       InstanceList;
//extern vector  < Hash_Instance >  InstanceHashTable;
extern deque   < SpefNet >        SpefNetList;
extern vector  < Hash_Instance >  spefNetHashTable;
//extern vector  < SpefNet* >       Sort;
//extern vector  < SpefNet* >       IncNetList;
//extern deque   < SpefNet >        WireList;
//extern queue   < SpefNet* >       InitNetList;
//extern vector  < Instance* >      PIList;
//extern vector  < Instance* >      POList;
extern vector  < InstanceIpin* >  ConeEndPoints;
extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
extern deque   < Slack_Instance > pathLUTSlackTable_Adv;

//extern std::tr1::unordered_map <char,bool> specCharLibDir;

/* End - global variables declaration -----------------------------------*/

/**
 * Compare function to sort nets according to hash
 */
struct CompareWireHash_Adv
{
	inline bool operator() (const SpefNet* x, const SpefNet* y)
	{
		return (x->hash < y->hash);
	}
} compareWireHash_Adv;

/**
 * Compare function to sort nets according to their levels (Topological order)
 */
struct CompareNetLevel_Adv
{
  inline bool operator() (const SpefNet* x, const SpefNet* y)
  {
	  return (x->level > y->level);
  }
}compareNetLevel_Adv;

/**
 * Constructor for CPPR class
 */
CPPR::CPPR()
{
	_CPPRflag = 0;
	backwardPropagate_ptr = NULL;
}

/**
 * Builds Sparse Table for storing logarithmic ancestors of ports in clock tree
 * Sparse Table is needed to make getLCA faster in O(log n) time
 */
void CPPR::buildSparseTable()
{
	SparseTable.clear();
	int64 N = 0; 									// Number of clock tree members

	// We initialize every element in ST with NULL
	for (int i = 0; i < (int)spefNetHashTable.size(); i++)
	{
	  SPARSE_TABLE_NODE stNode;
	  int index = spefNetHashTable[i].index;
	  if(SpefNetList[index].isCktreeMember != true)
		  continue;
	  N++;
	  // The first ancestor of every node i is T[i]
	  stNode.iPort = &SpefNetList[index].port;
	  if(stNode.iPort->pinPtr->cellPtr->cellLibIndex_e ==-4)
		  stNode.anc.push_back(NULL);  				// Clock source reached
	  else
		  stNode.anc.push_back(&stNode.iPort->pinPtr->cellPtr->iipList[0].wire->port);
	  SparseTable.push_back(stNode);
	  SpefNetList[index].port.stIndex = SparseTable.size()-1;
	}

	// Bottom up Dynamic Programming
	SpefPort* tempPort;
	int tempcnt=0;
	for (int j = 1; (1 << j) < N; j++)
	{
		tempcnt ++;
		for (int i = 0; i < N; i++)
		{
			if(SparseTable[i].iPort->pinPtr->cellPtr->cellLibIndex_e !=-4)
			{
				if((int64)SparseTable[i].anc.size()>j-1  && SparseTable[i].anc[j - 1] !=NULL
					 && (int64)SparseTable[SparseTable[i].anc[j - 1]->stIndex].anc.size()>j-1)
				{
				 tempPort = SparseTable[SparseTable[i].anc[j - 1]->stIndex].anc[j - 1];
				 SparseTable[i].anc.push_back(tempPort);
				}
			}
		}
	}
}

/**
 * \param pathFlag Contains PATH_FLAG data of concerned input pin
 * \param mode Mode (early/late)
 * \param trans Transition (rise/fall)
 * \param threadID ID of concerned thread (for multithreaded block based CPPR)
 * \param hash Flag to search for in pathFlag
 * \result bool true if flag matches in pathFlag, else false
 *
 * Searches for hash in pathFlag of associated threadID, mode and transition
 * Needed for path extraction after CPPR
 */
bool CPPR::searchFlag_Adv (PATH_FLAG pathFlag, bool mode, bool trans, int threadID, int64 hash)
{
	if (mode == EARLY)
	{
		if (trans == RISE)
		{
			if (pathFlag.flagRE[threadID] == hash)
				return true;
			else
				return false;
		}
		else
		{
			if (pathFlag.flagFE[threadID] == hash)
				return true;
			else
				return false;
		}
	}
	else
	{
		if (trans == RISE)
		{
			if (pathFlag.flagRL[threadID] == hash)
				return true;
			else
				return false;
		}
		else
		{
			if (pathFlag.flagFL[threadID] == hash)
				return true;
			else
				return false;
		}
	}
	return false;
}

/**
 * \param p Port before clock pin of Sequential cell 1
 * \param q Port before clock pin of Sequential cell 2
 * \result SpefPort* Pointer to lowest common ancestor port between p & q
 *
 * Finds the Lowest Common Ancestor between a launching and capturing flip-flop
 */
SpefPort* CPPR::getLCA_Adv (SpefPort* p, SpefPort* q)
{
    int64 log, i;
    SpefPort* tmp;

	// If p is situated on a lower level than q then we swap them
	// If (L[p] < L[q])
    if (p->pinPtr->wire->level < q->pinPtr->wire->level)
    {
    	tmp = p;
    	p = q;
    	q = tmp;
    }

    // We compute the value of [log(L[p)]
    for (log = 1; 1 << log <= p->pinPtr->wire->level; log++);

    // Reduce by one
    log--;

	// We find the ancestor of node p situated on the same level
	// with q using the values in P
    for (i = log; i >= 0; i--)
        if (p->pinPtr->wire->level - (1 << i) >= q->pinPtr->wire->level)
            p = SparseTable[p->stIndex].anc[i];

    if (p == q)
        return p;

	// We compute LCA(p, q) using the values in P
	//    for (i = log; i >= 0; i--)
	//        if (P[p][i] != -1 && P[p][i] != P[q][i])
	//            p = P[p][i], q = P[q][i];

    for (i = log; i >= 0; i--)
    {
        if ((int64)SparseTable[p->stIndex].anc.size() > i && SparseTable[p->stIndex].anc[i] != NULL
        		&& SparseTable[p->stIndex].anc[i] != SparseTable[q->stIndex].anc[i])
        {
        	p = SparseTable[p->stIndex].anc[i];
        	q = SparseTable[q->stIndex].anc[i];
        }
    }

    return &(p->cellPtr->iipList[0].wire->port);
}

/**
 * \param cpPort LCA common point port
 * \param credit Credit information for concerned launching FF (sent by reference)
 * \param ckParity Parity of transition at launching FF
 * \param timeSense Rising or Falling time sense of launching FF
 * \param Mode (early/late)
 *
 * Finds credit for a launching FF
 */
void CPPR::getCredit_Adv (SpefPort* cpPort, Credit& credit, bool ckParity, int timeSense, bool mode)
{
	InstanceOpin* cpPin = cpPort->pinPtr;
	if (mode == EARLY)
	{
		credit.creditFallEarly = cpPin->tData.atFallLate - cpPin->tData.atFallEarly;
		credit.creditRiseEarly = cpPin->tData.atRiseLate - cpPin->tData.atRiseEarly;
		if (ckParity == cpPort->parity)
		{
			if (timeSense == RISING_EDGE)
				credit.creditFallEarly = credit.creditRiseEarly;
			else
				credit.creditRiseEarly = credit.creditFallEarly;
		}
		else
		{
			if (timeSense == RISING_EDGE)
				credit.creditRiseEarly = credit.creditFallEarly;
			else
				credit.creditFallEarly = credit.creditRiseEarly;
		}
	}
	else
	{
		if (cpPort->parity == false)
		{
			credit.creditFallLate = cpPin->tData.atFallLate - cpPin->tData.atFallEarly - (CLKSRC->iopList[0].tData.atFallLate - CLKSRC->iopList[0].tData.atFallEarly);
			credit.creditRiseLate = cpPin->tData.atRiseLate - cpPin->tData.atRiseEarly - (CLKSRC->iopList[0].tData.atRiseLate - CLKSRC->iopList[0].tData.atRiseEarly);
		}
		else
		{
			credit.creditFallLate = cpPin->tData.atFallLate - cpPin->tData.atFallEarly - (CLKSRC->iopList[0].tData.atRiseLate - CLKSRC->iopList[0].tData.atRiseEarly);
			credit.creditRiseLate = cpPin->tData.atRiseLate - cpPin->tData.atRiseEarly - (CLKSRC->iopList[0].tData.atFallLate - CLKSRC->iopList[0].tData.atFallEarly);
		}
		if (ckParity == cpPort->parity)
		{
			if (timeSense == RISING_EDGE)
				credit.creditFallLate = credit.creditRiseLate;
			else
				credit.creditRiseLate = credit.creditFallLate;
		}
		else
		{
			if (timeSense == RISING_EDGE)
				credit.creditRiseLate = credit.creditFallLate;
			else
				credit.creditFallLate = credit.creditRiseLate;
		}
	}
}

/**
 * \param currCredit Credit information for concerned launching FF
 * \param oPin Output pin of launching FF to which the credit will be added
 * \param pdi ID of thread (for multithreading)
 * \param mode Mode (early/late)
 *
 * Adds credit for a launching FF to its output pin
 */
void CPPR::addCredit_Adv (Credit currCredit, InstanceOpin* oPin, int pdi, bool mode)
{
	if (mode == EARLY)
	{
		oPin->pData[pdi].atFallEarlyCPPR = (oPin->tData.atFallEarly == UNDEFINED_AT_EARLY) ? UNDEFINED_AT_EARLY : (oPin->tData.atFallEarly + currCredit.creditFallEarly);
		oPin->pData[pdi].atRiseEarlyCPPR = (oPin->tData.atRiseEarly == UNDEFINED_AT_EARLY) ? UNDEFINED_AT_EARLY : (oPin->tData.atRiseEarly + currCredit.creditRiseEarly);
	}
	else
	{
		oPin->pData[pdi].atFallLateCPPR = (oPin->tData.atFallLate == UNDEFINED_AT_LATE) ? UNDEFINED_AT_LATE : (oPin->tData.atFallLate - currCredit.creditFallLate);
		oPin->pData[pdi].atRiseLateCPPR = (oPin->tData.atRiseLate == UNDEFINED_AT_LATE) ? UNDEFINED_AT_LATE : (oPin->tData.atRiseLate - currCredit.creditRiseLate);
	}
}

/**
 * \param tap Tap to which fake AT is to be propagated
 * \param oPin Output pin from where fake AT is to be propagated
 * \param pdi ID of thread (for multithreading)
 * \param mode Mode (early/late)
 *
 * Propagates fake AT (e.g. atRiseLateCPPR) from a port pin to a tap pin
 */
void CPPR::slackUpdateIpin_Adv(SpefTap* tap, InstanceOpin* oPin, int pdi, bool mode)
{
	InstanceIpin* iPin;
	iPin = tap->pinPtr;

	if (mode == EARLY)
	{
		iPin->pData[pdi].atFallEarlyCPPR = (oPin->pData[pdi].atFallEarlyCPPR == UNDEFINED_AT_EARLY) ? UNDEFINED_AT_EARLY : (oPin->pData[pdi].atFallEarlyCPPR + tap->delayEarly);
		iPin->pData[pdi].atRiseEarlyCPPR = (oPin->pData[pdi].atRiseEarlyCPPR == UNDEFINED_AT_EARLY) ? UNDEFINED_AT_EARLY : (oPin->pData[pdi].atRiseEarlyCPPR + tap->delayEarly);
	}
	else
	{
		iPin->pData[pdi].atFallLateCPPR = (oPin->pData[pdi].atFallLateCPPR == UNDEFINED_AT_LATE) ? UNDEFINED_AT_LATE : (oPin->pData[pdi].atFallLateCPPR + tap->delayLate);
		iPin->pData[pdi].atRiseLateCPPR = (oPin->pData[pdi].atRiseLateCPPR == UNDEFINED_AT_LATE) ? UNDEFINED_AT_LATE : (oPin->pData[pdi].atRiseLateCPPR + tap->delayLate);
	}
}

/**
 * \param oPin Output pin to which fake AT is to be propagated
 * \param pdi ID of thread (for multithreading)
 * \param DpinHash Hash of name of CEP of concerned cone
 * \param mode Mode (early/late)
 *
 * Propagates fake AT (e.g. atRiseLateCPPR) from input pins of a cell to its output pin
 */
void CPPR::slackUpdateOpin_Adv(InstanceOpin* oPin, int pdi, int64 DpinHash, bool mode)
{
		int numIpins = oPin->relPinIndex.size();

		double temp[2];											//0-> FallEarly, 1-> RiseEarly, 2-> FallLate, 3-> RiseLate
		bool tempTransition[2];

		for (int i = 0; i < numIpins; i++)
		{
			Instance* cell = oPin->cellPtr;
			InstanceIpin* iPin = &(cell->iipList[oPin->dData[i].index]);
			DELAY_DATA dData = oPin->dData[i];

			//Default Initialization
			temp[0] = UNDEFINED_AT_EARLY;
			temp[1] = UNDEFINED_AT_EARLY;
			tempTransition[0] = FALL;
			tempTransition[1] = RISE;

			if (mode == EARLY)
			{
				switch (oPin->dData[i].timeSense)
				{
					case POSITIVE_UNATE:
						if (iPin->pData[pdi].atFallEarlyCPPR != UNDEFINED_AT_EARLY)
						{
							temp[0] =  (dData.delayFallEarly == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayFallEarly + iPin->pData[pdi].atFallEarlyCPPR);
							tempTransition[0] = FALL;
						}

						if (iPin->pData[pdi].atRiseEarlyCPPR != UNDEFINED_AT_EARLY)
						{
							temp[1] = (dData.delayRiseEarly == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayRiseEarly + iPin->pData[pdi].atRiseEarlyCPPR);
							tempTransition[1] = RISE;
						}
						break;

					case NEGATIVE_UNATE:
						if (iPin->pData[pdi].atRiseEarlyCPPR != UNDEFINED_AT_EARLY)
						{
							temp[0] =  (dData.delayFallEarly == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayFallEarly + iPin->pData[pdi].atRiseEarlyCPPR);
							tempTransition[0] = RISE;
						}

						if (iPin->pData[pdi].atFallEarlyCPPR != UNDEFINED_AT_EARLY)
						{
							temp[1] = (dData.delayRiseEarly == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayRiseEarly + iPin->pData[pdi].atFallEarlyCPPR);
							tempTransition[1] = FALL;
						}
						break;

					case RISING_EDGE:
						if (iPin->pData[pdi].atRiseEarlyCPPR != UNDEFINED_AT_EARLY)
						{
							temp[0] =  (dData.delayFallEarly == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayFallEarly + iPin->pData[pdi].atRiseEarlyCPPR);
							tempTransition[0] = RISE;

							temp[1] = (dData.delayRiseEarly == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayRiseEarly + iPin->pData[pdi].atRiseEarlyCPPR);
							tempTransition[1] = RISE;
						}
						break;

					case FALLING_EDGE:
						if (iPin->pData[pdi].atFallEarlyCPPR != UNDEFINED_AT_EARLY)
						{
							temp[0] =  (dData.delayFallEarly == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayFallEarly + iPin->pData[pdi].atFallEarlyCPPR);
							tempTransition[0] = FALL;

							temp[1] = (dData.delayRiseEarly == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayRiseEarly + iPin->pData[pdi].atFallEarlyCPPR);
							tempTransition[1] = FALL;
						}
						break;

					case NEITHER_EDGE:
						if (iPin->pData[pdi].atFallEarlyCPPR == UNDEFINED_AT_EARLY && iPin->pData[pdi].atRiseEarlyCPPR != UNDEFINED_AT_EARLY)
						{
							temp[0] =  (dData.delayFallEarlyNeg == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayFallEarlyNeg + iPin->pData[pdi].atRiseEarlyCPPR);
							tempTransition[0] = RISE;

							temp[1] = (dData.delayRiseEarly == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayRiseEarly + iPin->pData[pdi].atRiseEarlyCPPR);
							tempTransition[1] = RISE;

						}
						else if (iPin->pData[pdi].atFallEarlyCPPR != UNDEFINED_AT_EARLY && iPin->pData[pdi].atRiseEarlyCPPR == UNDEFINED_AT_EARLY)
						{
							temp[0] =  (dData.delayFallEarly == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayFallEarly + iPin->pData[pdi].atFallEarlyCPPR);
							tempTransition[0] = FALL;

							temp[1] = (dData.delayRiseEarlyNeg == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayRiseEarlyNeg + iPin->pData[pdi].atFallEarlyCPPR);
							tempTransition[1] = FALL;

						}
						else if (iPin->pData[pdi].atFallEarlyCPPR != UNDEFINED_AT_EARLY && iPin->pData[pdi].atRiseEarlyCPPR != UNDEFINED_AT_EARLY)
						{
							double a,b;
								a = (dData.delayFallEarly == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayFallEarly + iPin->pData[pdi].atFallEarlyCPPR);
								b= (dData.delayFallEarlyNeg == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayFallEarlyNeg + iPin->pData[pdi].atRiseEarlyCPPR);
								temp[0] = min(a,b);
								tempTransition[0] = a < b ? FALL : RISE;

								a= (dData.delayRiseEarlyNeg == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayRiseEarlyNeg + iPin->pData[pdi].atFallEarlyCPPR);
								b = (dData.delayRiseEarly == FORBIDDEN) ? UNDEFINED_AT_EARLY : (dData.delayRiseEarly + iPin->pData[pdi].atRiseEarlyCPPR);
								temp[1] = min(a,b);
								tempTransition[1] = a < b ? FALL : RISE;
						}
						break;

					default:
						continue;
						break;
				}
				if (i == 0)
				{
					oPin->pData[pdi].atFallEarlyCPPR = temp[0];
					oPin->pData[pdi].criticalAT.indexFallEarly = oPin->dData[i].index;
					oPin->pData[pdi].criticalAT.transitionFE = tempTransition[0];

					oPin->pData[pdi].atRiseEarlyCPPR = temp[1];
					oPin->pData[pdi].criticalAT.indexRiseEarly = oPin->dData[i].index;
					oPin->pData[pdi].criticalAT.transitionRE = tempTransition[1];
				}
				else
				{
					if (oPin->pData[pdi].atFallEarlyCPPR > temp[0])
					{
						oPin->pData[pdi].atFallEarlyCPPR = temp[0];
						oPin->pData[pdi].criticalAT.indexFallEarly = oPin->dData[i].index;
						oPin->pData[pdi].criticalAT.transitionFE = tempTransition[0];
					}
					if (oPin->pData[pdi].atRiseEarlyCPPR > temp[1])
					{
						oPin->pData[pdi].atRiseEarlyCPPR = temp[1];
						oPin->pData[pdi].criticalAT.indexRiseEarly = oPin->dData[i].index;
						oPin->pData[pdi].criticalAT.transitionRE = tempTransition[1];
					}
				}
			}
			else
			{
				switch (oPin->dData[i].timeSense)
				{
					case POSITIVE_UNATE:
						if (iPin->pData[pdi].atFallLateCPPR != UNDEFINED_AT_LATE)
						{
							temp[0] = (dData.delayFallLate == NFORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayFallLate + iPin->pData[pdi].atFallLateCPPR);
							tempTransition[0] = FALL;
						}
						if (iPin->pData[pdi].atRiseLateCPPR != UNDEFINED_AT_LATE)
						{
							temp[1] = (dData.delayRiseLate == NFORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayRiseLate + iPin->pData[pdi].atRiseLateCPPR);
							tempTransition[1] = RISE;
						}
						break;

					case NEGATIVE_UNATE:
						if (iPin->pData[pdi].atRiseLateCPPR != UNDEFINED_AT_LATE)
						{
							temp[0] = (dData.delayFallLate == NFORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayFallLate + iPin->pData[pdi].atRiseLateCPPR);
							tempTransition[0] = RISE;
						}
						if (iPin->pData[pdi].atFallLateCPPR != UNDEFINED_AT_LATE)
						{
							temp[1] = (dData.delayRiseLate == NFORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayRiseLate + iPin->pData[pdi].atFallLateCPPR);
							tempTransition[1] = FALL;
						}
						break;

					case RISING_EDGE:
						if (iPin->pData[pdi].atRiseLateCPPR != UNDEFINED_AT_LATE)
						{
							temp[0] = (dData.delayFallLate == NFORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayFallLate + iPin->pData[pdi].atRiseLateCPPR);
							tempTransition[0] = RISE;
							temp[1] = (dData.delayRiseLate == NFORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayRiseLate + iPin->pData[pdi].atRiseLateCPPR);
							tempTransition[1] = RISE;
						}
						break;

					case FALLING_EDGE:
						if (iPin->pData[pdi].atFallLateCPPR != UNDEFINED_AT_LATE)
						{
							temp[0] = (dData.delayFallLate == NFORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayFallLate + iPin->pData[pdi].atFallLateCPPR);
							tempTransition[0] = FALL;
							temp[1] = (dData.delayRiseLate == NFORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayRiseLate + iPin->pData[pdi].atFallLateCPPR);
							tempTransition[1] = FALL;
						}
						break;

					case NEITHER_EDGE:
						if (iPin->pData[pdi].atFallLateCPPR == UNDEFINED_AT_LATE && iPin->pData[pdi].atRiseLateCPPR != UNDEFINED_AT_LATE)
						{
							temp[0] =  (dData.delayFallLateNeg == NFORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayFallLateNeg + iPin->pData[pdi].atRiseLateCPPR);
							tempTransition[0] = RISE;

							temp[1] = (dData.delayRiseLate == NFORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayRiseLate + iPin->pData[pdi].atRiseLateCPPR);
							tempTransition[1] = RISE;

						}
						else if (iPin->pData[pdi].atFallLateCPPR != UNDEFINED_AT_LATE && iPin->pData[pdi].atRiseLateCPPR == UNDEFINED_AT_LATE)
						{
							temp[0] =  (dData.delayFallLate == NFORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayFallLate + iPin->pData[pdi].atFallLateCPPR);
							tempTransition[0] = FALL;

							temp[1] = (dData.delayRiseLateNeg == NFORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayRiseLateNeg + iPin->pData[pdi].atFallLateCPPR);
							tempTransition[1] = FALL;

						}
						else if (iPin->pData[pdi].atFallLateCPPR != UNDEFINED_AT_LATE && iPin->pData[pdi].atRiseLateCPPR != UNDEFINED_AT_LATE)
						{
								double a,b;
								a= (dData.delayFallLate == FORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayFallLate + iPin->pData[pdi].atFallLateCPPR);
								b = (dData.delayFallLateNeg == FORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayFallLateNeg + iPin->pData[pdi].atRiseLateCPPR);
								temp[0] = max(a,b);
								tempTransition[0] = a > b ? FALL : RISE ;

								a = (dData.delayRiseLateNeg == FORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayRiseLateNeg + iPin->pData[pdi].atFallLateCPPR);
								b = (dData.delayRiseLate == FORBIDDEN) ? UNDEFINED_AT_LATE : (dData.delayRiseLate + iPin->pData[pdi].atRiseLateCPPR);
								temp[1] = max(a,b);
								tempTransition[1] = a > b ? FALL : RISE ;
						}
						break;

					default:
						continue;
						break;
				}
				if (i == 0)
				{
					oPin->pData[pdi].atFallLateCPPR = temp[0];
					oPin->pData[pdi].criticalAT.indexFallLate = oPin->dData[i].index;
					oPin->pData[pdi].criticalAT.transitionFL = tempTransition[0];

					oPin->pData[pdi].atRiseLateCPPR = temp[1];
					oPin->pData[pdi].criticalAT.indexRiseLate = oPin->dData[i].index;
					oPin->pData[pdi].criticalAT.transitionRL = tempTransition[1];
				}
				else
				{
					if (oPin->pData[pdi].atFallLateCPPR < temp[0])
					{
						oPin->pData[pdi].atFallLateCPPR = temp[0];
						oPin->pData[pdi].criticalAT.indexFallLate = oPin->dData[i].index;
						oPin->pData[pdi].criticalAT.transitionFL = tempTransition[0];
					}
					if (oPin->pData[pdi].atRiseLateCPPR < temp[1])
					{
						oPin->pData[pdi].atRiseLateCPPR = temp[1];
						oPin->pData[pdi].criticalAT.indexRiseLate = oPin->dData[i].index;
						oPin->pData[pdi].criticalAT.transitionRL = tempTransition[1];
					}
				}
			}
		}
}

/**
 * \param currD Pointer to CEP pin of concerned cone
 * \param threadID ID of thread (for multithreading)
 * \param currCK Pointer to clock pin of concerned capturing FF of the cone
 *
 * Propagates Finds Fall Early cutoff for pruning by finding post-CPPR slack from pre-CPPR critical path
 */
double64 CPPR::getCutOff_FE (InstanceIpin* currD, int threadID, InstanceIpin* currCK)
{
	bool transition = FALL;
	InstanceIpin* iPin = currD;
	InstanceOpin* oPin = currD->wire->port.pinPtr;
	Instance* cell = oPin->cellPtr;

	while (1)
	{
		if(cell->cellLibIndex_e ==-2 || cell->cellLibIndex_e ==-4 || cell->info_early->isSequential)
			break;
		else
		{
			if (transition == RISE)
			{
					iPin = &(cell->iipList[oPin->tData.criticalAT.indexRiseEarly]);
					transition = oPin->tData.criticalAT.transitionRE;
			}
			else
			{
					iPin = &(cell->iipList[oPin->tData.criticalAT.indexFallEarly]);
					transition = oPin->tData.criticalAT.transitionFE;
			}
			oPin = iPin->wire->port.pinPtr;
			cell = oPin->cellPtr;
		}
	}

	SpefPort* cpPort;
	Credit currCredit;
	bool ckParity;

	if (currD->cellPtr->cellLibIndex_e == -3 || cell->cellLibIndex_e == -2 || cell->cellLibIndex_e == -4)
		currCredit.creditFallEarly = 0;

	else if (currCK->wire->port.parity != cell->ckList[0].wire->port.parity
			&& ((oPin->dData[0].timeSense == RISING_EDGE && currD->tData.setupSense == SETUP_RISING)
			|| (oPin->dData[0].timeSense == FALLING_EDGE && currD->tData.setupSense == SETUP_FALLING)))
		currCredit.creditFallEarly = 0;

	else if (currCK->wire->port.parity == cell->ckList[0].wire->port.parity
			&& ((oPin->dData[0].timeSense == RISING_EDGE && currD->tData.setupSense == SETUP_FALLING)
			|| (oPin->dData[0].timeSense == FALLING_EDGE && currD->tData.setupSense == SETUP_RISING)))
		currCredit.creditFallEarly = 0;

	else
	{
		cpPort = getLCA_Adv (&currCK->wire->port, &cell->ckList[0].wire->port);
		ckParity = cell->ckList[0].wire->port.parity;
		getCredit_Adv (cpPort, currCredit, ckParity, oPin->dData[0].timeSense, EARLY);
	}
	return (currD->tData.slackFallEarly + currCredit.creditFallEarly);
}

/**
 * \param currD Pointer to CEP pin of concerned cone
 * \param threadID ID of thread (for multithreading)
 * \param currCK Pointer to clock pin of concerned capturing FF of the cone
 *
 * Propagates Finds Rise Early cutoff for pruning by finding post-CPPR slack from pre-CPPR critical path
 */
double64 CPPR::getCutOff_RE (InstanceIpin* currD, int threadID, InstanceIpin* currCK)
{
	bool transition = RISE;
	InstanceIpin* iPin = currD;
	InstanceOpin* oPin = currD->wire->port.pinPtr;
	Instance* cell = oPin->cellPtr;

	while (1)
	{
		if(cell->cellLibIndex_e ==-2 || cell->cellLibIndex_e ==-4 || cell->info_early->isSequential)
			break;
		else
		{
			if (transition == RISE)
			{
					iPin = &(cell->iipList[oPin->tData.criticalAT.indexRiseEarly]);
					transition = oPin->tData.criticalAT.transitionRE;
			}
			else
			{
					iPin = &(cell->iipList[oPin->tData.criticalAT.indexFallEarly]);
					transition = oPin->tData.criticalAT.transitionFE;
			}
			oPin = iPin->wire->port.pinPtr;
			cell = oPin->cellPtr;
		}
	}

	SpefPort* cpPort;
	Credit currCredit;
	bool ckParity;

	if (currD->cellPtr->cellLibIndex_e == -3 || cell->cellLibIndex_e == -2 || cell->cellLibIndex_e == -4)
		currCredit.creditRiseEarly = 0;

	else if (currCK->wire->port.parity != cell->ckList[0].wire->port.parity
			&& ((oPin->dData[0].timeSense == RISING_EDGE && currD->tData.setupSense == SETUP_RISING)
			|| (oPin->dData[0].timeSense == FALLING_EDGE && currD->tData.setupSense == SETUP_FALLING)))
		currCredit.creditRiseEarly = 0;

	else if (currCK->wire->port.parity == cell->ckList[0].wire->port.parity
			&& ((oPin->dData[0].timeSense == RISING_EDGE && currD->tData.setupSense == SETUP_FALLING)
			|| (oPin->dData[0].timeSense == FALLING_EDGE && currD->tData.setupSense == SETUP_RISING)))
		currCredit.creditRiseEarly = 0;

	else
	{
		cpPort = getLCA_Adv (&currCK->wire->port, &cell->ckList[0].wire->port);
		ckParity = cell->ckList[0].wire->port.parity;
		getCredit_Adv (cpPort, currCredit, ckParity, oPin->dData[0].timeSense, EARLY);
	}

	return (currD->tData.slackRiseEarly + currCredit.creditRiseEarly);
}

/**
 * \param currD Pointer to CEP pin of concerned cone
 * \param threadID ID of thread (for multithreading)
 * \param currCK Pointer to clock pin of concerned capturing FF of the cone
 *
 * Propagates Finds Fall Late cutoff for pruning by finding post-CPPR slack from pre-CPPR critical path
 */
double64 CPPR::getCutOff_FL (InstanceIpin* currD, int threadID, InstanceIpin* currCK)
{
	bool transition = FALL;
	InstanceIpin* iPin = currD;
	InstanceOpin* oPin = currD->wire->port.pinPtr;
	Instance* cell = oPin->cellPtr;

	while (1)
	{
		if(cell->cellLibIndex_e ==-2 || cell->cellLibIndex_e ==-4 || cell->info_early->isSequential)
			break;
		else
		{
			if (transition == RISE)
			{
					iPin = &(cell->iipList[oPin->tData.criticalAT.indexRiseLate]);
					transition = oPin->tData.criticalAT.transitionRL;
			}
			else
			{
					iPin = &(cell->iipList[oPin->tData.criticalAT.indexFallLate]);
					transition = oPin->tData.criticalAT.transitionFL;
			}
			oPin = iPin->wire->port.pinPtr;
			cell = oPin->cellPtr;
		}
	}

	SpefPort* cpPort;
	Credit currCredit;
	bool ckParity;

	if (currD->cellPtr->cellLibIndex_e == -3 || cell->cellLibIndex_e == -2 || cell->cellLibIndex_e == -4)
		currCredit.creditFallLate = 0;

	else if (currCK->wire->port.parity != cell->ckList[0].wire->port.parity
			&& ((oPin->dData[0].timeSense == RISING_EDGE && currD->tData.setupSense == SETUP_RISING)
			|| (oPin->dData[0].timeSense == FALLING_EDGE && currD->tData.setupSense == SETUP_FALLING)))
		currCredit.creditFallLate = 0;

	else if (currCK->wire->port.parity == cell->ckList[0].wire->port.parity
			&& ((oPin->dData[0].timeSense == RISING_EDGE && currD->tData.setupSense == SETUP_FALLING)
			|| (oPin->dData[0].timeSense == FALLING_EDGE && currD->tData.setupSense == SETUP_RISING)))
		currCredit.creditFallLate = 0;

	else
	{
		cpPort = getLCA_Adv (&currCK->wire->port, &cell->ckList[0].wire->port);
		ckParity = cell->ckList[0].wire->port.parity;
		getCredit_Adv (cpPort, currCredit, ckParity, oPin->dData[0].timeSense, LATE);
	}

	return (currD->tData.slackFallLate + currCredit.creditFallLate);
}

/**
 * \param currD Pointer to CEP pin of concerned cone
 * \param threadID ID of thread (for multithreading)
 * \param currCK Pointer to clock pin of concerned capturing FF of the cone
 *
 * Propagates Finds Rise Late cutoff for pruning by finding post-CPPR slack from pre-CPPR critical path
 */
double64 CPPR::getCutOff_RL (InstanceIpin* currD, int threadID, InstanceIpin* currCK)
{
	bool transition = RISE;
	InstanceIpin* iPin = currD;
	InstanceOpin* oPin = currD->wire->port.pinPtr;
	Instance* cell = oPin->cellPtr;
	while (1)
	{
		if(cell->cellLibIndex_e ==-2 || cell->cellLibIndex_e ==-4 || cell->info_early->isSequential)
			break;
		else
		{
			if (transition == RISE)
			{
					iPin = &(cell->iipList[oPin->tData.criticalAT.indexRiseLate]);
					transition = oPin->tData.criticalAT.transitionRL;
			}
			else
			{
					iPin = &(cell->iipList[oPin->tData.criticalAT.indexFallLate]);
					transition = oPin->tData.criticalAT.transitionFL;
			}
			oPin = iPin->wire->port.pinPtr;
			cell = oPin->cellPtr;
		}
	}
	SpefPort* cpPort;
	Credit currCredit;
	bool ckParity;

	if (currD->cellPtr->cellLibIndex_e == -3 || cell->cellLibIndex_e == -2 || cell->cellLibIndex_e == -4)
		currCredit.creditRiseLate = 0;

	else if (currCK->wire->port.parity != cell->ckList[0].wire->port.parity
			&& ((oPin->dData[0].timeSense == RISING_EDGE && currD->tData.setupSense == SETUP_RISING)
			|| (oPin->dData[0].timeSense == FALLING_EDGE && currD->tData.setupSense == SETUP_FALLING)))
		currCredit.creditRiseLate = 0;

	else if (currCK->wire->port.parity == cell->ckList[0].wire->port.parity
			&& ((oPin->dData[0].timeSense == RISING_EDGE && currD->tData.setupSense == SETUP_FALLING)
			|| (oPin->dData[0].timeSense == FALLING_EDGE && currD->tData.setupSense == SETUP_RISING)))
		currCredit.creditRiseLate = 0;

	else
	{
		cpPort = getLCA_Adv (&currCK->wire->port, &cell->ckList[0].wire->port);
		ckParity = cell->ckList[0].wire->port.parity;
		getCredit_Adv (cpPort, currCredit, ckParity, oPin->dData[0].timeSense, LATE);
	}

	return (currD->tData.slackRiseLate + currCredit.creditRiseLate);
}

/**
 * \param currD Pointer to CEP pin of concerned cone
 * \param threadID ID of thread (for multithreading)
 * \param currCK Pointer to clock pin of concerned capturing FF of the cone
 *
 * Propagates Finds Early/Late cutoff for pruning by finding post-CPPR slack from pre-CPPR critical path
 */
double64 CPPR::getCutOff (InstanceIpin* currD, int threadID, InstanceIpin* currCK, bool mode)
{
	double64 cutOff = FORBIDDEN_NSLACK;

	if (mode == EARLY)
	{
		double64 cutOff_RE = getCutOff_RE (currD, threadID, currCK);
		double64 cutOff_FE = getCutOff_FE (currD, threadID, currCK);
		if (cutOff < cutOff_RE) cutOff = cutOff_RE;
		if (cutOff < cutOff_FE) cutOff = cutOff_FE;
	}
	else
	{
		double64 cutOff_RL = getCutOff_RL (currD, threadID, currCK);
		double64 cutOff_FL = getCutOff_FL (currD, threadID, currCK);
		if (cutOff < cutOff_RL) cutOff = cutOff_RL;
		if (cutOff < cutOff_FL) cutOff = cutOff_FL;
	}
	return cutOff;
}

/**
 * \param arg THREAD_DATA_T information passed by reference
 * \param thruPin Decides if post-CPPR slacks to be kept at CEP (if false). If true, means CPPR to run just for path extraction purposes through a pin
 *
 * Performs block based CPPR for a particular cone (along with making negPinList for the concerned cone)
 * Run multithreaded
 */
void CPPR::pinCPPR_Adv(void* arg, bool thruPin)
{
	THREAD_DATA_T* data = (THREAD_DATA_T*) arg;
	InstanceIpin* currD = data->currD;
	tbb::concurrent_priority_queue <TEMP_IPIN_LIST,  compareIpinListSlack>* negPinList = data->negPinList_G;
	int threadID = data->threadID;
	bool mode = data->mode;
	double64 CutOffValue = data->CutOffValue;

	int pdi = threadID;
	int64 k, l;
	Instance* currFF = currD->cellPtr;
	InstanceIpin* currCK = NULL;
	if (currFF->cellLibIndex_e != -3)
	{
		currCK = &(currFF->ckList[0]);			//Assumed only one CK input pin
	}
	int64 flagPath;
	flagPath = ++_CPPRflag;
	vector <InstanceOpin*> backPinList;
	Instance* cell;
	vector <SpefNet*> coneNetList;  			// Will be filled in topological order
	double64 cutOffValue_Pin;

	cutOffValue_Pin = getCutOff (currD, threadID, currCK, mode);

	double64 cutOff = (cutOffValue_Pin > CutOffValue) ? cutOffValue_Pin : CutOffValue;
	ComputeRATCritinCPPR(currD, backPinList, coneNetList, flagPath, pdi, cutOff, mode);

	SpefPort* cpPort;
	Credit currCredit;
	bool ckParity;
	for (k = 0; k < (int64)backPinList.size(); k++)
	{

		InstanceOpin* currQ = backPinList[k];
		cell = currQ->cellPtr;
		currQ->pData[pdi].flagPath = 0;
		if (currFF->cellLibIndex_e == -3 || cell->cellLibIndex_e == -2 || cell->cellLibIndex_e == -4)
		{
			if (mode == EARLY)
			{
				currCredit.creditFallEarly = 0;
				currCredit.creditRiseEarly = 0;
			}
			else
			{
				currCredit.creditFallLate = 0;
				currCredit.creditRiseLate = 0;
			}
		}
		else if (currCK->wire->port.parity != cell->ckList[0].wire->port.parity
				&& ((currQ->dData[0].timeSense == RISING_EDGE && currD->tData.setupSense == SETUP_RISING) || (currQ->dData[0].timeSense == FALLING_EDGE && currD->tData.setupSense == SETUP_FALLING)))
		{
			if (mode == EARLY)
			{
				currCredit.creditFallEarly = 0;
				currCredit.creditRiseEarly = 0;
			}
			else
			{
				currCredit.creditFallLate = 0;
				currCredit.creditRiseLate = 0;
			}
		}
		else if (currCK->wire->port.parity == cell->ckList[0].wire->port.parity
				&& ((currQ->dData[0].timeSense == RISING_EDGE && currD->tData.setupSense == SETUP_FALLING) || (currQ->dData[0].timeSense == FALLING_EDGE && currD->tData.setupSense == SETUP_RISING)))
		{
			if (mode == EARLY)
			{
				currCredit.creditFallEarly = 0;
				currCredit.creditRiseEarly = 0;
			}
			else
			{
				currCredit.creditFallLate = 0;
				currCredit.creditRiseLate = 0;
			}
		}
		else
		{
			cpPort = getLCA_Adv (&currCK->wire->port, &cell->ckList[0].wire->port);
			ckParity = cell->ckList[0].wire->port.parity;
			getCredit_Adv (cpPort, currCredit, ckParity, currQ->dData[0].timeSense, mode);
		}

		addCredit_Adv(currCredit, currQ, pdi, mode);
	}

	SpefNet* currWire, nextWire;
	for (int64 t = coneNetList.size() -1 ; t >=0  ; t--)
	{
		currWire = coneNetList[t];//  globalOut<<"begin timing info:"<<endl;
		cell = currWire->port.cellPtr;
		InstanceOpin* oPin = currWire->port.pinPtr;
		if (t == 0 || currWire->level <= coneNetList[t-1]->level)
		{
			if (cell->cellLibIndex_e != -2 && cell->cellLibIndex_e != -4 && cell->info_early->isSequential == false)
			{
				slackUpdateOpin_Adv(oPin, pdi, flagPath, mode);
			}
		}

		for (k = 0; k < (int64)currWire->taps.size(); k++)
		{
			InstanceIpin* iPin = currWire->taps[k].pinPtr;
			if (iPin->pData[pdi].flagPath == flagPath)
			{
				slackUpdateIpin_Adv(&(currWire->taps[k]), oPin, pdi, mode);

				if (mode == EARLY)
				{
					double tempSlackRE = iPin->pData[pdi].atRiseEarlyCPPR - iPin->pData[pdi].ratRiseEarly;
					double tempSlackFE = iPin->pData[pdi].atFallEarlyCPPR - iPin->pData[pdi].ratFallEarly;
					if (tempSlackRE < SANITY || tempSlackFE < SANITY)
					{
						cout << "Sanity failed" << endl;
						assert (0);
					}
					TEMP_IPIN_LIST tNode;
					tNode.iPin = iPin;
					if (tempSlackRE <= CutOffValue)
					{
						tNode.slack = tempSlackRE;
						tNode.trans = RISE;
						tNode.mode = EARLY;
						tNode.Dpin = currD;
						tNode.threadID = pdi;
						negPinList->push(tNode);
					}
					if (tempSlackFE <= CutOffValue)
					{
						tNode.slack = tempSlackFE;
						tNode.trans = FALL;
						tNode.mode = EARLY;
						tNode.Dpin = currD;
						tNode.threadID = pdi;
						negPinList->push(tNode);
					}
				}
				else
				{
					double tempSlackRL = iPin->pData[pdi].ratRiseLate - iPin->pData[pdi].atRiseLateCPPR;
					double tempSlackFL = iPin->pData[pdi].ratFallLate - iPin->pData[pdi].atFallLateCPPR;
					if (tempSlackRL < SANITY || tempSlackFL < SANITY)
					{
						cout << "Sanity failed" << endl;
						assert (0);
					}
					TEMP_IPIN_LIST tNode;
					tNode.iPin = iPin;
					if (tempSlackRL <= CutOffValue)
					{
						tNode.slack = tempSlackRL;
						tNode.trans = RISE;
						tNode.mode = LATE;
						tNode.Dpin = currD;
						tNode.threadID = pdi;
						negPinList->push(tNode);
					}
					if (tempSlackFL <= CutOffValue)
					{
						tNode.slack = tempSlackFL;
						tNode.trans = FALL;
						tNode.mode = LATE;
						tNode.Dpin = currD;
						tNode.threadID = pdi;
						negPinList->push(tNode);
					}
				}

				if (iPin->hash != currD->hash || iPin->name.cellid != currD->name.cellid)
				{
					// No need to put condition to check if reached currD as currD has no related pins
					for (l = 0; l < (int64)iPin->relPinIndex.size(); l++)
					{
						InstanceOpin* nextOpin = &(iPin->cellPtr->iopList[iPin->relPinIndex[l]]);
						if (nextOpin->pData[pdi].flagPath == flagPath){
							nextOpin->pData[pdi].flagPath = 0;
						}
					}
				}
			}
				iPin->pData[pdi].flagPath = 0;
		}
	}

	if (thruPin == false)
	{
		if (mode == EARLY)
		{
			if (currD->pData[pdi].atRiseEarlyCPPR != currD->tData.atRiseEarly)
			{
				currD->tData.ratRiseEarly -= currD->pData[pdi].atRiseEarlyCPPR - currD->tData.atRiseEarly;
				currD->tData.slackRiseEarly = currD->tData.atRiseEarly - currD->tData.ratRiseEarly;
			}
			if (currD->pData[pdi].atFallEarlyCPPR != currD->tData.atFallEarly)
			{
				currD->tData.ratFallEarly -= currD->pData[pdi].atFallEarlyCPPR - currD->tData.atFallEarly;
				currD->tData.slackFallEarly = currD->tData.atFallEarly - currD->tData.ratFallEarly;
			}
		}
		else
		{
			if (currD->pData[pdi].atRiseLateCPPR != currD->tData.atRiseLate)
			{
				currD->tData.ratRiseLate += currD->tData.atRiseLate - currD->pData[pdi].atRiseLateCPPR;
				currD->tData.slackRiseLate = currD->tData.ratRiseLate - currD->tData.atRiseLate;
			}
			if (currD->pData[pdi].atFallLateCPPR != currD->tData.atFallLate)
			{
				currD->tData.ratFallLate += currD->tData.atFallLate - currD->pData[pdi].atFallLateCPPR;
				currD->tData.slackFallLate = currD->tData.ratFallLate - currD->tData.atFallLate;
			}
		}
	}
}

/**
 * \param numInc ID number of a set of incremental operations
 *
 * Main function to perform CPPR and path extraction over complete design
 */
void CPPR::RunCPPR (int numInc)
{
	double64 CutOffValue = 0;
	buildSparseTable();

	int64 numCPPR = ConeEndPoints.size();
	if (numCPPR == 0)	return;

	while (!pathLUTSlackTable_Adv.empty())
	{
		int64 temp = Bsearch_iPin(ConeEndPoints, pathLUT_Adv[pathLUTSlackTable_Adv.front().index].DpinHash, 0, ConeEndPoints.size() - 1);
		if (temp == -1)
			pathQueue.push(pathLUTSlackTable_Adv.front());
		else
			pathLUT_Adv[pathLUTSlackTable_Adv.front().index].clear();
		pathLUTSlackTable_Adv.pop_front();
	}
	if ((int64)pathQueue.size() >= MaxNumPaths)
		CutOffValue = pathQueue.top().slack;


	deque < TEMP_CEP > CEPtemp;
	TEMP_CEP tempCEP;

	for (int64 i = 0; i < numCPPR; i++)
	{
		if (i!=0 && (ConeEndPoints[i]->hash == ConeEndPoints[i-1]->hash && ConeEndPoints[i]->name.cellid == ConeEndPoints[i-1]->name.cellid))
			continue;
		if (ConeEndPoints[i]->isConnected == false) continue;

		tempCEP.Dpin = ConeEndPoints[i];
		tempCEP.mode = EARLY;
		CEPtemp.push_back(tempCEP);
		tempCEP.mode = LATE;
		CEPtemp.push_back(tempCEP);
	}
	numCPPR = CEPtemp.size();

	tbb::concurrent_priority_queue <TEMP_IPIN_LIST, compareIpinListSlack> negPinList_G;
	int remainder = numCPPR % NUM_THREADS;

	omp_set_num_threads(NUM_THREADS);                       // Parallel execution.

	for (int cnt = 0 ; cnt < numCPPR ; cnt = cnt + NUM_THREADS)
	{
		if (cnt < remainder)
		{

// 			#pragma omp parallel
			{
// 				#pragma omp for
				for(int i = 0; i < remainder; i++)
				{
				    THREAD_DATA_T thread_data;
				    thread_data.currD = CEPtemp[ cnt + i].Dpin;
					thread_data.mode = CEPtemp[ cnt + i].mode;
					thread_data.threadID = i;
					thread_data.negPinList_G = &(negPinList_G);
					thread_data.CutOffValue = CutOffValue;
					pinCPPR_Adv(&thread_data);
				}
			}
			cnt = remainder - NUM_THREADS;
		}
		else
		{
			#pragma omp parallel
			{
				#pragma omp for
				for(int i = 0; i< NUM_THREADS; i++)
				{
					THREAD_DATA_T thread_data;
				    thread_data.currD = CEPtemp[ cnt + i].Dpin;
					thread_data.mode = CEPtemp[ cnt + i].mode;
					thread_data.threadID = i;
					thread_data.negPinList_G = &(negPinList_G);
					thread_data.CutOffValue = CutOffValue;
					pinCPPR_Adv(&thread_data);
				}
			}
		}

		int64 flagPath_Adv = PathFlag_G++;
		int64 negPinListsz = negPinList_G.size();
		for (int64 a = 0; a < negPinListsz; a++)
		{
			TEMP_IPIN_LIST curr;
			negPinList_G.try_pop(curr);
			int64 flagPath = curr.Dpin->tap->hash;
			if ((int64)pathQueue.size() >= MaxNumPaths)
			{
				PATH_TABLE_ADV* currAdv = &(pathLUT_Adv[pathQueue.top().index]);

				//Added for Pruning
				CutOffValue = pathQueue.top().slack;


				if (pathQueue.top().slack < curr.slack)
					break;
				if (curr.slack < currAdv->slack && searchFlag_Adv(curr.iPin->pathFlag, curr.mode, curr.trans, curr.threadID, flagPath_Adv) == false)
				{
					Slack_Instance siTemp;
					siTemp.index = pathQueue.top().index;
					siTemp.slack = curr.slack;
					currAdv->Dpin = curr.Dpin;
					currAdv->DpinHash = curr.Dpin->tap->hash;
					currAdv->mode = curr.mode;
					currAdv->slack = curr.slack;
					WORST_PIN wPin;
					wPin.Dpin = curr.Dpin;
					wPin.iPin = curr.iPin;
					wPin.mode = curr.mode;
					wPin.slack = curr.slack;
					wPin.transition = curr.trans;
					wPin.type = (curr.Dpin->tap->hash == curr.iPin->tap->hash) ? PO_PIN : IPIN;
					deque <PATH_ADV> p;
					TracePath(wPin, flagPath_Adv, p, flagPath, curr.threadID);
					currAdv->path.clear();
					currAdv->path.reserve(p.size());
					pathQueue.pop();
					pathQueue.push(siTemp);
					for (int64 a = 0; a < (int64)p.size(); a++)
						currAdv->path.push_back(p[a]);

					//Added for Pruning
					CutOffValue = pathQueue.top().slack;


				}
			}
			else if (curr.slack <= 0 && searchFlag_Adv(curr.iPin->pathFlag, curr.mode, curr.trans, curr.threadID, flagPath_Adv) == false)
			{
				PATH_TABLE_ADV newAdv;
				newAdv.Dpin = curr.Dpin;
				newAdv.DpinHash = curr.Dpin->tap->hash;
				newAdv.mode = curr.mode;
				newAdv.slack = curr.slack;
				WORST_PIN wPin;
				wPin.Dpin = curr.Dpin;
				wPin.iPin = curr.iPin;
				wPin.mode = curr.mode;
				wPin.slack = curr.slack;
				wPin.transition = curr.trans;
				wPin.type = (curr.Dpin->tap->hash == curr.iPin->tap->hash) ? PO_PIN : IPIN;
				deque <PATH_ADV> p;
				TracePath(wPin, flagPath_Adv, p, flagPath, curr.threadID);
				newAdv.path.reserve(p.size());
				for (int64 a = 0; a < (int64)p.size(); a++)
					newAdv.path.push_back(p[a]);

				pathLUT_Adv.push_back(newAdv);
				Slack_Instance hI;
				hI.slack = curr.slack;
				hI.index = pathLUT_Adv.size() - 1;
				pathQueue.push(hI);
			}
		}
		negPinList_G.clear();
	}
		while (!pathQueue.empty())
		{
			pathLUTSlackTable_Adv.push_front(pathQueue.top());
			pathQueue.pop();
		}

	ComputeRATinCPPR();

}

/**
 * \param tokens List of words in the command (from .ops) requesting for report worst paths through a pin
 * \param PinEndPoints CEPS limited to cones of concerned queried pin
 * \param pinLUT_Adv List of worst paths through the concerned pin
 * \param pinLUTSlackTable_Adv Slack Table for pinLUT_Adv to sort according to slack
 * \param MaxNumPaths_pin Maximum # of paths to report
 *
 * Main function to perform path extraction for paths through a given pin
 * Runs block based CPPR on related cones only, and without changing any slack value
 */
void CPPR::RunCPPR_Pin (deque<std::string>& tokens, deque < TEMP_CEP > PinEndPoints,
		deque < PATH_TABLE_ADV >& pinLUT_Adv, deque < Slack_Instance >& pinLUTSlackTable_Adv, int MaxNumPaths_pin)
{
	double64 CutOffValue = 0;
	priority_queue <Slack_Instance,deque<Slack_Instance>, ComparePathSlack_Adv> pinQueue;
	int64 numCPPR = PinEndPoints.size();
	if (numCPPR == 0)	return;


	tbb::concurrent_priority_queue <TEMP_IPIN_LIST, compareIpinListSlack> negPinList_G;
	int remainder = numCPPR % NUM_THREADS;

	for (int cnt = 0 ; cnt < numCPPR ; cnt = cnt + NUM_THREADS)
	{
		if (cnt < remainder)
		{

// 			#pragma omp parallel
			{
// 				#pragma omp for
				for( int i = 0; i < remainder; i++)
				{
					THREAD_DATA_T thread_data;
					thread_data.currD = PinEndPoints[ cnt + i].Dpin;
					thread_data.mode = PinEndPoints[ cnt + i].mode;
					thread_data.threadID = i;
					thread_data.negPinList_G = &(negPinList_G);
					thread_data.CutOffValue = CutOffValue;
					pinCPPR_Adv(&thread_data, true);


				}
			}
			cnt = remainder - NUM_THREADS;
		}
		else
		{
// 			#pragma omp parallel
			{
// 				#pragma omp for
				for( int i = 0; i< NUM_THREADS; i++)
				{
					THREAD_DATA_T thread_data;
					thread_data.currD = PinEndPoints[ cnt + i].Dpin;
					thread_data.mode = PinEndPoints[ cnt + i].mode;
					thread_data.threadID = i;
					thread_data.negPinList_G = &(negPinList_G);
					thread_data.CutOffValue = CutOffValue;
					pinCPPR_Adv(&thread_data, true);
				}
			}
		}

		int64 flagPath_Adv = PathFlag_G++;

		int64 negPinListsz = negPinList_G.size();
		for (int64 a = 0; a < negPinListsz; a++)
		{
			TEMP_IPIN_LIST curr;
			negPinList_G.try_pop(curr);
			int64 flagPath = curr.Dpin->tap->hash;
			int valid = false;
			if ((int64)pinQueue.size() >= MaxNumPaths_pin)
			{
				PATH_TABLE_ADV* currAdv = &(pinLUT_Adv[pinQueue.top().index]);

				//Added for Pruning
				CutOffValue = pinQueue.top().slack;


				if (pinQueue.top().slack < curr.slack)
					break;
				if (curr.slack < currAdv->slack && searchFlag_Adv(curr.iPin->pathFlag, curr.mode, curr.trans, curr.threadID, flagPath_Adv) == false)
				{
					Slack_Instance siTemp;
					siTemp.index = pinQueue.top().index;
					siTemp.slack = curr.slack;
					currAdv->Dpin = curr.Dpin;
					currAdv->DpinHash = curr.Dpin->tap->hash;
					currAdv->mode = curr.mode;
					currAdv->slack = curr.slack;
					WORST_PIN wPin;
					wPin.Dpin = curr.Dpin;
					wPin.iPin = curr.iPin;
					wPin.mode = curr.mode;
					wPin.slack = curr.slack;
					wPin.transition = curr.trans;
					wPin.type = (curr.Dpin->tap->hash == curr.iPin->tap->hash) ? PO_PIN : IPIN;
					deque <PATH_ADV> path;
					TracePath(wPin, flagPath_Adv, path, flagPath, curr.threadID);
					for (int64 j = 0; j < (int64)path.size(); j++)
					{
						if (*(path[j].icellName) == tokens[0] && *(path[j].pinName) == tokens[1])
						{
							valid = true;
							break;
						}
					}
					if (valid == true)
					{
						currAdv->path.clear();
						currAdv->path.reserve(path.size());
						pinQueue.pop();
						pinQueue.push(siTemp);
						for (int64 a = 0; a < (int64)path.size(); a++)
							currAdv->path.push_back(path[a]);
					}
					//Added for Pruning
					CutOffValue = pathQueue.top().slack;
				}
			}
			else if (curr.slack <= 0 && searchFlag_Adv(curr.iPin->pathFlag, curr.mode, curr.trans, curr.threadID, flagPath_Adv) == false)
			{
				PATH_TABLE_ADV newAdv;
				newAdv.Dpin = curr.Dpin;
				newAdv.DpinHash = curr.Dpin->tap->hash;
				newAdv.mode = curr.mode;
				newAdv.slack = curr.slack;
				WORST_PIN wPin;
				wPin.Dpin = curr.Dpin;
				wPin.iPin = curr.iPin;
				wPin.mode = curr.mode;
				wPin.slack = curr.slack;
				wPin.transition = curr.trans;
				wPin.type = (curr.Dpin->tap->hash == curr.iPin->tap->hash) ? PO_PIN : IPIN;
				deque <PATH_ADV> path;
				TracePath(wPin, flagPath_Adv, path, flagPath, curr.threadID);
				for (int64 j = 0; j < (int64)path.size(); j++)
				{
					if (*(path[j].icellName) == tokens[0] && *(path[j].pinName) == tokens[1])
					{
						valid = true;
						break;
					}
				}
				if (valid == true)
				{
					newAdv.path.reserve(path.size());
					for (int64 a = 0; a < (int64)path.size(); a++)
						newAdv.path.push_back(path[a]);

					pinLUT_Adv.push_back(newAdv);
					Slack_Instance hI;
					hI.slack = curr.slack;
					hI.index = pinLUT_Adv.size() - 1;
					pinQueue.push(hI);
				}
			}
		}
		negPinList_G.clear();
	}
	while (!pinQueue.empty())
	{
		pinLUTSlackTable_Adv.push_front(pinQueue.top());
		pinQueue.pop();
	}
}

/**
 * \param oPin Output pin from where path needs to be forward-extracted
 * \param path Container to store path
 * \param transition Related transition (rise/fall)
 * \param mode Mode (early/late)
 * \param flagPath Color for the path
 * \param Dpinhash Hash of a CEP pin (also end-point of concerned path)
 * \param pdi ID of thread (for multithreading)
 * \result InstanceIpin* Returns pointer to end pin of the path
 *
 * Extracts forward path from a given oPin using criticalRAT values
 */
InstanceIpin* CPPR::FrontTrace_Adv (InstanceOpin* oPin, deque <PATH_ADV>& path, bool transition, bool mode,
		                             int64 flagPath, int64 DpinHash, int pdi)
{
	PATH_ADV tempPin;
	SpefNet* wire = oPin->wire;
	InstanceIpin* iPin;

	if (transition == RISE)
	{
		if (mode == EARLY)
		{
			iPin = wire->taps[oPin->pData[pdi].criticalRAT.indexRiseEarly].pinPtr;
			iPin->pathFlag.flagRE[pdi] = flagPath;
		}
		else
		{
			iPin = wire->taps[oPin->pData[pdi].criticalRAT.indexRiseLate].pinPtr;
			iPin->pathFlag.flagRL[pdi] = flagPath;
		}
	}
	else
	{
		if (mode == EARLY)
		{
			iPin = wire->taps[oPin->pData[pdi].criticalRAT.indexFallEarly].pinPtr;
			iPin->pathFlag.flagFE[pdi] = flagPath;
		}
		else
		{
			iPin = wire->taps[oPin->pData[pdi].criticalRAT.indexFallLate].pinPtr;
			iPin->pathFlag.flagFL[pdi] = flagPath;
		}
	}


	tempPin.icellName = &(iPin->name.icellName);
	tempPin.pinName = &(iPin->name.pinName);
	tempPin.transition = (transition == RISE)? 'R':'F';
	path.push_front(tempPin);

	Instance* cell = iPin->cellPtr;
	while (cell->cellLibIndex_e != -3)
	{
		if (cell->info_early->isSequential == false)
		{
			InstanceOpin* oPin;
			if (transition == RISE)
			{
				if (mode == EARLY)
				{
					oPin = &(cell->iopList[iPin->pData[pdi].criticalRAT.indexRiseEarly]);
					transition = iPin->pData[pdi].criticalRAT.transitionRE;
				}
				else
				{
					oPin = &(cell->iopList[iPin->pData[pdi].criticalRAT.indexRiseLate]);
					transition = iPin->pData[pdi].criticalRAT.transitionRL;
				}
			}
			else
			{
				if (mode == EARLY)
				{
					oPin = &(cell->iopList[iPin->pData[pdi].criticalRAT.indexFallEarly]);
					transition = iPin->pData[pdi].criticalRAT.transitionFE;
				}
				else
				{
					oPin = &(cell->iopList[iPin->pData[pdi].criticalRAT.indexFallLate]);
					transition = iPin->pData[pdi].criticalRAT.transitionFL;
				}
			}

			tempPin.icellName = &(oPin->name.icellName);
			tempPin.pinName = &(oPin->name.pinName);
			tempPin.transition = (transition == RISE)? 'R':'F';
			path.push_front(tempPin);
			wire = oPin->wire;
			if (transition == RISE)
			{
				if (mode == EARLY)
				{
					iPin = wire->taps[oPin->pData[pdi].criticalRAT.indexRiseEarly].pinPtr;
					iPin->pathFlag.flagRE[pdi] = flagPath;
				}
				else
				{
					iPin = wire->taps[oPin->pData[pdi].criticalRAT.indexRiseLate].pinPtr;
					iPin->pathFlag.flagRL[pdi] = flagPath;
				}
			}
			else
			{
				if (mode == EARLY)
				{
					iPin = wire->taps[oPin->pData[pdi].criticalRAT.indexFallEarly].pinPtr;
					iPin->pathFlag.flagFE[pdi] = flagPath;
				}
				else
				{
					iPin = wire->taps[oPin->pData[pdi].criticalRAT.indexFallLate].pinPtr;
					iPin->pathFlag.flagFL[pdi] = flagPath;
				}
			}
			tempPin.icellName = &(iPin->name.icellName);
			tempPin.pinName = &(iPin->name.pinName);
			tempPin.transition = (transition == RISE)? 'R':'F';
			path.push_front(tempPin);

			cell = iPin->cellPtr;
		}
		else
		{
				break;
			}
	}
	return iPin;
}

/**
 * \param oPin Output pin from where path needs to be backward-extracted
 * \param path Container to store path
 * \param transition Related transition (rise/fall)
 * \param mode Mode (early/late)
 * \param flagPath Color for the path
 * \param Dpinhash Hash of a CEP pin (also end-point of concerned path)
 * \param pdi ID of thread (for multithreading)
 *
 * Extracts backward path from a given oPin using criticalAT values
 */
void CPPR::BackTrace_Adv (InstanceOpin* oPin, deque <PATH_ADV>& path, bool transition, bool mode,
		                   int64 flagPath, int64 DpinHash, int pdi)
{
	PATH_ADV tempPin;
	Instance* cell = oPin->cellPtr;
	tempPin.icellName = &(oPin->name.icellName);
	tempPin.pinName = &(oPin->name.pinName);
	tempPin.transition = (transition == RISE)? 'R':'F';
	path.push_back(tempPin);
	while (cell->cellLibIndex_e >= 0)
	{
		InstanceIpin* iPin;
		if (cell->info_early->isSequential == true)
		{
			iPin = &(cell->ckList[0]);
			transition = (cell->iipList[0].tData.holdSense == HOLD_RISING) ? RISE : FALL;
		}
		else
		{
			if (transition == RISE)
			{
				if (mode == EARLY)
				{
					iPin = &(cell->iipList[oPin->pData[pdi].criticalAT.indexRiseEarly]);
					transition = oPin->pData[pdi].criticalAT.transitionRE;
					if (transition == RISE)
						iPin->pathFlag.flagRE[pdi] = flagPath;
					else
						iPin->pathFlag.flagFE[pdi] = flagPath;
				}
				else
				{
					iPin = &(cell->iipList[oPin->pData[pdi].criticalAT.indexRiseLate]);
					transition = oPin->pData[pdi].criticalAT.transitionRL;
					if (transition == RISE)
						iPin->pathFlag.flagRL[pdi] = flagPath;
					else
						iPin->pathFlag.flagFL[pdi] = flagPath;
				}
			}
			else
			{
				if (mode == EARLY)
				{
					iPin = &(cell->iipList[oPin->pData[pdi].criticalAT.indexFallEarly]);
					transition = oPin->pData[pdi].criticalAT.transitionFE;
					if (transition == RISE)
						iPin->pathFlag.flagRE[pdi] = flagPath;
					else
						iPin->pathFlag.flagFE[pdi] = flagPath;
				}
				else
				{
					iPin = &(cell->iipList[oPin->pData[pdi].criticalAT.indexFallLate]);
					transition = oPin->pData[pdi].criticalAT.transitionFL;
					if (transition == RISE)
						iPin->pathFlag.flagRL[pdi] = flagPath;
					else
						iPin->pathFlag.flagFL[pdi] = flagPath;
				}
			}
		}
		tempPin.icellName = &(iPin->name.icellName);
		tempPin.pinName = &(iPin->name.pinName);
		tempPin.transition = (transition == RISE)? 'R':'F';
		path.push_back(tempPin);

		oPin = iPin->wire->port.pinPtr;
		tempPin.icellName = &(oPin->name.icellName);
		tempPin.pinName = &(oPin->name.pinName);
		tempPin.transition = (transition == RISE)? 'R':'F';
		path.push_back(tempPin);
		cell = oPin->cellPtr;
	}
}

/**
 * \param worstPin Worst pin (input pin) from where path needs to be extracted
 * \param flagPath Color for the path
 * \param path Container to store path
 * \param Dpinhash Hash of a CEP pin (also end-point of concerned path)
 * \param pdi ID of thread (for multithreading)
 *
 * Extracts worst path from a given worstPin data using criticalAT & criticalRAT values
 */
void CPPR::TracePath(WORST_PIN& worstPin, int64 flagPath, deque <PATH_ADV>& path, int64 DpinHash, int pdi)
{
	InstanceIpin* end;
	PATH_ADV tempPin;

	if (worstPin.type == IPIN)
	{
		InstanceIpin* iPin = worstPin.iPin;
		if (iPin->isConnected == false)
		{
			iPin->pathFlag.flagRE[pdi] = flagPath;
			iPin->pathFlag.flagFE[pdi] = flagPath;
			iPin->pathFlag.flagRL[pdi] = flagPath;
			iPin->pathFlag.flagFL[pdi] = flagPath;
			return;
		}

		InstanceOpin* oPin = iPin->wire->port.pinPtr;
		PATH_ADV temp;
		temp.icellName = &(iPin->name.icellName);
		temp.pinName = &(iPin->name.pinName);
		temp.transition = (worstPin.transition == RISE)? 'R':'F';
		path.push_back(temp);
		BackTrace_Adv (oPin, path, worstPin.transition, worstPin.mode, flagPath, DpinHash, pdi);
		if (iPin->cellPtr->info_early->isSequential == false)
		{
			bool transition;
			if (worstPin.transition == RISE)
			{
				if (worstPin.mode == EARLY)
				{
					iPin->pathFlag.flagRE[pdi] = flagPath;
					oPin = &(iPin->cellPtr->iopList[iPin->pData[pdi].criticalRAT.indexRiseEarly]);
					transition = iPin->pData[pdi].criticalRAT.transitionRE;
				}
				else
				{
					iPin->pathFlag.flagRL[pdi] = flagPath;
					oPin = &(iPin->cellPtr->iopList[iPin->pData[pdi].criticalRAT.indexRiseLate]);
					transition = iPin->pData[pdi].criticalRAT.transitionRL;
				}
			}
			else
			{
				if (worstPin.mode == EARLY)
				{
					iPin->pathFlag.flagFE[pdi] = flagPath;
					oPin = &(iPin->cellPtr->iopList[iPin->pData[pdi].criticalRAT.indexFallEarly]);
					transition = iPin->pData[pdi].criticalRAT.transitionFE;
				}
				else
				{
					iPin->pathFlag.flagFL[pdi] = flagPath;
					oPin = &(iPin->cellPtr->iopList[iPin->pData[pdi].criticalRAT.indexFallLate]);
					transition = iPin->pData[pdi].criticalRAT.transitionFL;
				}
			}

			temp.icellName = &(oPin->name.icellName);
			temp.pinName = &(oPin->name.pinName);
			temp.transition = (transition == RISE)? 'R':'F';
			path.push_front(temp);
			end = FrontTrace_Adv(oPin, path, transition, worstPin.mode, flagPath, DpinHash, pdi);
		}
		else
		{
			if (worstPin.transition == RISE)
			{
				if (worstPin.mode == EARLY)
					iPin->pathFlag.flagRE[pdi] = flagPath;
				else
					iPin->pathFlag.flagRL[pdi] = flagPath;
			}
			else
			{
				if (worstPin.mode == EARLY)
					iPin->pathFlag.flagFE[pdi] = flagPath;
				else
					iPin->pathFlag.flagFL[pdi] = flagPath;
			}
			end = iPin;
		}
	}
	else if (worstPin.type == PO_PIN)
	{
		InstanceIpin* iPin = worstPin.iPin;
		if (iPin->isConnected == false)
		{
			iPin->pathFlag.flagRE[pdi] = flagPath;
			iPin->pathFlag.flagFE[pdi] = flagPath;
			iPin->pathFlag.flagRL[pdi] = flagPath;
			iPin->pathFlag.flagFL[pdi] = flagPath;
			return;
		}

		InstanceOpin* oPin = worstPin.iPin->wire->port.pinPtr;
		end = worstPin.iPin;
		tempPin.icellName = &(end->name.icellName);
		tempPin.pinName = &(end->name.pinName);
		tempPin.transition = (worstPin.transition == RISE)? 'R':'F';
		path.push_front(tempPin);
		if (worstPin.transition == RISE)
		{
			if (worstPin.mode == EARLY)
				end->pathFlag.flagRE[pdi] = flagPath;
			else
				end->pathFlag.flagRL[pdi] = flagPath;
		}
		else
		{
			if (worstPin.mode == EARLY)
				end->pathFlag.flagFE[pdi] = flagPath;
			else
				end->pathFlag.flagFL[pdi] = flagPath;
		}
		BackTrace_Adv (oPin, path, worstPin.transition, worstPin.mode, flagPath, DpinHash, pdi);
	}
	else
	{
		InstanceIpin* iPin = worstPin.iPin;
		iPin->pathFlag.flagRE[pdi] = flagPath;
		iPin->pathFlag.flagFE[pdi] = flagPath;
		iPin->pathFlag.flagRL[pdi] = flagPath;
		iPin->pathFlag.flagFL[pdi] = flagPath;
		return;
	}
}

};



