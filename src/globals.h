/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_GLOBALS_H_
#define IIT_GLOBALS_H_

#include "include.h"
#include "timer.h"
#include "forward_propagate.h"
#include "backward_propagate.h"
#include "cppr.h"
#include "report.h"
#include "operations.h"
#include "utility.h"

/**
 * \file globals.h
 *
 * This file contains declaration of all the global variables used throughout the namespace
 */

namespace iit{

extern CelllibStorage      celllibstorage_e;                  ///< Early mode cell library storage
extern CelllibStorage      celllibstorage_l;                  ///< Late mode cell library storage
extern LibFileInfo         libinfo_e;                         ///< Early mode units and LUT template info of library
extern LibFileInfo         libinfo_l;                         ///< Late mode units and LUT template info of library
extern CompleteTimingInfo  timing;                            ///< Slew, AT, RAT assertions storage from .sdc/.timing file

extern Instance*     CLKSRC;                                  ///< Pointer to the clock source instance
extern int           numInc;                                  ///< ID number of a set of incremental operations
extern int           Verbose;                                 ///< Flag for verbose printing/debugging/logging
extern TimeIt        g_Timer;                                 ///< Variable to keep global timer
extern TimeIt        Var_TimeIt[NUM_FUNCTIONS];               ///< Array of runtime-keeping variables for functions
extern string        Func_Names[NUM_FUNCTIONS];               ///< Array of all function names
extern double64      Func_Total_T[NUM_FUNCTIONS];             ///< Array of all function-runtimes
extern int64         Func_Total_C[NUM_FUNCTIONS];             ///< Array of number of calls to functions
extern int64         MaxNumPaths;                             ///< Max number of paths to report among all the queries
extern int64         PathFlag_G;                              ///< Global color count for non-repetitive values throughout
extern string        BenchmarkDir;                            ///< Path to the benchmark directory that is being run
extern bool         _isStaticDone;                            ///< Flag for whether STA is done

extern int64         SkipCnt;                                 ///< Count of pins pruned in CPPR back-traversal
extern int64         NoSkipCnt;                               ///< Count of un-pruned pins in CPPR back-traversal
extern double64      MaxSlew;                                 ///< Default max slew constraint from cell library


extern deque   < Instance >       InstanceList;               ///< Set of gate instances in the design
extern vector  < Hash_Instance >  InstanceHashTable;          ///< Hash-table for gates in InstanceList
extern deque   < SpefNet >        SpefNetList;                ///< Set of net instances in the design
extern vector  < Hash_Instance >  spefNetHashTable;           ///< Hash-table for nets in SpefNetList
extern vector  < SpefNet* >       Sort;                       ///< Set of net-pointers ordered in increasing topological levels
extern vector  < SpefNet* >       IncNetList;                 ///< Set of incremental nets
extern deque   < SpefNet >        WireList;                   ///< Set of nets stored with their names initially in Verilog parser
extern queue   < SpefNet* >       InitNetList;                ///< Set of net-pointers to initiate incremental timing from
extern vector  < Instance* >      PIList;                     ///< Set of pointers to primary input instances
extern vector  < Instance* >      POList;                     ///< Set of pointers to primary output instances
extern vector  < InstanceIpin* >  ConeEndPoints;              ///< Set of pointers to end-point pins
extern deque   < PATH_TABLE_ADV > pathLUT_Adv;                ///< Set of top 'N' worst paths- a PathTable
extern deque   < Slack_Instance > pathLUTSlackTable_Adv;      ///< Set of paths form PathTable (pathLUT_Adv) sorted in slack criticality

extern std::tr1::unordered_map <char,bool> specCharLibDir;    ///< Delimiters for line splitting into tokens

// For sizer
extern FPLibStorage      FPLibstorage_e;                      ///< Footprint-wise information storage for cells - early mode
extern FPLibStorage      FPLibstorage_l;                      ///< Footprint-wise information storage for cells - late mode
extern SolutionState     TrackSoln;                           ///< Global tracker of circuit state- leakage, TNS, worst slack etc.



};


#endif /* IIT_GLOBALS_H_*/
