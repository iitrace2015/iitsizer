/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_ENUMERATE_H_
#define IIT_ENUMERATE_H_

/**
 * \file enumerate.h
 *
 * This file contains all the enumeration types
 */
namespace iit{
/**
 * Enumeration type for signal transition- Rise/Fall
 */
enum Transition
{
	RISE = 0,
	FALL = 1
};

/**
 * Enumeration type for signal mode - Early/Late
 */
enum Mode
{
	EARLY = 0,
	LATE  = 1
};

/**
 * Enumeration type for parity - Odd/Even
 */
enum Parity
{
	EVEN = 0,
	ODD  = 1
};

/**
 * Enumeration type for signal timing type, including both transition and mode
 */
enum TimingType {
	RE = 0,
	RL = 1,
	FE = 2,
	FL = 3
};

/**
 * Enumeration type for function names
 */
typedef enum FunctionTimeAnalyser {

	// lib parser functions
    init_lib_parser_enum                     = 0,
    read_library_info_enum                   = 1,
    read_LUT_template_enum                   = 2,
    _begin_read_LUT_template_enum            = 3,
    read_cell_info_enum                      = 4,
    read_line_as_tokens_enum                 = 5,
    _begin_read_lut_enum                     = 6,
    _begin_read_timing_info_enum             = 7,
    _begin_read_pin_info_enum                = 8,
    _begin_read_library_info_enum            = 9,
    _begin_read_cell_info_enum               = 10,
    _close_enum                              = 11,

	// verilog parser functions
	is_special_char_enum                     = 12,
	init_verilog_parser_enum                 = 14,
	read_module_enum                         = 15,
	read_primary_input_enum                  = 16,
	read_primary_output_enum                 = 17,
	read_wire_enum                           = 18,
	read_cell_inst_enum                      = 19,
	read_cell_inst2_enum                     = 20,
	fix_checkpoints_enum                     = 21,
	read_line_as_tokens2_enum                = 22,
	read_line_as_tokens3_enum                = 23,
	ProcessPI_enum                           = 24,
	ProcessNet_enum                          = 25,
	ProcessPO_enum                           = 26,
	is_special_char3_enum                    = 27,
	createNetHash_enum                       = 28,
	Canonicalize_enum                        = 29,

	// spef parser functions
	read_line_as_tokens_spef_enum            = 30,
	read_line_as_tokens_spef_res_enum        = 31,
	init_spef_parser_enum                    = 32,
	read_net_dataInc_enum                    = 33,
	close_enum                               = 34,
	read_connections_enum                    = 35,
	read_resistances_enum                    = 36,
	read_capacitances_enum                   = 37,
	read_connectionsInc_enum                 = 38,
	read_resistancesInc_enum                 = 39,
	read_capacitancesInc_enum                = 40,
	read_net_data_enum                       = 41,
	FastTapdelaybeta_enum                    = 42,
	linker_enum                              = 43,

	// timing parser functions
	read_timing_line_enum                    = 44,


	// functions from operations class
	read_ops_as_tokens_enum                  = 46,
	init_ops_parser_enum                     = 47,
	read_ops_as_tokens_slow_enum             = 48,
	InplaceMerge_enum                        = 50,
	ClearIncWireListDB_enum                  = 51,
	ComputeDBInc_enum                        = 52,
	ColorParentNets_enum                     = 53,
	linkerInc_enum                           = 54,
	FastTapdelaybetaInc_enum                 = 55,

	// Incremental commands
	DisconnectPin_enum                       = 56,
	ConnectPin_enum                          = 57,
	RepowerGate_enum                         = 58,
	ReadSpef_enum                            = 59,
	RemoveNet_enum                           = 60,
	CreateNet_enum                           = 61,
	RemoveGate_enum                          = 62,
	InsertGate_enum                          = 63,
	InitOperations_enum                      = 64,
	ReportOperations_enum                    = 65,

	// Utility functions
	timeit_start_enum                        = 66,
	timeit_stop_enum                         = 67,
	debug_time_enum                          = 68,
	ComputeHash_enum                         = 69,
	split_enum                               = 71,
    Bsearch_enum                             = 72,
	Bsearch_iPin_enum                        = 73,
	Threshold_enum                           = 74,

	// functions from Timer class
	Engine_enum                              = 75,
	BuildCircuit_enum                        = 76,
	RunCircuit_enum                          = 77,
	test_lib_parser_enum                     = 78,
	test_timing_parser_enum                  = 79,
	test_ops_parser_enum                     = 80,
	test_spef_parser_enum                    = 81,
	test_verilog_parser_enum                 = 82,
	read_tau2015_as_tokens_enum              = 83,
	is_special_char_tau2015_enum             = 84,

	// functions from Report class
	DumpResults_enum                         = 85,
	FrontColorPin_enum                       = 86,
	quickReport_enum                         = 87,
	quickReportPin_enum                      = 88,
	ReportWorstPaths_enum                    = 89,
	ReportAT_enum                            = 90,
	ReportRAT_enum                           = 91,
	ReportSlack_enum                         = 92,

	// functions from Forward Propagate class
	TopSort_enum                             = 94,
	Init_L_enum                              = 95,
	ReClear_enum                             = 96,
	TopSortInc_enum                          = 97,
	ATSlew_enum                              = 98,
	BFSInc_enum                              = 99,
	setInitNetSort_enum                      = 100,
	ReachableNets_enum                       = 101,
	InitForwardPropagate_enum                = 102,
	SetRelatedPins_enum                      = 103,
	FindMaxLevel_enum                        = 104,
	GetRelatedPins_enum                      = 105,
	ComputeSlew_enum                         = 106,
	EvaluateLUT_enum                         = 107,
	ComputePinTiming_enum                    = 108,

	// functions from Backward Propagate class
	ComputeRATCritinCPPR_enum                = 109,
	ComputeRATNetBased_enum                  = 110,
	is_special_char2_enum                    = 112,
	ClearRAT_enum                            = 113,
	addWireDelayParam_enum                   = 114,
	addWireDelayParam_PreCPPR_enum           = 115,
	addIpinRatParam_enum                     = 116,
	UpdateWireDelayParam_enum                = 117,
	UpdateWireDelayParam_preCPPR_enum        = 118,
	Copyrat_enum                             = 119,
	ComputeSetupHoldRat_enum                 = 120,
	BackTimingNetBased_enum                  = 121,
	ComputeRATckPin_enum                     = 122,
	BackTimingInCPPR_enum                    = 123,
	ComputeRATinCPPR_enum                    = 124,
	BackTimingInc_enum                       = 125,
	ComputeRATInc_enum                       = 126,
	ComputePreCPPRSlack_enum                 = 129,
	ComputeSlackinCPPRPin_enum               = 130,
    ComputeSlackPin_enum                     = 131,

	// functions from CPPR class
	RunCPPR_enum                             = 132,
	buildSparseTable_enum                    = 133,
	pinCPPR_Adv_enum                         = 134,
	reportCPPR_Adv_enum                      = 135,
	getCredit_Adv_enum                       = 136,
	addCredit_Adv_enum                       = 137,
	slackUpdateIpin_Adv_enum                 = 138,
	slackUpdateOpin_Adv_enum                 = 139,
	BackTrace_Adv_enum                       = 140,
	TracePath_enum                           = 141,
	searchFlag_Adv_enum                      = 142,
	RunCPPR_Pin_enum                         = 143,
	getLCA_Adv_enum                          = 144,
	FrontTrace_Adv_enum                      = 145,

	// functions from Sizer class
	BuildFootPrintLib_enum                   = 146,
	RunSizer_enum                            = 147,
	FindLoadSlewViolations_enum              = 148,
	InitialSizer_enum                        = 149,
	ReadVersion_enum                         = 150,
	DumpVersion_enum                         = 151,
	SetVersion_enum                          = 152,
	SetVersion1_enum                         = 153,
	SetVersion2_enum                         = 154,
	LRSSolver_enum                           = 155,
	LDPSolver_enum                           = 156,
	UpdateLM_enum                            = 157,
	GateVersionSelect_enum                   = 158,
	UpdateLocalTiming_enum                   = 159,
	STA_enum                                 = 160,
	ClearRATSetFlag_enum                     = 161,
	IncrementalSTA_enum                      = 162,
	Incremental_AT_enum                      = 163,
	UpdateBestSoln_enum                      = 164,
	FindGamma_enum                           = 165,
	ComputeLocalSlack_enum                   = 166,
	CheckLocalSlack_enum                     = 167,
	LDCost_1_enum                            = 168,
	LDCost_2_enum                            = 169,
	FindLoadViolations_enum                  = 170,
	CheckLoadViolations_enum                 = 171,
	GetScaler_enum                           = 172,
	GetStateParams_enum                      = 173,
	SetTapAT_enum                            = 174,
	WriteBestSoln_enum                       = 175,
	PrintNetEstimates_enum                   = 176,
	check_KKTConditions_enum                 = 177,
	TimingRecovery_enum                      = 178,
	FindCriticalGates_enum                   = 179,
	PowerReduction_enum                      = 180,
	FPLibSanityCheck_enum                    = 181,
	DumpSoln_enum                            = 182,
	check_violations_enum                    = 183,
	GatePowerReduction_enum                  = 184,
	GateTimingRecovery_enum                  = 185

} FuncTime;




};
#endif
