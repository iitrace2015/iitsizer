/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#include "utility.h"
#include "include.h"
#include "globals.h"

namespace iit{

 // GLOBAL VARIABLES THAT ARE USED HERE
extern TimeIt g_Timer;
extern string Func_Names[NUM_FUNCTIONS];
extern double64 Func_Total_T[NUM_FUNCTIONS];

/**
 * \param myclock TimeIt variable for noting time instances
 *
 * Starts the timer
 */
void timeit_start(TimeIt *myclock)
{
    struct timeval tv;
    gettimeofday(&tv, 0);
    myclock->begin = tv;
}

/**
 * \param myclock TimeIt variable for noting time instances
 *
 * Stops the timer
 */
void timeit_stop(TimeIt *myclock)
{
    struct timeval tv;
    gettimeofday(&tv, 0);
    myclock->end = tv;
	myclock->time_spent = ((myclock->end.tv_sec-myclock->begin.tv_sec)*1000000 +
			                myclock->end.tv_usec-myclock->begin.tv_usec)*1.0/1000000;
}

/**
 * \param s comments passed to the function for time debugging
 *
 * Calls timeit_stop on global TimeIt variable g_Timer
 */
void debug_time(const string& s) {

	timeit_stop(&g_Timer);
	if (Verbose) std::cerr << "[" << std::setw(12) << (int)(g_Timer.time_spent) << "] " << s << std::endl;
}

/**
 * \param function enum for the required function
 *
 * Starts time for the function specified by the enum for it 'function'
 */
void debug_t_start(int function)
{

	timeit_start(&Var_TimeIt[function]);

	return;
}

/**
 * \param function enum for the required function
 *
 * Stops time for the function specified by the enum for it 'function'. Increments the total number
 * of calls by one
 */
void debug_t_stop(int function)
{
	timeit_stop(&Var_TimeIt[function]);

	Func_Total_T[function] += Var_TimeIt[function].time_spent;
	Func_Total_C[function] += 1;

	return;
}

/**
 * Fills the global array containing names of all the functions
 */
void init_enum_to_function_name (void)
{

	for( int i = 0; i < NUM_FUNCTIONS ; i ++ )
	{
		Func_Total_T[i] = 0;
		Func_Total_C[i] = 0;
	}

	Func_Names[init_lib_parser_enum]                    = "init_lib_parser  ";
	Func_Names[read_library_info_enum]                  = "read_library_info  ";
	Func_Names[read_LUT_template_enum]                  = "read_LUT_template  ";
	Func_Names[_begin_read_LUT_template_enum]           = "_begin_read_LUT_template  ";
	Func_Names[read_cell_info_enum]                     = "read_cell_info  ";
	Func_Names[read_line_as_tokens_enum]                = "read_line_as_tokens  ";
	Func_Names[_begin_read_lut_enum]                    = "_begin_read_lut  ";
	Func_Names[_begin_read_timing_info_enum]            = "_begin_read_timing_info  ";
	Func_Names[_begin_read_pin_info_enum]               = "_begin_read_pin_info  ";
	Func_Names[_begin_read_library_info_enum]           = "_begin_read_library_info  ";
	Func_Names[_begin_read_cell_info_enum]              = "_begin_read_cell_info  ";
	Func_Names[_close_enum]                             = "_close  ";
	Func_Names[is_special_char_enum]                    = "is_special_char  ";
	Func_Names[init_verilog_parser_enum]                = "init_verilog_parser  ";
	Func_Names[read_module_enum]                        = "read_module  ";
	Func_Names[read_primary_input_enum]                 = "read_primary_input  ";
	Func_Names[read_primary_output_enum]                = "read_primary_output  ";
	Func_Names[read_wire_enum]                          = "read_wire  ";
	Func_Names[read_cell_inst_enum]                     = "read_cell_inst  ";
	Func_Names[read_cell_inst2_enum]                    = "read_cell_inst2  ";
	Func_Names[fix_checkpoints_enum]                    = "fix_checkpoints  ";
	Func_Names[read_line_as_tokens2_enum]               = "read_line_as_tokens2  ";
	Func_Names[read_line_as_tokens3_enum]               = "read_line_as_tokens3  ";
	Func_Names[ProcessPI_enum]                          = "ProcessPI  ";
	Func_Names[ProcessNet_enum]                         = "ProcessNet  ";
	Func_Names[ProcessPO_enum]                          = "ProcessPO  ";
	Func_Names[is_special_char3_enum]                   = "is_special_char3  ";
	Func_Names[createNetHash_enum]                      = "createNetHash  ";
	Func_Names[Canonicalize_enum]                       = "Canonicalize  ";
	Func_Names[read_line_as_tokens_spef_enum]           = "read_line_as_tokens_spef  ";
	Func_Names[read_line_as_tokens_spef_res_enum]       = "read_line_as_tokens_spef_res  ";
	Func_Names[init_spef_parser_enum]                   = "init_spef_parser  ";
	Func_Names[read_net_dataInc_enum]                   = "read_net_dataInc  ";
	Func_Names[close_enum]                              = "close  ";
	Func_Names[read_connections_enum]                   = "read_connections  ";
	Func_Names[read_resistances_enum]                   = "read_resistances  ";
	Func_Names[read_capacitances_enum]                  = "read_capacitances  ";
	Func_Names[read_connectionsInc_enum]                = "read_connectionsInc  ";
	Func_Names[read_resistancesInc_enum]                = "read_resistancesInc  ";
	Func_Names[read_capacitancesInc_enum]               = "read_capacitancesInc  ";
	Func_Names[read_net_data_enum]                      = "read_net_data  ";
	Func_Names[FastTapdelaybeta_enum]                   = "FastTapdelaybeta  ";
	Func_Names[linker_enum]                             = "linker  ";
	Func_Names[read_timing_line_enum]                   = "read_timing_line  ";
	Func_Names[read_ops_as_tokens_enum]                 = "read_ops_as_tokens  ";
	Func_Names[init_ops_parser_enum]                    = "init_ops_parser  ";
	Func_Names[read_ops_as_tokens_slow_enum]            = "read_ops_as_tokens_slow  ";
	Func_Names[InplaceMerge_enum]                       = "InplaceMerge  ";
	Func_Names[ClearIncWireListDB_enum]                 = "ClearIncWireListDB  ";
	Func_Names[ComputeDBInc_enum]                       = "ComputeDBInc  ";
	Func_Names[ColorParentNets_enum]                    = "ColorParentNets  ";
	Func_Names[linkerInc_enum]                          = "linkerInc  ";
	Func_Names[FastTapdelaybetaInc_enum]                = "FastTapdelaybetaInc  ";
	Func_Names[DisconnectPin_enum]                      = "DisconnectPin  ";
	Func_Names[ConnectPin_enum]                         = "ConnectPin  ";
	Func_Names[RepowerGate_enum]                        = "RepowerGate  ";
	Func_Names[ReadSpef_enum]                           = "ReadSpef  ";
	Func_Names[RemoveNet_enum]                          = "RemoveNet  ";
	Func_Names[CreateNet_enum]                          = "CreateNet  ";
	Func_Names[RemoveGate_enum]                         = "RemoveGate  ";
	Func_Names[InsertGate_enum]                         = "InsertGate  ";
	Func_Names[InitOperations_enum]                     = "InitOperations  ";
	Func_Names[ReportOperations_enum]                   = "ReportOperations  ";
	Func_Names[timeit_start_enum]                       = "timeit_start  ";
	Func_Names[timeit_stop_enum]                        = "timeit_stop  ";
	Func_Names[debug_time_enum]                         = "debug_time  ";
	Func_Names[ComputeHash_enum]                        = "ComputeHash  ";
	Func_Names[split_enum]                              = "split  ";
	Func_Names[Bsearch_enum]                            = "Bsearch  ";
	Func_Names[Bsearch_iPin_enum]                       = "Bsearch_iPin  ";
	Func_Names[Threshold_enum]                          = "Threshold  ";
	Func_Names[Engine_enum]                             = "Engine  ";
	Func_Names[BuildCircuit_enum]                       = "BuildCircuit  ";
	Func_Names[RunCircuit_enum]                         = "RunCircuit  ";
	Func_Names[test_lib_parser_enum]                    = "test_lib_parser  ";
	Func_Names[test_timing_parser_enum]                 = "test_timing_parser  ";
	Func_Names[test_ops_parser_enum]                    = "test_ops_parser  ";
	Func_Names[test_spef_parser_enum]                   = "test_spef_parser  ";
	Func_Names[test_verilog_parser_enum]                = "test_verilog_parser  ";
	Func_Names[read_tau2015_as_tokens_enum]             = "read_tau2015_as_tokens  ";
	Func_Names[is_special_char_tau2015_enum]            = "is_special_char_tau2015  ";
	Func_Names[DumpResults_enum]                        = "DumpResults  ";
	Func_Names[FrontColorPin_enum]                      = "FrontColorPin  ";
	Func_Names[quickReport_enum]                        = "quickReport  ";
	Func_Names[quickReportPin_enum]                     = "quickReportPin  ";
	Func_Names[ReportWorstPaths_enum]                   = "ReportWorstPaths  ";
	Func_Names[ReportAT_enum]                           = "ReportAT  ";
	Func_Names[ReportRAT_enum]                          = "ReportRAT  ";
	Func_Names[ReportSlack_enum]                        = "ReportSlack  ";
	Func_Names[TopSort_enum]                            = "TopSort  ";
	Func_Names[Init_L_enum]                             = "Init_L  ";
	Func_Names[ReClear_enum]                            = "ReClear  ";
	Func_Names[TopSortInc_enum]                         = "TopSortInc  ";
	Func_Names[ATSlew_enum]                             = "ATSlew  ";
	Func_Names[BFSInc_enum]                             = "BFSInc  ";
	Func_Names[setInitNetSort_enum]                     = "setInitNetSort  ";
	Func_Names[ReachableNets_enum]                      = "ReachableNets  ";
	Func_Names[InitForwardPropagate_enum]               = "InitForwardPropagate  ";
	Func_Names[SetRelatedPins_enum]                     = "SetRelatedPins  ";
	Func_Names[FindMaxLevel_enum]                       = "FindMaxLevel  ";
	Func_Names[GetRelatedPins_enum]                     = "GetRelatedPins  ";
	Func_Names[ComputeSlew_enum]                        = "ComputeSlew  ";
	Func_Names[EvaluateLUT_enum]                        = "EvaluateLUT  ";
	Func_Names[ComputePinTiming_enum]                   = "ComputePinTiming  ";
	Func_Names[ComputeRATCritinCPPR_enum]               = "ComputeRATCritinCPPR  ";
	Func_Names[ComputeRATNetBased_enum]                 = "ComputeRATNetBased  ";
	Func_Names[is_special_char2_enum]                   = "is_special_char2  ";
	Func_Names[ClearRAT_enum]                           = "ClearRAT  ";
	Func_Names[addWireDelayParam_enum]                  = "addWireDelayParam  ";
	Func_Names[addWireDelayParam_PreCPPR_enum]          = "addWireDelayParam_PreCPPR  ";
	Func_Names[addIpinRatParam_enum]                    = "addIpinRatParam  ";
	Func_Names[UpdateWireDelayParam_enum]               = "UpdateWireDelayParam  ";
	Func_Names[UpdateWireDelayParam_preCPPR_enum]       = "UpdateWireDelayParam_preCPPR  ";
	Func_Names[Copyrat_enum]                            = "Copyrat  ";
	Func_Names[ComputeSetupHoldRat_enum]                = "ComputeSetupHoldRat  ";
	Func_Names[BackTimingNetBased_enum]                 = "BackTimingNetBased  ";
	Func_Names[ComputeRATckPin_enum]                    = "ComputeRATckPin  ";
	Func_Names[BackTimingInCPPR_enum]                   = "BackTimingInCPPR  ";
	Func_Names[ComputeRATinCPPR_enum]                   = "ComputeRATinCPPR  ";
	Func_Names[BackTimingInc_enum]                      = "BackTimingInc  ";
	Func_Names[ComputeRATInc_enum]                      = "ComputeRATInc  ";
	Func_Names[ComputePreCPPRSlack_enum]                = "ComputePreCPPRSlack  ";
	Func_Names[ComputeSlackinCPPRPin_enum]              = "ComputeSlackinCPPRPin  ";
	Func_Names[ComputeSlackPin_enum]                    = "ComputeSlackPin  ";
	Func_Names[RunCPPR_enum]                            = "RunCPPR  ";
	Func_Names[buildSparseTable_enum]                   = "buildSparseTable  ";
	Func_Names[pinCPPR_Adv_enum]                        = "pinCPPR_Adv  ";
	Func_Names[reportCPPR_Adv_enum]                     = "reportCPPR_Adv  ";
	Func_Names[getCredit_Adv_enum]                      = "getCredit_Adv  ";
	Func_Names[addCredit_Adv_enum]                      = "addCredit_Adv  ";
	Func_Names[slackUpdateIpin_Adv_enum]                = "slackUpdateIpin_Adv  ";
	Func_Names[slackUpdateOpin_Adv_enum]                = "slackUpdateOpin_Adv  ";
	Func_Names[BackTrace_Adv_enum]                      = "BackTrace_Adv  ";
	Func_Names[TracePath_enum]                          = "TracePath  ";
	Func_Names[searchFlag_Adv_enum]                     = "searchFlag_Adv  ";
	Func_Names[RunCPPR_Pin_enum]                        = "RunCPPR_Pin  ";
	Func_Names[getLCA_Adv_enum]                         = "getLCA_Adv  ";
	Func_Names[FrontTrace_Adv_enum]                     = "FrontTrace_Adv  ";

	return;

}

/**
 * \param s Input string
 * \param delim Delimiter at which the string needs to be split
 * \param elems Result of the splitting performed on the given string 's'
 *
 * Splits the string using given delimiter and returns the set of tokens
 */
deque<string>& split(const string &s, char delim, deque<string> &elems)
{
    stringstream ss(s);
    string item;

    while (getline(ss, item, delim))
    {
        elems.push_back(item);
    }

    return elems;
}

/**
 * \param s Input string
 * \param delim Delimiter at which the string needs to be split
 *
 * Initializes the operation to split the string using given delimiter and returns the set of tokens
 */
deque<string> split(const string &s, char delim)
{
    deque<string> elems;
    split(s, delim, elems);
    return elems;
}

/**
 * \param numPaths Number of paths
 *
 * Computes threshold of additional paths to be reported apart from what is actually asked to report
 * in order to take into account differences due to rise/fall transition paths with same slack
 */
int Threshold(int64 numPaths)
{
	int threshold =0;
	if      (numPaths <100)                       threshold = 20;
	else if (numPaths>=100 && numPaths <1000)     threshold = 50;
	else if (numPaths>= 1000 && numPaths <=10000) threshold = 70;
	else if (numPaths > 10000)                    threshold = 100;
	return threshold;

}

/**
 * \param sorted_vec Sorted list of elements
 * \param key Hash key to be searched
 * \param start Starting point in sorted_vec for searching
 * \param end End point in sorted_vec for searching
 *
 * Searches for given input-pin hash in the vector- Version of Bsearch (binary search)
 */
int64 Bsearch_iPin (vector <InstanceIpin*> sorted_vec, int64 key, int64 start, int64 end)
{

   if(sorted_vec.size() == 0)
	   return -1;

   int64 mid, left = start ;
   int64 right     = end + 1; // one position passed the right end

   while (left < right)
   {
      mid = left + (right - left)/2;

      if (key > sorted_vec[mid]->tap->hash)
          left = mid+1;
      else if (key < sorted_vec[mid]->tap->hash)
    	  right = mid;
      else
    	  return mid;
   }

   return -1;
}

/**
 * \param _dbgFile Debug file name
 * \param _sort Set of nets
 *
 * Reports AT/RAT/Slack/Delay/Slew at input and outputs of each net from _sort for debugging purposes
 */
void PrintNetEstimates(ofstream& _dbgFile, vector <SpefNet*>& _sort)
{
	_dbgFile << "\nPrinting Net Estimates\n" << endl;
	for (int64 i = 0; i < (int64)_sort.size(); i++)
	{
		if(_sort[i]->isVirtualNet)
			continue;
		if(_sort[i]->isIncluded == false)
		{
			_dbgFile << "NetName\t" << _sort[i]->netName << " is not included" << endl;
			continue;
		}

		_dbgFile << "NetName\t" << _sort[i]->netName << "\tNo. of taps = " << _sort[i]->taps.size() << endl;
		InstanceOpin* oPin = _sort[i]->port.pinPtr;
		_dbgFile << endl;

		_dbgFile << "Port -------------------------------------> " << oPin->name.icellName << ":" << oPin->name.pinName << "\tCeff\t" << oPin->ceffLate << endl;
		_dbgFile << "Slew ----->" << "\t" << oPin->tData.slewRiseEarly << "\t\t" << oPin->tData.slewFallEarly << "\t\t" << oPin->tData.slewRiseLate << "\t\t" << oPin->tData.slewFallLate << endl;

		if (oPin->cellPtr->cellLibIndex_l == -2 || oPin->cellPtr->cellLibIndex_l == -4)
		{
			_dbgFile << "Port Pin is PI" << endl;
			_dbgFile << "AT  ------->" << "\t" << oPin->tData.atRiseEarly << "\t\t" << oPin->tData.atFallEarly << "\t\t" << oPin->tData.atRiseLate << "\t\t" << oPin->tData.atFallLate << endl;
			_dbgFile << "RAT ------->" << "\t" << oPin->tData.ratRiseEarly << "\t\t" << oPin->tData.ratFallEarly << "\t\t" << oPin->tData.ratRiseLate << "\t\t" << oPin->tData.ratFallLate << endl;
			_dbgFile << "SLK ------->" << "\t" << oPin->tData.slackRiseEarly << "\t\t" << oPin->tData.slackFallEarly << "\t\t" << oPin->tData.slackRiseLate << "\t\t" << oPin->tData.slackFallLate << endl;
		}
		else if (oPin->cellPtr->info_late->isSequential == true)
		{
			InstanceIpin* iPin = &(oPin->cellPtr->ckList[0]);
			_dbgFile << "Port Pin is FF oPin, ckPin -> " << iPin->name.icellName << ":" << iPin->name.pinName << endl;
			_dbgFile << "AT  ------->" << "\t" << oPin->tData.atRiseEarly << "\t\t" << oPin->tData.atFallEarly << "\t\t" << oPin->tData.atRiseLate << "\t\t" << oPin->tData.atFallLate << endl;
			_dbgFile << "RAT ------->" << "\t" << oPin->tData.ratRiseEarly << "\t\t" << oPin->tData.ratFallEarly << "\t\t" << oPin->tData.ratRiseLate << "\t\t" << oPin->tData.ratFallLate << endl;
			_dbgFile << "SLK ------->" << "\t" << oPin->tData.slackRiseEarly << "\t\t" << oPin->tData.slackFallEarly << "\t\t" << oPin->tData.slackRiseLate << "\t\t" << oPin->tData.slackFallLate << endl;
			_dbgFile << "Delay ---->" << "\t" << oPin->dData[0].delayRiseEarly << "\t\t" << oPin->dData[0].delayFallEarly << "\t\t" << oPin->dData[0].delayRiseLate << "\t\t" << oPin->dData[0].delayFallLate << endl;
		}
		else
		{
			_dbgFile << "No of Related iPins = " << oPin->relPinIndex.size() << endl;
			_dbgFile << endl;
			_dbgFile << "AT  ------->" << "\t" << oPin->tData.atRiseEarly << "\t\t" << oPin->tData.atFallEarly << "\t\t" << oPin->tData.atRiseLate << "\t\t" << oPin->tData.atFallLate << endl;
			_dbgFile << "RAT ------->" << "\t" << oPin->tData.ratRiseEarly << "\t\t" << oPin->tData.ratFallEarly << "\t\t" << oPin->tData.ratRiseLate << "\t\t" << oPin->tData.ratFallLate << endl;
			_dbgFile << "SLK ------->" << "\t" << oPin->tData.slackRiseEarly << "\t\t" << oPin->tData.slackFallEarly << "\t\t" << oPin->tData.slackRiseLate << "\t\t" << oPin->tData.slackFallLate << endl;

			for (int64 j = 0; j < (int64)oPin->dData.size(); j++)
			{
				InstanceIpin* iPin = &(oPin->cellPtr->iipList[oPin->dData[j].index]);
				_dbgFile << "iPin-> " << iPin->name.icellName << ":" << iPin->name.pinName << endl;
				_dbgFile << "Delay ---->" << "\t" << oPin->dData[j].delayRiseEarly << "\t\t" << oPin->dData[j].delayFallEarly << "\t\t" << oPin->dData[j].delayRiseLate << "\t\t" << oPin->dData[j].delayFallLate << endl;
				_dbgFile << "Slew ---->" << "\t" << iPin->tData.slewRiseEarly << "\t\t" << iPin->tData.slewFallEarly << "\t\t" << iPin->tData.slewRiseLate << "\t\t" << iPin->tData.slewFallLate << endl;
			}
		}
		_dbgFile << endl;
		for (int64 k = 0; k < (int64)_sort[i]->taps.size(); k++)
		{
			InstanceIpin* iPin = _sort[i]->taps[k].pinPtr;
				_dbgFile << "Tap ----------------> " << iPin->name.icellName << ":" << iPin->name.pinName <<" "<<" tap cap including pin cap: "<<_sort[i]->taps[k].capacitanceLate<<" "<<"delay: "<<_sort[i]->taps[k].delayLate<< endl;
				_dbgFile << "---------->" << "\tRE\t" << "\tFE\t" << "\tRL\t" << "\tFL\t" << endl;
				_dbgFile << "AT ------->" << "\t" << iPin->tData.atRiseEarly << "\t\t" << iPin->tData.atFallEarly << "\t\t" << iPin->tData.atRiseLate << "\t\t" << iPin->tData.atFallLate << endl;
				_dbgFile << "RAT ------->" << "\t" << iPin->tData.ratRiseEarly << "\t\t" << iPin->tData.ratFallEarly << "\t\t" << iPin->tData.ratRiseLate << "\t\t" << iPin->tData.ratFallLate << endl;
				_dbgFile << "SLK ------->" << "\t" << iPin->tData.slackRiseEarly << "\t\t" << iPin->tData.slackFallEarly << "\t\t" << iPin->tData.slackRiseLate << "\t\t" << iPin->tData.slackFallLate << endl;
				_dbgFile << "Delay ----->" << "\t" << iPin->tap->delayEarly << "\t\t" << iPin->tap->delayEarly << "\t\t" << iPin->tap->delayLate << "\t\t" << iPin->tap->delayLate << endl;
				_dbgFile << "M1    ----->" << "\t" << iPin->tap->m1_E << "\t\t" << iPin->tap->m1_E << "\t\t" << iPin->tap->m1_L << "\t\t" << iPin->tap->m1_L << endl;
				_dbgFile << "M2    ----->" << "\t" << iPin->tap->m2_E << "\t\t" << iPin->tap->m2_E << "\t\t" << iPin->tap->m2_L << "\t\t" << iPin->tap->m2_L << endl;
				_dbgFile << "Slew  ----->" << "\t" << iPin->tData.slewRiseEarly << "\t\t" << iPin->tData.slewFallEarly << "\t\t" << iPin->tData.slewRiseLate << "\t\t" << iPin->tData.slewFallLate << endl;
		}

		_dbgFile << endl;
		_dbgFile << endl;
	}

}

};

