/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_TIMEIT_H_
#define IIT_TIMEIT_H_

#include "typedef.h"
#include <time.h>
#include <sys/time.h>

/**
 * Element type to keep track of time spent on running the program (runtime)
 */
typedef class
{

public:

    class timeval  begin, end;     ///< Timestamps at beginning and at the end of an operation
    double64       time_spent;     ///< Total time spent in seconds between 'begin' and 'end'
    
} TimeIt;

#endif /* _TIMEIT_H_*/
