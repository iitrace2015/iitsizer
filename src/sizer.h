/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_SIZER_H_
#define IIT_SIZER_H_

#include "include.h"
#include "classdecs.h"
#include "cppr.h"
#include "forward_propagate.h"
#include "report.h"
#include "backward_propagate.h"
#include "operations.h"
#include "timer.h"
#include "spef.h"

namespace iit{

/**
 * Set of functions and variables for sizing engine. Element type of the sizer class. Timer class has
 * been inherited publicly. For inheriting template and inline functions from a base class (Timer in this case),
 * define and declare these functions in header files of base class (not in cpp).
 * Here, inherited Timer class publicly.
 * Public members of Timer are now public members of sizer.
 * Private members of Timer can never be inherited.
 * Protected members of Timer become protected in Sizer when Timer is inherited publicly
 */
class  Sizer: public Timer
{

public:

	Sizer();
	~Sizer();

	void Engine(int args, char** argv);

private:

	SolutionState _prevSoln;                               ///< Stores previous solution state
	SolutionState _currSoln;                               ///< Stores current solution state of the circuit
	SolutionState _bestSoln;                               ///< Stores the best of all solutions
	ofstream      _dbgFile;                                ///< Output file stream- debugging
	ofstream      _verFile;                                ///< Output file stream- version dump
	ofstream      _cmdFile;                                ///< Output file stream- commands
	ofstream      _ceffFile;                               ///< Output file stream- Effective capacitance dump
	ifstream      _inpFile;                                ///< Input file stream - Any version of the circuit
	int64         _numIters;                               ///< Count of iterations in LDP
	SpefParser*   _sp;                                     ///< Pointer to instance of a SpefParser class
	double64      _wtLD;                                   ///< Weight of LD in total cost function
	double64      _wtLkg;                                  ///< Weight of leakage in total cost function
	int64         _cellModsUp;                             ///< Number of cells upsized
	int64         _cellModsDown;                           ///< Number of cells downsized
	int64         _totalViolations;                        ///< Total violation count
	TimeIt        _time;                                   ///< Runtime measure
	std::tr1::unordered_map <int,int> _targetLRSSlackDir;  ///< Target slack directory- for LDP iterations
	vector  < SpefNet* >              _sort;               ///< List of nets in ordered manner- topology fixed throughout
	vector  < InstanceIpin* >         _coneEndPoints;      ///< Set of Cone-end-points in the design


	void              BuildFootprintLib              (void);
	void              WriteBestSoln                  (void);
	void              PrintNetEstimates              (void);
	bool              check_KKTConditions            (void);
	void              PowerReduction                 (void);
	void              FPLibSanityCheck               (void);
	void              DumpSoln                       (void);
	void              LRSSolver                      (void);
	void              LDPSolver                      (void);
	bool              check_violations 				 (void);
	void              STA                            (void);
	void              ClearRATSetFlag                (void);
	void              UpdateBestSoln                 (void);
	void              InitialSizer                   (void);
	void              RunSizer                       (void);
	SolutionState     GetStateParams                 (void);
	deque <Instance*> FindCriticalGates              (void);
	void              ReadVersion                    (int numIter);
	void              DumpVersion                    (int iterCnt);
	void              UpdateLM                       (double64 k);
	void              GateVersionSelect              (Instance* gate);
	void              IncrementalSTA                 (Instance* gate);
	void              IncrementalAT                  (Instance* gate);
	double64          ComputeLocalSlack              (Instance* gate);
	void              SetTapAT                       (SpefNet* currNet);
	double64          FindGamma                      (double64 worstSlack);
	bool              TimingRecovery                 (double64 targetTNS = 0.0);
	double64          GetScaler                      (double64 slack, double64 k);
	void              SetVersion                     (Instance* gate, GateVersionInfo* version);
	void              SetVersion1                    (Instance* gate, GateVersionInfo* version);
	void              SetVersion2                    (Instance* gate, GateVersionInfo* version);
	bool              FindLoadViolations             (LibParserCellInfo* version, Instance* gate, double64 alpha);
	bool              FindLoadSlewViolations         (LibParserCellInfo* version, Instance* gate, bool slewMode, double64 alpha = ALPHA);
	bool              CheckLoadViolations 	         (LibParserCellInfo* version, Instance* gate, double64 alpha);
	void              GatePowerReduction 			 (Instance* gate, GateVersionInfo* prevVersion,
			                                          int64& changeCnt);
	double64          ComputeLocalSlack              (Instance* gate, vector < SpefNet* >& drivingNets,
	                                                  vector < SpefNet* >& drainNets);
	bool 	          CheckLocalSlack                (Instance* gate, vector < SpefNet* >& drivingNets,
	                                                  vector < SpefNet* >& drainNets, double64 cutoff);
	double64          LDCost_1                       (Instance* gate, vector < SpefNet* >& drivingNets,
			                                          vector < std::pair<SpefTap*, int> >& sideTaps, vector < std::pair<SpefTap*, int> >& sinkTaps);
	double64          LDCost_2                       (Instance* gate, vector < SpefNet* >& drivingNets,
			                                          vector < std::pair<SpefTap*, int> >& sideTaps, vector < std::pair<SpefTap*, int> >& sinkTaps);
	int               GateTimingRecovery			 (Instance* &wGate, LibParserFPInfo* &fp, int64& idVth, int64& idArea,
												      deque <Instance*>& criticalGates, int& idx, double64& prevTNS);
	void              UpdateLocalTiming              (Instance* gate, vector < SpefNet* >& drivingNets,
			                                          vector < std::pair<SpefTap*, int> >& sideTaps,
												      vector < std::pair<SpefTap*, int> >& sinkTaps,
												      vector < SpefNet* >& drainNets);

};

}

#endif
