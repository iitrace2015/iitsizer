/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#include "timer.h"
#include "include.h"
#include "spef.h"
#include "net.h"
#include "utility.h"
#include "globals.h"
namespace iit {

/* Global variables declaration --------------------------------------- */

//extern CelllibStorage      celllibstorage_e;
//extern CelllibStorage      celllibstorage_l;
//extern LibFileInfo         libinfo_e;
//extern LibFileInfo         libinfo_l;
extern CompleteTimingInfo  timing;

//extern Instance* CLKSRC;
//extern int       numInc;
//extern int       Verbose;
//extern int64     MaxNumPaths;
//extern int64     PathFlag_G;
//extern string    BenchmarkDir;
//extern bool     _isStaticDone;

extern deque   < Instance >       InstanceList;
extern vector  < Hash_Instance >  InstanceHashTable;
extern deque   < SpefNet >        SpefNetList;
extern vector  < Hash_Instance >  spefNetHashTable;
//extern vector  < SpefNet* >       Sort;
//extern vector  < SpefNet* >       IncNetList;
extern deque   < SpefNet >        WireList;
//extern queue   < SpefNet* >       InitNetList;
//extern vector  < Instance* >      PIList;
//extern vector  < Instance* >      POList;
//extern vector  < InstanceIpin* >  ConeEndPoints;
//extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
//extern deque   < Slack_Instance > pathLUTSlackTable_Adv;

//extern std::tr1::unordered_map <char,bool> specCharLibDir;

/* End - global variables declaration -----------------------------------*/

/**
 * \param is Input .spef file stream
 * \param tokens Set of words in a line (separated by specified delimiters)
 * \result Returns true if tokens is not empty
 *
 * Reads from the input stream and splits a line into a set of words separated by delimiters
 */
inline bool SpefParser::read_line_as_tokens_spef (istream& is, deque<string>& tokens)
{
	tokens.clear();
	string line;
	std::getline (is,line);

	while (is && tokens.empty())
	{
		char* temp = strdup(line.c_str());
		char* tokenPtr;

		// Initialize the string tokenizer and receive pointer to first token
		tokenPtr = strtok(temp, " (),;:/#[]*\"\\");
		while(tokenPtr != NULL)
		{
			tokens.push_back(tokenPtr);
			tokenPtr = strtok(NULL, " (),;:/#[]*\"\\");
		}
		free(temp);

		// Previous line read was empty. Read the next one.
		if (tokens.empty())
			std::getline (is, line) ;
	}
	return !tokens.empty();
}

/**
 * \param is Input .spef file stream
 * \param tokens Set of words in a line (separated by specified delimiters)
 * \result Returns true if tokens is not empty
 *
 * Reads from the input stream and splits a line into a set of words separated by delimiters
 * Delimiters especially tailored for reading *RES section of .spef
 */
inline bool SpefParser::read_line_as_tokens_spef_res (istream& is, deque<string>& tokens)
{
	tokens.clear();
	string line;
	std::getline (is,line);

	while (is && tokens.empty())
	{
		char* temp = strdup(line.c_str());
		char*tokenPtr;

		// Initialize the string tokenizer and receive pointer to first token
		tokenPtr = strtok(temp, " (),;/#[]*\"\\");
		while(tokenPtr != NULL)
		{
			tokens.push_back(tokenPtr);
			tokenPtr = strtok(NULL, " (),;/#[]*\"\\");
		}
		free(temp);
		// Previous line read was empty. Read the next one.
		if (tokens.empty())
			std::getline (is, line) ;
	}
	return !tokens.empty();
}

/**
 * \param portNode Stores port information (by reference)
 * \param tapNode Stores information relating to all taps of a net (by reference)
 * \param spefNet Stores net information (by reference)
 * \param saveRC Flag to tell if storing RC parasitics tree needed (true) or not (false)
 * \result bool Return value indicates whether the *CONN section of .spef has been successfully read or not
 *
 * Read and stores connections (*CONN) information from .spef file
 */
bool SpefParser::read_connections (Cnode& portNode, vector <Cnode>& tapNode, SpefNet& spefNet,
		                             bool& saveRC)
{
	deque<string> tokens ;
	bool valid = read_line_as_tokens_spef (is, tokens);
	int64 tokensz= tokens.size();
	string tempstr;
	double64 ceffEarly = spefNet.netLumpedCap;
	double64 ceffLate = spefNet.netLumpedCap;

	while (valid && !(tokensz == 1 && tokens[0] == "CONN"))
	{
		valid = read_line_as_tokens_spef (is, tokens);
		tokensz = tokens.size();
	}

	while (valid)
	{
		valid = read_line_as_tokens_spef (is, tokens);
		tokensz = tokens.size();
		if (tokensz == 1 && tokens[0] == "CAP")
			break ; 								// The beginning of the next section

		if (tokensz < 3) continue;
		SpefTap itap;
		itap.clear();
		Cnode tempTap;
		tempTap.clear();
		int64 temp;

		if (tokens[0] == "P" && tokensz == 3)
		{
    		if (tokens[2] == "I")
    		{
    			spefNet.port.portName.n1 = tokens[1];
    			spefNet.port.portName.n2 = "";
    			spefNet.port.hash = ComputeHash(tokens[1]);
    			spefNet.port.portName.pinid = spefNet.port.hash;
    			spefNet.port.portType = 'P';
    			spefNet.port.capacitanceEarly = 0.0;
    			spefNet.port.capacitanceLate = 0.0;
    			portNode.clear();
    			portNode.nodename = spefNet.port.portName;
    			portNode.hash = spefNet.port.hash;
    			portNode.capacitanceEarly = spefNet.port.capacitanceEarly;
    			portNode.capacitanceLate = spefNet.port.capacitanceLate;
    		}
    		else if (tokens[2] == "O")
    		{
    			itap.tapName.n1 = tokens[1];
    			itap.tapName.pinid = ComputeHash(tokens[1]);
    			itap.tapName.n2 = "";
    			itap.hash = itap.tapName.pinid;
    			itap.tapType = 'P';
    			temp = Bsearch(timing.loadcap, itap.hash, 0, timing.loadcap.size()-1);
    			if (temp == -1)
    			{
    				itap.capacitanceEarly = 0 ;
    				itap.capacitanceLate = 0 ;
    			}
    			else
    			{
    				itap.capacitanceEarly = timing.loadcap[temp].earlycap;
    				itap.capacitanceLate = timing.loadcap[temp].latecap;
    			}
    			ceffEarly += itap.capacitanceEarly;
    			ceffLate += itap.capacitanceLate;
    			spefNet.taps.push_back(itap) ;
    			tempTap.nodename = itap.tapName;
    			tempTap.hash = itap.hash;
    			tempTap.capacitanceEarly = itap.capacitanceEarly;
    			tempTap.capacitanceLate = itap.capacitanceLate;
    			tapNode.push_back(tempTap);
    		}
    		else
    		{
    			continue;
    		}
    	}

  	  else if (tokensz == 4 && tokens[0] == "I")
  	  {
  		  int64 hashCell = ComputeHash(tokens[1]);
  		  temp = Bsearch(InstanceHashTable, hashCell, 0, InstanceHashTable.size()-1);
  		  int64 cellIndex, pinIndex;
  		  int64 tempHash = ComputeHash(tokens[2]);
  		  if (temp == -1)
  		  {
  			  continue;
  		  }
  		  cellIndex = InstanceHashTable[temp].index;
  		  if (InstanceList[cellIndex].repowerFlag == true)
  			  saveRC = true;
  		  if (tokens[3] == "O")
  		  {
    			spefNet.port.portName.n1 = tokens[1];
    			spefNet.port.portName.n2 = tokens[2];
    			tempstr = tokens[1];
    			tempstr = tempstr.append(1,':');
    			spefNet.port.hash = ComputeHash(tempstr.append(tokens[2]));
    			spefNet.port.portName.pinid = hashCell;
				spefNet.port.portType = 'I';

				temp = Bsearch(InstanceList[cellIndex].info_early->opinhashtable, tempHash , 0,
						         InstanceList[cellIndex].info_early->opinhashtable.size() - 1);
				if (temp == -1)
					continue;
				pinIndex = InstanceList[cellIndex].info_early->opinhashtable[temp].index;
				spefNet.port.capacitanceEarly = InstanceList[cellIndex].info_early->opins[pinIndex].capacitance;

				temp = Bsearch(InstanceList[cellIndex].info_late->opinhashtable, tempHash , 0,
						         InstanceList[cellIndex].info_late->opinhashtable.size() - 1);
				if (temp == -1)
					continue;
				pinIndex = InstanceList[cellIndex].info_late->opinhashtable[temp].index;
				spefNet.port.capacitanceLate = InstanceList[cellIndex].info_late->opins[pinIndex].capacitance;
				portNode.clear();
				portNode.nodename = spefNet.port.portName;
    			portNode.hash = spefNet.port.hash;
    			portNode.capacitanceEarly = spefNet.port.capacitanceEarly;
    			portNode.capacitanceLate = spefNet.port.capacitanceLate;
        	}
			else if (tokens[3] == "I")
			{
				itap.tapName.n1 = tokens[1];
				itap.tapName.pinid = hashCell;
				itap.tapName.n2 = tokens[2];
    			tempstr = tokens[1];
    			tempstr = tempstr.append(1,':');
				itap.hash = ComputeHash(tempstr.append(tokens[2]));
				itap.tapType = 'I';
				temp = Bsearch(InstanceList[cellIndex].info_early->ipinhashtable, tempHash , 0,
						        InstanceList[cellIndex].info_early->ipinhashtable.size() - 1);
				if (temp == -1)
		  		{
					temp = Bsearch(InstanceList[cellIndex].info_early->clkpin, tempHash , 0,
							        InstanceList[cellIndex].info_early->clkpin.size() - 1);
					if (temp == -1)
						continue;
					else
					{
						pinIndex = 0;
						itap.capacitanceEarly = InstanceList[cellIndex].info_early->clkpin[pinIndex].capacitance;
						ceffEarly += itap.capacitanceEarly;
					}
		  		}
				else
				{
					pinIndex = InstanceList[cellIndex].info_early->ipinhashtable[temp].index;
					itap.capacitanceEarly = InstanceList[cellIndex].info_early->ipins[pinIndex].capacitance;
					ceffEarly += itap.capacitanceEarly;
				}

				temp = Bsearch(InstanceList[cellIndex].info_late->ipinhashtable, tempHash , 0,
						        InstanceList[cellIndex].info_late->ipinhashtable.size() - 1);
				if (temp == -1)
		  		{
					temp = Bsearch(InstanceList[cellIndex].info_late->clkpin, tempHash , 0,
							        InstanceList[cellIndex].info_late->clkpin.size() - 1);
					if (temp == -1)
						continue;
					else
					{
						pinIndex = 0;
						itap.capacitanceLate = InstanceList[cellIndex].info_late->clkpin[pinIndex].capacitance;
						ceffLate += itap.capacitanceLate;
					}
		  		}
				else
				{
					pinIndex = InstanceList[cellIndex].info_late->ipinhashtable[temp].index;
					itap.capacitanceLate = InstanceList[cellIndex].info_late->ipins[pinIndex].capacitance;
					ceffLate += itap.capacitanceLate;
				}
				tempTap.nodename = itap.tapName;
    			tempTap.hash = itap.hash;
    			tempTap.capacitanceEarly = itap.capacitanceEarly;
    			tempTap.capacitanceLate = itap.capacitanceLate;
    			tapNode.push_back(tempTap);
    			spefNet.taps.push_back(itap);
			}
			else
			{
				continue;
			}
  	  }
  	  else
  	  {
			continue;
  	  }
	}
	spefNet.port.ceffEarly = ceffEarly;
	spefNet.port.ceffLate = ceffLate;
	sort(spefNet.taps.begin(),spefNet.taps.end(), CompareByHashnum<SpefTap>());
	sort(tapNode.begin(),tapNode.end(), CompareByHashnum<Cnode>());

	return true ;
}

/**
 * \param portNode Stores port information (by reference)
 * \param tapNode Stores information relating to all taps of a net (by reference)
 * \param spefNet Stores net information (by reference)
 * \param saveRC Flag to tell if storing RC parasitics tree needed (true) or not (false)
 * \result bool Return value indicates whether the *CONN section of .spef has been successfully read or not
 *
 * Read and stores connections (*CONN) information from .spef file during incremental changes
 */
bool SpefParser::read_connectionsInc (Cnode& portNode, vector <Cnode>& tapNode, SpefNet& spefNet,
		                                bool& saveRC)
{
	deque<string> tokens ;
	bool valid = read_line_as_tokens_spef (is, tokens);
	int64 tokensz= tokens.size();
	string tempstr;
	double64 ceffEarly = spefNet.netLumpedCap;
	double64 ceffLate = spefNet.netLumpedCap;

	// Skip the lines that are not "*CONN"
	while (valid && !(tokensz == 1 && tokens[0] == "CONN"))
	{
		valid = read_line_as_tokens_spef (is, tokens);
		tokensz = tokens.size();
	}

	while (valid)
	{
		valid = read_line_as_tokens_spef (is, tokens);
		tokensz = tokens.size();
		if (tokensz == 1 && tokens[0] == "CAP")
			break ; 											// The beginning of the next section

		// Line format: "*nodeType nodeName direction"
		// Note that nodeName can be either a single token or 3 tokens

		if (tokensz < 3)
			continue;
		SpefTap itap;
		itap.clear();
		Cnode tempTap;
		tempTap.clear();
		int64 temp;

		//Following lines check to differentiate port and taps
		if (tokens[0] == "P" && tokensz == 3)
		{
    		if (tokens[2] == "I")
    		{
    			spefNet.port.portName.n1 = tokens[1];
    			spefNet.port.portName.n2 = "";
    			spefNet.port.hash = ComputeHash(tokens[1]);
    			spefNet.port.portName.pinid = spefNet.port.hash;
    			spefNet.port.portType = 'P';
    			spefNet.port.capacitanceEarly = 0.0;
    			spefNet.port.capacitanceLate = 0.0;
    			portNode.clear();
    			portNode.nodename = spefNet.port.portName;
    			portNode.hash = spefNet.port.hash;
    			portNode.capacitanceEarly = spefNet.port.capacitanceEarly;
    			portNode.capacitanceLate = spefNet.port.capacitanceLate;
    		}
    		else if (tokens[2] == "O")
    		{
    			itap.tapName.n1 = tokens[1];
    			itap.tapName.pinid = ComputeHash(tokens[1]);
    			itap.tapName.n2 = "";
    			itap.hash = itap.tapName.pinid;
    			itap.tapType = 'P';
    			temp = Bsearch(timing.loadcap, itap.hash, 0, timing.loadcap.size()-1);
    			if (temp == -1)
    			{
    				itap.capacitanceEarly = 0;
    				itap.capacitanceLate = 0;
    			}
    			else
    			{
    				itap.capacitanceEarly = timing.loadcap[temp].earlycap;
    				itap.capacitanceLate = timing.loadcap[temp].latecap;
    			}
    			ceffEarly += itap.capacitanceEarly;
    			ceffLate += itap.capacitanceLate;
    			spefNet.taps.push_back(itap) ;
    			tempTap.nodename = itap.tapName;
    			tempTap.hash = itap.hash;
    			tempTap.capacitanceEarly = itap.capacitanceEarly;
    			tempTap.capacitanceLate = itap.capacitanceLate;
    			tapNode.push_back(tempTap);
    		}
    		else
    		{
				continue;
    		}
    	}

  	  else if (tokensz == 4 && tokens[0] == "I")
  	  {
  		  int64 hashCell = ComputeHash(tokens[1]);
  		  temp = Bsearch(InstanceHashTable, hashCell, 0, InstanceHashTable.size()-1);
  		  int64 cellIndex, pinIndex;
  		  int64 tempHash = ComputeHash(tokens[2]);
  		  if (temp == -1)
  		  {
				continue;
  		  }
  		  cellIndex = InstanceHashTable[temp].index;
  		  if (tokens[3] == "O") {
    			spefNet.port.portName.n1 = tokens[1];
    			spefNet.port.portName.n2 = tokens[2];
    			tempstr = tokens[1];
    			tempstr = tempstr.append(1,':');
    			spefNet.port.hash = ComputeHash(tempstr.append(tokens[2]));
    			spefNet.port.portName.pinid = hashCell;
				spefNet.port.portType = 'I';
				//Getting port capacitance from cell library
				temp = Bsearch(InstanceList[cellIndex].info_early->opinhashtable, tempHash , 0,
						        InstanceList[cellIndex].info_early->opinhashtable.size() - 1);
				if (temp == -1)
					continue;
				pinIndex = InstanceList[cellIndex].info_early->opinhashtable[temp].index;
				spefNet.port.capacitanceEarly = InstanceList[cellIndex].info_early->opins[pinIndex].capacitance;

				temp = Bsearch(InstanceList[cellIndex].info_late->opinhashtable, tempHash , 0,
						        InstanceList[cellIndex].info_late->opinhashtable.size() - 1);
				if (temp == -1)
					continue;
				pinIndex = InstanceList[cellIndex].info_late->opinhashtable[temp].index;
				spefNet.port.capacitanceLate = InstanceList[cellIndex].info_late->opins[pinIndex].capacitance;
				portNode.clear();
				portNode.nodename = spefNet.port.portName;
    			portNode.hash = spefNet.port.hash;
    			portNode.capacitanceEarly = spefNet.port.capacitanceEarly;
    			portNode.capacitanceLate = spefNet.port.capacitanceLate;
        	}
			else if (tokens[3] == "I") {
				itap.tapName.n1 = tokens[1];
				itap.tapName.pinid = hashCell;
				itap.tapName.n2 = tokens[2];
    			tempstr = tokens[1];
    			tempstr = tempstr.append(1,':');
				itap.hash = ComputeHash(tempstr.append(tokens[2]));
				itap.tapType = 'I';

				// Getting tap capacitance from cell library
				temp = Bsearch(InstanceList[cellIndex].info_early->ipinhashtable, tempHash , 0,
						        InstanceList[cellIndex].info_early->ipinhashtable.size() - 1);
				if (temp == -1)
		  		{
					temp = Bsearch(InstanceList[cellIndex].info_early->clkpin, tempHash , 0,
							        InstanceList[cellIndex].info_early->clkpin.size() - 1);
					if (temp == -1)
						continue;
					else
					{
						pinIndex = 0;
						itap.capacitanceEarly = InstanceList[cellIndex].info_early->clkpin[pinIndex].capacitance;
						ceffEarly += itap.capacitanceEarly;
					}
		  		}
				else
				{
					pinIndex = InstanceList[cellIndex].info_early->ipinhashtable[temp].index;
					itap.capacitanceEarly = InstanceList[cellIndex].info_early->ipins[pinIndex].capacitance;
					ceffEarly += itap.capacitanceEarly;
				}

				temp = Bsearch(InstanceList[cellIndex].info_late->ipinhashtable, tempHash , 0,
						        InstanceList[cellIndex].info_late->ipinhashtable.size() - 1);
				if (temp == -1)
		  		{
					temp = Bsearch(InstanceList[cellIndex].info_late->clkpin, tempHash , 0,
							        InstanceList[cellIndex].info_late->clkpin.size() - 1);
					if (temp == -1)
						continue;
					else
					{
						pinIndex = 0;
						itap.capacitanceLate = InstanceList[cellIndex].info_late->clkpin[pinIndex].capacitance;
						ceffLate += itap.capacitanceLate;
					}
		  		}
				else
				{
					pinIndex = InstanceList[cellIndex].info_late->ipinhashtable[temp].index;
					itap.capacitanceLate = InstanceList[cellIndex].info_late->ipins[pinIndex].capacitance;
					ceffLate += itap.capacitanceLate;
				}

				tempTap.nodename = itap.tapName;
    			tempTap.hash = itap.hash;
    			tempTap.capacitanceEarly = itap.capacitanceEarly;
    			tempTap.capacitanceLate = itap.capacitanceLate;
    			tapNode.push_back(tempTap);
    			spefNet.taps.push_back(itap);
			}
			else
			{
				continue;
			}
  	  }
  	  else
  		{
			continue;
  		}
	}
	spefNet.port.ceffEarly = ceffEarly;
	spefNet.port.ceffLate = ceffLate;
	sort(spefNet.taps.begin(),spefNet.taps.end(), CompareByHashnum<SpefTap>());
	sort(tapNode.begin(),tapNode.end(), CompareByHashnum<Cnode>());

	return true ;
}

/**
 * \param otherNode Stores all RC tree information (including port and taps) (by reference)
 * \param portNode Stores port information (by reference)
 * \param tapNode Stores information relating to all taps of a net (by reference)
 * \param spefNet Stores net information (by reference)
 *
 * Read and stores capacitance (*CAP) information from .spef file
 */
void SpefParser::read_capacitances (vector <Cnode>& otherNode, Cnode& portNode,
		                             vector <Cnode>& tapNode, SpefNet& spefNet)
{
	  deque<string> tokens ;
	  bool valid = true ;
	  string tempstr;
	  otherNode.clear();
	  int64 i;
	  while (valid) {

		  valid = read_line_as_tokens_spef (is, tokens) ;
		  int64 tokensz = tokens.size();
		  if (tokensz == 1 && tokens[0] == "RES")
			  break ; 											// The beginning of the next section

		  // Line format: "index nodeName cap"
		  // Note that nodeName can be either a single token or 3 tokens
		  Cnode curr;
		  curr.clear();
		  curr.nodename.n1 = tokens[1];

		  if (tokensz == 3)
		  {
			  curr.nodename.pinid = ComputeHash(tokens[1]);
			  curr.hash = curr.nodename.pinid;
			  curr.capacitanceEarly = std::atof(tokens[2].c_str());
			  curr.capacitanceLate = curr.capacitanceEarly;
			  if (curr.capacitanceEarly < 0) curr.capacitanceEarly = 0;
			  if (curr.capacitanceLate < 0) curr.capacitanceLate = 0;
		  }
		  else if (tokensz == 4)
		  {
			  curr.nodename.n2 = tokens[2];
			  tempstr = tokens[1];
			  tempstr = tempstr.append(1,':');
			  curr.hash = ComputeHash(tempstr.append(tokens[2]));
			  curr.nodename.pinid = curr.hash;
			  curr.capacitanceEarly = std::atof(tokens[3].c_str());
			  curr.capacitanceLate = curr.capacitanceEarly;
			  if (curr.capacitanceEarly < 0) curr.capacitanceEarly = 0;
			  if (curr.capacitanceLate < 0) curr.capacitanceLate = 0;
		  }
		  else
		  {
			  continue;
		  }

		  if (curr.hash == portNode.hash)
		  {
			  spefNet.port.capacitanceEarly += curr.capacitanceEarly;
			  spefNet.port.capacitanceLate += curr.capacitanceLate;
			  portNode.capacitanceEarly = spefNet.port.capacitanceEarly;
			  portNode.capacitanceLate = spefNet.port.capacitanceLate;
			  portNode.hash = spefNet.port.hash;
		  }
		  else
		  {
			  i = Bsearch(spefNet.taps, curr.hash, 0, spefNet.taps.size() - 1);
			  if (i != -1)
			  {
				  spefNet.taps[i].capacitanceEarly += curr.capacitanceEarly;
				  spefNet.taps[i].capacitanceLate += curr.capacitanceLate;
				  tapNode[i].capacitanceEarly = spefNet.taps[i].capacitanceEarly;
				  tapNode[i].capacitanceLate = spefNet.taps[i].capacitanceLate;
			  }
			  else
				  otherNode.push_back(curr);
		  }
	  }
}

/**
 * \param otherNode Stores all RC tree information (including port and taps) (by reference)
 * \param portNode Stores port information (by reference)
 * \param tapNode Stores information relating to all taps of a net (by reference)
 * \param spefNet Stores net information (by reference)
 *
 * Read and stores capacitance (*CAP) information from .spef file during incremental changes
 */
void SpefParser::read_capacitancesInc (vector <Cnode>& otherNode, Cnode& portNode,
		                                vector <Cnode>& tapNode, SpefNet& spefNet)
{
	  deque<string> tokens ;
	  bool valid = true ;
	  string tempstr;
	  otherNode.clear();
	  int64 i;
	  while (valid) {

		  valid = read_line_as_tokens_spef (is, tokens) ;
		  int64 tokensz = tokens.size();
		  if (tokensz == 1 && tokens[0] == "RES")
			  break ; // the beginning of the next section

		  // Line format: "index nodeName cap"
		  // Note that nodeName can be either a single token or 3 tokens
		  Cnode curr;
		  curr.clear();
		  curr.nodename.n1 = tokens[1];

		  if (tokensz == 3)
		  {
			  curr.nodename.pinid = ComputeHash(tokens[1]);
			  curr.hash = curr.nodename.pinid;
			  curr.capacitanceEarly = std::atof(tokens[2].c_str());
			  curr.capacitanceLate = curr.capacitanceEarly;
			  if (curr.capacitanceEarly < 0) curr.capacitanceEarly = 0;
			  if (curr.capacitanceLate < 0) curr.capacitanceLate = 0;
		  }
		  else if (tokensz == 4)
		  {
			  curr.nodename.n2 = tokens[2];
			  tempstr = tokens[1];
			  tempstr = tempstr.append(1,':');
			  curr.hash = ComputeHash(tempstr.append(tokens[2]));
			  curr.nodename.pinid = curr.hash;
			  curr.capacitanceEarly = std::atof(tokens[3].c_str());
			  curr.capacitanceLate = curr.capacitanceEarly;
			  if (curr.capacitanceEarly < 0) curr.capacitanceEarly = 0;
			  if (curr.capacitanceLate < 0) curr.capacitanceLate = 0;
		  }
		  else
		  {
			  continue;
		  }

		  if (curr.hash == portNode.hash)
		  {
			  spefNet.port.capacitanceEarly += curr.capacitanceEarly;
			  spefNet.port.capacitanceLate += curr.capacitanceLate;
			  portNode.capacitanceEarly = spefNet.port.capacitanceEarly;
			  portNode.capacitanceLate = spefNet.port.capacitanceLate;
			  portNode.hash = spefNet.port.hash;
		  }
		  else
		  {
			  i = Bsearch(spefNet.taps, curr.hash, 0, spefNet.taps.size() - 1);
			  if (i != -1)
			  {
				  spefNet.taps[i].capacitanceEarly += curr.capacitanceEarly;
				  spefNet.taps[i].capacitanceLate += curr.capacitanceLate;
				  tapNode[i].capacitanceEarly = spefNet.taps[i].capacitanceEarly;
				  tapNode[i].capacitanceLate = spefNet.taps[i].capacitanceLate;
			  }
			  else
				  otherNode.push_back(curr);
		  }
	  }
}

/**
 * \param otherNode Stores all RC tree information (including port and taps) (by reference)
 * \param portNode Stores port information (by reference)
 * \param tapNode Stores information relating to all taps of a net (by reference)
 * \param rList Stores resistance information of RC tree (by reference)
 *
 * Read and stores resistance (*RES) information from .spef file
 */
void SpefParser::read_resistances (vector <Cnode>& otherNode, Cnode& portNode,
		                            vector <Cnode>& tapNode, vector <Redge>& rList)
{

	deque <string> tokens ;
	bool valid = true ;
	while (valid)
	{
		valid = read_line_as_tokens_spef_res (is, tokens);
		int64 tokensz = tokens.size();
	    if ( tokensz == 1 && tokens[0] == "END")
	    	break ;												 // End for this net

	    // Line format: "index fromNodeName toNodeName res"
	    // Note that each nodeName can be either a single token or 3 tokens

	    if (tokens.size() != 4) continue;

	    deque <string> temp1;
	    deque <string> temp2;
	    char*tokenPtr;

	    char *t1 = strdup(tokens[1].c_str());
		tokenPtr = strtok(t1, ":");
		while(tokenPtr != NULL)
		{
			temp1.push_back(tokenPtr);
			tokenPtr = strtok(NULL, ":");
		}
		free(t1);

	    char *t2 = strdup(tokens[2].c_str());
		tokenPtr = strtok(t2, ":");
		while(tokenPtr != NULL)
		{
			temp2.push_back(tokenPtr);
			tokenPtr = strtok(NULL, ":");
		}
		free(t2);

		Redge curr ;
	    int64 temp;
	    string tempstr;
	    curr.clear();
	    curr.node1.n1 = temp1[0];
	    curr.node1.n2 = temp2[0];
        curr.resistance = std::atof(tokens[3].c_str());

	    if (temp1.size() == 2)
	    {
			 curr.node1.n2 = temp1[1];
			 tempstr = temp1[0];
			 tempstr = tempstr.append(1,':');
			 curr.node1.pinid = ComputeHash(tempstr.append(temp1[1]));
			 temp = Bsearch(otherNode, curr.node1.pinid, 0, otherNode.size() - 1);
			 if (temp == -1)
			 {
				 if (portNode.hash != curr.node1.pinid)
				 {
					 Cnode missingNode;
					 missingNode.capacitanceEarly = 0;
					 missingNode.capacitanceLate = 0;
					 missingNode.hash = curr.node1.pinid;
					 missingNode.nodename.n1 = curr.node1.n1;
					 missingNode.nodename.n2 = curr.node1.n2;
					 otherNode.push_back(missingNode);
					 sort(otherNode.begin(),otherNode.end(),CompareByHashnum<Cnode>());
				 }
			 }
			 if (temp2.size() == 2)
			 {
				 curr.node2.n2 = temp2[1];
				 tempstr = temp2[0];
				 tempstr = tempstr.append(1,':');
				 curr.node2.pinid = ComputeHash(tempstr.append(temp2[1]));
				 temp = Bsearch(otherNode, curr.node2.pinid, 0, otherNode.size() - 1);
				 if (temp == -1)
				 {
					 if (portNode.hash != curr.node2.pinid)
					 {
						 Cnode missingNode;
						 missingNode.capacitanceEarly = 0;
						 missingNode.capacitanceLate = 0;
						 missingNode.hash = curr.node2.pinid;
						 missingNode.nodename.n1 = curr.node2.n1;
						 missingNode.nodename.n2 = curr.node2.n2;
						 otherNode.push_back(missingNode);
						 sort(otherNode.begin(),otherNode.end(),CompareByHashnum<Cnode>());
					 }
				 }
			  }
			  else
			  {
				 curr.node2.n2 = "";
				 curr.node2.pinid = ComputeHash(temp2[0]);
			  }
	    }
	    else
	    {
			  curr.node1.n2 = "";
			  curr.node1.pinid = ComputeHash(temp1[0]);

			  if (temp2.size() == 2)
			  {
				 curr.node2.n2 = temp2[1];
				 tempstr = temp2[0];
				 tempstr = tempstr.append(1,':');
				 curr.node2.pinid = ComputeHash(tempstr.append(temp2[1]));
				 temp = Bsearch(otherNode, curr.node2.pinid, 0, otherNode.size() - 1);
				 if (temp == -1)
				 {
					 if (portNode.hash != curr.node2.pinid)
					 {
						 Cnode missingNode;
						 missingNode.capacitanceEarly = 0;
						 missingNode.capacitanceLate = 0;
						 missingNode.hash = curr.node2.pinid;
						 missingNode.nodename.n1 = curr.node2.n1;
						 missingNode.nodename.n2 = curr.node2.n2;
						 otherNode.push_back(missingNode);
						 sort(otherNode.begin(),otherNode.end(),CompareByHashnum<Cnode>());
					 }
				 }
			  }
			  else
			  {
				 curr.node2.n2 = "";
				 curr.node2.pinid = ComputeHash(temp2[0]);
			  }
	    }
	    if (curr.resistance < 0) curr.resistance = 0;
	    rList.push_back(curr);
	}
}

/**
 * \param otherNode Stores all RC tree information (including port and taps) (by reference)
 * \param portNode Stores port information (by reference)
 * \param tapNode Stores information relating to all taps of a net (by reference)
 * \param rList Stores resistance information of RC tree (by reference)
 *
 * Read and stores resistance (*RES) information from .spef file during incremental changes
 */
void SpefParser::read_resistancesInc (vector <Cnode>& otherNode, Cnode& portNode,
		                               vector <Cnode>& tapNode, vector <Redge>& rList)
{
	deque <string> tokens ;
	bool valid = true ;
	while (valid) {
		valid = read_line_as_tokens_spef_res (is, tokens) ;
		int64 tokensz = tokens.size();
	    if ( tokensz == 1 && tokens[0] == "END")
	    	break ;                                                        // End for this net

	    // Line format: "index fromNodeName toNodeName res"
	    // Note that each nodeName can be either a single token or 3 tokens

	    if (tokens.size() != 4) continue;

	    deque <string> temp1;
	    deque <string> temp2;
	    char*tokenPtr;

	    char *t1 = strdup(tokens[1].c_str());
		tokenPtr = strtok(t1, ":");
		while(tokenPtr != NULL)
		{
			temp1.push_back(tokenPtr);
			tokenPtr = strtok(NULL, ":");
		}
		free(t1);

	    char *t2 = strdup(tokens[2].c_str());
		tokenPtr = strtok(t2, ":");
		while(tokenPtr != NULL)
		{
			temp2.push_back(tokenPtr);
			tokenPtr = strtok(NULL, ":");
		}
		free(t2);

	    Redge curr ;
	    int64 temp;
	    string tempstr;
	    curr.clear();
	    curr.node1.n1 = temp1[0];
	    curr.node1.n2 = temp2[0];
        curr.resistance = std::atof(tokens[3].c_str());

	    if (temp1.size() == 2)
	    {
			 curr.node1.n2 = temp1[1];
			 tempstr = temp1[0];
			 tempstr = tempstr.append(1,':');
			 curr.node1.pinid = ComputeHash(tempstr.append(temp1[1]));
			 temp = Bsearch(otherNode, curr.node1.pinid, 0, otherNode.size() - 1);
			 if (temp == -1)
			 {
				 if (portNode.hash != curr.node1.pinid)
				 {
					 Cnode missingNode;
					 missingNode.capacitanceEarly = 0;
					 missingNode.capacitanceLate = 0;
					 missingNode.hash = curr.node1.pinid;
					 missingNode.nodename.n1 = curr.node1.n1;
					 missingNode.nodename.n2 = curr.node1.n2;
					 otherNode.push_back(missingNode);
					 sort(otherNode.begin(),otherNode.end(),CompareByHashnum<Cnode>());
				 }
			 }
			 if (temp2.size() == 2)
			 {
				 curr.node2.n2 = temp2[1];
				 tempstr = temp2[0];
				 tempstr = tempstr.append(1,':');
				 curr.node2.pinid = ComputeHash(tempstr.append(temp2[1]));
				 temp = Bsearch(otherNode, curr.node2.pinid, 0, otherNode.size() - 1);
				 if (temp == -1)
				 {
					 if (portNode.hash != curr.node2.pinid)
					 {
						 Cnode missingNode;
						 missingNode.capacitanceEarly = 0;
						 missingNode.capacitanceLate = 0;
						 missingNode.hash = curr.node2.pinid;
						 missingNode.nodename.n1 = curr.node2.n1;
						 missingNode.nodename.n2 = curr.node2.n2;
						 otherNode.push_back(missingNode);
						 sort(otherNode.begin(),otherNode.end(),CompareByHashnum<Cnode>());
					 }
				 }
			  }
			  else
			  {
				 curr.node2.n2 = "";
				 curr.node2.pinid = ComputeHash(temp2[0]);
			  }
	    }
	    else
	    {
			  curr.node1.n2 = "";
			  curr.node1.pinid = ComputeHash(temp1[0]);

			  if (temp2.size() == 2)
			  {
				 curr.node2.n2 = temp2[1];
				 tempstr = temp2[0];
				 tempstr = tempstr.append(1,':');
				 curr.node2.pinid = ComputeHash(tempstr.append(temp2[1]));
				 temp = Bsearch(otherNode, curr.node2.pinid, 0, otherNode.size() - 1);
				 if (temp == -1)
				 {
					 if (portNode.hash != curr.node2.pinid)
					 {
						 Cnode missingNode;
						 missingNode.capacitanceEarly = 0;
						 missingNode.capacitanceLate = 0;
						 missingNode.hash = curr.node2.pinid;
						 missingNode.nodename.n1 = curr.node2.n1;
						 missingNode.nodename.n2 = curr.node2.n2;
						 otherNode.push_back(missingNode);
						 sort(otherNode.begin(),otherNode.end(),CompareByHashnum<Cnode>());
					 }
				 }
			  }
			  else
			  {
				 curr.node2.n2 = "";
				 curr.node2.pinid = ComputeHash(temp2[0]);
			  }
	    }
	    if (curr.resistance < 0) curr.resistance = 0;
	    rList.push_back(curr);
	}
}

/**
 * Closes the input .spef file stream
 */
void SpefParser::close (void)
{
	is.close();
}

/**
 * \param SpefNetListsz Size of spefNetList
 * \result Returns false if valid net information was not read
 *
 * Special .spef parser function for ISPD 2012 Contest benchmarks
 * ISPD 2012 benchmarks do not have RC parasitic tree, only lumped-up capacitance for nets
 */
bool SpefParser::read_net_data_ispd2012 (int64 SpefNetListsz)
{
	std::deque<string> tokens ;
	bool valid = read_line_as_tokens_spef (is, tokens) ;

	// Read until a valid D_NET line is found
	while (valid) {
	if (tokens.size() == 3 && tokens[0] == "D_NET")
	{
		int64 hash = ComputeHash(tokens[1]);
		string netName = tokens[1] ;
		double64 netLumpedCap = std::atof(tokens[2].c_str()) ;

		bool valid = true ;
		while (valid) {
			valid = read_line_as_tokens_spef_res (is, tokens) ;
			int64 tokensz = tokens.size();
			if ( tokensz == 1 && tokens[0] == "END")
				break ; // end for this net
		}

		int64 index = Bsearch(spefNetHashTable, hash, 0, SpefNetListsz - 1);
		if (index != -1)
		{
			SpefNetList[spefNetHashTable[index].index].netLumpedCap = netLumpedCap;
			SpefNetList[spefNetHashTable[index].index].port.ceffEarly += netLumpedCap;
			SpefNetList[spefNetHashTable[index].index].port.ceffLate += netLumpedCap;
		}
	}
	valid = read_line_as_tokens_spef (is, tokens) ;
	}
	return false ; // a valid net was not read
}

/**
 * \param spefNet Net for which data needs to be read from .spef (by reference)
 * \result Returns false if valid net information was not read
 *
 * Reads net data for a net from .spef file
 */
bool SpefParser::read_net_data (SpefNet& spefNet)
{
	std::deque<string> tokens;
	bool valid = read_line_as_tokens_spef (is, tokens);

	// Read until a valid D_NET line is found
	while (valid)
	{
		if (tokens.size() == 3 && tokens[0] == "D_NET")
		{
			int64 hash = ComputeHash(tokens[1]);
			spefNet.netName = tokens[1];
			spefNet.hash = hash;
			spefNet.netLumpedCap = std::atof(tokens[2].c_str());
			vector <Cnode> otherNode;
			vector <Cnode> tapNode;
			Cnode portNode;
			portNode.clear();

			bool saveRC = false;
			vector <Redge> rList;
			bool readConns = read_connections (portNode, tapNode, spefNet, saveRC);
			if (readConns)
			{
				read_capacitances (otherNode, portNode, tapNode, spefNet);

				// Pushing tapNode at the end of otherNode
				for (int i = 0; i < (int)tapNode.size(); i++)
				  otherNode.push_back(tapNode[i]);
				sort(otherNode.begin(),otherNode.end(),CompareByHashnum<Cnode>());

				read_resistances (otherNode, portNode, tapNode, rList) ;
			}

			//Getting all delays and beta of taps
			otherNode.push_back(portNode);
			FastTapdelaybeta(otherNode, rList, spefNet);

			if (saveRC == true || SAVE_NET == true)
			{
			  for (int64 i = 0; i < (int64)otherNode.size(); i++)
				  spefNet.otherNode.push_back(otherNode[i]);
			}
			if (saveRC == true)
			{
			  for (int64 i = 0; i < (int64)rList.size(); i++)
				  spefNet.rList.push_back(rList[i]);
			}
			return true ;
		}
		valid = read_line_as_tokens_spef (is, tokens) ;
	}

	return false ; 						// A valid net was not read
}

/**
 * \param linkWireList Set of pointers to nets read from .spef file during incremental changes (by reference)
 * \result Returns false if valid net information was not read
 *
 * Reads net data for a net from .spef file during incremental changes
 */
bool SpefParser::read_net_dataInc (vector<SpefNet*>& linkWireList)
{
	std::deque<string> tokens;
	bool valid = read_line_as_tokens_spef (is, tokens);

	// Read until a valid D_NET line is found
	while (valid)
	{
		if (tokens.size() == 3 && tokens[0] == "D_NET")
		{
			int64 hash = ComputeHash(tokens[1]);
			int64 netInd=Bsearch(spefNetHashTable,hash,0,spefNetHashTable.size()-1);
			if (netInd == -1)
			{
				valid = read_line_as_tokens_spef (is, tokens);
				continue;
			}
			netInd=spefNetHashTable[netInd].index;

			SpefNetList[netInd].taps.clear();
			SpefNetList[netInd].isIncluded=true;
			SpefNetList[netInd].isVirtualNet = false;
			SpefNetList[netInd].isIncforRAT = false;
			SpefNetList[netInd].netName = tokens[1];
			SpefNetList[netInd].hash = hash;
			SpefNetList[netInd].netLumpedCap = std::atof(tokens[2].c_str());
			SpefNetList[netInd].otherNode.clear();
			SpefNetList[netInd].rList.clear();

			if(SpefNetList[netInd].isIncforRAT == false)
			{
				linkWireList.push_back(&SpefNetList[netInd]);
				SpefNetList[netInd].isIncforRAT = true;
			}

			vector <Cnode> otherNode;
			vector <Cnode> tapNode;
			Cnode portNode;
			portNode.clear();
			vector <Redge> rList;

			bool saveRC = true;
			bool readConns = read_connectionsInc (portNode, tapNode, SpefNetList[netInd], saveRC);

			if (readConns)
			{
			  read_capacitancesInc (otherNode, portNode, tapNode, SpefNetList[netInd]);

			  // Pushing tapNode at the end of otherNode
			  for (int i = 0; i < (int)tapNode.size(); i++)
				  otherNode.push_back(tapNode[i]);
			  sort(otherNode.begin(),otherNode.end(),CompareByHashnum<Cnode>());

			  read_resistancesInc (otherNode, portNode, tapNode, rList) ;
			}

			//Getting all delays and beta of taps
			otherNode.push_back(portNode);
			FastTapdelaybeta(otherNode, rList, SpefNetList[netInd]);
			if (saveRC == true)
			{
			  for (int64 i = 0; i < (int64)otherNode.size(); i++)
				  SpefNetList[netInd].otherNode.push_back(otherNode[i]);
			  for (int64 i = 0; i < (int64)rList.size(); i++)
				  SpefNetList[netInd].rList.push_back(rList[i]);
			}

			return true ;
		}
		valid = read_line_as_tokens_spef (is, tokens) ;
	}
	return false ; 													// A valid net was not read
}

/**
 * \param filename Name of .spef file
 *
 * Function of class Timer that calls init_spef_parser function of class SpefParser to parse .spef file
 */
void Timer::test_spef_parser (string filename)
{
	_spef_ptr->init_spef_parser (filename);
	return;
}

/**
 * \param filename Name of .spef file
 *
 * Function to parse .spef file and collect all parasitic information, finds net delays and moments and linking to cell pins
 * Note - SpefParser assumes the following units - Time (1 PS), Capacitance (1 FF), Resistance (1 KOhm)
 */
void SpefParser::init_spef_parser (string filename)
{
	is.open(filename.c_str());

	int64 SpefNetListsz = 0;
	SpefNet spefNet ;
	Hash_Instance tempHashInstance;
	TimeIt timeit1;

	if (ISPD_2012 == false)
	{
		bool valid = read_net_data (spefNet);
		while (valid)
		{
			timeit_start(&timeit1);
			SpefNetList.push_back(spefNet);
			tempHashInstance.index = SpefNetListsz;
			tempHashInstance.hash = spefNet.hash;
			spefNetHashTable.push_back(tempHashInstance);
			SpefNetListsz++;
			spefNet.clear();
			valid = read_net_data (spefNet);
			timeit_stop(&timeit1);
		}

		sort(spefNetHashTable.begin(),spefNetHashTable.end(),CompareByHashnum<Hash_Instance>());
	}

	// Checking for missing nets in .spef and adding them
	int64 count = 0;
	for (int64 i = 0; i < (int)WireList.size(); i++)
	{
		int64 index = Bsearch(spefNetHashTable, WireList[i].hash, 0, SpefNetListsz - 1);
		if (index == -1)
		{
			if (!(WireList[i].port.portName.n1 == "" /*&& WireList[i].port.portName.n2 == ""*/) && WireList[i].taps.size() != 0)
			{
			  double64 ceffEarlyTotal = 0;
			  double64 ceffLateTotal = 0;
			  for (int64 j = 0; j < (int64)WireList[i].taps.size(); j++)
			  {
				  double64 ceffEarly = 0;
				  double64 ceffLate = 0;
				  int64 temp = Bsearch(InstanceHashTable, WireList[i].taps[j].tapName.pinid, 0, InstanceHashTable.size() - 1);
				  if (temp == -1) continue;
				  Instance* cell = &(InstanceList[InstanceHashTable[temp].index]);
				  int64 tempHash;
				  if (WireList[i].taps[j].tapName.n2 == "")
					  tempHash = 0;
				  else
					  tempHash = ComputeHash(WireList[i].taps[j].tapName.n2);

				  if (tempHash != 0)
				  {
					  temp = Bsearch(cell->info_early->ipinhashtable, tempHash , 0, cell->info_early->ipinhashtable.size() - 1);
					  if (temp == -1)
					  {
							//TODO; Below two lines not required actually
							temp = Bsearch(cell->info_early->clkpin, tempHash , 0, cell->info_early->clkpin.size() - 1);
							if (temp == -1) continue;
							ceffEarly += cell->info_early->clkpin[0].capacitance;
					  }
					  else
						  ceffEarly += cell->info_early->ipins[cell->info_early->ipinhashtable[temp].index].capacitance;

					  temp = Bsearch(cell->info_late->ipinhashtable, tempHash , 0, cell->info_late->ipinhashtable.size() - 1);
					  if (temp == -1)
					  {
							//TODO; Below two lines not required actually
							temp = Bsearch(cell->info_late->clkpin, tempHash , 0, cell->info_late->clkpin.size() - 1);
							if (temp == -1) continue;
							ceffLate += cell->info_late->clkpin[0].capacitance;
					  }
					  else
						  ceffLate += cell->info_late->ipins[cell->info_late->ipinhashtable[temp].index].capacitance;
				  }
				  else
				  {
						temp = Bsearch(timing.loadcap, WireList[i].taps[j].hash, 0, timing.loadcap.size()-1);
						if (temp == -1)
						{
							ceffEarly += 0 ;
							ceffLate += 0 ;
						}

						else
						{
							ceffEarly += timing.loadcap[temp].earlycap;
							ceffLate += timing.loadcap[temp].latecap;
						}
				  }
				  WireList[i].taps[j].capacitanceEarly = ceffEarly;
				  WireList[i].taps[j].capacitanceLate = ceffLate;
				  ceffEarlyTotal += ceffEarly;
				  ceffLateTotal += ceffLate;
			  }
			  WireList[i].port.ceffEarly = ceffEarlyTotal;
			  WireList[i].port.ceffLate = ceffLateTotal;
			  WireList[i].port.capacitanceEarly = ceffEarlyTotal;
			  WireList[i].port.capacitanceLate = ceffLateTotal;
			  SpefNetList.push_back(WireList[i]);
			  tempHashInstance.index = SpefNetListsz + count;
			  tempHashInstance.hash = WireList[i].hash;
			  spefNetHashTable.push_back(tempHashInstance);
			  count++;
			}
		}
	}

	WireList.clear();

	SpefNetListsz += count;
	sort(spefNetHashTable.begin(),spefNetHashTable.end(),CompareByHashnum<Hash_Instance>());

	if (ISPD_2012 == true)
	{
		read_net_data_ispd2012 (SpefNetListsz);
	}

	linker();
	close();
}

/**
 * \param tap Pointer to concerned tap
 *
 * Stores moments and assigns delay based on delay model chosen in macrodef.h
 */
void SpefParser::UpdateParasitics(SpefTap* tap)
{
	tap->m1_E = tap->delayEarly;
	tap->m2_E = tap->betaEarly;

	tap->m1_L = tap->delayLate;
	tap->m2_L = tap->betaLate;

	/////////////////////ELMORE///////////////////////////////////
	if (ELMORE == true)	return;

	/////////////////////D2M///////////////////////////////////
	else if (D2M == true)
	{
		if(tap->m2_E != 0)
			tap->delayEarly = 0.6931472*(pow(tap->m1_E,2)/sqrt(tap->m2_E));
		if(tap->m2_L != 0)
			tap->delayLate  = 0.6931472*(pow(tap->m1_L,2)/sqrt(tap->m2_L));

	/////////////////////D2M ENDS///////////////////////////////////
	}

	/////////////////////DM1///////////////////////////////////
	else if (DM1 == true)
	{
		double64 m3_E = sqrt(4*tap->m2_E - 3*pow(tap->m1_E,2));
		double64 m3_L = sqrt(4*tap->m2_L - 3*pow(tap->m1_L,2));

		tap->delayEarly = 0.5*(m3_E + tap->m1_E)*log(1 + (tap->m1_E/m3_E));
		tap->delayLate  = 0.5*(m3_L + tap->m1_L)*log(1 + (tap->m1_L/m3_L));

	/////////////////////DM1 ENDS///////////////////////////////////
	}

	/////////////////////DM1///////////////////////////////////
	else if (DM2 == true)
	{
		tap->delayEarly = 0.6931472*sqrt(2*tap->m2_E - pow(tap->m1_E,2));
		tap->delayLate  = 0.6931472*sqrt(2*tap->m2_L - pow(tap->m1_L,2));

	/////////////////////DM2 ENDS///////////////////////////////////
	}
}

/**
 * \param otherNode Contains all RC tree information (including port and taps<)
 * \param rList Contains resistance information of RC tree
 * \param spefNet Concerned net (by reference)
 * \param isInc Flag to identify if function call is during incremental changes (true) or not (false)
 *
 * Computes 1st & 2nd moments of RC parasitics tree and finds delay information for the net
 */
void SpefParser::FastTapdelaybeta(vector <Cnode>& otherNode, vector <Redge>& rList,
		                           SpefNet& spefNet, bool isInc)
{
	int64 otherNodesz = otherNode.size();

	spefNet.adjList.clear();
	spefNet.childNode.clear();
	spefNet.parentNode.clear();
	spefNet.backCapSumEarly.clear();
	spefNet.backCapSumLate.clear();
	spefNet.backDCapSumEarly.clear();
	spefNet.backDCapSumLate.clear();

	if (isInc == false)
		spefNet.otherNode.clear();
	else
	{
		spefNet.c1 = 0;
		spefNet.c2 = 0;
		spefNet.r = 0;
		spefNet.pi.clear();
	}

	Cnode portNode = otherNode[otherNodesz - 1];

	deque < vector < RES_NODE > > adjList;
	int64 i,j;

	vector < RES_NODE > tempDQ;
	for (i = 0; i < otherNodesz; i++ )
	{
		adjList.push_back(tempDQ);
		if (SAVE_NET == true)
			spefNet.adjList.push_back(tempDQ);
	}

	int64 source, sink;
	RES_NODE temp;
	for(i = 0; i < (int64)rList.size(); i++)
	{
		if (rList[i].node1.pinid == portNode.hash)
			source = otherNodesz - 1;
		else
			source = Bsearch(otherNode, rList[i].node1.pinid, 0, otherNodesz - 2);

		if (rList[i].node2.pinid == portNode.hash)
			sink = otherNodesz - 1;
		else
			sink = Bsearch(otherNode, rList[i].node2.pinid, 0, otherNodesz - 2);

		if (sink == -1 || source == -1)
		{
			continue;
		}
		temp.res = rList[i].resistance;
		temp.idx = sink;
		adjList[source].push_back(temp);
		if (SAVE_NET == true)
			spefNet.adjList[source].push_back(temp);
		temp.idx = source;
		adjList[sink].push_back(temp);
		if (SAVE_NET == true)
			spefNet.adjList[sink].push_back(temp);
	}

	vector < int64 > parentNode;
	deque < vector <CHILD_NODE> > childNode;
	vector < int64 > pathColor;
	vector < double64 > alldelayEarly;
	vector < double64 > alldelayLate;
	vector < double64 > backCapSumEarly;
	vector < double64 > backCapSumLate;
	vector < double64 > backDCapSumEarly;
	vector < double64 > backDCapSumLate;
	vector < bool > doneFlag;
	vector < bool > doneFlag2;
	vector < bool > doneFlag3;
	vector < bool > doneFlag4;

    // Added for New Effective Capacitance
	Pi tmp;
	tmp.clear();

	for(i = 0; i < otherNodesz ; i++)
	{
		vector <CHILD_NODE> t;
		parentNode.push_back(-1);
		pathColor.push_back(-1);
		alldelayEarly.push_back(0);
		alldelayLate.push_back(0);
		childNode.push_back(t);
		backCapSumEarly.push_back(0);
		backCapSumLate.push_back(0);
		backDCapSumEarly.push_back(0);
		backDCapSumLate.push_back(0);
		doneFlag.push_back(false);
		doneFlag2.push_back(false);
		doneFlag3.push_back(false);
		if (ECM == true)
			doneFlag4.push_back(false);

		if (SAVE_NET == true)
		{
			spefNet.parentNode.push_back(-1);
			spefNet.childNode.push_back(t);
			spefNet.backCapSumEarly.push_back(0);
			spefNet.backCapSumLate.push_back(0);
			spefNet.backDCapSumEarly.push_back(0);
			spefNet.backDCapSumLate.push_back(0);
			if (ECM == true)
				spefNet.pi.push_back(tmp);
		}
	}

	// Create a queue for BFS
	list< int64> queue;
	queue.push_back(otherNodesz-1);
	int64 curr;
	CHILD_NODE cNode;
	while(!queue.empty())
	{
	    curr = queue.front();
	    queue.pop_front();
	    for(i = 0; i < (int)adjList[curr].size(); i++)
	    {
	    	if(parentNode[adjList[curr][i].idx] == -1 )
	        {
	    		if (adjList[curr][i].idx == otherNodesz - 1)
	    		{
	    			parentNode[adjList[curr][i].idx] = -1;
	    			if (SAVE_NET == true)
		    			spefNet.parentNode[adjList[curr][i].idx] = -1;
		            queue.push_back(adjList[curr][i].idx);
	    		}
	    		else
	    		{
		    		parentNode[adjList[curr][i].idx] = curr;
	    			if (SAVE_NET == true)
	    				spefNet.parentNode[adjList[curr][i].idx] = curr;
		    		cNode.adIdx = i;
		    		cNode.idx = adjList[curr][i].idx;
		    		childNode[curr].push_back(cNode);
	    			if (SAVE_NET == true)
			    		spefNet.childNode[curr].push_back(cNode);
		            queue.push_back(adjList[curr][i].idx);
	    		}
	        }
	    }
	}




	for (i = otherNodesz - 2; i >= 0; i--)
	{
		if (childNode[i].size() == 0)
		{
			doneFlag[i] = true;
			queue.push_back(parentNode[i]);
		}
	}

	while(!queue.empty())
	{
	    curr = queue.front();
	    queue.pop_front();
	    if (curr == -1 || doneFlag[curr] == true)
	    	continue;
	    doneFlag[curr] = true;
	    for (i = 0; i < (int64)childNode[curr].size(); i++)
	    {
    		backCapSumEarly[curr] += backCapSumEarly[childNode[curr][i].idx] +
    				                  otherNode[childNode[curr][i].idx].capacitanceEarly;
    		backCapSumLate[curr] += backCapSumLate[childNode[curr][i].idx] +
    				                  otherNode[childNode[curr][i].idx].capacitanceLate;

			if (SAVE_NET == true)
    		{
    			spefNet.backCapSumEarly[curr] = backCapSumEarly[curr];
    			spefNet.backCapSumLate[curr] = backCapSumLate[curr];
    		}

			if (doneFlag[childNode[curr][i].idx] == false)
	    	{
	    	    doneFlag[curr] = false;
	    	    break;
	    	}


			if (ECM == true && SAVE_NET == true)
    		{
    			if (childNode[curr].size() > 1)
    			{
        			spefNet.pi[curr].segC = 0;
        			spefNet.pi[curr].segR = 0;

        			if (doneFlag4[childNode[curr][i].idx] == false)
        			{
        				for (int x = 0; x < (int)adjList[curr].size(); x++)
        				{
        					if (adjList[curr][x].idx == childNode[curr][i].idx)
        					{
        	    				spefNet.pi[childNode[curr][i].idx].segR += adjList[curr][x].res;
        	    				break;
        					}
        				}
            			spefNet.pi[childNode[curr][i].idx].segC += otherNode[childNode[curr][i].idx].capacitanceLate;
        				doneFlag4[childNode[curr][i].idx] = true;
        			}
    			}
    			else if (childNode[curr].size() == 1)
        		{
    				spefNet.pi[curr].segC = spefNet.pi[childNode[curr][i].idx].segC + otherNode[childNode[curr][i].idx].capacitanceLate;
    				for (int x = 0; x < (int)adjList[curr].size(); x++)
    				{
    					if (adjList[curr][x].idx == childNode[curr][i].idx)
    					{
    	    				spefNet.pi[curr].segR = spefNet.pi[childNode[curr][i].idx].segR + adjList[curr][x].res;
    	    				break;
    					}
    				}
        		}
        		else
        		{
        			spefNet.pi[curr].segC = 0;
        			spefNet.pi[curr].segR = 0;
        		}
    		}

	    }

		if (doneFlag[curr] == false)
	    {
	    	backCapSumEarly[curr] = 0;
	    	backCapSumLate[curr] = 0;
    		if (SAVE_NET == true)
    		{
    			spefNet.backCapSumEarly[curr] = 0;
    			spefNet.backCapSumLate[curr] = 0;

    			if (ECM == true)
    			{
        			spefNet.pi[curr].segC = 0;
        			spefNet.pi[curr].segR = 0;
    			}
    		}
	    }

	    else if (parentNode[curr] != otherNodesz - 1)
	    	queue.push_back(parentNode[curr]);

	    else
		{
	    	if (ECM == true)
	    	{
				spefNet.pi[curr].segC += otherNode[curr].capacitanceLate;
				for (int x = 0; x < (int)adjList[curr].size(); x++)
				{
					if (adjList[curr][x].idx == otherNodesz - 1)
					{
	    				spefNet.pi[curr].segR += adjList[curr][x].res;
	    				break;
					}
				}

//				if (childNode[otherNodesz - 1].size() > 1)
//				{
//					cout << "Found " << childNode[otherNodesz - 1].size()  << " children of net " << spefNet.netName << endl;
//				}
	    	}
		}
	}

	if (FAST_MI == true)
	{
		vector < double64 > newDelayE;
		vector < double64 > newDelayL;
		vector < double64 > newBetaE;
		vector < double64 > newBetaL;

		for(i = 0; i < otherNodesz ; i++)
		{
			newDelayE.push_back(0);
			newDelayL.push_back(0);
			newBetaE.push_back(0);
			newBetaL.push_back(0);
			doneFlag[i] = false;
			doneFlag2[i] = false;
			doneFlag3[i] = false;
		}

		newDelayE[otherNodesz - 1] = 0;
		newDelayL[otherNodesz - 1] = 0;
		newBetaE[otherNodesz - 1] = 0;
		newBetaL[otherNodesz - 1] = 0;


		// For Delay
		queue.push_back(otherNodesz - 1);
		double64 rcE = 0, rcL = 0;
		while(!queue.empty())
		{
		    curr = queue.front();
		    queue.pop_front();

		    if (curr == -1 || doneFlag[curr] == true)
		    	continue;
		    doneFlag[curr] = true;

		    if (curr != (otherNodesz - 1))
		    {
			    for (i = 0; i < (int)adjList[curr].size(); i++)
			    {
			    	if (adjList[curr][i].idx == parentNode[curr])
			    	{
			    		rcE = adjList[curr][i].res*(backCapSumEarly[curr] + otherNode[curr].capacitanceEarly);
			    		rcL = adjList[curr][i].res*(backCapSumLate[curr] + otherNode[curr].capacitanceLate);
			    	}
			    	else
				    	queue.push_back(adjList[curr][i].idx);
			    }

		    	newDelayE[curr] = newDelayE[parentNode[curr]] + rcE;
		    	newDelayL[curr] = newDelayL[parentNode[curr]] + rcL;
		    }
		    else
		    {
			    for (i = 0; i < (int)childNode[curr].size(); i++)
			    	queue.push_back(childNode[curr][i].idx);
		    }

		}

		//For Beta
		for (i = otherNodesz - 2; i >= 0; i--)
		{
			if (childNode[i].size() == 0)
			{
				doneFlag2[i] = true;
				queue.push_back(parentNode[i]);
			}
		}

		while(!queue.empty())
		{
			curr = queue.front();
			queue.pop_front();

			if (curr == -1 || doneFlag2[curr] == true)
				continue;
			doneFlag2[curr] = true;
			for (i = 0; i < (int)childNode[curr].size(); i++)
			{
				backDCapSumEarly[curr] += backDCapSumEarly[childNode[curr][i].idx] +
						                   otherNode[childNode[curr][i].idx].capacitanceEarly*newDelayE[childNode[curr][i].idx];
				backDCapSumLate[curr] += backDCapSumLate[childNode[curr][i].idx] +
						                   otherNode[childNode[curr][i].idx].capacitanceLate*newDelayL[childNode[curr][i].idx];
				if (doneFlag2[childNode[curr][i].idx] == false)
				{
					doneFlag2[curr] = false;
					break;
				}
	    		if (SAVE_NET == true)
	    		{
	    			spefNet.backDCapSumEarly[curr] = backDCapSumEarly[curr];
	    			spefNet.backDCapSumLate[curr] = backDCapSumLate[curr];
	    		}
			}
    		if (ECM == true && SAVE_NET == true && doneFlag2[curr] == true)
    		{
    			if (childNode[curr].size() > 1)
    			{
    				//Do Something
    				spefNet.pi[curr].y1 = 0;
    				spefNet.pi[curr].y2 = 0;
    				spefNet.pi[curr].y3 = 0;
    				for (int i = 0; i < (int)childNode[curr].size(); i++)
    				{
    					spefNet.pi[curr].y1 += spefNet.pi[childNode[curr][i].idx].y1 + spefNet.pi[childNode[curr][i].idx].segC;
    					spefNet.pi[curr].y2 += spefNet.pi[childNode[curr][i].idx].y2
    											- spefNet.pi[childNode[curr][i].idx].segR*(
													pow(spefNet.pi[childNode[curr][i].idx].y1, 2)
													+ spefNet.pi[childNode[curr][i].idx].segC*spefNet.pi[childNode[curr][i].idx].y1
													+ 0.3333333333*pow(spefNet.pi[childNode[curr][i].idx].segC, 2));

    					spefNet.pi[curr].y3 += spefNet.pi[childNode[curr][i].idx].y3
    											- spefNet.pi[childNode[curr][i].idx].segR*(
    													2*spefNet.pi[childNode[curr][i].idx].y1*spefNet.pi[childNode[curr][i].idx].y2
														+ spefNet.pi[childNode[curr][i].idx].segC*spefNet.pi[childNode[curr][i].idx].y2)
												+ pow(spefNet.pi[childNode[curr][i].idx].segR, 2)*(
														pow(spefNet.pi[childNode[curr][i].idx].y1,3)
														+ 1.33333333*spefNet.pi[childNode[curr][i].idx].segC*pow(spefNet.pi[childNode[curr][i].idx].y1,2)
    													+ 0.66666666*pow(spefNet.pi[childNode[curr][i].idx].segC, 2)*spefNet.pi[childNode[curr][i].idx].y1
														+ 0.13333333*pow(spefNet.pi[childNode[curr][i].idx].segC, 3));

    				}
    			}
    			else if (childNode[curr].size() == 1)
        		{
    				// Do Nothing
        			spefNet.pi[curr].y1 = spefNet.pi[childNode[curr][0].idx].y1;
        			spefNet.pi[curr].y2 = spefNet.pi[childNode[curr][0].idx].y2;
        			spefNet.pi[curr].y3 = spefNet.pi[childNode[curr][0].idx].y3;
        		}
        		else
        		{
    				// Do Nothing
        			spefNet.pi[curr].y1 = 0;
        			spefNet.pi[curr].y2 = 0;
        			spefNet.pi[curr].y3 = 0;
        		}

    			if (parentNode[curr] == otherNodesz - 1)
    			{
    				//Do Something
					double64 y1 = 0;
					double64 y2 = 0;
					double64 y3 = 0;

					y1 = spefNet.pi[curr].y1 + spefNet.pi[curr].segC;

					for (int x = 0; x < (int)adjList[curr].size(); x++)
					{
						if (adjList[curr][x].idx == otherNodesz - 1)
						{
							y2 = spefNet.pi[curr].y2
													- spefNet.pi[curr].segR*(
														pow(spefNet.pi[curr].y1, 2)
														+ spefNet.pi[curr].segC*spefNet.pi[curr].y1
														+ 0.3333333333*pow(spefNet.pi[curr].segC, 2));

	    					y3 = spefNet.pi[curr].y3
	    											- spefNet.pi[curr].segR*(
	    													2*spefNet.pi[curr].y1*spefNet.pi[curr].y2
															+ spefNet.pi[curr].segC*spefNet.pi[curr].y2)
													+ pow(spefNet.pi[curr].segR, 2)*(
															pow(spefNet.pi[curr].y1,3)
															+ 1.33333333*spefNet.pi[curr].segC*pow(spefNet.pi[curr].y1,2)
	    													+ 0.66666666*pow(spefNet.pi[curr].segC, 2)*spefNet.pi[curr].y1
															+ 0.13333333*pow(spefNet.pi[curr].segC, 3));
	    					break;
						}
					}
					spefNet.pi[curr].y1 = y1;
					spefNet.pi[curr].y2 = y2;
					spefNet.pi[curr].y3 = y3;
    			}

    		}
			if (doneFlag2[curr] == false)
			{
				backDCapSumEarly[curr] = 0;
				backDCapSumLate[curr] = 0;
	    		if (SAVE_NET == true)
	    		{
	    			spefNet.backDCapSumEarly[curr] = 0;
	    			spefNet.backDCapSumLate[curr] = 0;
	    		}
			}

			else if (parentNode[curr] != otherNodesz - 1)
				queue.push_back(parentNode[curr]);
			else
			{
	    		if (ECM == true && SAVE_NET == true)
	    		{
					spefNet.pi[otherNodesz - 1].y1 = otherNode[otherNodesz - 1].capacitanceLate;
					spefNet.pi[otherNodesz - 1].y2 = 0;
					spefNet.pi[otherNodesz - 1].y3 = 0;
					if (spefNet.pi[otherNodesz - 1].y1 < 0)
					{
						assert(0);
					}

    				for (int x = 0; x < (int)childNode[otherNodesz - 1].size(); x++)
    				{
    					spefNet.pi[otherNodesz - 1].y1 += spefNet.pi[childNode[otherNodesz - 1][x].idx].y1;
    					spefNet.pi[otherNodesz - 1].y2 += spefNet.pi[childNode[otherNodesz - 1][x].idx].y2;
    					spefNet.pi[otherNodesz - 1].y3 += spefNet.pi[childNode[otherNodesz - 1][x].idx].y3;
    				}
	    			spefNet.c1 = pow(spefNet.pi[otherNodesz - 1].y2, 2) / spefNet.pi[otherNodesz - 1].y3;
	    			spefNet.c2 = spefNet.pi[otherNodesz - 1].y1 - spefNet.c1;
	    			spefNet.r  = (-1)*pow(spefNet.pi[otherNodesz - 1].y3, 2) / pow(spefNet.pi[otherNodesz - 1].y2, 3);

	    			spefNet.port.ceffLate = spefNet.c1 + spefNet.c2;
	    		}
			}
		}

		queue.push_back(otherNodesz - 1);
		double64 rcdE = 0, rcdL = 0;
		while(!queue.empty())
		{
		    curr = queue.front();
		    queue.pop_front();

		    if (curr == -1 || doneFlag3[curr] == true)
		    	continue;
		    doneFlag3[curr] = true;

		    if (curr != (otherNodesz - 1))
		    {
			    for (i = 0; i < (int)adjList[curr].size(); i++)
			    {
			    	if (adjList[curr][i].idx == parentNode[curr])
			    	{
			    		rcdE = adjList[curr][i].res*(backDCapSumEarly[curr] + otherNode[curr].capacitanceEarly*newDelayE[curr]);
			    		rcdL = adjList[curr][i].res*(backDCapSumLate[curr] + otherNode[curr].capacitanceLate*newDelayL[curr]);
			    	}
			    	else
				    	queue.push_back(adjList[curr][i].idx);
			    }

		    	newBetaE[curr] = newBetaE[parentNode[curr]] + rcdE;
		    	newBetaL[curr] = newBetaL[parentNode[curr]] + rcdL;
		    }
		    else
		    {
			    for (i = 0; i < (int)childNode[curr].size(); i++)
			    	queue.push_back(childNode[curr][i].idx);
		    }

		}

		int64 numTaps = spefNet.taps.size();
		int64 t;
		for (t = 0; t < numTaps; t++)
		{
			i = Bsearch(otherNode, spefNet.taps[t].hash, 0, otherNodesz - 2);
			if (i == -1) continue;
			spefNet.taps[t].delayEarly = newDelayE[i];
			spefNet.taps[t].delayLate = newDelayL[i];
			spefNet.taps[t].betaEarly = newBetaE[i];
			spefNet.taps[t].betaLate = newBetaL[i];
			UpdateParasitics(&(spefNet.taps[t]));
		}
	}

	else if (NEW_MI == false)
	{
		//For delay
		for (i = 0; i < otherNodesz - 1; i++)
		{
			curr = i;
			while(curr != (otherNodesz - 1))
			{
				pathColor[curr] = i;
				curr = parentNode[curr];
			}
			alldelayEarly[i] = 0;
			alldelayLate[i] = 0;
			double64 commonRes = 0;
			while (curr != i)
			{
				int64 child;
				if (childNode[curr].size() == 1)
				{
					child = childNode[curr][0].idx;
					commonRes += adjList[curr][childNode[curr][0].adIdx].res;
					alldelayEarly[i] += commonRes*otherNode[child].capacitanceEarly;
					alldelayLate[i] += commonRes*otherNode[child].capacitanceLate;
					curr = child;
				}
				else
				{
					int64 trueChild;
					int64 trueChildidx;
					double64 branchCapEarly = 0.0;
					double64 branchCapLate = 0.0;
					for (j = 0; j < (int64)childNode[curr].size(); j++)
					{
						child = childNode[curr][j].idx;
						if (pathColor[child] != i)
						{
							branchCapEarly += backCapSumEarly[child] + otherNode[child].capacitanceEarly;
							branchCapLate += backCapSumLate[child] + otherNode[child].capacitanceLate;
						}
						else
						{
							trueChild = child;
							trueChildidx = childNode[curr][j].adIdx;
						}
					}
					alldelayEarly[i] += commonRes*branchCapEarly;
					alldelayLate[i] += commonRes*branchCapLate;
					commonRes += adjList[curr][trueChildidx].res;
					alldelayEarly[i] += commonRes*otherNode[trueChild].capacitanceEarly;
					alldelayLate[i] += commonRes*otherNode[trueChild].capacitanceLate;
					curr = trueChild;
				}
			}
			alldelayEarly[i] += commonRes*backCapSumEarly[i];
			alldelayLate[i] += commonRes*backCapSumLate[i];
		}

		//For Beta
		for (i = otherNodesz - 2; i >= 0; i--)
		{
			if (childNode[i].size() == 0)
			{
				doneFlag2[i] = true;
				queue.push_back(parentNode[i]);
			}
		}

		while(!queue.empty())
		{
			curr = queue.front();
			queue.pop_front();

			if (curr == -1 || doneFlag2[curr] == true)
				continue;
			doneFlag2[curr] = true;
			for (i = 0; i < (int)childNode[curr].size(); i++)
			{
				backDCapSumEarly[curr] += backDCapSumEarly[childNode[curr][i].idx] + otherNode[childNode[curr][i].idx].capacitanceEarly*alldelayEarly[childNode[curr][i].idx];
				backDCapSumLate[curr] += backDCapSumLate[childNode[curr][i].idx] + otherNode[childNode[curr][i].idx].capacitanceLate*alldelayLate[childNode[curr][i].idx];
	    		if (SAVE_NET == true)
	    		{
	    			spefNet.backDCapSumEarly[curr] = backDCapSumEarly[curr];
	    			spefNet.backDCapSumLate[curr] = backDCapSumLate[curr];
	    		}
				if (doneFlag2[childNode[curr][i].idx] == false)
				{
					doneFlag2[curr] = false;
					break;
				}
			}
			if (doneFlag2[curr] == false)
			{
				backDCapSumEarly[curr] = 0;
				backDCapSumLate[curr] = 0;
	    		if (SAVE_NET == true)
	    		{
	    			spefNet.backDCapSumEarly[curr] = 0;
	    			spefNet.backDCapSumLate[curr] = 0;
	    		}
			}

			else if (parentNode[curr] != otherNodesz - 1)
				queue.push_back(parentNode[curr]);
		}

		int64 numTaps = spefNet.taps.size();
		int64 t;
		for (t = 0; t < numTaps; t++)
		{
			i = Bsearch(otherNode, spefNet.taps[t].hash, 0, otherNodesz - 2);
			if (i == -1) continue;
			spefNet.taps[t].delayEarly = alldelayEarly[i];
			spefNet.taps[t].delayLate = alldelayLate[i];
			curr = i;
			while(curr != (otherNodesz - 1))
			{
				pathColor[curr] = i;
				curr = parentNode[curr];
			}
			spefNet.taps[t].betaEarly = 0;
			spefNet.taps[t].betaLate = 0;
			double64 commonRes = 0;
			while (curr != i)
			{
				int64 child;
				if (childNode[curr].size() == 1)
				{
					child = childNode[curr][0].idx;
					commonRes += adjList[curr][childNode[curr][0].adIdx].res;
					spefNet.taps[t].betaEarly += commonRes*otherNode[child].capacitanceEarly*alldelayEarly[child];
					spefNet.taps[t].betaLate += commonRes*otherNode[child].capacitanceLate*alldelayLate[child];
					curr = child;
				}
				else
				{
					int64 trueChild;
					int64 trueChildidx;
					double64 branchCapEarly = 0.0;
					double64 branchCapLate = 0.0;
					for (j = 0; j < (int)childNode[curr].size(); j++)
					{
						child = childNode[curr][j].idx;
						if (pathColor[child] != i)
						{
							branchCapEarly += backDCapSumEarly[child] + otherNode[child].capacitanceEarly*alldelayEarly[child];
							branchCapLate += backDCapSumLate[child] + otherNode[child].capacitanceLate*alldelayLate[child];
						}
						else
						{
							trueChild = child;
							trueChildidx = childNode[curr][j].adIdx;
						}
					}
					spefNet.taps[t].betaEarly += commonRes*branchCapEarly;
					spefNet.taps[t].betaLate += commonRes*branchCapLate;
					commonRes += adjList[curr][trueChildidx].res;
					spefNet.taps[t].betaEarly += commonRes*otherNode[trueChild].capacitanceEarly*alldelayEarly[trueChild];
					spefNet.taps[t].betaLate += commonRes*otherNode[trueChild].capacitanceLate*alldelayLate[trueChild];
					curr = trueChild;
				}
			}
			spefNet.taps[t].betaEarly += commonRes*backDCapSumEarly[i];
			spefNet.taps[t].betaLate += commonRes*backDCapSumLate[i];
			spefNet.taps[t].lumpedRes = commonRes;
			UpdateParasitics(&spefNet.taps[t]);
		}

	}

	else
	{
		vector < double64 > newDelay;
		vector < double64 > newSlew;

		for(i = 0; i < otherNodesz ; i++)
		{
			newDelay.push_back(0);
			newSlew.push_back(0);
			doneFlag[i] = false;
		}

		newDelay[otherNodesz - 1] = 0;
		newSlew[otherNodesz - 1] = 0;

		queue.push_back(otherNodesz - 1);

		double64 rc;
		while(!queue.empty())
		{
			curr = queue.front();
			queue.pop_front();
			if (curr == -1 || doneFlag[curr] == true)
				continue;
			doneFlag[curr] = true;

			if (curr != (otherNodesz - 1))
			{
				for (i = 0; i < (int)spefNet.adjList[curr].size(); i++)
				{
					if (spefNet.adjList[curr][i].idx == spefNet.parentNode[curr])
						rc = spefNet.adjList[curr][i].res*(backCapSumLate[curr] + otherNode[curr].capacitanceLate);
					else
						queue.push_back(spefNet.adjList[curr][i].idx);
				}

				newDelay[curr] = newDelay[spefNet.parentNode[curr]] + rc;
				newSlew[curr] = sqrt(pow(newSlew[spefNet.parentNode[curr]], 2) + 1.93*pow(rc,2));
			}
			else
			{
				for (i = 0; i < (int)spefNet.childNode[curr].size(); i++)
					queue.push_back(spefNet.childNode[curr][i].idx);
			}

		}
		int64 numTaps = spefNet.taps.size();
		int64 t;
		for (t = 0; t < numTaps; t++)
		{
			i = Bsearch(otherNode, spefNet.taps[t].hash, 0, otherNodesz - 2);
			if (i == -1) continue;
			spefNet.taps[t].delayEarly = newDelay[i];
			spefNet.taps[t].delayLate = newDelay[i];
			spefNet.taps[t].betaEarly = newSlew[i];
			spefNet.taps[t].betaLate = newSlew[i];
		}

	}
}

/**
 * \param spefNet Concerned net (by reference)
 * \param tIdx Index of tap in spefNet
 * \param capChange Change in pin capacitance of tap pin due to version change of the connected gate (for iitSizer)
 *
 * Computes 1st & 2nd moments of RC parasitics tree and finds delay information for the net (for iitSizer)
 * Performs fast downstream capacitance update instead of normal complete BFS way of FastTapdelabeta
 */
void SpefParser::NFSTapdelaybeta(SpefNet& spefNet, int tIdx, double64 capChange)
{

	vector <Cnode>& otherNode = spefNet.otherNode;

	int64 otherNodesz = otherNode.size();
	Cnode portNode = otherNode[otherNodesz - 1];
	int64 i,j;

	vector < int64 > pathColor;
	vector < double64 > alldelayEarly;
	vector < double64 > alldelayLate;

	vector < bool > doneFlag;
	vector < bool > doneFlag2;
	vector < bool > doneFlag3;

	for(i = 0; i < otherNodesz ; i++)
	{
		pathColor.push_back(-1);
		alldelayEarly.push_back(0);
		alldelayLate.push_back(0);
		doneFlag.push_back(false);
		doneFlag2.push_back(false);
		doneFlag3.push_back(false);
		spefNet.backDCapSumEarly[i] = 0;
		spefNet.backDCapSumLate[i] = 0;

	}

	list< int64> queue;
	int64 curr;

	bool modSegCFlag = false;

	if (tIdx != -1)
	{
		if (ECM == true && (spefNet.parentNode[tIdx] == otherNodesz - 1 || spefNet.childNode[spefNet.parentNode[tIdx]].size() > 1))
		{
			spefNet.pi[tIdx].segC += capChange;
			modSegCFlag = true;
		}

		queue.push_back(spefNet.parentNode[tIdx]);
	}
	else
		assert(0);


	while(!queue.empty())
	{
	    curr = queue.front();
	    queue.pop_front();

		if (ECM == true && modSegCFlag == false)
	    {
			spefNet.pi[curr].segC += capChange;
			if (spefNet.parentNode[curr] == otherNodesz - 1 || spefNet.childNode[spefNet.parentNode[curr]].size() > 1)
				modSegCFlag = true;
	    }

	    if (curr == otherNodesz - 1)
	    	continue;
		spefNet.backCapSumEarly[curr] += capChange;
		spefNet.backCapSumLate[curr] += capChange;


		queue.push_back(spefNet.parentNode[curr]);
	}


	if (FAST_MI == true)
	{
		vector < double64 > newDelayL;
		vector < double64 > newBetaL;

		for(i = 0; i < otherNodesz ; i++)
		{
			newDelayL.push_back(0);
			newBetaL.push_back(0);
			doneFlag[i] = false;
			doneFlag2[i] = false;
			doneFlag3[i] = false;

			if (ECM == true)
			{
				spefNet.pi[i].y1 = 0;
				spefNet.pi[i].y2 = 0;
				spefNet.pi[i].y3 = 0;
			}
		}

		newDelayL[otherNodesz - 1] = 0;
		newBetaL[otherNodesz - 1] = 0;

		// For Delay
		queue.push_back(otherNodesz - 1);
//		double64 rcE = 0;
		double64 rcL = 0;
		while(!queue.empty())
		{
		    curr = queue.front();
		    queue.pop_front();

		    if (curr == -1 || doneFlag[curr] == true)
		    	continue;
		    doneFlag[curr] = true;

		    if (curr != (otherNodesz - 1))
		    {
			    for (i = 0; i < (int64)spefNet.adjList[curr].size(); i++)
			    {
			    	if (spefNet.adjList[curr][i].idx == spefNet.parentNode[curr])
			    	{
//			    		rcE = spefNet.adjList[curr][i].res*(spefNet.backCapSumEarly[curr] + otherNode[curr].capacitanceEarly);
			    		rcL = spefNet.adjList[curr][i].res*(spefNet.backCapSumLate[curr] + otherNode[curr].capacitanceLate);
			    	}
			    	else
				    	queue.push_back(spefNet.adjList[curr][i].idx);
			    }

//		    	newDelayE[curr] = newDelayE[spefNet.parentNode[curr]] + rcE;
		    	newDelayL[curr] = newDelayL[spefNet.parentNode[curr]] + rcL;
		    }
		    else
		    {
			    for (i = 0; i < (int64)spefNet.childNode[curr].size(); i++)
			    	queue.push_back(spefNet.childNode[curr][i].idx);
		    }

		}

		//For Beta
		for (i = otherNodesz - 2; i >= 0; i--)
		{
			if (spefNet.childNode[i].size() == 0)
			{
				doneFlag2[i] = true;
				queue.push_back(spefNet.parentNode[i]);
			}
		}

		while(!queue.empty())
		{
			curr = queue.front();
			queue.pop_front();

			if (curr == -1 || doneFlag2[curr] == true)
				continue;
			doneFlag2[curr] = true;
			for (i = 0; i < (int64)spefNet.childNode[curr].size(); i++)
			{
				spefNet.backDCapSumLate[curr] += spefNet.backDCapSumLate[spefNet.childNode[curr][i].idx] + otherNode[spefNet.childNode[curr][i].idx].capacitanceLate*newDelayL[spefNet.childNode[curr][i].idx];
				if (doneFlag2[spefNet.childNode[curr][i].idx] == false)
				{
					doneFlag2[curr] = false;
					break;
				}
			}
			if (ECM == true && SAVE_NET == true && doneFlag2[curr] == true)
			{
				if (spefNet.childNode[curr].size() > 1)
				{
					//Do Something
					spefNet.pi[curr].y1 = 0;
					spefNet.pi[curr].y2 = 0;
					spefNet.pi[curr].y3 = 0;
					for (int i = 0; i < (int)spefNet.childNode[curr].size(); i++)
					{
						spefNet.pi[curr].y1 += spefNet.pi[spefNet.childNode[curr][i].idx].y1 + spefNet.pi[spefNet.childNode[curr][i].idx].segC;
    					spefNet.pi[curr].y2 += spefNet.pi[spefNet.childNode[curr][i].idx].y2
												- spefNet.pi[spefNet.childNode[curr][i].idx].segR*(
													pow(spefNet.pi[spefNet.childNode[curr][i].idx].y1, 2)
													+ spefNet.pi[spefNet.childNode[curr][i].idx].segC*spefNet.pi[spefNet.childNode[curr][i].idx].y1
													+ 0.3333333333*pow(spefNet.pi[spefNet.childNode[curr][i].idx].segC, 2));
						spefNet.pi[curr].y3 += spefNet.pi[spefNet.childNode[curr][i].idx].y3
												- spefNet.pi[spefNet.childNode[curr][i].idx].segR*(
														2*spefNet.pi[spefNet.childNode[curr][i].idx].y1*spefNet.pi[spefNet.childNode[curr][i].idx].y2
														+ spefNet.pi[spefNet.childNode[curr][i].idx].segC*spefNet.pi[spefNet.childNode[curr][i].idx].y2)
												+ pow(spefNet.pi[spefNet.childNode[curr][i].idx].segR, 2)*(
														pow(spefNet.pi[spefNet.childNode[curr][i].idx].y1,3)
														+ 1.33333333*spefNet.pi[spefNet.childNode[curr][i].idx].segC*pow(spefNet.pi[spefNet.childNode[curr][i].idx].y1,2)
														+ 0.66666666*pow(spefNet.pi[spefNet.childNode[curr][i].idx].segC, 2)*spefNet.pi[spefNet.childNode[curr][i].idx].y1
														+ 0.13333333*pow(spefNet.pi[spefNet.childNode[curr][i].idx].segC, 3));

					}
				}
				else if (spefNet.childNode[curr].size() == 1)
				{
					// Do Nothing
					spefNet.pi[curr].y1 = spefNet.pi[spefNet.childNode[curr][0].idx].y1;
					spefNet.pi[curr].y2 = spefNet.pi[spefNet.childNode[curr][0].idx].y2;
					spefNet.pi[curr].y3 = spefNet.pi[spefNet.childNode[curr][0].idx].y3;
				}
				else
				{
					// Do Nothing
					spefNet.pi[curr].y1 = 0;
					spefNet.pi[curr].y2 = 0;
					spefNet.pi[curr].y3 = 0;
				}

				if (spefNet.parentNode[curr] == otherNodesz - 1)
				{
					//Do Something
					double64 y1 = 0;
					double64 y2 = 0;
					double64 y3 = 0;

					y1 = spefNet.pi[curr].y1 + spefNet.pi[curr].segC;

					for (int x = 0; x < (int)spefNet.adjList[curr].size(); x++)
					{
						if (spefNet.adjList[curr][x].idx == otherNodesz - 1)
						{
							y2 = spefNet.pi[curr].y2
													- spefNet.pi[curr].segR*(
														pow(spefNet.pi[curr].y1, 2)
														+ spefNet.pi[curr].segC*spefNet.pi[curr].y1
														+ 0.3333333333*pow(spefNet.pi[curr].segC, 2));

							y3 = spefNet.pi[curr].y3
													- spefNet.pi[curr].segR*(
															2*spefNet.pi[curr].y1*spefNet.pi[curr].y2
															+ spefNet.pi[curr].segC*spefNet.pi[curr].y2)
													+ pow(spefNet.pi[curr].segR, 2)*(
															pow(spefNet.pi[curr].y1,3)
															+ 1.33333333*spefNet.pi[curr].segC*pow(spefNet.pi[curr].y1,2)
															+ 0.66666666*pow(spefNet.pi[curr].segC, 2)*spefNet.pi[curr].y1
															+ 0.13333333*pow(spefNet.pi[curr].segC, 3));
							break;
						}
					}
					spefNet.pi[curr].y1 = y1;
					spefNet.pi[curr].y2 = y2;
					spefNet.pi[curr].y3 = y3;
				}

			}
			if (doneFlag2[curr] == false)
			{
				spefNet.backDCapSumLate[curr] = 0;
			}

			else if (spefNet.parentNode[curr] != otherNodesz - 1)
				queue.push_back(spefNet.parentNode[curr]);
			else
			{
				if (ECM == true && SAVE_NET == true)
				{
					spefNet.pi[otherNodesz - 1].y1 = otherNode[otherNodesz - 1].capacitanceLate;
					spefNet.pi[otherNodesz - 1].y2 = 0;
					spefNet.pi[otherNodesz - 1].y3 = 0;
					if (spefNet.pi[otherNodesz - 1].y1 < 0)
					{
						cout << "capChange = " << capChange << "\t" << spefNet.port.pinPtr->info_late->capacitance  << "\t" << otherNode[otherNodesz - 1].capacitanceLate << endl;
						assert(0);
					}
					for (int x = 0; x < (int)spefNet.childNode[otherNodesz - 1].size(); x++)
					{
						spefNet.pi[otherNodesz - 1].y1 += spefNet.pi[spefNet.childNode[otherNodesz - 1][x].idx].y1;
						spefNet.pi[otherNodesz - 1].y2 += spefNet.pi[spefNet.childNode[otherNodesz - 1][x].idx].y2;
						spefNet.pi[otherNodesz - 1].y3 += spefNet.pi[spefNet.childNode[otherNodesz - 1][x].idx].y3;
					}

					spefNet.c1 = pow(spefNet.pi[otherNodesz - 1].y2, 2) / spefNet.pi[otherNodesz - 1].y3;
					spefNet.c2 = spefNet.pi[otherNodesz - 1].y1 - spefNet.c1;
					spefNet.r  = (-1)*pow(spefNet.pi[otherNodesz - 1].y3, 2) / pow(spefNet.pi[otherNodesz - 1].y2, 3);

					spefNet.port.ceffLate = spefNet.c1 + spefNet.c2;
					spefNet.port.pinPtr->ceffLate = spefNet.port.ceffLate;
				}
			}
		}

		queue.push_back(otherNodesz - 1);
//		double64 rcdE = 0;
		double64 rcdL = 0;
		while(!queue.empty())
		{
		    curr = queue.front();
		    queue.pop_front();
		    if (curr == -1 || doneFlag3[curr] == true)
		    	continue;
		    doneFlag3[curr] = true;

		    if (curr != (otherNodesz - 1))
		    {
			    for (i = 0; i < (int)spefNet.adjList[curr].size(); i++)
			    {
			    	if (spefNet.adjList[curr][i].idx == spefNet.parentNode[curr])
			    	{
//			    		rcdE = spefNet.adjList[curr][i].res*(spefNet.backDCapSumEarly[curr] + otherNode[curr].capacitanceEarly*newDelayE[curr]);
			    		rcdL = spefNet.adjList[curr][i].res*(spefNet.backDCapSumLate[curr] + otherNode[curr].capacitanceLate*newDelayL[curr]);
			    	}
			    	else
				    	queue.push_back(spefNet.adjList[curr][i].idx);
			    }

//		    	newBetaE[curr] = newBetaE[spefNet.parentNode[curr]] + rcdE;
		    	newBetaL[curr] = newBetaL[spefNet.parentNode[curr]] + rcdL;
		    }
		    else
		    {
			    for (i = 0; i < (int)spefNet.childNode[curr].size(); i++)
			    	queue.push_back(spefNet.childNode[curr][i].idx);
		    }

		}

		int64 numTaps = spefNet.taps.size();
		int64 t;
		for (t = 0; t < numTaps; t++)
		{
			i = Bsearch(otherNode, spefNet.taps[t].hash, 0, otherNodesz - 2);
			if (i == -1) continue;
			spefNet.taps[t].delayLate = newDelayL[i];
			spefNet.taps[t].betaLate = newBetaL[i];
			UpdateParasitics(&(spefNet.taps[t]));
		}

	}

	else if (NEW_MI == false)
	{
		//For delay
		for (i = 0; i < otherNodesz - 1; i++)
		{
			curr = i;
			while(curr != (otherNodesz - 1))
			{
				pathColor[curr] = i;
				curr = spefNet.parentNode[curr];
			}
			alldelayEarly[i] = 0;
			alldelayLate[i] = 0;
			double64 commonRes = 0;
			while (curr != i)
			{
				int64 child;
				if (spefNet.childNode[curr].size() == 1)
				{
					child = spefNet.childNode[curr][0].idx;
					commonRes += spefNet.adjList[curr][spefNet.childNode[curr][0].adIdx].res;
					alldelayEarly[i] += commonRes*otherNode[child].capacitanceEarly;
					alldelayLate[i] += commonRes*otherNode[child].capacitanceLate;
					curr = child;
				}
				else
				{
					int64 trueChild;
					int64 trueChildidx;
					double64 branchCapEarly = 0.0;
					double64 branchCapLate = 0.0;
					for (j = 0; j < (int)spefNet.childNode[curr].size(); j++)
					{
						child = spefNet.childNode[curr][j].idx;
						if (pathColor[child] != i)
						{
							branchCapEarly += spefNet.backCapSumEarly[child] + otherNode[child].capacitanceEarly;
							branchCapLate += spefNet.backCapSumLate[child] + otherNode[child].capacitanceLate;
						}
						else
						{
							trueChild = child;
							trueChildidx = spefNet.childNode[curr][j].adIdx;
						}
					}
					alldelayEarly[i] += commonRes*branchCapEarly;
					alldelayLate[i] += commonRes*branchCapLate;
					commonRes += spefNet.adjList[curr][trueChildidx].res;
					alldelayEarly[i] += commonRes*otherNode[trueChild].capacitanceEarly;
					alldelayLate[i] += commonRes*otherNode[trueChild].capacitanceLate;
					curr = trueChild;
				}
			}
			alldelayEarly[i] += commonRes*spefNet.backCapSumEarly[i];
			alldelayLate[i] += commonRes*spefNet.backCapSumLate[i];
		}


		//For Beta
		for (i = otherNodesz - 2; i >= 0; i--)
		{
			if (spefNet.childNode[i].size() == 0)
			{
				doneFlag2[i] = true;
				queue.push_back(spefNet.parentNode[i]);
			}
		}

		while(!queue.empty())
		{
			curr = queue.front();
			queue.pop_front();

			if (curr == -1 || doneFlag2[curr] == true)
				continue;
			doneFlag2[curr] = true;
			for (i = 0; i < (int)spefNet.childNode[curr].size(); i++)
			{
				spefNet.backDCapSumEarly[curr] += spefNet.backDCapSumEarly[spefNet.childNode[curr][i].idx] + otherNode[spefNet.childNode[curr][i].idx].capacitanceEarly*alldelayEarly[spefNet.childNode[curr][i].idx];
				spefNet.backDCapSumLate[curr] += spefNet.backDCapSumLate[spefNet.childNode[curr][i].idx] + otherNode[spefNet.childNode[curr][i].idx].capacitanceLate*alldelayLate[spefNet.childNode[curr][i].idx];
				if (doneFlag2[spefNet.childNode[curr][i].idx] == false)
				{
					doneFlag2[curr] = false;
					break;
				}
			}
			if (doneFlag2[curr] == false)
			{
				spefNet.backDCapSumEarly[curr] = 0;
				spefNet.backDCapSumLate[curr] = 0;
			}

			else if (spefNet.parentNode[curr] != otherNodesz - 1)
				queue.push_back(spefNet.parentNode[curr]);
		}

		int64 numTaps = spefNet.taps.size();
		int64 t;
		for (t = 0; t < numTaps; t++)
		{
			i = Bsearch(otherNode, spefNet.taps[t].hash, 0, otherNodesz - 2);
			if (i == -1) continue;
			spefNet.taps[t].delayEarly = alldelayEarly[i];
			spefNet.taps[t].delayLate = alldelayLate[i];
			curr = i;
			while(curr != (otherNodesz - 1))
			{
				pathColor[curr] = i;
				curr = spefNet.parentNode[curr];
			}
			spefNet.taps[t].betaEarly = 0;
			spefNet.taps[t].betaLate = 0;
			double64 commonRes = 0;
			while (curr != i)
			{
				int64 child;
				if (spefNet.childNode[curr].size() == 1)
				{
					child = spefNet.childNode[curr][0].idx;
					commonRes += spefNet.adjList[curr][spefNet.childNode[curr][0].adIdx].res;
					spefNet.taps[t].betaEarly += commonRes*otherNode[child].capacitanceEarly*alldelayEarly[child];
					spefNet.taps[t].betaLate += commonRes*otherNode[child].capacitanceLate*alldelayLate[child];
					curr = child;
				}
				else
				{
					int64 trueChild;
					int64 trueChildidx;
					double64 branchCapEarly = 0.0;
					double64 branchCapLate = 0.0;
					for (j = 0; j < (int64)spefNet.childNode[curr].size(); j++)
					{
						child = spefNet.childNode[curr][j].idx;
						if (pathColor[child] != i)
						{
							branchCapEarly += spefNet.backDCapSumEarly[child] + otherNode[child].capacitanceEarly*alldelayEarly[child];
							branchCapLate += spefNet.backDCapSumLate[child] + otherNode[child].capacitanceLate*alldelayLate[child];
						}
						else
						{
							trueChild = child;
							trueChildidx = spefNet.childNode[curr][j].adIdx;
						}
					}
					spefNet.taps[t].betaEarly += commonRes*branchCapEarly;
					spefNet.taps[t].betaLate += commonRes*branchCapLate;
					commonRes += spefNet.adjList[curr][trueChildidx].res;
					spefNet.taps[t].betaEarly += commonRes*otherNode[trueChild].capacitanceEarly*alldelayEarly[trueChild];
					spefNet.taps[t].betaLate += commonRes*otherNode[trueChild].capacitanceLate*alldelayLate[trueChild];
					curr = trueChild;
				}
			}
			spefNet.taps[t].betaEarly += commonRes*spefNet.backDCapSumEarly[i];
			spefNet.taps[t].betaLate += commonRes*spefNet.backDCapSumLate[i];
			spefNet.taps[t].lumpedRes = commonRes;
			UpdateParasitics(&spefNet.taps[t]);
		}

	}

	else
	{
		vector < double64 > newDelay;
		vector < double64 > newSlew;

		for(i = 0; i < otherNodesz ; i++)
		{
			newDelay.push_back(0);
			newSlew.push_back(0);
			doneFlag[i] = false;
		}

		newDelay[otherNodesz - 1] = 0;
		newSlew[otherNodesz - 1] = 0;

		queue.push_back(otherNodesz - 1);

		double64 rc;
		while(!queue.empty())
		{
		    curr = queue.front();
		    queue.pop_front();

		    if (curr == -1 || doneFlag[curr] == true)
		    	continue;
		    doneFlag[curr] = true;

		    if (curr != (otherNodesz - 1))
		    {
			    for (i = 0; i < (int64)spefNet.adjList[curr].size(); i++)
			    {
			    	if (spefNet.adjList[curr][i].idx == spefNet.parentNode[curr])
			    		rc = spefNet.adjList[curr][i].res*(spefNet.backCapSumLate[curr] + otherNode[curr].capacitanceLate);
			    	else
				    	queue.push_back(spefNet.adjList[curr][i].idx);
			    }

		    	newDelay[curr] = newDelay[spefNet.parentNode[curr]] + rc;
		    	newSlew[curr] = sqrt(pow(newSlew[spefNet.parentNode[curr]], 2) + 1.93*pow(rc,2));
		    }
		    else
		    {
			    for (i = 0; i < (int64)spefNet.childNode[curr].size(); i++)
			    	queue.push_back(spefNet.childNode[curr][i].idx);
		    }

		}
		int64 numTaps = spefNet.taps.size();
		int64 t;
		for (t = 0; t < numTaps; t++)
		{
			i = Bsearch(otherNode, spefNet.taps[t].hash, 0, otherNodesz - 2);
			if (i == -1) continue;
			spefNet.taps[t].delayEarly = newDelay[i];
			spefNet.taps[t].delayLate = newDelay[i];
			spefNet.taps[t].betaEarly = newSlew[i];
			spefNet.taps[t].betaLate = newSlew[i];
		}

	}
}

/**
 * Links the newly formed nets to pins of cell instances in the design
 */
void SpefParser::linker(void)
{
	int64 i, k, pinIndex, cellIndex;
	int64 temp;
	bool clkFlag = false;
	int64 tempHash;
	for(i = 0; i < (int64)SpefNetList.size(); i++)
	{
		temp = Bsearch(InstanceHashTable,SpefNetList[i].port.portName.pinid, 0, InstanceHashTable.size() - 1);
		if (temp == -1)
		{
			continue;
		}
		cellIndex = InstanceHashTable[temp].index;
		Instance* cell = &(InstanceList[cellIndex]); 									// Identified instance of cell connected to this port

		if (cell->cellLibIndex_e != -2 && cell->cellLibIndex_e != -4)
		{
			tempHash = ComputeHash(SpefNetList[i].port.portName.n2);
			temp = Bsearch(cell->info_early->opinhashtable,tempHash, 0,
					        cell->info_early->opinhashtable.size() - 1); 				// ipinhashtable shud be sorted
			if (temp == -1)
			{
				continue;
			}
			pinIndex = cell->info_early->opinhashtable[temp].index; 					// Got correct input pin index in ipins

			cell->iopList[temp].name.pinName = SpefNetList[i].port.portName.n2;
			cell->iopList[temp].info_early = &(cell->info_early->opins[pinIndex]);
			cell->iopList[temp].info_late = &(cell->info_late->opins[pinIndex]);    	// Setting all params of op correctly
			delete cell->iopList[temp].wire; 											// Freeing the virtual net to this pin
		}
		else
		{
			if(cell->iopList.size()==0) continue;
			temp = 0;

		}

		cell->iopList[temp].port = &(SpefNetList[i].port);
		cell->iopList[temp].name.cellid = SpefNetList[i].port.portName.pinid;
		cell->iopList[temp].name.icellName = SpefNetList[i].port.portName.n1;
		SpefNetList[i].isVirtualNet = false;
		cell->iopList[temp].wire = &(SpefNetList[i]);

		cell->iopList[temp].capacitanceEarly = SpefNetList[i].port.capacitanceEarly;
		cell->iopList[temp].capacitanceLate = SpefNetList[i].port.capacitanceLate;
		cell->iopList[temp].cellPtr = cell;
		cell->iopList[temp].ceffEarly = SpefNetList[i].port.ceffEarly;
		cell->iopList[temp].ceffLate = SpefNetList[i].port.ceffLate;
		cell->iopList[temp].isConnected = true;
		cell->outDegree++;

		Hash_Instance h;
		SpefNetList[i].port.cellPtr = cell;                						// Setting the backlink from spefport to pin/instance
		SpefNetList[i].port.pinPtr = &(cell->iopList[temp]);

		/* Assigning input pins of the cell*/
		deque<SpefTap> taps;
		for(k = 0; k < (int64)SpefNetList[i].taps.size(); k++) 						// For each tap of the net
		{
			clkFlag = false;
			temp = Bsearch(InstanceHashTable, SpefNetList[i].taps[k].tapName.pinid, 0, InstanceHashTable.size() - 1);
			if (temp == -1)
			{
				continue;
			}
			cellIndex = InstanceHashTable[temp].index;
			Instance* cell = &(InstanceList[cellIndex]);
			tempHash = ComputeHash(SpefNetList[i].taps[k].tapName.n2);
			if (cell->cellLibIndex_e != -3)
			{
				temp = Bsearch(cell->info_early->ipinhashtable, tempHash, 0, cell->info_early->ipinhashtable.size() - 1); 		//ipinhashtable shud be sorted
				if (temp == -1)
				{
					temp = Bsearch(cell->info_early->clkpin, tempHash, 0,
							        cell->info_early->clkpin.size() - 1); 		// ipinhashtable shud be sorted
					if (temp == -1)
					{
						continue;
					}
					else
					{
						pinIndex = 0;											//Pin is clock pin of FF
						temp = 0;
						clkFlag = true;
					}
				}
				else
					pinIndex = cell->info_early->ipinhashtable[temp].index; 	// Got correct input pin index in ipins

				if(clkFlag == false)
				{
					cell->iipList[temp].name.pinName = SpefNetList[i].taps[k].tapName.n2;
					cell->iipList[temp].name.icellName = SpefNetList[i].taps[k].tapName.n1;
					cell->iipList[temp].name.cellid = SpefNetList[i].taps[k].tapName.pinid;
					cell->iipList[temp].wire = &SpefNetList[i];
					cell->iipList[temp].tap = &(SpefNetList[i].taps[k]);
					cell->iipList[temp].capacitanceEarly = SpefNetList[i].taps[k].capacitanceEarly;
					cell->iipList[temp].capacitanceLate = SpefNetList[i].taps[k].capacitanceLate;

				}

				if(clkFlag == false)
				{
					cell->iipList[temp].info_early = &(cell->info_early->ipins[pinIndex]);
					cell->iipList[temp].info_late = &(cell->info_late->ipins[pinIndex]);
					cell->iipList[temp].isClk = false;
					cell->iipList[temp].cellPtr = cell;
					cell->iipList[temp].isConnected = true;
					Hash_Instance h;
					SpefNetList[i].taps[k].pinPtr = &(cell->iipList[temp]);
				}
				else
				{
					cell->ckList[0].name.pinName = SpefNetList[i].taps[k].tapName.n2;
					cell->ckList[0].name.icellName = SpefNetList[i].taps[k].tapName.n1;
					cell->ckList[0].name.cellid = SpefNetList[i].taps[k].tapName.pinid;
					cell->ckList[0].wire = &SpefNetList[i];
					cell->ckList[0].tap = &(SpefNetList[i].taps[k]);
					cell->ckList[0].capacitanceEarly = SpefNetList[i].taps[k].capacitanceEarly;
					cell->ckList[0].capacitanceLate = SpefNetList[i].taps[k].capacitanceLate;


					cell->ckList[0].info_early = &(cell->info_early->clkpin[pinIndex]);
					cell->ckList[0].info_late = &(cell->info_late->clkpin[pinIndex]);
					cell->ckList[0].isClk = true;
					cell->ckList[0].cellPtr = cell;
					cell->ckList[0].isConnected = true;
					SpefNetList[i].taps[k].pinPtr = &(cell->ckList[0]);
				}
			}
			else
			{
				if(cell->iipList.size()==0) continue;

				cell->iipList[0].name.icellName = SpefNetList[i].taps[k].tapName.n1;
				cell->iipList[0].name.cellid = SpefNetList[i].taps[k].tapName.pinid;
				cell->iipList[0].wire = &SpefNetList[i];
				cell->iipList[0].tap = &(SpefNetList[i].taps[k]);
				cell->iipList[0].capacitanceEarly = SpefNetList[i].taps[k].capacitanceEarly;
				cell->iipList[0].capacitanceLate = SpefNetList[i].taps[k].capacitanceLate;
				cell->iipList[0].cellPtr = cell;
				cell->iipList[0].hash=ComputeHash(SpefNetList[i].taps[k].tapName.n1);
				cell->iipList[0].isConnected = true;

				SpefNetList[i].taps[k].pinPtr = &(cell->iipList.back());
			}
			SpefNetList[i].taps[k].cellPtr = cell;
		}

	}
}

};
