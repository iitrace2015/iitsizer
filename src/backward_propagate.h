/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_BACKWARD_PROPAGATE_H_
#define IIT_BACKWARD_PROPAGATE_H_

#include "include.h"
#include "classdecs.h"
#include "utility.h"
#include "timing_info.h"
#include "instance.h"
#include "net.h"

namespace iit{

/* Declaration of external global variables */
extern SolutionState     TrackSoln;

/**
 * Contains functions and data types for backward timing propagation in normal mode as well as during CPPR - RAT and Slack computation
 */
class BackwardPropagate
{

public:

	void            ComputeRATNetBased           (void);
	void            ComputeRATinCPPR             (void);
	void            ComputeRATInc                (void);
	SolutionState   GetStateParams               (void);
	bool            is_special_char2             (char c);
	void            ComputePreCPPRSlack          (void);
	inline int64    ComputeHash                  (string cellName);
	void            ComputeRATckPin              (Instance* cell);
	void            BackTimingNetBased           (SpefNet* net);
	bool            read_line_as_tokens          (istream& is, deque<string>& tokens, bool includeSpecialChars /*=false*/ );
	double64        EvaluateLUT                  (bool isEarly,double64 slew , double64 cap, LibParserLUT& sample);
	void            addWireDelayParam            (NODE_TIMING_DATA *out, NODE_TIMING_DATA *nodeT, SpefTap *w);
	void            addWireDelayParam_PreCPPR    (NODE_TIMING_DATA *out, InstanceIpin* ipin, int pdataIndex, int mode = BOTH);
	inline void     Copyrat                      (NODE_TIMING_DATA *x, NODE_TIMING_DATA *y);
	pair<int,int>   ComputeSetupHoldRat          (NODE_TIMING_DATA* clkTimingData, int64 clkPinHash, int64 iPinHash,
			                                      int64 cellIndexEarly, int64 cellIndexLate, NODE_TIMING_DATA& iPinTimingData);
	void            BackTimingInCPPR             (priority_queue < SpefNet*,deque<SpefNet*>, CompareNetLevel2 >& prioQueue,
			                                      priority_queue < SpefNet*,deque<SpefNet*>, CompareNetLevel2 >& clockQueue);
	void            BackTimingInc                (priority_queue < SpefNet*,deque<SpefNet*>, CompareNetLevel2 >& prioQueue,
			                                      priority_queue < SpefNet*,deque<SpefNet*>, CompareNetLevel2 >& clockQueue);
	void            ComputeRATCritinCPPR         (InstanceIpin* iip, vector< InstanceOpin* >& backPinList, vector<SpefNet*>& conePinList,
			                                      int64 flagPath, int pdi, double64 cutOff, bool mode );
	template        <typename PinType>
	void            ComputeSlackinCPPRPin        (PinType* pin);
	template        <typename PinType1>
	void            ComputeSlackPin              (PinType1* pin, bool isValidOPin = false);
	template        <typename ratdata >
	void            ClearRAT                     (ratdata* tData, int mode = BOTH);
	template        <typename parallel_data2,
	                typename parallel_data3>
	void            addIpinRatParam              (parallel_data2 *ipin, parallel_data3 *out, DELAY_DATA *w,CRITICALITY& iPinrat, int mode = BOTH);
	template        <typename parallel_data4>
	void            UpdateWireDelayParam         (parallel_data4 *out, NODE_TIMING_DATA *inTime, parallel_data4 *inTemp,
			                                      CRITICALITY& oPinrat,CRITICALITY& tapCrit, int mode = BOTH);
	template        <typename parallel_data>
	void            UpdateWireDelayParam_preCPPR (parallel_data *out, NODE_TIMING_DATA *inTime, parallel_data *inTemp,
			                                      CRITICALITY& oPinrat,CRITICALITY& tapCrit);



 private:

	int bracket_cnt;		///< Curly-bracket count from a file being read


};

/**
 * \param pin Pointer to instance of an input or an output pin
 *
 * Computes slack of a pin using its AT and RAT values
 */
template <typename PinType>
void BackwardPropagate::ComputeSlackinCPPRPin(PinType* pin)
{
	pin->tData.slackFallEarly = (pin->tData.atFallEarly == UNDEFINED_AT_EARLY || pin->tData.ratFallEarly == UNDEFINED_RAT_EARLY ) ?
								 UNDEFINED_SLACK : (pin->tData.atFallEarly - pin->tData.ratFallEarly );
	pin->tData.slackRiseEarly = (pin->tData.atRiseEarly == UNDEFINED_AT_EARLY || pin->tData.ratRiseEarly == UNDEFINED_RAT_EARLY ) ?
								 UNDEFINED_SLACK : (pin->tData.atRiseEarly - pin->tData.ratRiseEarly );
	pin->tData.slackFallLate  = (pin->tData.atFallLate == UNDEFINED_AT_LATE || pin->tData.ratFallLate == UNDEFINED_RAT_LATE ) ?
								 UNDEFINED_SLACK : (pin->tData.ratFallLate - pin->tData.atFallLate );
	pin->tData.slackRiseLate  = (pin->tData.atRiseLate == UNDEFINED_AT_LATE || pin->tData.ratRiseLate == UNDEFINED_RAT_LATE ) ?
								 UNDEFINED_SLACK : (pin->tData.ratRiseLate - pin->tData.atRiseLate );

	return;
}

/**
 * \param pin Pointer to instance of an input or an output pin
 * \param isValidOPin Check if pin is a valid output pin as as to update
 * 	worst slack and TNS of the design
 *
 * 	Computes slack of a pin using AT/RAT values and update global TNS and worst slack
 */
template <typename PinType1>
void BackwardPropagate::ComputeSlackPin(PinType1* pin, bool isValidOPin)
{
	pin->tData.slackFallEarly = (pin->tData.atFallEarly == UNDEFINED_AT_EARLY || pin->tData.ratFallEarly == UNDEFINED_RAT_EARLY ) ?
								 UNDEFINED_SLACK : (pin->tData.atFallEarly - pin->tData.ratFallEarly );
	pin->tData.slackRiseEarly = (pin->tData.atRiseEarly == UNDEFINED_AT_EARLY || pin->tData.ratRiseEarly == UNDEFINED_RAT_EARLY ) ?
								 UNDEFINED_SLACK : (pin->tData.atRiseEarly - pin->tData.ratRiseEarly );

	if(isValidOPin == true)
	{
		TrackSoln.tns        -= pin->tData.slackFallLate < 0.0 ? pin->tData.slackFallLate : 0.0 ;
		TrackSoln.tns        -= pin->tData.slackRiseLate < 0.0 ? pin->tData.slackRiseLate : 0.0 ;
	}
	pin->tData.slackFallLate  = (pin->tData.atFallLate == UNDEFINED_AT_LATE || pin->tData.ratFallLate == UNDEFINED_RAT_LATE ) ?
								 UNDEFINED_SLACK : (pin->tData.ratFallLate - pin->tData.atFallLate );
	pin->tData.slackRiseLate  = (pin->tData.atRiseLate == UNDEFINED_AT_LATE || pin->tData.ratRiseLate == UNDEFINED_RAT_LATE ) ?
								 UNDEFINED_SLACK : (pin->tData.ratRiseLate - pin->tData.atRiseLate );
	if(isValidOPin == true)
	{

		TrackSoln.tns        += pin->tData.slackFallLate < 0.0 ? pin->tData.slackFallLate : 0.0 ;
		TrackSoln.tns        += pin->tData.slackRiseLate < 0.0 ? pin->tData.slackRiseLate : 0.0 ;
		TrackSoln.worstSlack  = min<double64> (TrackSoln.worstSlack, pin->tData.slackRiseLate);
		TrackSoln.worstSlack  = min<double64> (TrackSoln.worstSlack, pin->tData.slackFallLate);
	}

	return;
}

/**
 * \param ipin Input pin pointer for reading timing data
 * \param out Output pin pointer for writing timing data
 * \param w Pointer to a delay data storage
 * \param iPinrat RAT-criticality for input pin
 * \param mode Early/Late mode specifier
 *
 * Computes input-pin RAT and RAT-criticality based on delay of timing arc and RAT of output pin
 */
template<typename parallel_data2, typename parallel_data3>
 void BackwardPropagate:: addIpinRatParam(parallel_data2 *ipin, parallel_data3 *out, DELAY_DATA *w,CRITICALITY& iPinrat, int mode)
 {

	if(mode == BOTH)
	{
		ipin->isRatSet             = true;
     	DELAY_DATA localW          = *w;

     	if(localW.timeSense == POSITIVE_UNATE)
     	{
     		ipin->ratRiseEarly     = localW.delayRiseEarly != FORBIDDEN_EARLY ?
			(out->ratRiseEarly != UNDEFINED_RAT_EARLY ? out->ratRiseEarly - localW.delayRiseEarly : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;
				ipin->ratFallEarly = localW.delayFallEarly != FORBIDDEN_EARLY ?
			(out->ratFallEarly != UNDEFINED_RAT_EARLY ? out->ratFallEarly - localW.delayFallEarly : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;
				ipin->ratRiseLate  = localW.delayRiseLate  != FORBIDDEN_LATE ?
			(out->ratRiseLate !=UNDEFINED_RAT_LATE ? out->ratRiseLate  - localW.delayRiseLate : UNDEFINED_RAT_LATE ) : UNDEFINED_RAT_LATE;
				ipin->ratFallLate  = localW.delayFallLate  != FORBIDDEN_LATE ?
			(out->ratFallLate != UNDEFINED_RAT_LATE ? out->ratFallLate  - localW.delayFallLate: UNDEFINED_RAT_LATE ) : UNDEFINED_RAT_LATE;

			iPinrat.transitionFE   = FALL;
			iPinrat.transitionFL   = FALL;
			iPinrat.transitionRE   = RISE;
			iPinrat.transitionRL   = RISE;

     	}
 		else if(localW.timeSense == NEGATIVE_UNATE)
 		{
 			ipin->ratFallEarly     = localW.delayRiseEarly != FORBIDDEN_EARLY ?
			(out->ratRiseEarly != UNDEFINED_RAT_EARLY ? out->ratRiseEarly - localW.delayRiseEarly : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;
				ipin->ratRiseEarly = localW.delayFallEarly != FORBIDDEN_EARLY ?
			(out->ratFallEarly != UNDEFINED_RAT_EARLY ? out->ratFallEarly - localW.delayFallEarly : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;
				ipin->ratFallLate  = localW.delayRiseLate  != FORBIDDEN_LATE ?
			(out->ratRiseLate !=UNDEFINED_RAT_LATE ? out->ratRiseLate  - localW.delayRiseLate : UNDEFINED_RAT_LATE ) : UNDEFINED_RAT_LATE;
				ipin->ratRiseLate  = localW.delayFallLate  != FORBIDDEN_LATE ?
			(out->ratFallLate != UNDEFINED_RAT_LATE ? out->ratFallLate  - localW.delayFallLate: UNDEFINED_RAT_LATE ) : UNDEFINED_RAT_LATE;

			iPinrat.transitionFE   = RISE;
			iPinrat.transitionFL   = RISE;
			iPinrat.transitionRE   = FALL;
			iPinrat.transitionRL   = FALL;
 		}
 		else
 		{
			if (localW.timeSense == RISING_EDGE || localW.timeSense == COMBINATIONAL )
			{
				if(localW.timeSense == RISING_EDGE)
				{
					ipin->ratFallEarly       = UNDEFINED_RAT_EARLY;
					ipin->ratFallLate        =  UNDEFINED_RAT_LATE;
					localW.delayFallEarlyNeg = localW.delayFallEarly;
					localW.delayFallLateNeg  = localW.delayFallLate;
				}

				double64 a  = localW.delayRiseEarly != FORBIDDEN_EARLY ?
				(out->ratRiseEarly != UNDEFINED_RAT_EARLY ? out->ratRiseEarly - localW.delayRiseEarly : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;
				double64 b  = localW.delayFallEarlyNeg != FORBIDDEN_EARLY ?
				(out->ratFallEarly != UNDEFINED_RAT_EARLY ? out->ratFallEarly - localW.delayFallEarlyNeg : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;

				ipin->ratRiseEarly   = a > b ? a : b;
				iPinrat.transitionRE = a > b ? RISE : FALL;

				a = localW.delayRiseLate  != FORBIDDEN_LATE ?
				(out->ratRiseLate!= UNDEFINED_RAT_LATE ? out->ratRiseLate - localW.delayRiseLate : UNDEFINED_RAT_LATE) : UNDEFINED_RAT_LATE;
				b = localW.delayFallLateNeg  != FORBIDDEN_LATE ?
				(out->ratFallLate != UNDEFINED_RAT_LATE ? out->ratFallLate - localW.delayFallLateNeg : UNDEFINED_RAT_LATE) : UNDEFINED_RAT_LATE;

				ipin->ratRiseLate    = a > b ? b : a;
				iPinrat.transitionRL = a > b ? FALL : RISE;


			}
			if(localW.timeSense==FALLING_EDGE || localW.timeSense==COMBINATIONAL)
			{
				if(localW.timeSense==FALLING_EDGE)
				{
					  ipin->ratRiseEarly       = UNDEFINED_RAT_EARLY;
					  ipin->ratRiseLate        = UNDEFINED_RAT_LATE;
					  localW.delayRiseEarlyNeg = localW.delayRiseEarly;
					  localW.delayRiseLateNeg  = localW.delayRiseLate;
				  }

				double64 a = localW.delayRiseEarlyNeg != FORBIDDEN_EARLY ?
				(out->ratRiseEarly != UNDEFINED_RAT_EARLY ? out->ratRiseEarly - localW.delayRiseEarlyNeg : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;
				double64 b = localW.delayFallEarly != FORBIDDEN_EARLY ?
				(out->ratFallEarly != UNDEFINED_RAT_EARLY ? out->ratFallEarly - localW.delayFallEarly : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;

				ipin->ratFallEarly   = a > b ? a : b;
				iPinrat.transitionFE = a > b ? RISE : FALL;

				a = localW.delayRiseLateNeg  != FORBIDDEN_LATE ?
				(out->ratRiseLate != UNDEFINED_RAT_LATE ? out->ratRiseLate - localW.delayRiseLateNeg : UNDEFINED_RAT_LATE) : UNDEFINED_RAT_LATE;
				b = localW.delayFallLate  != FORBIDDEN_LATE ?
				(out->ratFallLate != UNDEFINED_RAT_LATE ? out->ratFallLate - localW.delayFallLate : UNDEFINED_RAT_LATE) : UNDEFINED_RAT_LATE;

				ipin->ratFallLate    = a > b ? b : a;
				iPinrat.transitionFL = a > b ? FALL : RISE;


			}
 		}

	}
	else if(mode == EARLY)
	{
		ipin->isRatSet    = true;
     	DELAY_DATA localW = *w;

     	if(localW.timeSense == POSITIVE_UNATE)
     	{
     		ipin->ratRiseEarly   = localW.delayRiseEarly != FORBIDDEN_EARLY ?
			(out->ratRiseEarly  != UNDEFINED_RAT_EARLY ? out->ratRiseEarly - localW.delayRiseEarly : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;
			ipin->ratFallEarly   = localW.delayFallEarly != FORBIDDEN_EARLY ?
			(out->ratFallEarly  != UNDEFINED_RAT_EARLY ? out->ratFallEarly - localW.delayFallEarly : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;

			iPinrat.transitionFE = FALL;
			iPinrat.transitionRE = RISE;

     	}
 		else if(localW.timeSense == NEGATIVE_UNATE)
 		{
			ipin->ratFallEarly   = localW.delayRiseEarly != FORBIDDEN_EARLY ?
			(out->ratRiseEarly  != UNDEFINED_RAT_EARLY ? out->ratRiseEarly - localW.delayRiseEarly : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;
			ipin->ratRiseEarly   = localW.delayFallEarly != FORBIDDEN_EARLY ?
			(out->ratFallEarly  != UNDEFINED_RAT_EARLY ? out->ratFallEarly - localW.delayFallEarly : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;
			iPinrat.transitionFE = RISE;
			iPinrat.transitionRE = FALL;

 		}
 		else
 		{
			if (localW.timeSense == RISING_EDGE || localW.timeSense == COMBINATIONAL )
			{
				if(localW.timeSense == RISING_EDGE)
				{
					ipin->ratFallEarly       = UNDEFINED_RAT_EARLY;
					localW.delayFallEarlyNeg = localW.delayFallEarly;
				}

				double64 a = localW.delayRiseEarly != FORBIDDEN_EARLY ?
				(out->ratRiseEarly != UNDEFINED_RAT_EARLY ? out->ratRiseEarly - localW.delayRiseEarly : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;
				double64 b = localW.delayFallEarlyNeg != FORBIDDEN_EARLY ?
				(out->ratFallEarly != UNDEFINED_RAT_EARLY ? out->ratFallEarly - localW.delayFallEarlyNeg : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;

				ipin->ratRiseEarly   = a > b ? a : b;
				iPinrat.transitionRE = a > b ? RISE : FALL;

			}
			if(localW.timeSense == FALLING_EDGE || localW.timeSense == COMBINATIONAL)
			{
				if(localW.timeSense==FALLING_EDGE)
				{
					  ipin->ratRiseEarly= UNDEFINED_RAT_EARLY;
					  localW.delayRiseEarlyNeg = localW.delayRiseEarly;
				}

				double64 a = localW.delayRiseEarlyNeg != FORBIDDEN_EARLY ?
				(out->ratRiseEarly != UNDEFINED_RAT_EARLY ? out->ratRiseEarly - localW.delayRiseEarlyNeg : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;
				double64 b = localW.delayFallEarly != FORBIDDEN_EARLY ?
				(out->ratFallEarly != UNDEFINED_RAT_EARLY ? out->ratFallEarly - localW.delayFallEarly : UNDEFINED_RAT_EARLY) : UNDEFINED_RAT_EARLY;

				ipin->ratFallEarly   = a > b ? a : b;
				iPinrat.transitionFE = a > b ? RISE : FALL;

			}
 		}

	}
	else if(mode == LATE)
	{
		ipin->isRatSet    =true;
     	DELAY_DATA localW = *w;

     	if(localW.timeSense == POSITIVE_UNATE)
     	{
			ipin->ratRiseLate  = localW.delayRiseLate  != FORBIDDEN_LATE ?
			(out->ratRiseLate !=UNDEFINED_RAT_LATE ? out->ratRiseLate  - localW.delayRiseLate : UNDEFINED_RAT_LATE ) : UNDEFINED_RAT_LATE;
			ipin->ratFallLate  = localW.delayFallLate  != FORBIDDEN_LATE ?
			(out->ratFallLate != UNDEFINED_RAT_LATE ? out->ratFallLate  - localW.delayFallLate: UNDEFINED_RAT_LATE ) : UNDEFINED_RAT_LATE;
			iPinrat.transitionFL   = FALL;
			iPinrat.transitionRL   = RISE;

     	}
 		else if(localW.timeSense == NEGATIVE_UNATE)
 		{
			ipin->ratFallLate    = localW.delayRiseLate  != FORBIDDEN_LATE ?
			(out->ratRiseLate !=UNDEFINED_RAT_LATE ? out->ratRiseLate  - localW.delayRiseLate : UNDEFINED_RAT_LATE ) : UNDEFINED_RAT_LATE;
			ipin->ratRiseLate    = localW.delayFallLate  != FORBIDDEN_LATE ?
			(out->ratFallLate != UNDEFINED_RAT_LATE ? out->ratFallLate  - localW.delayFallLate: UNDEFINED_RAT_LATE ) : UNDEFINED_RAT_LATE;
			iPinrat.transitionFL = RISE;
			iPinrat.transitionRL = FALL;
 		}
 		else
 		{
			if (localW.timeSense == RISING_EDGE || localW.timeSense == COMBINATIONAL )
			{
				if(localW.timeSense == RISING_EDGE)
				{
					ipin->ratFallLate        =  UNDEFINED_RAT_LATE;
					localW.delayFallLateNeg  = localW.delayFallLate;
				}

				double64 a = localW.delayRiseLate  != FORBIDDEN_LATE ?
				(out->ratRiseLate!= UNDEFINED_RAT_LATE ? out->ratRiseLate - localW.delayRiseLate : UNDEFINED_RAT_LATE) : UNDEFINED_RAT_LATE;
				double64 b = localW.delayFallLateNeg  != FORBIDDEN_LATE ?
				(out->ratFallLate != UNDEFINED_RAT_LATE ? out->ratFallLate - localW.delayFallLateNeg : UNDEFINED_RAT_LATE) : UNDEFINED_RAT_LATE;
				ipin->ratRiseLate    = a > b ? b : a;
				iPinrat.transitionRL = a > b ? FALL : RISE;


			}
			if(localW.timeSense == FALLING_EDGE || localW.timeSense == COMBINATIONAL)
			{
				if(localW.timeSense == FALLING_EDGE)
				{
					  ipin->ratRiseLate        = UNDEFINED_RAT_LATE;
					  localW.delayRiseLateNeg  = localW.delayRiseLate;
				  }

				double64 a = localW.delayRiseLateNeg  != FORBIDDEN_LATE ?
				(out->ratRiseLate != UNDEFINED_RAT_LATE ? out->ratRiseLate - localW.delayRiseLateNeg : UNDEFINED_RAT_LATE) : UNDEFINED_RAT_LATE;
				double64 b = localW.delayFallLate  != FORBIDDEN_LATE ?
				(out->ratFallLate != UNDEFINED_RAT_LATE ? out->ratFallLate - localW.delayFallLate : UNDEFINED_RAT_LATE) : UNDEFINED_RAT_LATE;
				ipin->ratFallLate    = a > b ? b : a;
				iPinrat.transitionFL = a > b ? FALL : RISE;


			}
 		}

	}

 }

/**
 * \param out Output pin pointer whose RAT and Criticality need to be updated
 * \param inTime RAT data for comparison
 * \param inTemp RAT data for comparison
 * \param oPinrat RAT-criticality of out
 * \param tapCrit criticality of tap pin
 * \param mode Early/Late mode specifier
 *
 * Computes RAT (and criticality) of an output pin based on worst among two input RAT-data sets
 */
template<typename parallel_data4>
void BackwardPropagate:: UpdateWireDelayParam(parallel_data4 *out, NODE_TIMING_DATA *inTime, parallel_data4 *inTemp,CRITICALITY& oPinrat,CRITICALITY& tapCrit, int mode)
{
	if(mode == BOTH)
	{
		if(inTime->ratFallEarly > inTemp->ratFallEarly )
		{
			out->ratFallEarly      = inTime->ratFallEarly;
			oPinrat.indexFallEarly = tapCrit.indexFallEarly;
			oPinrat.transitionFE   = tapCrit.transitionFE;
		}
		else
			out->ratFallEarly      = inTemp->ratFallEarly;

		if(inTime->ratRiseEarly > inTemp->ratRiseEarly)
		{
			out->ratRiseEarly      = inTime->ratRiseEarly;
			oPinrat.indexRiseEarly = tapCrit.indexRiseEarly;
			oPinrat.transitionRE   = tapCrit.transitionRE;
		} else
			out->ratRiseEarly      = inTemp->ratRiseEarly;

		if(inTime->ratFallLate  > inTemp->ratFallLate)
			out->ratFallLate       = inTemp->ratFallLate ;
		else
		{
			out->ratFallLate       = inTime->ratFallLate;
			oPinrat.indexFallLate  = tapCrit.indexFallLate;
			oPinrat.transitionFL   = tapCrit.transitionFL;
		}

		if(inTime->ratRiseLate  > inTemp->ratRiseLate)
			out->ratRiseLate       = inTemp->ratRiseLate;
		else
		{
			out->ratRiseLate       = inTime->ratRiseLate;
			oPinrat.indexRiseLate  = tapCrit.indexRiseLate;
			oPinrat.transitionRL   = tapCrit.transitionRL;
		}

		out->isRatSet              = true;

	}
	else if(mode == EARLY)
	{
		if(inTime->ratFallEarly > inTemp->ratFallEarly )
		{
			out->ratFallEarly      = inTime->ratFallEarly;
			oPinrat.indexFallEarly = tapCrit.indexFallEarly;
			oPinrat.transitionFE   = tapCrit.transitionFE;
		}
		else
			out->ratFallEarly      = inTemp->ratFallEarly;

		if(inTime->ratRiseEarly > inTemp->ratRiseEarly)
		{
			out->ratRiseEarly      = inTime->ratRiseEarly;
			oPinrat.indexRiseEarly = tapCrit.indexRiseEarly;
			oPinrat.transitionRE   = tapCrit.transitionRE;
		}
		else
			out->ratRiseEarly      = inTemp->ratRiseEarly;

		out->isRatSet              = true;


	}
	else if(mode == LATE)
	{
		if( inTime->ratFallLate  > inTemp->ratFallLate )
		{
			out->ratFallLate      = inTemp->ratFallLate ;
		}
		else
		{
			out->ratFallLate      = inTime->ratFallLate;
			oPinrat.indexFallLate = tapCrit.indexFallLate;
			oPinrat.transitionFL  = tapCrit.transitionFL;
		}

		if(inTime->ratRiseLate  > inTemp->ratRiseLate)
		{
			out->ratRiseLate      = inTemp->ratRiseLate;

		}
		else
		{
			out->ratRiseLate      = inTime->ratRiseLate;
			oPinrat.indexRiseLate = tapCrit.indexRiseLate;
			oPinrat.transitionRL  = tapCrit.transitionRL;
		}

		out->isRatSet             = true;
	}

return;
}

/**
 * \param tData Input timing data
 * \param mode Early/Late mode specifier
 *
 * Clears RAT, i.e., sets to forbidden the RAT values from given timing data
 */
template <typename ratdata >
void BackwardPropagate::ClearRAT(ratdata* tData, int mode)
{
	if(mode == BOTH)
	{
		tData->ratRiseEarly  = tData->ratFallEarly  = UNDEFINED_RAT_EARLY;
		tData->ratRiseLate   = tData->ratFallLate   = UNDEFINED_RAT_LATE;
		tData->isRatSet      = false;
	}
	else if(mode == EARLY)
	{
		tData->ratRiseEarly  = tData->ratFallEarly  = UNDEFINED_RAT_EARLY;
		tData->isRatSet      = false;

	}
	else if(mode == LATE)
	{
		tData->ratRiseLate   = tData->ratFallLate   = UNDEFINED_RAT_LATE;
		tData->isRatSet      = false;

	}
}

};



#endif /* IIT_BACKWARD_PROPAGATE_H_ */
