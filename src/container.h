/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_CONTAINER_H_
#define IIT_CONTAINER_H_

#include "include.h"
#include "classdecs.h"
#include "net.h"

namespace iit
{

/**
 * A hash container to store index and hash value for a Hash Table
 */
class Hash_Instance
{

public:
	int64 hash;                         ///< hash value of string name
	int64 index;                        ///< index of element in list associated with Hash Table

	Hash_Instance()
	{
		index = -1;
		hash  =  0;
	};
};

/**
 * Stores an element of Sparse table used to get Lowest Common Ancestor during CPPR
 */
class SPARSE_TABLE_NODE
{

public:
	SpefPort*           iPort;           ///< Stores port element of a net
	vector < SpefPort*> anc ;            ///< Logarithmic ancestors associated with iPort -> 2^ith ancestors
};

/**
 * Store input pins (and its attributes) during CPPR and Path extraction
 * Building block of negPinList
 */
class TEMP_IPIN_LIST
{

public:
	InstanceIpin* Dpin;                 ///< Dpin of the associated cone
	InstanceIpin* iPin;                 ///< Input pin
	double64      slack;                ///< Slack value of input pin
	bool          trans;                ///< Transition (rise/fall) associated with slack value
	bool          mode;                 ///< Mode (early/late) associated with slack value
	int           threadID;             ///< ID of the thread (for multithreading)

	TEMP_IPIN_LIST ()
	{
		threadID = 0;
		Dpin     = NULL;
		iPin     = NULL;
		slack    = FORBIDDEN_SLACK;
		trans    = RISE;
		mode     = EARLY;
	}
};

/**
 * Stores cone-end point and mode during block based CPPR
 */
class TEMP_CEP {

public:
	InstanceIpin* Dpin;                ///< Cone-end point of cone under consideration
	bool mode;                         ///< Mode (early/late) of associated cone
};

};

#endif /* IIT_CONTAINER_H_ */
