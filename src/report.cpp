/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/




#include "include.h"
#include "report.h"
#include "cppr.h"
#include "utility.h"
#include "timer.h"
#include "globals.h"

namespace iit{

/* Global variables declaration --------------------------------------- */

//extern CelllibStorage      celllibstorage_e;
//extern CelllibStorage      celllibstorage_l;
//extern LibFileInfo         libinfo_e;
//extern LibFileInfo         libinfo_l;
//extern CompleteTimingInfo  timing;

//extern Instance* CLKSRC;
//extern int       numInc;
//extern int       Verbose;
//extern int64     MaxNumPaths;
//extern int64     PathFlag_G;
//extern string    BenchmarkDir;
//extern bool     _isStaticDone;

extern deque   < Instance >       InstanceList;
extern vector  < Hash_Instance >  InstanceHashTable;
//extern deque   < SpefNet >        SpefNetList;
//extern vector  < Hash_Instance >  spefNetHashTable;
//extern vector  < SpefNet* >       Sort;
//extern vector  < SpefNet* >       IncNetList;
//extern deque   < SpefNet >        WireList;
//extern queue   < SpefNet* >       InitNetList;
//extern vector  < Instance* >      PIList;
//extern vector  < Instance* >      POList;
//extern vector  < InstanceIpin* >  ConeEndPoints;
extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
extern deque   < Slack_Instance > pathLUTSlackTable_Adv;

//extern std::tr1::unordered_map <char,bool> specCharLibDir;

/* End - global variables declaration -----------------------------------*/

/**
 * Compare function to sort input pins according to hash
 */
struct CompareIpinHash
{
	inline bool operator() (const InstanceIpin* x, const InstanceIpin* y)
	{
	  return ((x->tap->hash) < (y->tap->hash));
	}
} compareIpinHash;


/**
 * Compare function to compare characters in alphabetical order
 */
inline bool mycomp (char c1, char c2)
{
	return std::tolower(c1) < std::tolower(c2);
}

/**
 * Compare function to sort paths in lexicographical order
 */
struct CompareLex
{
	inline  bool operator() (const Slack_Instance x, const Slack_Instance y)
	{
	  return (lexicographical_compare(pathLUT_Adv[x.index].Dpin->cellPtr->instanceName.begin(),
			  pathLUT_Adv[x.index].Dpin->cellPtr->instanceName.end(),
			  pathLUT_Adv[y.index].Dpin->cellPtr->instanceName.begin(),
			  pathLUT_Adv[y.index].Dpin->cellPtr->instanceName.end(), mycomp));
  }
} compareLex;

/**
 * Compare function to sort paths through queried pin in lexicographical order
 */
struct CompareLexPin1 : std::binary_function <Slack_Instance,Slack_Instance,bool>
{
	inline CompareLexPin1 (deque < PATH_TABLE_ADV >& pinLUT_Adv ) : p( pinLUT_Adv ) {}
    bool operator() (const Slack_Instance x, const Slack_Instance y)
    {
    	  return (lexicographical_compare(p[x.index].Dpin->cellPtr->instanceName.begin(),
    			  p[x.index].Dpin->cellPtr->instanceName.end(),
    			  p[y.index].Dpin->cellPtr->instanceName.begin(),
    			  p[y.index].Dpin->cellPtr->instanceName.end(), mycomp));
      }

private:
    deque<PATH_TABLE_ADV>& p;
};

/**
 * \param oPin Output pin whose forward-out-cone limits the scope of path extraction
 * \param color Color used to restrict search of path extraction through queried pin
 * \param DpinList List of CEPs reached by forward propagating from oPin (by reference)
 *
 * Restricts path extraction within a set of cones by coloring forward-out-cone of given oPin
 * oPin is the queried pin or related cell output pin of queried pin (in case queried pin is input pin)
 * This restriction is applied to enable extracting worst paths through a specified pin
 */
void Report::FrontColorPin (InstanceOpin* oPin, int64 color, deque <TEMP_CEP>& DpinList)
{
	if (oPin->isConnected == false) return;
	list < InstanceIpin* > queue;
	for (int64 i = 0; i < (int64) oPin->wire->taps.size(); i++)
	{
		if(oPin->wire->taps[i].pinPtr != NULL)
			queue.push_back(oPin->wire->taps[i].pinPtr);
	}

	InstanceIpin* curr;
	deque <InstanceIpin*> tempPinList;
	Instance* cell;
	TEMP_CEP tempCEP;
	while(!queue.empty())
	{
		curr = queue.front();
		queue.pop_front();
		cell = curr->cellPtr;
		if (cell->cellLibIndex_e == -3)
		{
			curr->pinColor = color;
			tempPinList.push_back(curr);
			tempCEP.Dpin = curr;
			tempCEP.mode = EARLY;
			DpinList.push_back(tempCEP);
			tempCEP.mode = LATE;
			DpinList.push_back(tempCEP);
		}

		else if (cell->info_early->isSequential == true)
		{
			curr->pinColor = color;
			tempPinList.push_back(curr);
			tempCEP.Dpin = curr;
			tempCEP.mode = EARLY;
			DpinList.push_back(tempCEP);
			tempCEP.mode = LATE;
			DpinList.push_back(tempCEP);
		}

		else
		{
			if (curr->pinColor == color)
				continue;
			curr->pinColor = color;
			tempPinList.push_back(curr);
			for(int64 k = 0; k < (int64)curr->relPinIndex.size(); k++)
			{
				InstanceOpin* oPin = &(cell->iopList[curr->relPinIndex[k]]);
				for (int64 i = 0; i < (int64)oPin->wire->taps.size(); i++)
				{
					if (oPin->wire->taps[i].pinPtr->pinColor != color)
						queue.push_back(oPin->wire->taps[i].pinPtr);
				}
			}
		}
	}
	for (int i = 0; i < (int)tempPinList.size(); i++)
		tempPinList[i]->pinColor = 0;
}

/**
 * \param _reportFile Output stream file where paths need to be reported
 *
 * Reports global top 'N' worst paths in _reportFile
 */
void Report::quickReport (ofstream& _reportFile)
{
	int64 pathLUTSTsz = pathLUTSlackTable_Adv.size();
	int64 minPaths = (_numPaths < pathLUTSTsz) ? _numPaths : pathLUTSTsz;

	int64 j;
	for (int64 i = 0; i < (minPaths - 1); i++)
	{
		j = i+1;
		while (j < ((int64)pathLUTSlackTable_Adv.size() - 1) && pathLUT_Adv[pathLUTSlackTable_Adv[i].index].slack
				                          == pathLUT_Adv[pathLUTSlackTable_Adv[j].index].slack)
		{

			j++;
			assert((int64)pathLUT_Adv.size()>pathLUTSlackTable_Adv[i].index);
			assert((int64)pathLUT_Adv.size()>pathLUTSlackTable_Adv[j].index);
		}

		// Sort here from i to j-1
		sort (pathLUTSlackTable_Adv.begin() + i, pathLUTSlackTable_Adv.begin() + j, compareLex);
		i = j;
	}

	_reportFile.flush();
	streampos reportStart = _reportFile.tellp();
	_reportFile << "                                                               " << endl;
	_reportFile.flush();
	int64 numReports = 0;
	for (int64 n = 0; n < minPaths; n++)
	{
		PATH_TABLE_ADV* pathTN = &(pathLUT_Adv[pathLUTSlackTable_Adv[numReports].index]);
		if (numReports > (minPaths - 2) && minPaths == _numPaths && numReports <= (_numPaths + _numPathsThreshold)
				&& fabs(pathLUT_Adv[pathLUTSlackTable_Adv[minPaths - 2].index].slack - pathTN->slack) < FLOATING_ERROR_THRESHOLD)
		{
			if(numReports < pathLUTSTsz - 2)
				n--;
		}
		else if (numReports > (minPaths - 2) && minPaths == _numPaths)
			break;
		string pathType = (pathTN->Dpin->cellPtr->cellLibIndex_e == -3) ? "RAT" : ((pathTN->mode == EARLY) ? "Hold":"Setup");
		_reportFile << "Path " << ++numReports << ": " << pathType << " " << pathTN->slack << " " << pathTN->path.size() << " " << ((pathTN->mode == EARLY)?"E":"L") << endl;
		for (int64 j = 0; j < (int64)pathTN->path.size(); j++)
		{
			_reportFile << *(pathTN->path[j].icellName) << ((*(pathTN->path[j].pinName) != "") ? ":" : "") << (*(pathTN->path[j].pinName)) << " " << pathTN->path[j].transition << endl;
		}
		_reportFile.flush();
	}
	streampos reportEnd = _reportFile.tellp();
	_reportFile.seekp(reportStart);
	_reportFile << "report_worst_paths " << numReports;
	_reportFile.flush();
	_reportFile.seekp(reportEnd);
}

/**
 * \param _reportFile Output stream file where paths need to be reported
 * \param pinLUT_Adv List of worst paths through queried pin
 * \param pinLUTSlackTable_Adv Slack Table for pinLUT_Adv sorted in worst slack order
 *
 * Reports top 'N' worst paths through a queried pin in _reportFile
 */
void Report::quickReportPin (ofstream& _reportFile, deque < PATH_TABLE_ADV >& pinLUT_Adv,
		                       deque < Slack_Instance >& pinLUTSlackTable_Adv)
{
	//TODO: Add lexicographic ordering
	int64 pinLUTSTsz = pinLUTSlackTable_Adv.size();
	int64 minPaths = (_numPaths < pinLUTSTsz) ? _numPaths : pinLUTSTsz;

	int64 j;
	for (int64 i = 0; i < (minPaths - 1); i++)
	{
		j = i+1;
		while (j < ((int64)pinLUTSlackTable_Adv.size() - 1) && pinLUT_Adv[pinLUTSlackTable_Adv[i].index].slack
				                 == pinLUT_Adv[pinLUTSlackTable_Adv[j].index].slack)
		{
			j++;
			assert((int64)pinLUT_Adv.size()>pinLUTSlackTable_Adv[i].index);
			assert((int64)pinLUT_Adv.size()>pinLUTSlackTable_Adv[j].index);
		}

		//sort here from i to j-1
		sort (pinLUTSlackTable_Adv.begin() + i, pinLUTSlackTable_Adv.begin() + j, CompareLexPin1(pinLUT_Adv));
		i = j;
	}

	_reportFile.flush();
	streampos reportStart = _reportFile.tellp();
	_reportFile << "                                                               " << endl;
	_reportFile.flush();
	int64 numReports = 0;
	for (int64 n = 0; n < minPaths; n++)
	{
		if (pinLUTSlackTable_Adv[numReports].index >= (int64)pinLUT_Adv.size())
		{
			_reportFile.flush();
			assert(0);
		}
		PATH_TABLE_ADV* pathTN = &(pinLUT_Adv[pinLUTSlackTable_Adv[numReports].index]);
		if (numReports > (minPaths - 2) && minPaths == _numPaths && numReports <= (_numPaths + _numPathsThreshold)
				&& fabs(pinLUT_Adv[pinLUTSlackTable_Adv[minPaths - 2].index].slack - pathTN->slack) < FLOATING_ERROR_THRESHOLD)
		{
			if(numReports < pinLUTSTsz - 2)
				n--;
		}
		else if (numReports > (minPaths - 2) && minPaths == _numPaths)
			break;
		string pathType = (pathTN->Dpin->cellPtr->cellLibIndex_e == -3) ? "RAT" : ((pathTN->mode == EARLY) ? "Hold":"Setup");
		_reportFile << "Path " << ++numReports << ": " << pathType << " " << pathTN->slack << " " << pathTN->path.size() << " " << ((pathTN->mode == EARLY)?"E":"L") << endl;
		for (int64 j = 0; j < (int64)pathTN->path.size(); j++)
		{
			_reportFile << *(pathTN->path[j].icellName) << ((*(pathTN->path[j].pinName) != "") ? ":" : "") << (*(pathTN->path[j].pinName)) << " " << pathTN->path[j].transition << endl;
		}
		_reportFile.flush();
	}
	streampos reportEnd = _reportFile.tellp();
	_reportFile.seekp(reportStart);
	_reportFile << "report_worst_paths " << numReports;
	_reportFile.flush();
	_reportFile.seekp(reportEnd);
}

/**
 * \param myfile Output file stream where results need to be reported
 * \param reports A report command split into words
 *
 * Identifies a particular report command and reports the answer in output file
 */
void Report::DumpResults(ofstream& myfile,deque< string > & reports)
{
	bool transition = RISE,mode = LATE;
	string pinName = "";
	int64 numPaths = 1;
	int sizeline = reports.size();

	switch(sizeline)
	{
		case(3):
			{
				transition=RISE;mode=EARLY;
				pinName=reports[2];
				break;
			}
		case(4):
			{
				for(int m=0;m<4;m++)
				{
					if(reports[m]=="pin") pinName=reports[m+1];
					if(reports[m]=="early" || reports[m]=="rise"){transition=RISE;mode=EARLY;}
					else if(reports[m]=="fall") {transition=FALL;mode=EARLY;}
					else if(reports[m]=="late") {transition=RISE;mode=LATE;}
				}
				break;
			}
		case(5):
			{
				for(int m=0;m<5;m++)
				{
					if(reports[m]=="pin") pinName=reports[m+1];
					if(reports[m]=="early")      mode=EARLY;
					else if(reports[m]=="rise") transition=RISE;
					else if(reports[m]=="late") mode=LATE;
					else if(reports[m]=="fall") transition=FALL;
				}
				break;
			}
	}
	_pinName = pinName;
	_transition = transition;
	_mode = mode;

	if (reports[0] == "report_at")
		myfile << ReportAT() << endl;
	else if (reports[0] == "report_rat")
		myfile << ReportRAT() << endl;
	else if(reports[0] == "report_slack")
		myfile << ReportSlack() << endl;
	else if (reports[0] == "report_worst_paths")
	{
		_pinName="";
		for(int m=0;m<sizeline;m++)
		{
			if (reports[m]=="pin")
				_pinName = reports[m+1];
			else if (reports[m] == "numPaths")
				numPaths=std::atoi(reports[m+1].c_str());
		}
		if (numPaths <= 1)
			numPaths = 1;
		myfile.flush();

		 _numPaths = numPaths + 1;  				// We give one additional path intentionally for rise/fall conflict
		 _numPathsThreshold = Threshold(numPaths);
		ReportWorstPaths(myfile);					// (Numpaths + 1) just to make sure that both rise and fall are reported when slack is same
	}
	return;
}

/**
 * \param _reportfile Output file stream where results need to be reported
 *
 * Reports top 'N' worst paths (globally or through a pin) and reports paths in output file
 */
void Report::ReportWorstPaths(ofstream& _reportFile)
{
	// Global path reporting when pin not specified
	if (_pinName == "")
	{
		quickReport(_reportFile);
		return;
	}

	// Pin is specified
	else
	{

		std::deque<std::string> tokens = split(_pinName, ':');
		int tokensz = tokens.size();
		if(!(tokensz == 1 || tokensz == 2))
		{
			_reportFile << "report_worst_paths " << 0 << endl;
			return;
		}

		int64 temp = Bsearch(InstanceHashTable, ComputeHash(tokens[0]), 0, InstanceHashTable.size() - 1);
		if (temp == -1)
		{
			_reportFile << "report_worst_paths " << 0 << endl;
			return;
		}
		Instance* cell = &(InstanceList[InstanceHashTable[temp].index]);

		int64 typePin = 0;											// PI -> -2, PO -> -3, iPin -> 2, oPin -> 3
		int64 pinIndex = -1;
		if (cell->cellLibIndex_e == -4)
		{
			_reportFile << "report_worst_paths " << 0 << endl;
			return;
		}
		else if (cell->cellLibIndex_e == PI)
		{
			//Pin is PI
			typePin = -2;
		}
		else if (cell->cellLibIndex_e == PO)
		{
			//Pin is PO
			typePin = -3;
		}
		else
		{
			int64 tempHash = ComputeHash(tokens[1]);
			if (cell->info_early->isSequential == false && cell->iopList[0].isConnected == true
					&& cell->iopList[0].wire->isCktreeMember == true)
			{
				_reportFile << "report_worst_paths " << 0 << endl;
				return;
			}
			else if (cell->info_early->isSequential == true && cell->ckList[0].isConnected == true
					&& tempHash == cell->ckList[0].hash)
			{
				// Pin is ckPin
				_reportFile << "report_worst_paths " << 0 << endl;
				return;
			}
			else
			{
				pinIndex = Bsearch(cell->iopList, tempHash, 0, cell->iopList.size() - 1);
				if (pinIndex != -1)
				{
					// Pin is oPin
					typePin = 3;
				}
				else
				{
					pinIndex = Bsearch(cell->iipList, tempHash, 0, cell->iipList.size() - 1);
					if (pinIndex != -1)
					{
						// Pin is iPin
						typePin = 2;
					}
					else
					{
						_reportFile << "report_worst_paths " << 0 << endl;
						return;
					}
				}

			}
		}

		deque <TEMP_CEP> frontPinList;
		InstanceIpin* iPin;
		InstanceOpin* oPin;
		TEMP_CEP tempCEP;
		switch (typePin)											// PI -> -2, PO -> -3, iPin -> 2, oPin -> 3
		{
		case -2:
			FrontColorPin(&(cell->iopList[0]), (cell->iopList[0].hash + cell->iopList[0].name.cellid), frontPinList);
			break;

		case -3:
			tempCEP.Dpin = &(cell->iipList[0]);
			tempCEP.mode = EARLY;
			frontPinList.push_back(tempCEP);
			tempCEP.mode = LATE;
			frontPinList.push_back(tempCEP);

			break;

		case 2:
			iPin = &(cell->iipList[pinIndex]);

			if (cell->info_early->isSequential == true)
			{
				tempCEP.Dpin = &(cell->iipList[pinIndex]);
				tempCEP.mode = EARLY;
				frontPinList.push_back(tempCEP);
				tempCEP.mode = LATE;
				frontPinList.push_back(tempCEP);
			}

			else
			{
				for (int64 k = 0; k < (int64)iPin->relPinIndex.size(); k++)
					FrontColorPin(&(cell->iopList[iPin->relPinIndex[k]]),
							       (cell->iopList[iPin->relPinIndex[k]].hash + cell->iopList[iPin->relPinIndex[k]].name.cellid),
							       frontPinList);
			}
			break;

		case 3:
			oPin = &(cell->iopList[pinIndex]);
			FrontColorPin(oPin, (oPin->hash + oPin->name.cellid), frontPinList);
			break;

		default:
			_reportFile << "report_worst_paths " << 0 << endl;
			return;
			break;
		}

		if (tokensz == 1)
			tokens.push_back("");
		deque < PATH_TABLE_ADV > pinLUT_Adv;
		deque < Slack_Instance > pinLUTSlackTable_Adv;

		RunCPPR_Pin (tokens, frontPinList, pinLUT_Adv, pinLUTSlackTable_Adv,
				        (_numPaths + _numPathsThreshold));
		quickReportPin(_reportFile, pinLUT_Adv, pinLUTSlackTable_Adv);
		return;
	}
}

/**
 * Reports Arrival Time at a particular queried pin and given mode (early/late) and transition (rise/fall)
 */
double Report::ReportAT ()
{
	std::deque<std::string> tokens = split(_pinName, ':');
	int tokensz = tokens.size();
	if (!(tokensz == 1 || tokensz == 2))
		return 0.0 ;

	int64 temp = Bsearch(InstanceHashTable, ComputeHash(tokens[0]), 0, InstanceHashTable.size() - 1);
	if (temp == -1)
		return 0.0 ;

	Instance* cell = &(InstanceList[InstanceHashTable[temp].index]);
	if (cell->cellLibIndex_e == PI)
	{
		if (tokensz != 1)
			return 0.0 ;

		if (_transition == RISE)
		{
			if (_mode == EARLY)
				return cell->iopList[0].tData.atRiseEarly;
			else
				return cell->iopList[0].tData.atRiseLate;
		}
		else
		{
			if (_mode == EARLY)
				return cell->iopList[0].tData.atFallEarly;
			else
				return cell->iopList[0].tData.atFallLate;
		}
	}
	else if (cell->cellLibIndex_e == PO)
	{
		if(tokensz != 1)
			return 0.0 ;

		if (_transition == RISE)
		{
			if (_mode == EARLY)
				return cell->iipList[0].tData.atRiseEarly;
			else
				return cell->iipList[0].tData.atRiseLate;
		}
		else
		{
			if (_mode == EARLY)
				return cell->iipList[0].tData.atFallEarly;
			else
				return cell->iipList[0].tData.atFallLate;
		}
	}
	else if (cell->cellLibIndex_e == CLK_SRC)
	{
		if (tokensz != 1)
			return 0.0 ;

		if (_transition == RISE)
		{
			if (_mode == EARLY)
				return cell->iopList[0].tData.atRiseEarly;
			else
				return cell->iopList[0].tData.atRiseLate;
		}
		else
		{
			if (_mode == EARLY)
				return cell->iopList[0].tData.atFallEarly;
			else
				return cell->iopList[0].tData.atFallLate;
		}
	}
	else
	{
		if (tokensz != 2)
			return 0.0 ;

		int64 hash = ComputeHash(tokens[1]);
		int64 pinIndex = Bsearch(cell->iipList, hash, 0, cell->iipList.size() - 1);
		if (pinIndex != -1)
		{

			if (_transition == RISE)
			{
				if (_mode == EARLY)
					return cell->iipList[pinIndex].tData.atRiseEarly;
				else
				{
					return cell->iipList[pinIndex].tData.atRiseLate;
				}
			}
			else
			{
				if (_mode == EARLY)
					return cell->iipList[pinIndex].tData.atFallEarly;
				else
				{
					return cell->iipList[pinIndex].tData.atFallLate;
				}
			}
		}
		else
		{
			pinIndex = Bsearch(cell->iopList, hash, 0, cell->iopList.size() - 1);
			if (pinIndex != -1)
			{
				if (_transition == RISE)
				{
					if (_mode == EARLY)
						return cell->iopList[pinIndex].tData.atRiseEarly;
					else
						return cell->iopList[pinIndex].tData.atRiseLate;
				}
				else
				{
					if (_mode == EARLY)
						return cell->iopList[pinIndex].tData.atFallEarly;
					else
						return cell->iopList[pinIndex].tData.atFallLate;
				}
			}
			else
			{
				if (cell->ckList[0].hash != hash)
				{
					return 0.0;
				}
				if (_transition == RISE)
				{
					if (_mode == EARLY)
						return cell->ckList[0].tData.atRiseEarly;
					else
						return cell->ckList[0].tData.atRiseLate;
				}
				else
				{
					if (_mode == EARLY)
						return cell->ckList[0].tData.atFallEarly;
					else
						return cell->ckList[0].tData.atFallLate;
				}
			}
		}
	}
}

/**
 * Reports Required Arrival Time at a particular queried pin and given mode (early/late) and transition (rise/fall)
 */
double Report::ReportRAT ()
{
	std::deque<std::string> tokens = split(_pinName, ':');
	int tokensz = tokens.size();
	if (!(tokensz == 1 || tokensz == 2))
		return 0.0 ;

	int64 temp = Bsearch(InstanceHashTable, ComputeHash(tokens[0]), 0, InstanceHashTable.size() - 1);
	if (temp == -1)
		return 0.0 ;

	Instance* cell = &(InstanceList[InstanceHashTable[temp].index]);
	if (cell->cellLibIndex_e == PI)
	{
		if (tokensz != 1)
			return 0.0 ;

		if (_transition == RISE)
		{
			if (_mode == EARLY)
				return cell->iopList[0].tData.ratRiseEarly;
			else
				return cell->iopList[0].tData.ratRiseLate;
		}
		else
		{
			if (_mode == EARLY)
				return cell->iopList[0].tData.ratFallEarly;
			else
				return cell->iopList[0].tData.ratFallLate;
		}
	}
	else if (cell->cellLibIndex_e == PO)
	{
		if (tokensz != 1)
			return 0.0 ;

		if (_transition == RISE)
		{
			if (_mode == EARLY)
				return cell->iipList[0].tData.ratRiseEarly;
			else
				return cell->iipList[0].tData.ratRiseLate;
		}
		else
		{
			if (_mode == EARLY)
				return cell->iipList[0].tData.ratFallEarly;
			else
				return cell->iipList[0].tData.ratFallLate;
		}
	}
	else if (cell->cellLibIndex_e == CLK_SRC)
	{
		if (tokensz != 1)
			return 0.0 ;

		if (_transition == RISE)
		{
			if (_mode == EARLY)
				return cell->iopList[0].tData.ratRiseEarly;
			else
				return cell->iopList[0].tData.ratRiseLate;
		}
		else
		{
			if (_mode == EARLY)
				return cell->iopList[0].tData.ratFallEarly;
			else
				return cell->iopList[0].tData.ratFallLate;
		}
	}
	else
	{
		if (tokensz != 2)
			return 0.0 ;

		int64 hash = ComputeHash(tokens[1]);
		int64 pinIndex = Bsearch(cell->iipList, hash, 0, cell->iipList.size() - 1);
		if (pinIndex != -1)
		{
			if (_transition == RISE)
			{
				if (_mode == EARLY)
					return cell->iipList[pinIndex].tData.ratRiseEarly;
				else
					return cell->iipList[pinIndex].tData.ratRiseLate;
			}
			else
			{
				if (_mode == EARLY)
					return cell->iipList[pinIndex].tData.ratFallEarly;
				else
					return cell->iipList[pinIndex].tData.ratFallLate;
			}
		}
		else
		{
			pinIndex = Bsearch(cell->iopList, hash, 0, cell->iopList.size() - 1);
			if (pinIndex != -1)
			{
				if (_transition == RISE)
				{
					if (_mode == EARLY)
						return cell->iopList[pinIndex].tData.ratRiseEarly;
					else
						return cell->iopList[pinIndex].tData.ratRiseLate;
				}
				else
				{
					if (_mode == EARLY)
						return cell->iopList[pinIndex].tData.ratFallEarly;
					else
						return cell->iopList[pinIndex].tData.ratFallLate;
				}
			}
			else
			{
				if (cell->ckList[0].hash != hash)
				{
					return 0.0 ;
				}
				if (_transition == RISE)
				{
					if (_mode == EARLY)
						return cell->ckList[0].tData.ratRiseEarly;
					else
						return cell->ckList[0].tData.ratRiseLate;
				}
				else
				{
					if (_mode == EARLY)
						return cell->ckList[0].tData.ratFallEarly;
					else
						return cell->ckList[0].tData.ratFallLate;
				}
			}
		}
	}
}

/**
 * Reports Slack at a particular queried pin and given mode (early/late) and transition (rise/fall)
 */
double Report::ReportSlack ()
{
	std::deque<std::string> tokens = split(_pinName, ':');
	int tokensz = tokens.size();
	if (!(tokensz == 1 || tokensz == 2))
		return 0.0 ;

	int64 temp = Bsearch(InstanceHashTable, ComputeHash(tokens[0]), 0, InstanceHashTable.size() - 1);
	if (temp == -1)
		return 0.0 ;

	Instance* cell = &(InstanceList[InstanceHashTable[temp].index]);
	if (cell->cellLibIndex_e == PI)
	{
		if (tokensz != 1)
			return 0.0 ;

		if (_transition == RISE)
		{
			if (_mode == EARLY)
				return cell->iopList[0].tData.slackRiseEarly;
			else
				return cell->iopList[0].tData.slackRiseLate;
		}
		else
		{
			if (_mode == EARLY)
				return cell->iopList[0].tData.slackFallEarly;
			else
				return cell->iopList[0].tData.slackFallLate;
		}
	}
	else if (cell->cellLibIndex_e == PO)
	{
		if (tokensz != 1)
			return 0.0 ;

		if (_transition == RISE)
		{
			if (_mode == EARLY)
				return cell->iipList[0].tData.slackRiseEarly;
			else
				return cell->iipList[0].tData.slackRiseLate;
		}
		else
		{
			if (_mode == EARLY)
				return cell->iipList[0].tData.slackFallEarly;
			else
				return cell->iipList[0].tData.slackFallLate;
		}
	}
	else if (cell->cellLibIndex_e == CLK_SRC)
	{
		if (tokensz != 1)
			return 0.0 ;

		if (_transition == RISE)
		{
			if (_mode == EARLY)
				return cell->iopList[0].tData.slackRiseEarly;
			else
				return cell->iopList[0].tData.slackRiseLate;
		}
		else
		{
			if (_mode == EARLY)
				return cell->iopList[0].tData.slackFallEarly;
			else
				return cell->iopList[0].tData.slackFallLate;
		}
	}
	else
	{
		if (tokensz != 2)
			return 0.0 ;

		int64 hash = ComputeHash(tokens[1]);
		int64 pinIndex = Bsearch(cell->iipList, hash, 0, cell->iipList.size() - 1);
		if (pinIndex != -1)
		{
			if (_transition == RISE)
			{
				if (_mode == EARLY)
					return cell->iipList[pinIndex].tData.slackRiseEarly;
				else
					return cell->iipList[pinIndex].tData.slackRiseLate;
			}
			else
			{
				if (_mode == EARLY)
					return cell->iipList[pinIndex].tData.slackFallEarly;
				else
					return cell->iipList[pinIndex].tData.slackFallLate;
			}
		}
		else
		{
			pinIndex = Bsearch(cell->iopList, hash, 0, cell->iopList.size() - 1);
			if (pinIndex != -1)
			{
				if (_transition == RISE)
				{
					if (_mode == EARLY)
						return cell->iopList[pinIndex].tData.slackRiseEarly;
					else
						return cell->iopList[pinIndex].tData.slackRiseLate;
				}
				else
				{
					if (_mode == EARLY)
						return cell->iopList[pinIndex].tData.slackFallEarly;
					else
						return cell->iopList[pinIndex].tData.slackFallLate;
				}
			}
			else
			{
				if (cell->ckList[0].hash != hash)
				{
					return 0.0 ;
				}
				if (_transition == RISE)
				{
					if (_mode == EARLY)
						return cell->ckList[0].tData.slackRiseEarly;
					else
						return cell->ckList[0].tData.slackRiseLate;
				}
				else
				{
					if (_mode == EARLY)
						return cell->ckList[0].tData.slackFallEarly;
					else
						return cell->ckList[0].tData.slackFallLate;
				}
			}
		}
	}
}

};
