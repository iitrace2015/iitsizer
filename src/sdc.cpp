/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#include "sdc.h"
#include "timer.h"
#include "utility.h"
namespace iit{

/* Global variables declaration --------------------------------------- */

//extern CelllibStorage      celllibstorage_e;
extern CelllibStorage      celllibstorage_l;
//extern LibFileInfo         libinfo_e;
//extern LibFileInfo         libinfo_l;
extern CompleteTimingInfo  timing;

//extern Instance* CLKSRC;
//extern int       numInc;
//extern int       Verbose;
//extern int64     MaxNumPaths;
//extern int64     PathFlag_G;
//extern string    BenchmarkDir;
//extern bool     _isStaticDone;

//extern deque   < Instance >       InstanceList;
//extern vector  < Hash_Instance >  InstanceHashTable;
//extern deque   < SpefNet >        SpefNetList;
//extern vector  < Hash_Instance >  spefNetHashTable;
//extern vector  < SpefNet* >       Sort;
//extern vector  < SpefNet* >       IncNetList;
//extern deque   < SpefNet >        WireList;
//extern queue   < SpefNet* >       InitNetList;
//extern vector  < Instance* >      PIList;
//extern vector  < Instance* >      POList;
//extern vector  < InstanceIpin* >  ConeEndPoints;
//extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
//extern deque   < Slack_Instance > pathLUTSlackTable_Adv;

//extern std::tr1::unordered_map <char,bool> specCharLibDir;

/* End - global variables declaration -----------------------------------*/

/**
 * \param filename SDC file-name for parsing
 *
 * Begins the file parsing- .sdc
 */
void Timer::test_sdc_parser(string filename)
{
	SdcParser sp(filename);
	sp.init_sdc_parser(timing);
}

/**
 * \param tinfo Storage for timing assertions from .sdc
 *
 * Initiates SDC file parsing, sets units as per the directions in the file, and reads and stores timing assertions at PIs and POs
 */
void SdcParser:: init_sdc_parser (CompleteTimingInfo& tinfo)
{

	  PIPOTimingInfo      pipotiming;
	  POLoadCapTimingInfo loadtiming;
	  bool isClkATset   = false;
	  bool isClkSlewset = false;
	  deque<string>     tokens ;
	  _capScalingFac    = 1.0 ;
	  _timeScalingFac   = 1.0 ;

	  bool valid=read_line_as_tokens (is, tokens) ;

	  while(valid)
	  {
		  if(tokens[0] == "set_units")
		  {
			  assert(tokens.size() == 3);
			  if(tokens[1] == "-capacitance")
			  {
				  string val, unit;
				  val  = "";
				  unit = "";
				  int unit_starting_index = 0;

				  for (int i = 0; i < (int)tokens[2].size(); i++)
				  {
					  char c = tokens[2][i];
					  if(c == 'f' || c == 'p' || c == 'n' || c == 'u')
					  {
						  unit_starting_index = i;
						  break;
					  }
					  val.push_back(c);
				  }


				  double64 givenScale;
				  if(val == "")
					  givenScale = 1;
				  else
					  givenScale = atof(val.c_str());

				  for (int i = unit_starting_index ; i < (int)tokens[2].size(); i++)
				  {
					  unit.push_back(tokens[2][i]);
				  }

				   _capScalingFac = _capScalingFac* givenScale;

				   if(unit=="pf"      || unit=="PF" || unit=="pF")
				 	  _capScalingFac = _capScalingFac* 1000;
				   else if(unit=="nf" || unit=="NF" || unit=="nF")
				   	  _capScalingFac = _capScalingFac* 1000000;
				   else if(unit=="uf" || unit=="UF" || unit=="uF")
					  _capScalingFac = _capScalingFac* 1000000000;


			  }
			  else if(tokens[1]=="-time")
			  {
				  string val, unit;
				  val = "";
				  unit = "";
				  int unit_starting_index = 0;

				  for (int i = 0; i < (int)tokens[2].size(); i++)
				  {
					  char c = tokens[2][i];
					  if(c == 'f' || c == 'p' || c == 'n' || c == 'u')
					  {
						  unit_starting_index = i;
						  break;
					  }
					  val.push_back(c);
				  }

				  double64 givenScale;

				  if(val == "")
					  givenScale = 1;
				  else
					  givenScale = atof(val.c_str());

				  for (int i = unit_starting_index ; i < (int)tokens[2].size(); i++)
				  {
					  unit.push_back(tokens[2][i]);
				  }

				   _timeScalingFac = _timeScalingFac* givenScale;

				   if(unit=="fs"      || unit=="FS" || unit=="fS" )
				 	  _timeScalingFac = 0.001;
				   else if(unit=="ns" || unit=="NS" || unit=="nS" )
				   	  _timeScalingFac = 1000;
				   else if(unit=="us" || unit=="US" || unit=="uS" )
				 	  _timeScalingFac = 1000000;
				   else if(unit=="ms" || unit=="uS" || unit=="mS" )
				   	  _timeScalingFac = 1000000000;

			  }

		  }

		  if(tokens[0] == "set_input_delay")
		  {
			  pipotiming.earlyrise = 0;
			  pipotiming.earlyfall = 0;
			  pipotiming.laterise  = 0;
			  pipotiming.latefall  = 0;
			  bool inputDelay = false;
			  for(int v = 0; v < (int)tokens.size(); v++)
			  {
				  if(tokens[v] == "get_ports")
				  {
					  pipotiming.name      = tokens[v+1];
					  pipotiming.hash      = ComputeHash(tokens[v+1]);

				  }
					  else if(tokens[v] == "-add_delay")
				  {

					  inputDelay = true;
					  pipotiming.earlyrise = std::atof(tokens[v+1].c_str())*_timeScalingFac;
					  pipotiming.earlyfall = std::atof(tokens[v+1].c_str())*_timeScalingFac;
					  pipotiming.laterise  = std::atof(tokens[v+1].c_str())*_timeScalingFac;
					  pipotiming.latefall  = std::atof(tokens[v+1].c_str())*_timeScalingFac;

				  }
				  if(tokens[v]==tinfo.clock.name)
					  isClkATset = true;

			  }
			  if(inputDelay == false)
			  {
				  pipotiming.earlyrise = std::atof(tokens[1].c_str())*_timeScalingFac;
				  pipotiming.earlyfall = std::atof(tokens[1].c_str())*_timeScalingFac;
				  pipotiming.laterise  = std::atof(tokens[1].c_str())*_timeScalingFac;
				  pipotiming.latefall  = std::atof(tokens[1].c_str())*_timeScalingFac;

			  }


			  tinfo.at.push_back(pipotiming);
		  }
		  else if(tokens[0] == "set_driving_cell")
		  {

			  pipotiming.earlyrise = 0;
			  pipotiming.earlyfall = 0;
			  pipotiming.laterise  = 0;
			  pipotiming.latefall  = 0;

			  for(int v = 0; v < (int)tokens.size(); v++)
			  {
				  if(tokens[v] == "get_ports")
				  {
					  pipotiming.name=tokens[v+1];
					  pipotiming.hash = ComputeHash(tokens[v+1]);

				  }
				  else if(tokens[v] == "-input_transition_fall")
				  {
					  pipotiming.earlyfall = std::atof(tokens[v+1].c_str())*_timeScalingFac;
					  pipotiming.latefall  = std::atof(tokens[v+1].c_str())*_timeScalingFac;

				  }
				  else if(tokens[v] == "-input_transition_rise")
				  {
					  pipotiming.earlyrise = std::atof(tokens[v+1].c_str())*_timeScalingFac;
					  pipotiming.laterise  = std::atof(tokens[v+1].c_str())*_timeScalingFac;

				  }
				  else if(tokens[v] == "-lib_cell")
				  {

					  int64 ind = Bsearch(celllibstorage_l.cellhashtable,ComputeHash(tokens[v+1]),0,celllibstorage_l.cellhashtable.size()-1);
					  assert(ind != -1);
					  pipotiming.drivingInd = celllibstorage_l.cellhashtable[ind].index;
				  }

				  if(tokens[v] == tinfo.clock.name)
					  isClkSlewset = true;


			  }

			  tinfo.slew.push_back(pipotiming);


		  }
		  else if(tokens[0] == "set_output_delay")
		  {

			  pipotiming.earlyrise = 0;
			  pipotiming.earlyfall = 0;
			  pipotiming.laterise  = 0;
			  pipotiming.latefall  = 0;
			  bool addDelay = false;
			  for(int v = 0; v < (int)tokens.size(); v++)
			  {
				  if(tokens[v] == "get_ports")
				  {
					  pipotiming.name      = tokens[v+1];
					  pipotiming.hash      = ComputeHash(tokens[v+1]);

				  }
					  else if(tokens[v] == "-add_delay")
				  {

					  addDelay = true;
					  pipotiming.earlyrise = std::atof(tokens[v+1].c_str())*_timeScalingFac;
					  pipotiming.earlyfall = std::atof(tokens[v+1].c_str())*_timeScalingFac;
					  pipotiming.laterise  = std::atof(tokens[v+1].c_str())*_timeScalingFac;
					  pipotiming.latefall  = std::atof(tokens[v+1].c_str())*_timeScalingFac;

				  }
			  }
			  if(addDelay == false)
			  {
				  pipotiming.earlyrise = std::atof(tokens[1].c_str())*_timeScalingFac;
				  pipotiming.earlyfall = std::atof(tokens[1].c_str())*_timeScalingFac;
				  pipotiming.laterise  = std::atof(tokens[1].c_str())*_timeScalingFac;
				  pipotiming.latefall  = std::atof(tokens[1].c_str())*_timeScalingFac;

			  }
			  tinfo.delay.push_back(pipotiming);

			  //Assuming that the clock has already been read.
			  pipotiming.earlyrise = UNDEFINED_RAT_EARLY;
			  pipotiming.earlyfall = UNDEFINED_RAT_EARLY;
			  pipotiming.laterise  = tinfo.clock.period - pipotiming.laterise;
			  pipotiming.latefall  = tinfo.clock.period - pipotiming.latefall;

			  tinfo.rat.push_back(pipotiming);

		  }
		  else if(tokens[0] == "set_load")
		  {
			  loadtiming.poname   = tokens[4];
			  loadtiming.earlycap = std::atof(tokens[2].c_str())*_capScalingFac;
			  loadtiming.latecap  = loadtiming.earlycap*_capScalingFac;
			  loadtiming.hash     = ComputeHash(tokens[4]);

			  tinfo.loadcap.push_back(loadtiming);

		  }
		  else if(tokens[0] == "create_clock")
		  {
			  for( int v = 0; v < (int)tokens.size(); v++)
			  {
				  if(tokens[v]=="get_ports")
				  {
					  tinfo.clock.name = tokens[v+1];
				  }
				  if(tokens[v]=="-period")
					  tinfo.clock.period = std::atof(tokens[v+1].c_str())*_timeScalingFac;

			  }

		  }

		  // go ahead and read next line and run the loop back again.
		  valid=read_line_as_tokens (is, tokens) ;
	  }

	  if(isClkATset == false)
	  {
		  pipotiming.name      = tinfo.clock.name;
		  pipotiming.earlyrise = 0;
		  pipotiming.earlyfall = 0;
		  pipotiming.laterise  = 0;
		  pipotiming.latefall  = 0;
		  pipotiming.hash      = ComputeHash(tinfo.clock.name);
		  tinfo.at.push_back(pipotiming);
	  }
	  if(isClkSlewset == false)
	  {
		  pipotiming.name      = tinfo.clock.name;
		  pipotiming.earlyrise = 0;
		  pipotiming.earlyfall = 0;
		  pipotiming.laterise  = 0;
		  pipotiming.latefall  = 0;
		  pipotiming.hash      = ComputeHash(tinfo.clock.name);
		  tinfo.slew.push_back(pipotiming);
	  }
	  // closing the file
	  free(is);
	  is.close();


	  // sorting various hashtables

	  std::sort (timing.at.begin()      , timing.at.end()     , CompareByHashnum < PIPOTimingInfo      >());
	  std::sort (timing.rat.begin()     , timing.rat.end()    , CompareByHashnum < PIPOTimingInfo      >());
	  std::sort (timing.slew.begin()    , timing.slew.end()   , CompareByHashnum < PIPOTimingInfo      >());
	  std::sort (timing.loadcap.begin() , timing.loadcap.end(), CompareByHashnum < POLoadCapTimingInfo >());
	  std::sort (timing.delay.begin()   , timing.delay.end()  , CompareByHashnum < PIPOTimingInfo      >());

 return;

}

/**
 * \param is Input file stream
 * \param tokens Set of tokens read and split from a line
 * \param includeSpecialChars Flag for including special characters/delimiters as a part of the token name
 * \return Flag for line reading was successful or failed
 *
 * Reads a line from the current position of input file stream and returns set of meaningful tokens from the line that is split
 */
bool SdcParser:: read_line_as_tokens (istream& is, deque<string>& tokens, bool includeSpecialChars)
{

  tokens.clear() ;

  string line ;
  std::getline (is, line) ;

  while (is && tokens.empty())
  {

    string token = "" ;

    for (int i=0; i < (int)line.size(); ++i)
    {
		  char currChar = line[i] ;
		  bool isSpecialChar = _specCharSdcDir[currChar] ;

		  if (std::isspace (currChar) || isSpecialChar)
		  {

				if (!token.empty())
				{
				  // Add the current token to the list of tokens
				  tokens.push_back(token) ;
				  token.clear() ;
				}

				if (includeSpecialChars && isSpecialChar)
				{
				  tokens.push_back(string(1, currChar)) ;
				}

		  }
		  else
		  {
			// Add the char to the current token
			token.push_back(currChar) ;
		  }

    }

    if (!token.empty())
      tokens.push_back(token) ;


    if (tokens.empty())
      // Previous line read was empty. Read the next one.
      std::getline (is, line) ;
  }

  return !tokens.empty() ;
}

};


