/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_MACRODEF_H_
#define IIT_MACRODEF_H_
namespace iit{

#define X                             0                   ///< Horizontal axis or row entry in 2D array
#define Y                             1                   ///< Vertical axis or column entry in 2D array
#define PI			                 -2                   ///< Cell library index for PI in celllib from celllibstorage
#define PO			                 -3                   ///< Cell library index for PO in celllib from celllibstorage
#define INF                           999999999           ///< Generic large number, towards plus infinity
#define NINF                         -999999999           ///< Generic large number, towards minus infinity
#define IPIN		                  1                   ///< Pin is input pin
#define OPIN	                      2                   ///< Pin is output pin
#define SLEW                          0                   ///< Used in slew computations
#define FAIL                         -1                   ///< Operation fail flag
#define ZERO	 	                  0                   ///< Absolute zero
#define BOTH 						  2                   ///< Signal mode, both means Early as well as Late
#define DEBUG                         0                   ///< Debug mode
#define CKPIN	 	                  3                   ///< Pin is a clock pin
#define PI_PIN	 	                 -2                   ///< Pin is a primary input pin
#define SANITY                       -9999999             ///< Again, large negative number
#define PO_PIN	 	                 -3                   ///< Pin is a primary output pin
#define CLK_SRC		                 -4                   ///< Cell library index for Clock source in cellllib from celllibstorage
#define TESTING                       0                   ///< Testing/logging
#define FORWARD                       0                   ///< Timing propagation sense- forward
#define REVERSE                       1                   ///< Timing propagation sense- backward/reverse
#define STARTPI                       100                 ///< Line indicator of PI-start for verilog parsing
#define STARTPO                       101                 ///< Line indicator of PO-start for verilog parsing
#define MAXIPINS                      1000                ///< Maximum assumed bound on no. of input pins of a cell
#define MAXOPINS                      200                 ///< Maximum assumed bound on no. of output pins of a cell
#define PARALLEL 					  1                   ///< Flag for multithreading
#define POSITIVE                      true                ///< Positive unate
#define NEGATIVE                      false               ///< Negative unate
#define STARTNET                      102                 ///< Line indicator of wire-start for verilog parsing
#define STARTCELL                     103                 ///< Line indicator of cell-start for verilog parsing
#define MAXBUFLEN 					  512                 ///< Maximum buffer length when a line is read in
#define FORBIDDEN                     987654              ///< Forbidden timing value
#define NFORBIDDEN                   -987654              ///< Forbidden negative timing value
#define MaxBktSize                    78643300            ///< Bucket size for computing hash of a string
#define CLKPININDEX                  -4                   ///< Clock pin index
#define HOLD_RISING                   1                   ///< Timing sense- hold rising
#define CAPACITANCE                   1                   ///< Capacitance
#define RISING_EDGE                   2                   ///< Timing sense- rising edge
#define NUM_THREADS 				  1                   ///< Number of threads for openMP multithreading
#define FALLING_EDGE                 -2                   ///< Timing sense- falling edge
#define NEITHER_EDGE                  3                   ///< Timing sense- neither of the two edges
#define HOLD_FALLING                  0                   ///< Timing sense- hold falling
#define DEFAULTINDEX                  -6                  ///< Default index for AT criticality
#define SETUP_RISING                  1                   ///< Timing sense- setup rising
#define COMBINATIONAL                 3                   ///< Timing type- combinational
#define SETUP_FALLING                 0                   ///< Timing sense- Setup falling
#define NUM_FUNCTIONS                 300                 ///< Total number of functions in the project
#define NEGATIVE_UNATE               -1                   ///< Negative unate timing arc
#define POSITIVE_UNATE                1                   ///< Positive unate timing arc
#define FORBIDDEN_LATE               -987654              ///< Forbidden value in late mode
#define PATH_BLOCK_SIZE               100                 ///< Path block size- for printing paths in chunks
#define FORBIDDEN_SLACK               987654              ///< Forbidden slack value where it is undefined
#define FORBIDDEN_EARLY               987654              ///< Forbidden value in early mode
#define UNDEFINED_SLACK               987654              ///< Undefined slacks at pins
#define FORBIDDEN_NSLACK 			  -987654             ///< Forbidden/undefined negative slacks at pins
#define COMMAND_HASH_SIZE             786433              ///< Command hash size
#define UNDEFINED_AT_LATE            -987654              ///< Forbidden AT value, late mode
#define UNDEFINED_RAT_LATE 			  987654              ///< Forbidden RAT value, late mode
#define UNDEFINED_AT_EARLY            987654              ///< Forbidden AT value, early mode
#define UNDEFINED_RAT_EARLY 		 -987654              ///< Forbidden RAT value, early mode
#define PQUEUE_INITIAL_BUFFER         20000               ///< Initial sizing count for priority queues
#define SPEF_LIST_INITIAL_BUFFER      200000              ///< Initial sizing count for spef list
#define FLOATING_ERROR_THRESHOLD      0.01                ///< Floating point error threshold, for evaluations
#define SPEF_LIST_INCREMENT_BUFFER    10000               ///< Incremental sizing count for the spef buffer
#define SPEF_NET_HASH_TABLE_BUFFER    200000              ///< Buffer size count for spef-net hash table

// SIZER MACROS ADDED
#define DONE                          1                   ///< Done flag, ubiquitous
#define UNBIASED                      3                   ///< Guiding the LDP convergence- the priority style- unbiased cost functions
#define LDP_ALPHA					  1                   ///< ALPHA for LDP iterations- Max. fanout scale down factor
#define ALPHA 						  0.7                 ///< Max. capacitance scale down factor in initial sizing
#define FAST_MI           			  true                ///< Fast moments computation algo.
#define NEW_MI           			  false               ///< Another different flag for moments computation
#define MAX_ITR          			  200                 ///< Iteration upper bound in LDP
#define GOOD_TNS				      0                   ///< Acceptable TNS threshold
#define SET_BETA           			  true                ///< Compute second moment flag
#define ZERO_VAL                      -1e-6               ///< Zero TNS value considering floating point errors
#define NOT_DONE                      0                   ///< Operation not-done flag, ubiquitous
#define STUCK_TNS				      2                   ///< State- TNS is stuck in LRS iterations
#define STUCK_LKG				      3                   ///< State- Leakage is stuck in LRS interations
#define STUCK_COST				      4                   ///< State- cost is stuck in LRS iterations
#define GOOD_SLACK				      1                   ///< Approval flag for good slack
#define SLACK_CUTOFF                  20.0                ///< Cut-off slack
#define DEFAULT_LAMBDA 				  12                  ///< Initial value of Lagrangian multipliers
#define CONVERGENCE_COUNT 			  1                   ///< No. of times LRS iterations to be converged before stopping
#define APPROX_PR					  true                ///< Local timing based power reduction flag
#define APPROX_TR					  true                ///< Local timing based timing recovery flag
#define AT_MODE                       false               ///< Timing arc selection based on AT criticality for Ceff computation
#define DELAY_MODE                    true                ///< Timing arc selection based on delay criticality for Ceff computation
#define SLEW_MODE                     false               ///< Timing arc selection based on slew criticality for Ceff computation
#define NUM_CEFF_ITR                  1                   ///< Number of iterations for effective capacitance computation


///////////////////  SIZER OR TIMER ?  ///////////////////////////////////////////////////////////////////////////
#define SIZER                         true                ///< Tool mode- either sizer or timer
#define ISPD_2012					  true               ///< True for running ISPD 2012 benchmarks
#define TIMER                         !SIZER              ///< Tool mode- Timer when sizer not running
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#if SIZER == false
#define SAVE_NET           			  false               ///< Saving net parasitics for sizing

#define ELMORE	 					  true			      ///< Elmore delay model (also for TAU contest)
#define D2M		     				  false               ///< D2M delay model
#define DM1	     					  false               ///< DM1 delay model
#define DM2 						  false               ///< DM2 delay model

#define SCALED_S2M					  false			      ///< Scaled S2M slew model
#define TAU_SLEW					  true                ///< TAU contest slew model
#define PERI    					  false               ///< PERI slew model

#define ECM                           false               ///< Effective capacitance modeling, else somple lumped-up model
#endif

#if SIZER == true && ISPD_2012 == true
#define SAVE_NET           			  true                ///< Saving net parasitics for sizing

#define ELMORE	 					  false			      ///< Elmore delay model (also for TAU contest)
#define D2M		     				  true                ///< D2M delay model
#define DM1	     					  false               ///< DM1 delay model
#define DM2 						  false               ///< DM2 delay model

#define SCALED_S2M					  false			      ///< Scaled S2M slew model
#define TAU_SLEW					  false               ///< TAU contest slew model
#define PERI    					  true                ///< PERI slew model

#define ECM                           false               ///< Effective capacitance modeling, else somple lumped-up model
#endif

#if SIZER == true && ISPD_2012 == false
#define SAVE_NET           			  true                ///< Saving net parasitics for sizing

#define ELMORE	 					  false			      ///< Elmore delay model (also for TAU contest)
#define DM1	     					  false               ///< DM1 delay model
#define DM2 						  false               ///< DM2 delay model
#define D2M		     				  true                ///< D2M delay model

#define PERI    					  false               ///< PERI slew model
#define SCALED_S2M					  true			      ///< Scaled S2M slew model
#define TAU_SLEW					  false               ///< TAU contest slew model

#define ECM                           true                ///< Effective capacitance modeling, else somple lumped-up model
#endif



};

#endif /*IIT_MACRODEF_H_*/
