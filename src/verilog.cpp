/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#include "include.h"
#include "verilog.h"
#include "utility.h"
#include "timer.h"
#include "container.h"
#include "globals.h"
namespace iit{


/* Global variables declaration --------------------------------------- */

extern CelllibStorage      celllibstorage_e;
extern CelllibStorage      celllibstorage_l;
//extern LibFileInfo         libinfo_e;
//extern LibFileInfo         libinfo_l;
extern CompleteTimingInfo  timing;

extern Instance* CLKSRC;
//extern int       numInc;
//extern int       Verbose;
//extern int64     MaxNumPaths;
//extern int64     PathFlag_G;
//extern string    BenchmarkDir;
//extern bool     _isStaticDone;

extern deque   < Instance >       InstanceList;
extern vector  < Hash_Instance >  InstanceHashTable;
//extern deque   < SpefNet >        SpefNetList;
//extern vector  < Hash_Instance >  spefNetHashTable;
//extern vector  < SpefNet* >       Sort;
//extern vector  < SpefNet* >       IncNetList;
extern deque   < SpefNet >        WireList;
//extern queue   < SpefNet* >       InitNetList;
extern vector  < Instance* >      PIList;
extern vector  < Instance* >      POList;
//extern vector  < InstanceIpin* >  ConeEndPoints;
//extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
//extern deque   < Slack_Instance > pathLUTSlackTable_Adv;

//extern std::tr1::unordered_map <char,bool> specCharLibDir;

/* End - global variables declaration -----------------------------------*/

/**
 * \param c Input character for checking
 *
 * Checks whether the given character 'c' belongs to the pre-decided set of special characters.
 * These characters are not included in the tokens extracted from a line in file reading
 */
inline bool Verilog:: is_special_char (char c)
{

  static const char specialChars[] = {'(', ')', ',', ':', ';', '/', '#', '[', ']', '{', '}', '*', '\"', '\\'} ;

  for (int i=0; i < (int)sizeof(specialChars); ++i)
  {
	if (c == specialChars[i])
		return true ;
  }

  return false ;
}

/**
 * \param buffer Verilog file name
 *
 * Creates an instance of verilog class to open 'buffer' and initiates the file parsing
 */
void Timer:: test_verilog_parser(string buffer)
{
	Verilog vp (buffer) ;

	vp.forwardPropagate_ptr = _forwardPropagate_ptr;

	vp.init_verilog_parser(buffer);

	return;
}

/**
 * \param buffer Verilog file name
 *
 * Reads-in the verilog file, processes primary inputs, outputs and wires separately. It then traverses through the
 * module-net connections specified to create the circuit graph
 */
void Verilog:: init_verilog_parser (string buffer)
{


	  fix_checkpoints();
	  createNetHash();

	  is.seekg(_moduleStart);
	  string moduleName ;

	  bool valid = read_module (moduleName) ;
	  _isModuleDone = true;

	  if(_nextStroke==STARTPI)
	  {
		  ProcessPI();
		  _isPIDone = true;
	  }
	  else if(_nextStroke==STARTPO)
	  {
		  ProcessPO();
		  _isPODone = true;
	  }
	  else if(_nextStroke==STARTNET)
	  {
		  ProcessNet();
	  }
	  if(_nextStroke==STARTPI)
	  {
		  ProcessPI();
		  _isPIDone = true;
	  }
	  else if(_nextStroke==STARTPO)
	  {
		  ProcessPO();
		  _isPODone = true;
	  }
	  else if(_nextStroke==STARTNET)
	  {
		  ProcessNet();
	  }

	  if(_isPIDone && _isPODone)
	  {
		  sort(PIPONets.begin(),PIPONets.end(),CompareByHashnum<Hash_Instance>());
	  }

	  if(_nextStroke==STARTPI)
	  {
		  ProcessPI();
		  _isPIDone = true;
	  }
	  else if(_nextStroke==STARTPO)
	  {
		  ProcessPO();
		  _isPODone = true;
	  }
	  else if(_nextStroke==STARTNET)
	  {
		  ProcessNet();
	  }

	  sort(PIPONets.begin(),PIPONets.end(),CompareByHashnum<Hash_Instance>());
	  _isNetDone = true;

	  do {
			is.seekg(_cursorPos);

			string cellType, cellInst ;
			deque<pair<string, string> > pinNetPairs ;
			valid = read_cell_inst (cellType, cellInst, pinNetPairs) ;

			if (valid)
			{
				Instance cell;
				int64 key1   = ComputeHash(cellType);
				int64 index1 = Bsearch(celllibstorage_e.cellhashtable,key1,0,celllibstorage_e.cellhashtable.size()-1);
				int64 index2 = Bsearch(celllibstorage_l.cellhashtable,key1,0,celllibstorage_l.cellhashtable.size()-1);

				if(index1 == -1 || index2 == -1  )
				  continue;

				int64 index_e         = celllibstorage_e.cellhashtable[index1].index; //to get the index in cell lib
				int64 index_l         = celllibstorage_l.cellhashtable[index2].index;
				cell.info_early       = &(celllibstorage_e.celllib[index_e]);
				cell.info_late        = &(celllibstorage_l.celllib[index_l]);
				cell.isIncluded       = true;
				cell.cellLibIndex_e   = index_e;cell.cellLibIndex_l = index_l;
				int64 key             = ComputeHash(cellInst);
				cell.instanceName     = cellInst;
				cell.hash             = key;

				cell.iipList.reserve(cell.info_early->ipinhashtable.size());

				for(int z = 0;z < (int)cell.info_early->ipinhashtable.size();z++)
				{
					InstanceIpin ip;
					ip.hash           = cell.info_early->ipinhashtable[z].hash;
					ip.name.cellid    = cell.hash;
					ip.name.icellName = cell.instanceName;
					ip.name.pinName   = cell.info_early->ipins[cell.info_early->ipinhashtable[z].index].name;
					cell.iipList.push_back(ip);
				}

				InstanceList.push_back(cell);
				InstanceList.back().iopList.reserve(cell.info_early->opinhashtable.size());

				for(int z = 0;z < (int)cell.info_early->opinhashtable.size();z++)
				{
					InstanceOpin op;
					op.hash               = cell.info_early->opinhashtable[z].hash;
					op.name.cellid        = cell.hash;
					op.name.icellName     = cell.instanceName;
					op.name.pinName       = cell.info_early->opins[cell.info_early->opinhashtable[z].index].name;
					SpefNet* wirePtr      = new SpefNet();
					op.capacitanceEarly   = cell.info_early->opins[cell.info_early->opinhashtable[z].index].capacitance;
					op.capacitanceLate    = cell.info_late->opins[cell.info_late->opinhashtable[z].index].capacitance;
					InstanceList.back().iopList.push_back(op);
					wirePtr->port.pinPtr  = &InstanceList.back().iopList.back();
					wirePtr->port.cellPtr = &InstanceList.back();

					InstanceList.back().iopList.back().wire = wirePtr;
				}

				if(cell.info_early->isSequential)
				{
					InstanceIpin ip;
					ip.name.cellid     = cell.hash;
					ip.name.icellName  = cell.instanceName;
					ip.name.pinName    = cell.info_early->clkpin[0].name;
					ip.hash            = cell.info_early->clkpin[0].hash;
					InstanceList.back().ckList.push_back(ip);
				}

				SetRelatedPins(&InstanceList.back());
				Hash_Instance h;                    //making the indexTable of InstanceList

				h.hash  = key;
				h.index = InstanceList.size()-1;
				InstanceHashTable.push_back(h);

				for (int i = 0; i < (int)pinNetPairs.size(); ++i)
				{
				int64 token = ComputeHash(pinNetPairs[i].second);
				int64 index = Bsearch(wireHashTable,token,0,wireHashTable.size()-1);

				if(index == -1) continue;
				if(wireHashTable[index].index == -1)
				{
					SpefNet wire;
					wire.netName = pinNetPairs[i].second;
					wire.hash    = token;
					WireList.push_back(wire);

					wireHashTable[index].index = WireList.size()-1;
					index                      = WireList.size()-1;
				}
				else
				{
					index = wireHashTable[index].index; //the index in WireList gets stored here
				}

				/*check the pin specified is an output pin or an input pin of the given cell*/

				//first find the hash of the pin

				int64 pinHash = ComputeHash(pinNetPairs[i].first);


				//check this is a PI pin
				//search in the PI list

				int64 ipinIndex = Bsearch(cell.info_early->ipinhashtable,pinHash,0,cell.info_early->ipinhashtable.size()-1);
				if(ipinIndex!=-1)
				{
					ipinIndex         = cell.info_early->ipinhashtable[ipinIndex].index;  //stores the ipinIndex for the cell
					SpefTap tap;
					tap.tapName.n1    = cellInst;
					tap.tapName.n2    = pinNetPairs[i].first;
					tap.tapName.pinid = key; // hash of the instance name
					string temp       = cellInst;
					temp.append(1,':');
					temp.append(pinNetPairs[i].first);

					tap.hash          = ComputeHash(temp);

					WireList[index].taps.push_back(tap);



				}
				else if(int64 opinIndex = Bsearch(cell.info_early->opinhashtable,pinHash,0,cell.info_early->opinhashtable.size()-1)!=-1) //TODO:check for syntax validity
				{
					opinIndex = cell.info_early->opinhashtable[opinIndex].index;
					SpefPort port;

					port.portName.n1    = cellInst;
					port.portName.n2    = pinNetPairs[i].first;
					port.portName.pinid = key;
					string temp         = cellInst;

					temp.append(1,':');
					temp.append(pinNetPairs[i].first);

					port.hash            = ComputeHash(temp);
					WireList[index].port = port;

				}
				else if(cell.info_early->isSequential) //check for a ck pin
				{
					SpefTap tap;
					tap.tapName.n1    = cellInst;
					tap.tapName.n2    = pinNetPairs[i].first;
					tap.tapName.pinid = key; // hash of the instance name

					string temp = cellInst;
					temp.append(1,':');
					temp.append(pinNetPairs[i].first);

					tap.hash = ComputeHash(temp);

					WireList[index].taps.push_back(tap);

				}
				}

			}
	    }

	  while (valid) ;
	  _isCellDone = true;
}

/**
 * \param moduleName Name of the verilog module
 *
 * Reads-in the verilog module details
 */
bool Verilog:: read_module (string& moduleName)
{

  deque<string> tokens ;
  bool valid = read_line_as_tokens2(is, tokens,false) ;

  while (valid) {

    if (tokens[0] == "module")
    {
		moduleName = tokens[1] ;
		break ;
    }

    valid = read_line_as_tokens2 (is, tokens,false) ;
  }

  // Read and skip the port names in the module definition
  // until we encounter the tokens {"Start", "PIs"}
  while (valid && !(tokens[0]=="input" || tokens[0]=="wire" || tokens[0]=="output"))
  {
    valid = read_line_as_tokens2 (is, tokens) ;
  }

  if(tokens[0]=="input")
	  _nextStroke   = STARTPI;
  else if(tokens[0]=="wire")
	  _nextStroke   = STARTNET;
  else if(tokens[0]=="output")
	  _nextStroke   = STARTPO;
  else
	  _nextStroke   = -1;

  return valid ;
}

/**
 * \param primaryInput Set of primary inputs in one line (may be a bus)
 *
 * Reads a set of primary inputs from one line at a time
 */
bool Verilog :: read_primary_input (deque < string > & primaryInput)
{
	primaryInput.clear();
	deque<string> tokens ;
	bool valid = read_line_as_tokens2 (is, tokens,false) ;

	if (valid && tokens[0] == "input")
	{
		if(tokens.size() == 2)  // Single bit input
		{
			primaryInput.push_back(tokens[1]) ;
		}
		else // Input is a multi-bit bus, damn you verilog !
		{
			string baseBus;
			string higher, lower;
			int highNum, lowNum;
			higher = "";
			lower  = "";

			for(int i = 0; i < (int)tokens[1].size(); i++)
			{
			  char c = tokens[1][i];
			  if(c != '[' && c != ']')
			  {
				  higher.push_back(c);
			  }
			}

			for(int i = 0; i < (int)tokens[2].size(); i++)
			{
			  char c = tokens[1][i];
			  if(c != '[' && c != ']')
			  {
				  lower.push_back(c);
			  }
			}

			highNum = atoi(higher.c_str());
			lowNum  = atoi(lower.c_str());

			assert(lowNum == 0);

			for(int i = lowNum ; i < highNum ; i++)
			{
				baseBus = tokens[3];
				baseBus.append("[");
				ostringstream convert;   // stream used for the conversion
				convert << i;      // insert the textual representation of 'Number' in the characters in the stream
				baseBus.append(convert.str());
				baseBus.append("]");
				primaryInput.push_back(baseBus);
			}

		}

	}
	else if(tokens[0]=="output")
	{
		_nextStroke = STARTPO;
		string lin;
		return false ;
	}
	else if(tokens[0]=="wire")
	{
		_nextStroke = STARTNET;
		return false;
	}
	else if(tokens.size()>2)
	{
		_nextStroke = STARTCELL;
		return false;
	}
	else
	{
		_nextStroke = -1;
		return false;
	}
	return valid ;
}


/**
 * \param primaryOutput Set of primary outputs in one line (may be a bus)
 *
 * Reads a set of primary outputs from one line at a time
 */
bool Verilog::read_primary_output (deque<string> & primaryOutput)
{

	primaryOutput.clear();

	deque<string> tokens ;
	bool valid = read_line_as_tokens2 (is, tokens,false) ;

	if (valid && tokens[0] == "output")
	{
		if(tokens.size() == 2)  // Single bit output
		{
			primaryOutput.push_back(tokens[1]) ;
		}
		else // Output is a multi-bit bus, damn you verilog !
		{
			string baseBus;
			string higher, lower;
			int highNum, lowNum;
			higher = "";
			lower = "";

			for(int i = 0; i < (int)tokens[1].size(); i++)
			{
				char c = tokens[1][i];
				if(c != '[' && c != ']')
				{
					higher.push_back(c);
				}
			}

			for(int i = 0; i < (int)tokens[2].size(); i++)
			{
				char c = tokens[1][i];
				if(c != '[' && c != ']')
				{
				  lower.push_back(c);
				}
			}

			highNum = atoi(higher.c_str());
			lowNum  = atoi(lower.c_str());
			assert(lowNum == 0);

			for(int i = lowNum ; i < highNum ; i++)
			{
				baseBus = tokens[3];
				baseBus.append("[");
				ostringstream convert;   // stream used for the conversion
				convert << i;      // insert the textual representation of 'Number' in the characters in the stream
				baseBus.append(convert.str());
				baseBus.append("]");
				primaryOutput.push_back(baseBus);
			}

		}

	}
	else if(tokens[0]=="input")
	{
		_nextStroke = STARTPI;
		return false ;
	}
	else if(tokens[0]=="wire")
	{
		_nextStroke = STARTNET;
		return false;
	}
	else if(tokens.size()>2)
	{
		_nextStroke = STARTCELL;
		return false;
	}
	else
	{
		_nextStroke = -1;
		return false;
	}

	return valid ;
}

/**
 * \param wire wire (name) in one line (may be a bus)
 *
 * Reads a wire (from declaration) from one line at a time
 */
bool Verilog::read_wire (string& wire)
{

	wire = "" ;

	deque<string> tokens ;
	bool valid = read_line_as_tokens2 (is, tokens,false) ;

	if (valid && tokens[0] == "wire")
	{
		wire = tokens[1] ;
	}
	else if(tokens[0]=="input")
	{
		_nextStroke = STARTPI;
		return false ;
	}
	else if(tokens[0]=="output")
	{
		_nextStroke = STARTPO;
		return false ;
	}
	else if(tokens.size()>2)
	{
		_nextStroke = STARTCELL;
		return false ;
	}
	else
	{
		_nextStroke = -1;
		return false ;
	}

	return valid ;
}

/**
 * \param cellType Cell type of the gate
 * \param cellInstName Name of the gate
 * \param pinNetPairs Pin-net connection pairs for the given gate
 *
 * Reads connectivity information of a gate, constructs pin-net connection pairs from the given information
 */
bool Verilog::read_cell_inst (string& cellType, string& cellInstName,
                                    deque<std::pair<string, string> >& pinNetPairs)
{

	cellType     = "" ;
	cellInstName = "" ;
	pinNetPairs  .clear() ;

	deque<string> tokens ;

	bool valid = read_line_as_tokens2 (is, tokens,false) ;

	if (tokens.size() == 1)
		return false ;


	cellType     = tokens[0] ;
	cellInstName = tokens[1] ;

	for (int i = 2; i < (int)tokens.size()-1; i += 2)
	{
		string pinName = tokens[i].substr(1) ;  // skip the first character of tokens[i]
		pinNetPairs.push_back(std::make_pair(pinName, tokens[i+1])) ;
	}

	return valid ;
}

/**
 * \param is Input file stream
 * \param tokens Set of tokens read and split from a line
 * \param includeSpecialChars Flag for including special characters in the tokens while splitting a line
 * \return Flag for line reading was successful or failed
 *
 * Reads a line from the current position of input file stream
 */
bool Verilog::read_line_as_tokens2 (istream& is, deque<string>& tokens, bool includeSpecialChars )
{

	tokens.clear() ;

	string line ;
	std::streampos init;

	init = is.tellg();
	std::getline (is, line) ;

	if(line.empty())
		_cursorPos = is.tellg();
	else
		_cursorPos = init;

	if(_isPIDone && _isPODone && (!line.empty()) && /*_isCellDone && */_isNetDone)
	{
	  _cursorPos  = is.tellg();
	  _isCellDone = true;
	}
	while (is && tokens.empty())
	{

		string token = "" ;

		for (int i = 0; i < (int)line.size(); ++i)
		{
			char currChar = line[i] ;
			if (currChar=='{')
			{
				_bracket_cnt2=_bracket_cnt2+1;
			}
			else if(currChar=='}')
			{
				_bracket_cnt2=_bracket_cnt2-1;
			}
			bool isSpecialChar = is_special_char3(currChar) ;

			if (std::isspace (currChar) || isSpecialChar)
			{

				if (!token.empty())
				{
					// Add the current token to the list of tokens
					tokens.push_back(token) ;
					token.clear() ;
				}

				if (includeSpecialChars && isSpecialChar)
				{
					tokens.push_back(string(1, currChar)) ;
				}

			}
			else
			{
				// Add the char to the current token
				token.push_back(currChar) ;
			}

		}

		if (!token.empty())
		  tokens.push_back(token) ;

		if (tokens.empty() || tokens[0][0]=='/')
		{
			// Previous line read was empty. Read the next one.
			tokens.clear();
			_cursorPos = is.tellg();
			std::getline (is, line) ;
		}

	}

	return !tokens.empty() ;
}

/**
 * Processes all the primary input declarations and creates Instances for them in InstanceList
 */
void Verilog::ProcessPI()
{
	bool valid = false;
	is.seekg(_cursorPos);
	std::streampos h;
	h  = _cursorPos;

	string line;
	do
	{
	    deque <string> primaryInput ;
	    valid = read_primary_input (primaryInput) ;

	    for (int z = 0; z < (int)primaryInput.size(); z++)
	    {
			if (valid)
			{
			  string pInput     = primaryInput[z];
			  Instance cell;
			  int64 key         = ComputeHash(pInput);
			  cell.hash         = key;
			  cell.instanceName = pInput;

			  if(pInput == timing.clock.name)
			  {
				  cell.cellLibIndex_e = -4;
				  cell.cellLibIndex_l = -4;
			  }
			  else
			  {
				  cell.cellLibIndex_e = -2;
				  cell.cellLibIndex_l = -2;
			  }
			  if(key!=0)
			  {
				  Hash_Instance h;                    //making the indexTable of InstanceList
				  cell.isIncluded = true;

				  /*creation of a basic opin*/
				  InstanceOpin op;
				  op.hash            = key;
				  op.name.cellid     = key;
				  op.name.icellName  = pInput;
				  op.name.pinName    = "";
				  cell.iopList.push_back(op);

				  InstanceList.push_back(cell);
				  PIList.push_back(&(InstanceList.back()));
				  h.hash  = key;
				  h.index = InstanceList.size()-1;
				  InstanceHashTable.push_back(h);
			  }
				  if(pInput == timing.clock.name)
				  {
					  CLKSRC = &(InstanceList.back());
					  CLKSRC->isCkTreeMember = true;
				  }

				  int64 ind;
				  ind = Bsearch(wireHashTable,key,0,wireHashTable.size()-1);

				  if(ind !=-1)
				  {
					  SpefNet wire;
					  wire.netName = pInput;
					  wire.hash    = ComputeHash(pInput);
					  Hash_Instance hj;
					  wire.port.portName.n1    = pInput;
					  wire.port.portName.n2    = "";
					  wire.port.portName.pinid = key;
					  WireList.push_back(wire);

					  wireHashTable[ind].index = WireList.size()-1;
					  hj.hash = key;
					  PIPONets.push_back(hj);

				  }


			}
		}
	  } while (valid) ;

}

/**
 * Processes all the primary output declarations and creates Instances for them in InstanceList
 */

void Verilog::ProcessPO()
{

	bool valid = false;
	is.seekg(_cursorPos);
	string line;
	do {
			deque<string> primaryOutput ;

			valid = read_primary_output (primaryOutput) ;

			for (int z = 0; z < (int)primaryOutput.size(); z++)
			{
				  string pOutput = primaryOutput[z];
				  Instance cell;
				  int64 key           = ComputeHash(pOutput);
				  cell.hash           = key;
				  cell.instanceName   = pOutput;
				  cell.cellLibIndex_e = -3;
				  cell.cellLibIndex_l = -3;
				  cell.isIncluded     = true;

				  if(key != 0)
				  {
					  Hash_Instance h;                    //making the indexTable of InstanceList
					  InstanceIpin ip;
					  ip.hash           = key;
					  ip.name.cellid    = key;
					  ip.name.icellName = pOutput;
					  ip.name.pinName   = "";
					  cell.iipList.push_back(ip);

					  InstanceList.push_back(cell);
					  POList.push_back(&(InstanceList.back()));
					  h.hash  = key;
					  h.index = InstanceList.size()-1;

					  InstanceHashTable.push_back(h);
					  int64 ind;
					  ind = Bsearch(wireHashTable,key,0,wireHashTable.size()-1);

					  if(ind != -1)
					  {
							SpefNet wire;
							wire.netName     = pOutput;
							wire.hash        = ComputeHash(pOutput);
							Hash_Instance hj;
							SpefTap tap;
							tap.tapName.n1    = pOutput;
							tap.tapName.n2    = "";
							tap.tapName.pinid = key;
							tap.hash          = ComputeHash(pOutput);

							wire.taps.push_back(tap);

							WireList.push_back(wire);

							hj.hash = key;
							PIPONets.push_back(hj);
							wireHashTable[ind].index = WireList.size()-1;
					  }
				  }
		}

	  } while (valid) ;
}

/**
 * Processes all the wire declarations and creates Instances for them in SpefNetList
 */

void Verilog::ProcessNet()
{
	bool valid = false;
	is.seekg(_cursorPos);
	  /* initing net params */
	do
	{
		string net ;
		valid = read_wire (net) ;
	} while (valid) ;
}


/**
 * \param c Input character for checking
 *
 * Checks whether the given character 'c' belongs to the pre-decided set of special characters.
 * These characters are not included in the tokens extracted from a line in file reading
 */
inline bool Verilog:: is_special_char3 (char c)
{

	static const char specialChars[] = {'(', ')', ',', ':', ';', '#', '{', '}', '*', '\"', '\\','\''} ;

	for (int i = 0; i < (int)sizeof(specialChars); ++i)
	{
		if (c == specialChars[i])
			return true ;
	}

	return false ;
}

/**
 * Identifies and saves location of PI/PO/wire declaration each and detailed connection starting point
 */
void Verilog:: fix_checkpoints()
{
	  deque<string> tokens ;
	  bool valid = read_line_as_tokens3(is, tokens,false) ;

	  while(valid)
	  {
		  if (tokens[0] == "module")
		  {
			  _moduleStart = _cursorPos1;
			  break;
		  }
	  }

	  while (valid && !(tokens[0]=="input" || tokens[0]=="wire" || tokens[0]=="output"))
	  {
		  valid = read_line_as_tokens3(is, tokens,false) ;
	  }

	  if(tokens[0]=="input")
	  {
		  _PIStart   = _cursorPos1;
		  _isPIFixed = true;
	  }
	  else if(tokens[0]=="wire")
	  {
		  _wireStart   = _cursorPos1;
		  _isWireFixed = true;
	  }
	  else if(tokens[0]=="output")
	  {
		  _POStart   = _cursorPos1;
		  _isPOFixed = true;
	  }

	  while(valid)
	  {
		  if(_isPIFixed && _isPOFixed && _isWireFixed)
			  break;

		  valid = read_line_as_tokens3(is, tokens,false) ;

		  if(!_isPIFixed && tokens[0]=="input")
		  {
			  _isPIFixed = true;
			  _PIStart   = _cursorPos1;
		  }
		  else if(!_isPOFixed && tokens[0]=="output")
		  {
			  _isPOFixed = true;
			  _POStart   = _cursorPos1;
		  }
		  else if(!_isWireFixed && tokens[0]=="wire")
		  {
			  _isWireFixed = true;
			  _wireStart   = _cursorPos1;
		  }
	  }

	  _cellStart = _cursorPos1;
	  string line;
}

/**
 * \param is Input file stream
 * \param tokens Set of tokens read and split from a line
 * \param includeSpecialChars Flag for including special characters in tokens extracted from a line during parsing
 * \return Flag for line reading was successful or failed
 *
 * Reads a line from the current position of input file stream
 */
bool Verilog:: read_line_as_tokens3 (istream& is, deque<string>& tokens, bool includeSpecialChars)
{

	tokens.clear() ;
	string line ;
	std::streampos init;

	init = is.tellg();
	std::getline (is, line) ;

	if(line.empty())
		_cursorPos1 = is.tellg();

	else _cursorPos1 = init;

	while (is && tokens.empty())
	{

		string token = "" ;

		for (int i=0; i < (int)line.size(); ++i)
		{
			char currChar = line[i] ;
			if (currChar=='{')
			{
				_bracket_cnt2=_bracket_cnt2+1;
			}
			else if(currChar=='}')
			{
				_bracket_cnt2=_bracket_cnt2-1;
			}
			bool isSpecialChar = is_special_char3(currChar) ;

			if (std::isspace (currChar) || isSpecialChar)
			{
				if (!token.empty())
				{
					// Add the current token to the list of tokens
					tokens.push_back(token) ;
					token.clear() ;
				}

				if (includeSpecialChars && isSpecialChar)
				{
					tokens.push_back(string(1, currChar)) ;
				}

			}
			else
			{
				// Add the char to the current token
				token.push_back(currChar) ;
			}

		}

		if (!token.empty())
			tokens.push_back(token) ;

		if (tokens.empty() || tokens[0][0]=='/')
		{
			tokens.clear();
			_cursorPos = is.tellg();
			std::getline (is, line) ;

		}
	}
	return !tokens.empty() ;
}

/**
 * Creates a temporary Hash-bank of all the nets given in connectivity information of gates/nets
 */
void Verilog::createNetHash()
{
	bool valid;
	is.seekg(_cellStart);;
	do
	{
	    string cellType, cellInst ;

	    deque<std::pair<string, string> > pinNetPairs ;

	    valid = read_cell_inst2 (cellType, cellInst, pinNetPairs) ;

	    if(valid)
		{
			for (int i = 0; i < (int)pinNetPairs.size(); ++i)
			{
				int64 token = ComputeHash(pinNetPairs[i].second);
				Hash_Instance h;
				h.hash      = token;
				HashBank.push_back(h);
			}
		}

	} while(valid);

	sort(HashBank.begin(),HashBank.end(),CompareByHashnum<Hash_Instance>());

	Canonicalize();
}

/**
 * Extracts non-repetitive hashes from HashBank and stores them into wireHashTable, another temporary hash table. HashBank
 * keep the hash entries in sorted order
 */
void Verilog:: Canonicalize()
{
	int64 last = -987654;

	for(int64 i = 0;i < (int64)HashBank.size();i++)
	{
		if((int64)wireHashTable.size() == 0 || last != HashBank[i].hash)
		{
			wireHashTable.push_back(HashBank[i]);
			last = HashBank[i].hash;
		}
	}
	HashBank.clear();
}

/**
 * \param cellType Cell type of the gate
 * \param cellInstName Name of the gate
 * \param pinNetPairs Pin-net connection pairs for the given gate
 *
 * Reads connectivity information of a gate, constructs pin-net connection pairs from the given information
 */
bool Verilog::read_cell_inst2 (string& cellType, string& cellInstName, deque<std::pair<string, string> >& pinNetPairs)
{

	cellType     = "" ;
	cellInstName = "" ;
	pinNetPairs.clear() ;

	deque<string> tokens ;
	bool valid = read_line_as_tokens3 (is, tokens,false) ;

	if (tokens.size() == 1)
	{
		return false ;
	}

	cellType     = tokens[0] ;
	cellInstName = tokens[1] ;

	for (int i = 2; i < (int)tokens.size()-1; i += 2)
	{
		string pinName = tokens[i].substr(1) ;  // skip the first character of tokens[i]
		pinNetPairs.push_back(std::make_pair(pinName, tokens[i+1])) ;
	}

	return valid ;
}

};
