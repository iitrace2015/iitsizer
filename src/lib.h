/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_LIB_H_
#define IIT_LIB_H_

#include "include.h"
#include "classdecs.h"

/**
 * \file lib.h
 *
 * This file contains foundation data structures for storing cells and detailed information from cell
 * library in a more structured way
 */

namespace iit{

/**
 * Element type for hash-tables of gates and nets.
 */
class TempHashTable {
public:
	int64 hash;                    ///< Hash of gate/net name
	int   index;                   ///< Index of element in the list (InstanceList or SpefNetList or so on)

};

/**
 * Store information on list of related pins to a given pin (timing arc exists from one to the other)
 */
class RelatedPinTable {
public:
	int64         hash;            ///< Hash of the current pin
	int           index;           ///< Index of current pin in pin-list
	vector<int64> relPinHash;      ///< Set of pin-indices of pins related to the current pin (timing arc exists)
};

/**
 * Information on clock source of the design
 */
class ClockTimingInfo {
public:
	string   name;                 ///< Main clock pin name ( It will be a PI, of course)
	double64 period;               ///< Clock-period
	double64 low;                  ///< Duty cycle of the clock- T_off percentage
};

/**
 * Element type to store load-information at primary outputs
 */
class POLoadCapTimingInfo {
public:
	string   poname;               ///< Name of the primary output
	int64    hash;                 ///< Hash of primary-output-name
	double64 earlycap;             ///< Load capacitance assertion in early mode
	double64 latecap;              ///< Load capacitance assertion in late mode
};

/**
 * Element type to store timing assertion information from .timing/.sdc at primary inputs & primary outputs.
 * This can be in terms of AT, RAT, input_delay, output_delay and slew
 */
class PIPOTimingInfo {
public:
	string    name;                ///< Name of the PI/PO
	int64     hash;                ///< Hash of the name
	double64  earlyrise;           ///< Early rise data- can be slew/AT/RAT.
	double64  earlyfall;           ///< Early fall data- can be slew/AT/RAT.
	double64  laterise;            ///< Late rise data- can be slew/AT/RAT.
	double64  latefall;            ///< Late fall data- can be slew/AT/RAT.
	int64     drivingInd;          ///< Index of the cell driving current PI/PO in celllibstorage_l (celllib)

	/**
	 * Constructor for the class- set all the RF/EL values and hashes to 0
	 */
	PIPOTimingInfo()
	{
		name = "";
		hash = 0;
		earlyrise = 0;
		earlyfall = 0;
		laterise = 0;
		latefall = 0;
		drivingInd = -1;
	}
};

/**
 * Element type to contain complete timing info for all the primary inputs and outputs, stored as vectors of timing element type class
 */
class CompleteTimingInfo {
public:

	ClockTimingInfo                clock;      ///< Clock-source information
	vector < PIPOTimingInfo      > slew;       ///< Slew information from .sdc/.timing at all PIs
	vector < PIPOTimingInfo      > at;         ///< AT information from .sdc/.timing at all PIs
	vector < PIPOTimingInfo      > rat;        ///< RAT information from .sdc/.timing at all POs
	vector < POLoadCapTimingInfo > loadcap;    ///< Load capacitance information from .sdc/.timing at all POs
	vector < PIPOTimingInfo      > delay;      ///< Output delay information from .sdc/.timing at all POs
};

/**
 * Element type for storing a 2-dimensional Look-up table of delay/transition from cell library
 */
class LibParserLUT {
public:
	string                      tableName; ///< Table name
	vector < double64         > index_1;   ///< first index for interpolation- can be input_net_transition or net_output_capacitance
	vector < double64         > index_2;   ///< second index for interpolation- can be net_output_capacitance or input_net_transition
	vector < vector<double64> > tableVals; ///< 2D array storing values for corresponding pair of indices from index 1 and index 2

};

/**
 * Element type to store all the timing information on a given timing arc
 */
class LibParserTimingInfo {
public:
	string       fromPin ;                 ///< Timing arc source pin
	int64        hashFromPin;              ///< Hash of the source pin-name
	int64        hashToPin;                ///< Hash of the sink pin-name
	string       toPin ;                   ///< Timing arc sink pin
	string       timingSense ;             ///< Timing sense of the arc- positive/negative/non unate
    string       timingType;               ///< Timing type specified for sequential cells- combinational or rising/falling edge
	LibParserLUT fallDelay ;               ///< Cell fall-delay LUT
	LibParserLUT riseDelay ;               ///< Cell rise-delay LUT
	LibParserLUT fallTransition ;          ///< Cell fall-transition LUT
	LibParserLUT riseTransition ;          ///< Cell rise-transition LUT
	LibParserLUT riseConstraint;           ///< Cell setup/hold rise constraint LUT
	LibParserLUT fallConstraint;           ///< Cell setup/hold fall constraint LUT

};

/**
 * Element type for a pin instance of a cell in cell library
 */
class LibParserPinInfo {
public:
  string    name ;                         ///< Pin name
  double64  capacitance ;                  ///< Input pin cap
  double64  maxCapacitance ;               ///< The max load this pin can drive
  double64  minCapacitance;                ///< Minimum capacitance
  bool      isInput ;                      ///< Whether the pin is input or output pin
  bool      isClock ;                      ///< Whether the pin is a clock pin or not
  bool      isUsed;                        ///< Whether the pin is being used or not
  int64     hash;                          ///< Hash of the pin-name
  int       index;                         ///< Index of pin in pin-list

  /**
   * Constructor for the class- reset all the flags and capacitance values for the pin
   */
  LibParserPinInfo ()
  {
	  capacitance    = 0;
	  maxCapacitance = 0;
	  minCapacitance = 0;
      isInput        = true;
      isClock        = false;
      isUsed         = false;
      hash           = 0;
      index          = -1;
  }


} ;


/**
 * Element type for storing detailed cell information
 */
class LibParserCellInfo {
public:
  string   name ;             ///< Cell name
  int64    hash;              ///< Hash number for cell name
  bool     isSequential ;     ///< If true then sequential cell, else combinational
  int      numipins;          ///< Number of input pins of the cell- Doesn't include clk pin
  int      numopins;          ///< Number of output pins of the cell
  double64 leakage;           ///< Cell leakage
  double64 area;              ///< Cell area
  string   footprint;         ///< Footprint name
  int64    fphash;            ///< Hash of footprint name
  int64    idxVth [2];        ///< [0]-> index of vth version, [1]-> index of area version (column no.) for a 'x' value in fp
  int64    idxLk;             ///< Index in fp->version_lk - based on absolute leakage, independent of Vth.

  LibParserFPInfo*               fp;                    ///< Pointer to its footprint versions in footprintlib
  vector < LibParserPinInfo    > ipins;                 ///< Set of input pins with pin info. class (doesn't include clock pin)
  vector < LibParserPinInfo    > opins;                 ///< Set of output pins with pin info. class
  vector < LibParserPinInfo    > clkpin;                ///< Clock pin info. using pin info. class - empty for a combinational cell
  vector < LibParserTimingInfo > timingArcs ;           ///< Set of all timing arcs of the cell
  vector < RelatedPinTable     > opinhashtable;         ///< Hash table for output pin-list
  vector < RelatedPinTable     > ipinhashtable;         ///< Hash table for input pin-list (doesn't include for clock pin)
  vector < RelatedPinTable     > clkToIpinhashtable;    ///< Related input pins of clock pin of the cell
  vector < RelatedPinTable     > clkToOpinhashtable;    ///< Related output pins of clock pin of the cell

  /**
   * Constructor of the class- initialize all the variables to suitable values 0/-1, and reset the flags
   */
  LibParserCellInfo ()
  {
	  hash         = 0;
	  isSequential = false;
	  numipins     = 0;
	  numopins     = 0;
	  idxLk        = -1;
	  idxVth [0]   = -1;
	  idxVth [1]   = -1;
	  leakage      = 0.0;
	  area         = 1;
	  fp           = NULL;
	  fphash       = 0;
  }


} ;



/**
 * Element type to store gate version information
 */
class GateVersionInfo {
public:
	LibParserCellInfo* vPtr;         ///< Pointer to version of gate in celllib from celllibstorage
	int64              celllibIdx;   ///< Cell index in celllib
};

/**
 * Element type containing information on all the versions for a given cell-footprint
 */
class LibParserFPInfo {
public:
	string footprint;                                ///< Footprint name
	int64  fphash;                                   ///< Hash of fp-name
	deque < deque<GateVersionInfo>  > version_vth;   ///< Variants stored in decreasing Vth order
	deque < GateVersionInfo         > version_lk;    ///< Variants stored in increasing leakage order
};

/**
 * Element type for entire footprint library storage
 */
class FPLibStorage {
public:
	deque < LibParserFPInfo   > fplib;               ///< Complete footprint library
	deque < TempHashTable     > fphashtable;         ///< Has table for footprint lib.

};


/**
 * Element type for entire cell library storage
 */
class CelllibStorage {
public:
	vector < LibParserCellInfo > celllib;            ///< Complete cell library
	vector < TempHashTable     > cellhashtable;      ///< Hash table for footprint lib.

};

/**
 * Element type to store template of a LUT
 */
class LibLutTemplate {
public:
	string  name;                                    ///< Name of the LUT template
	string  variable1;                               ///< The attribute index 1 stands for (transition or load cap)
	string  variable2;                               ///< The attribute index 2 stands for (transition or load cap)

};


/**
 * Element type store liberty file information
 */
class LibFileInfo {
public:
	string   res_unit;                               ///< Resistance unit
	string   time_unit;                              ///< Time unit
	string   vol_unit;                               ///< Voltage unit
	vector < string         > cap_unit;              ///< Capacitance unit, e.g. <1,ff> , then unit will be 1ff. (multiples of 1ff)
	vector < LibLutTemplate > lut_templates;         ///< Set of all LUT templates from cell library
};


/**
 * Liberty file parsing and storing the circuit completely
 */
class LibParser {
public:

	int   lib_parsing_mode;        ///< Mode of the liberty file- Early/Late

	bool  read_library_info        (LibFileInfo* libinfo);
	bool  read_LUT_template        (LibFileInfo* libinfo);
	void _begin_read_LUT_template  (string LutName,  LibFileInfo* libinfo);
	void  read_cell_info           (CelllibStorage* tempcelllib, LibFileInfo* libinfo);
	void  init_lib_parser          (string filename, CelllibStorage* tempcelllib, LibFileInfo* libinfo) ;

	/**
	 * Constructor for the class- set 'filename' as the file for reading
	 */
	LibParser (string filename): is(filename.c_str())
	{
		_bracket_cnt      = 0;
		_libInfo          = NULL;
		_capScalingFac    = 1;
		_timeScalingFac   = 1;
		_slewScalingFac   = 1;
		lib_parsing_mode  = LATE;
	}

private:

	ifstream      is ;              ///< Stream for input file stream for parsing purposes
	int           _bracket_cnt;     ///< Curly-bracket count
	LibFileInfo*  _libInfo;         ///< Pointer to liberty file into type storage
	double64      _capScalingFac;   ///< Capacitance scaling factor, base unit is 'fF'
	double64      _timeScalingFac;  ///< Time scaling factor, base unit is 'ps'
	double64      _slewScalingFac;  ///< Slew scaling factor, base unit is 'ps'

	void _close                     (void);
	void _begin_read_library_info   (LibFileInfo* libinfo);
	void _begin_read_cell_info      (string cellName, LibParserCellInfo& cell) ;
	bool _begin_read_timing_info    (string toPin, int64 toPinHash, LibParserTimingInfo& timing) ;
	void _begin_read_lut            (LibParserLUT& lut, string dimension, deque<string> & tokens) ;
	void _begin_read_pin_info       (string pinName, LibParserCellInfo& cell, LibParserPinInfo& pin) ;
	bool  read_line_as_tokens       (istream& is, deque<string>& tokens, bool includeSpecialChars = false );


} ;

};

#endif
