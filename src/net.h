/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_NET_H_
#define IIT_NET_H_

#include "include.h"
#include "instance.h"
#include "classdecs.h"

namespace iit{

/**
 * Stores resistance and node index of a node in RC parasitics tree
 */
class RES_NODE
{

public:
	double64 res;			       ///< Resistance value with neighbour
	int64    idx;		           ///< Index of neighbour in otherNode
};

/**
 * Stores index information of a child node in RC parasitics tree
 */
class CHILD_NODE
{

public:
	int64 adIdx;			      ///< Index of child in adjList
	int64 idx;				      ///< Index of child in otherNode
};

/**
 * Stores name of a node in RC parasitics tree
 */
class SpefNodeName
{

public:
	string n1 ;                   ///< Equivalent cell name i.e. for u1:b2, n1 is u1
	string n2 ;                   ///< Equivalent pin name  i.e. for u1:b2, n2 is b2
	int64  pinid;                 ///< Hash of complete name i.e. for u1:b2, pinid is hash(u1:b2)

	void clear()
	{
		n1    = "";
		pinid = 0;
		n2    = "";
	}
} ;

/**
 * Stores intermediate information for computing Pi model of RC parasitic tree
 */
class Pi
{

public:
    double64     y1;               ///< First coefficient of Taylor series expansion of driving-point admittance
    double64     y2;               ///< Second coefficient of Taylor series expansion of driving-point admittance
    double64     y3;               ///< Third coefficient of Taylor series expansion of driving-point admittance

    double64     segC;             ///< Stores total segment capacitance from node to a split point or tap end
    double64     segR;             ///< Stores total segment resistance from node to a split point or tap end

    void clear()
	{
		y1   = 0;
		y2   = 0;
		y3   = 0;

		segC = 0;
		segR = 0;
	}
};

/**
 * Stores capacitance related information for a node in RC parasitics tree
 */
class Cnode
{

public:
    double64     capacitanceEarly;          ///< Early capacitance (from .spef + from .lib (in case of port/tap))
    double64     capacitanceLate;           ///< Late capacitance (from .spef + from .lib (in case of port/tap))
    SpefNodeName nodename;                  ///< Name of the node
    int64        hash;                      ///< Same as nedename.pinid

	void clear()
	{
		capacitanceEarly = 0;
		capacitanceLate  = 0;
		hash             = 0;
		nodename.clear();
	}
};

/**
 * Stores resistance related information for a pair of nodes in RC parasitics tree
 */
class Redge
{

public:
	double64     resistance;          ///< Resistance between nodes
	SpefNodeName node1;               ///< Name of first node
	SpefNodeName node2;               ///< Name of second node

	void clear()
	{
		resistance = 0.0;
		node1.clear();
		node2.clear();
	}
};

/**
 * Structure of port of a net
 */
class SpefPort
{

public:
	char          portType ;            ///< Either 'P' (port) or 'I' (internal)
	SpefNodeName  portName ;		    ///< Name of port (Note - portName.pinid is hash
	                                    ///< of cellInstanceName only e.g. u1
	int64         hash;			        ///< Hash of cellInstanceName:PinName e.g. hash(u1:b)
	double64      capacitanceEarly;     ///< Early capacitance (from .spef + from .lib)
	double64      capacitanceLate;      ///< Late capacitance (from .spef + from .lib)
	double64      ceffEarly;            ///< Effective load capacitance (lumped or ECM, same as that at output pin.ceff)
	double64      ceffLate;             ///< Effective load capacitance (lumped or ECM, same as that at output pin.ceff)
	Instance*     cellPtr;              ///< Pointer to cell instance of the connected cell
	InstanceOpin* pinPtr;               ///< Pointer to output pin of the connected pin
	bool          parity;               ///< Needed for finding correct transition when finding credit during CPPR
	int64         stIndex;              ///< Index of this port in Sparse Table (only when its a clock tree member)

	SpefPort()
	{
		  cellPtr          = NULL;
		  pinPtr           = NULL;
		  ceffEarly        = 0;
		  ceffLate         = 0;
		  capacitanceEarly = 0;
		  capacitanceLate  = 0;
		  parity           = false;
		  stIndex          = 0;

	}

	void clear()
	{

		capacitanceEarly = 0.0;
		capacitanceLate  = 0;
		ceffEarly        = 0;
		ceffLate         = 0;
		cellPtr          = NULL;
		pinPtr           = NULL;
		parity           = false;
		portName.clear();
	}

} ;

/**
 * Structure of a tap of a net
 */
class SpefTap {

public:
	char          tapType ;             ///< Either 'P' (port) or 'I' (internal)
	SpefNodeName  tapName ;		        ///< Name of port (Note - tapName.pinid is hash
                                        ///< of cellInstanceName only e.g. u1
	int64         hash;			        ///< Hash of cellInstanceName:PinName e.g. hash(u1:b)
	double64      capacitanceEarly;     ///< Early capacitance (from .spef + from .lib)
	double64      capacitanceLate;      ///< Late capacitance (from .spef + from .lib)

	double64      delayEarly;           ///< Early delay from port to this tap (also stores 1st moment during FastTapDelayBeta)
	double64      delayLate;            ///< Late delay from port to this tap (also stores 1st moment during FastTapDelayBeta)
	double64      betaEarly;            ///< Early beta (2nd moment) from port to this tap
	double64      betaLate;             ///< Late beta (2nd moment) from port to this tap

	double64      m1_E;                 ///< Early 1st moment of RC tree
	double64      m1_L;                 ///< Late 1st moment of RC tree
	double64      m2_E;                 ///< Early 2nd moment of RC tree
	double64      m2_L;                 ///< Late 2nd moment of RC tree

	double64      lumpedRes;            ///< Total resistance from port to tap (not used in code)
	Instance*     cellPtr;              ///< Pointer to cell to which tap is connected
	InstanceIpin* pinPtr;               ///< Pointer to input pin to which tap is connected

    SpefTap()
	{

		hash             = 0;
		cellPtr          = NULL;
		pinPtr           = NULL;
		capacitanceEarly = 0;
		capacitanceLate  = 0;
		delayEarly       = 0;
		delayLate        = 0;
		betaEarly        = 0;
		betaLate         = 0;
		lumpedRes        = 0;
		m1_E             = 0;
		m2_E             = 0;
		m1_L             = 0;
		m2_L             = 0;
		tapName.clear();
	}

    void clear()
    {

		hash             = 0;
		cellPtr          = NULL;
		pinPtr           = NULL;
		capacitanceEarly = 0;
		capacitanceLate  = 0;
		delayEarly       = 0;
		delayLate        = 0;
		betaEarly        = 0;
		betaLate         = 0;
		lumpedRes        = 0;
		tapName.clear();
  }
} ;

/**
 * Structure of a net
 */
class SpefNet
{

public:
	string     netName ;                                 ///< Name of the net
	int64      hash;                                     ///< Hash of net name
	double64   netLumpedCap ;                            ///< Total lumped parasitic capacitance of a net
	SpefPort   port ;                                    ///< Port of the net
	bool       isIncluded;                               ///< Check if net is included in design for incremental purposes
	int        level;                                    ///< Net level according to topological order
	bool       isRemoved;		                         ///< Temporary storage for TopSort of nets
	bool       isIncremental;                            ///< Flag if net is included in incremental nets
	bool       isReachable;                              ///< Flag if net is reachable from an incrementally modified net for incremental purposes
	bool       isCktreeMember;                           ///< Flag if net is clock tree member
	bool       isIncforRAT;                              ///< Flag to avoid multiple pushes of same net in priority queue while calculating RAT,
														 // is incremental and pushed in priority queue already, while calculating RAT
	bool       isVirtualNet;                             ///< Disconnected pins have virtual nets connected to them
	bool       isDiscovered;                             ///< Flag used in TopSort for topological sorting of nets
	bool       isRepowered;                              ///< Flag to check whether gates connected to this net are re-powered or not (in incremental ops)

	deque  < SpefTap >             taps ;                ///< Set of taps of the net
	vector < Cnode   >             otherNode;            ///< Contains capacitance information of all nodes (internal, port and taps) of RC tree
	vector < Redge   >             rList;                ///< Contains resistance information of all nodes (internal, port and taps) of RC tree
	vector < vector < RES_NODE > > adjList;              ///< Adjacency list of connected nodes via resistance in RC tree for all nodes (internal, port and taps)
	vector < int64 >               parentNode;           ///< Contains index of parent node of all nodes (internal, port and taps) of RC tree
	vector < vector <CHILD_NODE> > childNode;            ///< Contains children information of all nodes (internal, port and taps) of RC tree
	vector < double64 >            backCapSumEarly;      ///< Contains early downstream capacitance of all nodes (internal, port and taps) of RC tree
	vector < double64 >            backCapSumLate;       ///< Contains late downstream capacitance of all nodes (internal, port and taps) of RC tree
	vector < double64 >            backDCapSumEarly;     ///< Contains early mode downstream (1st moment times capacitance) of all nodes (internal, port and taps) of RC tree
	vector < double64 >            backDCapSumLate;      ///< Contains late mode downstream (1st moment times capacitance) of all nodes (internal, port and taps) of RC tree

    // Added for New Effective Capacitance
	double64 	c1;                                      ///< Pi model capacitance 1 of the net
	double64 	c2;                                      ///< Pi model capacitance 2 of the net
	double64 	r;                                       ///< Pi model resistance of the net
    vector < Pi >	pi;                                  ///< Intermediate information needed at all nodes in RC tree to find Pi model of net

	SpefNet()
	{
		netName    .clear();
		port       .clear();
		taps       .clear();
		otherNode  .clear();
		rList      .clear();

		netLumpedCap    = 0.0 ;
		level           = 1;
		isIncluded      = true;
		isIncremental   = false;
		isReachable     = false;
		isRemoved       = false;
		isCktreeMember  = false;
		isVirtualNet    = true;
		isRepowered     = false;
		isDiscovered    = false;
		isIncforRAT     = false;

		c1 = 0;
		c2 = 0;
		r  = 0;
		pi.clear();
	}

    void clear()
    {
		netName    .clear();
		port       .clear();
		taps       .clear();
		otherNode  .clear();
		rList      .clear();
		adjList    .clear();
		childNode  .clear();
		parentNode .clear();
		backCapSumEarly.clear();
		backCapSumLate.clear();
		backDCapSumEarly.clear();
		backDCapSumLate.clear();

		netLumpedCap  = 0.0 ;
		level         = 1;
		isIncluded    = true;
		isIncremental = false;
	    isReachable   = false;
		isRemoved     = false;

		c1 = 0;
		c2 = 0;
		r  = 0;
		pi.clear();
  }

} ;

};

#endif
