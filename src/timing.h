/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_TIMING_H_
#define IIT_TIMING_H_

#include "include.h"
#include "classdecs.h"
#include "lib.h"

namespace iit
{

/**
 * Class for supporting parsing of .timing, define clock, setting PI input AT/slew and setting PO RAT
 * Note - .timing file is not used in iitSizer. Instead, .sdc file is used in it for setting timing constraints
 */
class TimingParser
{

public:

	bool read_timing_line    (CompleteTimingInfo& tinfo) ;
	bool read_line_as_tokens (istream& is, deque < string >& tokens, bool includeSpecialChars = false );

	TimingParser             (string filename): is(filename.c_str()) {};

private:
	ifstream                 is;                   ///< Input .timing file stream

	void _close              (void);

} ;

};

#endif
