/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_TIMING_INFO_H_
#define IIT_TIMING_INFO_H_
#include "include.h"
#include "classdecs.h"

namespace iit
{

/**
 * Class to store index from where dominating timing information has propagated
 * Example - For output pin AT Criticality: index of input pin in iipList of cell from where dominating arrival time has come to output pin
 * Required to know critical path during reporting worst path and for pruning during block based CPPR
 */
class CRITICALITY
{

public:
	int  indexRiseEarly;   ///< Index of related pin that decided Rise Early dominating timing
	int  indexFallEarly;   ///< Index of related pin that decided Fall Early dominating timing
	int  indexRiseLate;	   ///< Index of related pin that decided Rise Late dominating timing
	int  indexFallLate;    ///< Index of related pin that decided Fall Late dominating timing

	// transition contains Rise(0) or Fall(1) transition of related critical pin
	bool transitionRE;     ///< Corresponding transition of related pin that decided Rise Early dominating timing
	bool transitionFE;     ///< Corresponding transition of related pin that decided Fall Early dominating timing
	bool transitionRL;     ///< Corresponding transition of related pin that decided Rise Late dominating timing
	bool transitionFL;     ///< Corresponding transition of related pin that decided Fall Late dominating timing

	CRITICALITY()
	{
		indexRiseLate  = 0;
		indexFallLate  = 0;
		indexRiseEarly = 0;
		indexFallEarly = 0;
		transitionFE   = FALL;
		transitionRE   = RISE;
		transitionFL   = FALL;
		transitionRL   = RISE;

	}
};

/**
* Stores timing information at a pin
*/
class NODE_TIMING_DATA
{
public:
	bool isRatSet;        ///< Sets if RAT of pin is computed (only used in Static part of the timing)
	int  holdSense;       ///< Sets rising/falling sense for sequential cells for Hold constraints (only for sequential cell inputs)
	int  setupSense;      ///< Sets rising/falling sense for sequential cells for Setup constraints (only for sequential cell inputs)

	double64 atRiseLate;      ///< Rise Late Arrival Time at pin
	double64 atFallLate;      ///< Fall Late Arrival Time at pin
	double64 atRiseEarly;     ///< Rise Early Arrival Time at pin
	double64 atFallEarly;     ///< Fall Early Arrival Time at pin

	double64 ratRiseLate;     ///< Rise Late Required Arrival Time at pin
	double64 ratFallLate;     ///< Fall Late Required Arrival Time at pin
	double64 ratRiseEarly;    ///< Rise Early Required Arrival Time at pin
	double64 ratFallEarly;    ///< Fall Early Required Arrival Time at pin

    double64 slackRiseLate;   ///< Rise Late Slack at pin
	double64 slackFallLate;   ///< Fall Late Slack at pin
	double64 slackRiseEarly;  ///< Rise Early Slack at pin
	double64 slackFallEarly;  ///< Fall Early Slack at pin

	double64 slewRiseLate;    ///< Rise Late Slew (also referred as Transition) at pin
	double64 slewFallLate;    ///< Fall Late Slew (also referred as Transition) at pin
	double64 slewRiseEarly;   ///< Rise Early Slew (also referred as Transition) at pin
	double64 slewFallEarly;   ///< Fall Early Slew (also referred as Transition) at pin

	//Added for pruning in block based CPPR
	CRITICALITY criticalAT;      ///< For output pin AT Criticality: index of input pin in iipList
	                             ///< of cell from where dominating arrival time has come to output pin

	NODE_TIMING_DATA()
	{
		isRatSet       = false;

		atRiseEarly    = UNDEFINED_AT_EARLY;
		atFallEarly    = UNDEFINED_AT_EARLY;
		atRiseLate     = UNDEFINED_AT_LATE;
		atFallLate     = UNDEFINED_AT_LATE;

		ratRiseEarly   = UNDEFINED_RAT_EARLY;
		ratFallEarly   = UNDEFINED_RAT_EARLY;
		ratRiseLate    = UNDEFINED_RAT_LATE;
		ratFallLate    = UNDEFINED_RAT_LATE;

		slackRiseLate  = FORBIDDEN_SLACK;
		slackFallLate  = FORBIDDEN_SLACK;
		slackRiseEarly = FORBIDDEN_SLACK;
		slackFallEarly = FORBIDDEN_SLACK;

		holdSense      = FORBIDDEN_EARLY;
		setupSense     = FORBIDDEN_LATE;

		slewFallLate   = FORBIDDEN_LATE;
		slewRiseLate   = FORBIDDEN_LATE;
		slewFallEarly  = FORBIDDEN_EARLY;
		slewRiseEarly  = FORBIDDEN_EARLY;
	}

	void clear()
	{
		isRatSet       = false;

		atRiseEarly    = UNDEFINED_AT_EARLY;
		atFallEarly    = UNDEFINED_AT_EARLY;
		atRiseLate     = UNDEFINED_AT_LATE;
		atFallLate     = UNDEFINED_AT_LATE;

		ratRiseEarly   = UNDEFINED_RAT_EARLY;
		ratFallEarly   = UNDEFINED_RAT_EARLY;
		ratRiseLate    = UNDEFINED_RAT_LATE;
		ratFallLate    = UNDEFINED_RAT_LATE;

		slackRiseLate  = FORBIDDEN_SLACK;
		slackFallLate  = FORBIDDEN_SLACK;
		slackRiseEarly = FORBIDDEN_SLACK;
		slackFallEarly = FORBIDDEN_SLACK;
	}

	void clearPO()
	{
		isRatSet       = true;

		atRiseEarly    = UNDEFINED_AT_EARLY;
		atFallEarly    = UNDEFINED_AT_EARLY;
		atRiseLate     = UNDEFINED_AT_LATE;
		atFallLate     = UNDEFINED_AT_LATE;

		slackRiseLate  = FORBIDDEN_SLACK;
		slackFallLate  = FORBIDDEN_SLACK;
		slackRiseEarly = FORBIDDEN_SLACK;
		slackFallEarly = FORBIDDEN_SLACK;
	}

	void clearPI()
	{
		isRatSet       = false;

		ratRiseEarly   = UNDEFINED_RAT_EARLY;
		ratFallEarly   = UNDEFINED_RAT_EARLY;
		ratRiseLate    = UNDEFINED_RAT_LATE;
		ratFallLate    = UNDEFINED_RAT_LATE;

		slackRiseLate  = FORBIDDEN_SLACK;
		slackFallLate  = FORBIDDEN_SLACK;
		slackRiseEarly = FORBIDDEN_SLACK;
		slackFallEarly = FORBIDDEN_SLACK;
	}
};


/**
 * Stores timing arc slew information
 */
class SLEW_DATA
{

public:
	int      index;                              ///< For output pin, index of associated input pin
	int      timeSense;                          ///< Unateness of timing arc

	double64 slewRiseEarly;                      ///< Rise Early Slack at pin
	double64 slewFallEarly;                      ///< Rise Early Slack at pin
	double64 slewRiseLate;                       ///< Rise Early Slack at pin
	double64 slewFallLate;                       ///< Rise Early Slack at pin

	double64 slewRiseLateNeg;                    ///< Neg slews only for non_unate case where output slew can come from both the transitions
	double64 slewFallLateNeg;                    ///< Neg slews only for non_unate case where output slew can come from both the transitions
	double64 slewRiseEarlyNeg;                   ///< Neg slews only for non_unate case where output slew can come from both the transitions
	double64 slewFallEarlyNeg;                   ///< Neg slews only for non_unate case where output slew can come from both the transitions

	SLEW_DATA()
	{
		slewRiseLate     = FORBIDDEN_LATE;
		slewFallLate     = FORBIDDEN_LATE;
		slewRiseEarly    = FORBIDDEN_EARLY;
		slewFallEarly    = FORBIDDEN_EARLY;
		slewRiseLateNeg  = FORBIDDEN_LATE;
		slewFallLateNeg  = FORBIDDEN_LATE;
		slewRiseEarlyNeg = FORBIDDEN_EARLY;
		slewFallEarlyNeg = FORBIDDEN_EARLY;
		index            = 0;
		timeSense        = POSITIVE_UNATE;
	}

};

/**
 * Stores timing arc delay information
 */
class DELAY_DATA
{

public:
	int      index;						         ///< Index of other end of timing arc
    int      timeSense;                          ///< Unateness of timing arc

	double64 lambdaFL;					         ///< Lagrangian Multiplier for timing arc (for iitSizer)
	double64 lambdaRL;                           ///< Lagrangian Multiplier for timing arc (for iitSizer)

	double64 delayRiseEarly;                     ///< Rise Early Delay of timing arc
	double64 delayFallEarly;                     ///< Fall Early Delay of timing arc
    double64 delayRiseLate;                      ///< Rise Late Delay of timing arc
	double64 delayFallLate;                      ///< Fall Late Delay of timing arc

	// These 4 fields will be filled only for non_unate combinational type of arcs, where
	// delayRiseLate comes from i/p slewRiseLate and op cap while
	// delayRiseLateNeg comes from i/p slewFallLate and op cap.
	double64 delayRiseLateNeg;                    ///< Neg delays only for non_unate case where output delay can come from both the transitions
	double64 delayFallLateNeg;                    ///< Neg delays only for non_unate case where output delay can come from both the transitions
	double64 delayFallEarlyNeg;                   ///< Neg delays only for non_unate case where output delay can come from both the transitions
	double64 delayRiseEarlyNeg;                   ///< Neg delays only for non_unate case where output delay can come from both the transitions

	DELAY_DATA()
	{
		index             = 0;
		timeSense         = POSITIVE_UNATE;
		delayRiseLate     = FORBIDDEN_LATE;
		delayFallLate     = FORBIDDEN_LATE;
		delayRiseEarly    = FORBIDDEN_EARLY;
		delayFallEarly    = FORBIDDEN_EARLY;

		delayRiseLateNeg  = FORBIDDEN_LATE;
		delayFallLateNeg  = FORBIDDEN_LATE;
		delayFallEarlyNeg = FORBIDDEN_EARLY;
		delayRiseEarlyNeg = FORBIDDEN_EARLY;

		lambdaFL = DEFAULT_LAMBDA;
		lambdaRL = DEFAULT_LAMBDA;

	}

};

};

#endif
