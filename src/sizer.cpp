/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/


/**
 * \namespace iit
 *
 *        Copyright (c) 2016, Indian Institute of Technology Madras, India.
 *        All Rights Reserved.
 *        This program is free software. You can redistribute and/or modify
 *        it in accordance with the terms of the accompanying license agreement.
 *        See LICENSE in the top-level directory for details.
 *
 * Defined namespace 'iit'
 */

#include "include.h"
#include "forward_propagate.h"
#include "backward_propagate.h"
#include "cppr.h"
#include "report.h"
#include "operations.h"
#include "utility.h"
#include "globals.h"
#include "sanity.h"
#include "sizer.h"


namespace iit{

/* Global variables declaration --------------------------------------- */

//extern CelllibStorage      celllibstorage_e;
extern CelllibStorage      celllibstorage_l;
//extern LibFileInfo         libinfo_e;
//extern LibFileInfo         libinfo_l;
extern CompleteTimingInfo  timing;

//extern Instance* CLKSRC;
//extern int       numInc;
extern int       Verbose;
//extern int64     MaxNumPaths;
extern int64     PathFlag_G;
//extern string    BenchmarkDir;
extern bool     _isStaticDone;
//extern int64     SkipCnt;
//extern int64     NoSkipCnt;

extern deque   < Instance >       InstanceList;
extern vector  < Hash_Instance >  InstanceHashTable;
//extern deque   < SpefNet >        SpefNetList;
//extern vector  < Hash_Instance >  spefNetHashTable;
extern vector  < SpefNet* >       Sort;
//extern vector  < SpefNet* >       IncNetList;
//extern deque   < SpefNet >        WireList;
//extern queue   < SpefNet* >       InitNetList;
//extern vector  < Instance* >      PIList;
//extern vector  < Instance* >      POList;
extern vector  < InstanceIpin* >  ConeEndPoints;
//extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
//extern deque   < Slack_Instance > pathLUTSlackTable_Adv;

extern std::tr1::unordered_map <char,bool> specCharLibDir;

// For sizer
//extern FPLibStorage      FPLibstorage_e;
extern FPLibStorage      FPLibstorage_l;
extern SolutionState     TrackSoln;


/* End - global variables declaration -----------------------------------*/

/**
 * Default constructor for the class Sizer
 */
Sizer::Sizer(): Timer()
{
	_numIters              = 0;
	_currSoln.lkg          = 0;
	_currSoln.tns          = 0;
	_currSoln.worstSlack   = FORBIDDEN_SLACK;
	_bestSoln              = _currSoln;
	_sp                    = new SpefParser();
	_wtLD                  = 1.0;
	_wtLkg                 = 1.0;
	_cellModsUp            = 0;
	_cellModsDown          = 0;
	_totalViolations       = 0;
	_targetLRSSlackDir[0]  = 0;
	_targetLRSSlackDir[1]  = 0;
	_targetLRSSlackDir[2]  = 0;
	_targetLRSSlackDir[3]  = 1;
	_targetLRSSlackDir[4]  = 2;
	_targetLRSSlackDir[5]  = 3;
	_targetLRSSlackDir[6]  = 3;
	_targetLRSSlackDir[7]  = 3;
	_targetLRSSlackDir[8]  = 4;
	_targetLRSSlackDir[9]  = 4;
	_targetLRSSlackDir[10] = 5;
	_targetLRSSlackDir[11] = 6;
	_targetLRSSlackDir[12] = 7;
	_targetLRSSlackDir[13] = 8;
	_targetLRSSlackDir[14] = 9;
	_targetLRSSlackDir[15] = 10;
	_targetLRSSlackDir[16] = 11; 	// Initializing target LRS slack directory.
	                                // This essentially tells what should be order of ending TNS
	                                // at the last iteration of LRS given initial order of TNS

}

/**
 * Default destructor of the class Sizer
 */
Sizer::~Sizer()
{
	_currSoln.lkg        = 0;
	_currSoln.tns        = 0;
	_currSoln.worstSlack = FORBIDDEN_SLACK;
	_bestSoln            = _currSoln;

	_dbgFile.close();

	delete _sp;
}

/**
 * Sizing Engine- (i) Builds the circuit graph from design inputs (ii) Runs sizing algorithms once the circuit is ready
 */
void Sizer:: Engine(int args, char** argv)
{

	/* License Declaration */
	cout <<
	"/*************************************************************************************" << endl <<
	 "* ********************************************************************************* *" << endl <<
	 "* *                                                                               * *" << endl <<
	 "* *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *" << endl <<
	 "* *                                                                               * *" << endl <<
	 "* *  All Rights Reserved.                                                         * *" << endl <<
	 "* *                                                                               * *" << endl <<
	 "* *  This program is free software. You can redistribute and/or modify            * *" << endl <<
	 "* *  it in accordance with the terms of the accompanying license agreement.       * *" << endl <<
	 "* *  See LICENSE in the top-level directory for details.                          * *" << endl <<
	 "* *                                                                               * *" << endl <<
	 "* ********************************************************************************* *" << endl <<
	 "*************************************************************************************/" << endl << endl << endl;
	if (args < 6)
	{
		cout << "Usage example: ./iitRACE <.lib> <.v> <.spef> <.sdc> <output> [verbose]" <<
				"\nMake sure that you have <.lib> stored as <.lib.early> and <.lib.late> in the same directory "<< endl ;
		    exit(0) ;
	}

	if ((args > 6) && (strcmp(argv[6], "verbose")==0))
		Verbose = 1;

	debug_time("*Let the iitSizer begin...*");

	PathFlag_G = 1;

	static const char specialChars[] = {'(', ')', ',', ':', ';', '/', '#', '[', ']', '*', '\"', '\\'};

	for (int i=0; i < (int)sizeof(specialChars); ++i)
		specCharLibDir[specialChars[i]] = true;

	string libFile      = argv[1];
	string verilogFile  = argv[2];
	string spefFile     = argv[3];
	string sdcFile      = argv[4];
	string outFile      = argv[5];
	string sizeFile;

	if (args >= 7)
		sizeFile = argv[6];

	string dummy,dummy2;

	TimeIt totalTime;

	timeit_start(&totalTime);

	ofstream myfile;
	string benchmark;
	deque <string> splits;
	splits= split(verilogFile,'/');

	if(splits.size() >= 2)
		benchmark = splits[splits.size()-2];
	else
		benchmark= "Run";

	_libFile       = libFile;
	_verilogFile   = verilogFile;
	_spefFile      = spefFile;
	_sdcFile       = sdcFile;
	_outFile       = outFile;
	_sizeFile      = sizeFile;
	_tau2015File   = "";
	_timingFile    = "";
	_opsFile       = "";
	string logFile = _outFile;

	_dbgFile.open(logFile.append(".log").c_str());

	debug_t_start (BuildCircuit_enum);

	cout << "Building circuit" << endl;
	BuildCircuit  ();
	debug_t_stop  (BuildCircuit_enum);

	cout << "Running circuit" << endl;
	debug_t_start(RunSizer_enum);
	RunSizer();
	debug_t_stop(RunSizer_enum);

	cout << "Running circuit - DONE" << endl;

	debug_time(benchmark + " Session completed.");

	return ;
}

/**
 * Runs the sizing algorithms - (i) Builds footprint library (ii) Runs initial sizing (iii) Runs LDP iterator followed by
 * (iv) Timing recovery and (v) Power reduction
 */
void Sizer:: RunSizer (void)
{
	debug_t_start(BuildFootPrintLib_enum);

	BuildFootprintLib();

	debug_t_stop(BuildFootPrintLib_enum);

	string out;
	string delayModel;
	string slewModel;
	string ceffModel;

	if (ELMORE)
		delayModel = "ELMORE";
	else if (D2M)
		delayModel = "D2M";
	else if (DM1)
		delayModel = "DM1";
	else
		delayModel = "DM2";

	if (SCALED_S2M)
		slewModel = "SCALED S2M";
	else if (TAU_SLEW)
		slewModel = "TAU_SLEW";
	else
		slewModel = "PERI";

	if (ECM)
		ceffModel = "ECM";
	else
		ceffModel = "Lumped";

	cout << endl << "Delay Model = " << delayModel << "  Ceff Model = " << ceffModel << "  Slew Model = " << slewModel << endl << endl;
	_dbgFile << endl << "Delay Model = " << delayModel << "  Ceff Model = " << ceffModel << "  Slew Model = " << slewModel << endl << endl;

	debug_t_start(InitialSizer_enum);

	InitialSizer();

	debug_t_stop(InitialSizer_enum);


	debug_t_start(LDPSolver_enum);

	LDPSolver();

	debug_t_stop(LDPSolver_enum);

	_bestSoln = TrackSoln;

	_totalViolations = 0;
	out =  !check_violations() ? "violations present" : "no violations";

	_dbgFile << "Solution after LDP --> **" <<out << "**" << "  #Violations = " << _totalViolations << endl << "\tleakage = " << _bestSoln.lkg <<
			"\tTNS = " << _bestSoln.tns  << "\tWorst slack = " << _bestSoln.worstSlack
			<< "\tNo.of iterations = "  << _numIters << endl;
	cout   << "Solution after LDP --> **" <<out<< "**" << "  #Violations = " << _totalViolations << endl << "\tleakage = " << _bestSoln.lkg <<
			"\tTNS = " << _bestSoln.tns << "\tWorst slack = " << _bestSoln.worstSlack
			<< "\tNo.of iterations = " << _numIters << endl;

	// Global timing recovery
	debug_t_start(TimingRecovery_enum);

	bool trSuccess = TimingRecovery();

	debug_t_stop(TimingRecovery_enum);

	if (trSuccess == true)
	{
		_dbgFile << "Successfully recovered timing"<< endl;
		cout     << "Successfully recovered timing"<< endl;
	}
	else
	{
		_dbgFile << "Timing recovery failed"<< endl;
		cout     << "Timing recovery failed"<< endl;
	}

	_totalViolations = 0;
	out =  !check_violations() ? "violations present" : "no violations";

	_dbgFile << "Solution after TR1 --> **"<<out << "**" << "  #Violations = " << _totalViolations << endl << "\tleakage = " << _bestSoln.lkg <<
			"\tTNS = " << _bestSoln.tns << "\tWorst slack = " << _bestSoln.worstSlack
			<< "\tNo.of iterations = " << _numIters << endl;
	cout << "Solution after TR1 --> **"<<out << "**" << "  #Violations = " << _totalViolations << endl << "\tleakage = " << _bestSoln.lkg <<
			"\tTNS = " << _bestSoln.tns << "\tWorst slack = " << _bestSoln.worstSlack
			<< "\tNo.of iterations = " << _numIters << endl;

	STA();
	TrackSoln = GetStateParams();

	_dbgFile << "Solution after TR2 --> **"<<out << "**" << endl << "\tleakage = " << TrackSoln.lkg <<
			"\tTNS = " << TrackSoln.tns << "\tWorst slack = " << TrackSoln.worstSlack
			<< "\tNo.of iterations = " << _numIters << endl;

	_dbgFile << "Solution after TR3 --> **"<<out << "**" << endl << "\tleakage = " << TrackSoln.lkg <<
			"\tTNS = " << TrackSoln.tns << "\tWorst slack = " << TrackSoln.worstSlack
			<< "\tNo.of iterations = " << _numIters << endl;


	// Final power reduction algorithm
	debug_t_start(PowerReduction_enum);

	PowerReduction();

	debug_t_stop(PowerReduction_enum);

	_dbgFile << "Power Reduction completed"<< endl;
	cout     << "Power Reduction completed"<< endl;

	DumpVersion(-1);

	_bestSoln = TrackSoln;

	_totalViolations = 0;
	out =  !check_violations() ? "violations present" : "no violations";

	_dbgFile << "Final Solution --> **"<<out<< "**" << "  #Violations = " << _totalViolations << endl <<"\tleakage = " << _bestSoln.lkg <<
			"\tTNS = " << _bestSoln.tns << "\tWorst slack = " << _bestSoln.worstSlack
			<< "\tNo.of iterations = " << _numIters << "  #Violations = " << _totalViolations << endl;
	cout << "Final Solution --> **"<<out<< "**" << "  #Violations = " << _totalViolations << endl <<"\tleakage = " << _bestSoln.lkg <<
			"\tTNS = " << _bestSoln.tns << "\tWorst slack = " << _bestSoln.worstSlack
			<< "\tNo.of iterations = " << _numIters << endl;

/* The following lines of code dump the Ceff at each pin and delay calculation commands to be sent to external signoff timer*/

//	string cmdFile = _outFile;
//	_cmdFile.open(cmdFile.append(".command").c_str());
//
//	string ceffFile = _outFile;
//	_ceffFile.open(ceffFile.append(".ceff").c_str());
//
//	bool firstTime = true;
//	for (int64 i = 0; i < _sort.size() ; i++)
//	{
//		Instance* gate = _sort[i]->port.cellPtr;
//		if (gate->cellLibIndex_l == PI || gate->cellLibIndex_l == CLK_SRC)
//			continue;
//		if (firstTime == true)
//		{
//			_cmdFile << "report_delay_calculation -from " << gate->instanceName << "/" << gate->iipList[0].name.pinName
//					<< " -to " << gate->instanceName << "/" << gate->iopList[0].name.pinName << " > /home/aman/iitSizer/pt_results/temp.txt"  << endl;
//			firstTime = false;
//		}
//		else
//			_cmdFile << "report_delay_calculation -from " << gate->instanceName << "/" << gate->iipList[0].name.pinName
//			<< " -to " << gate->instanceName << "/" << gate->iopList[0].name.pinName << " >> /home/aman/iitSizer/pt_results/temp.txt"  << endl;
//		_ceffFile << gate->instanceName << "/" << gate->iopList[0].name.pinName << " : " << gate->iopList[0].ceffLate << endl;
//	}

	cout << "----------------------------------------------------------------------------------------------------" << endl;
    _dbgFile << "----------------------------------------------------------------------------------------------------" << endl;
}

/**
 * Builds footprint library - For each footprint name, it stores information about all of its variants in a more
 * structured way
 */
void Sizer:: BuildFootprintLib(void)
{
	int64  fphash;
	string footprint;
	int64  index = -1;
	TempHashTable tempHash;
    GateVersionInfo gVer;
	// Iterating over each cell in liberty
	for(int i = 0; i < (int)celllibstorage_l.celllib.size(); i++)
	{
		index  = -1;
		fphash = celllibstorage_l.celllib[i].fphash;

		// Check if footprint of current cell exists in fplib
		for(int j = 0; j < (int)FPLibstorage_l.fplib.size(); j++)
		{
			if(fphash == FPLibstorage_l.fplib[j].fphash)
			{
				index           = j;
				gVer.vPtr       = &(celllibstorage_l.celllib[i]);
				gVer.celllibIdx = i;
				FPLibstorage_l.fplib[j].version_lk.push_back(gVer);
				celllibstorage_l.celllib[i].fp = &(FPLibstorage_l.fplib[j]);
				break;
			}
		}

		if(index == -1)  // If footprint of current cell doesn't exist in fplib, then create it.
		{
			LibParserFPInfo tempFP;
			tempFP.footprint = celllibstorage_l.celllib[i].footprint;
			tempFP.fphash    = celllibstorage_l.celllib[i].fphash;
			gVer.vPtr        = &(celllibstorage_l.celllib[i]);
			gVer.celllibIdx  = i;
			tempFP.version_lk.push_back(gVer);

			FPLibstorage_l.fplib.push_back(tempFP);

			// Creating hash table for fplib
			tempHash.index = FPLibstorage_l.fplib.size()-1;
			tempHash.hash  = tempFP.fphash;

			FPLibstorage_l.fphashtable.push_back(tempHash);

			celllibstorage_l.celllib[i].fp = &(FPLibstorage_l.fplib[FPLibstorage_l.fplib.size()-1]);
		}
	}

	// sorting the foot print lib's hash table
	sort (FPLibstorage_l.fphashtable.begin(), FPLibstorage_l.fphashtable.end(), CompareByHashnum < TempHashTable >());

	int  vth_cnt      = 0;
	bool vth_noting   = NOT_DONE;

	for(int i = 0; i < (int) FPLibstorage_l.fplib.size(); i++)
	{
		
		if(FPLibstorage_l.fplib[i].version_lk.size() > 0 &&  FPLibstorage_l.fplib[i].version_lk[0].vPtr->isSequential == true)
			{
				FPLibstorage_l.fplib[i].version_vth.resize(1);
				FPLibstorage_l.fplib[i].version_vth[0].push_back(FPLibstorage_l.fplib[i].version_lk[0]);
				FPLibstorage_l.fplib[i].version_lk[0].vPtr->idxVth[0] = 0;
				FPLibstorage_l.fplib[i].version_lk[0].vPtr->idxVth[1] = 0;
				FPLibstorage_l.fplib[i].version_lk[0].vPtr->idxLk = 0;

				continue;

			}
			int numVersions = FPLibstorage_l.fplib[i].version_lk.size();

		sort (FPLibstorage_l.fplib[i].version_lk.begin(), FPLibstorage_l.fplib[i].version_lk.end(), CompareByArea <GateVersionInfo>());

		if(vth_noting == NOT_DONE)
		{
			double64 commonArea = 0;
			if(FPLibstorage_l.fplib[i].version_lk.size() > 0 )
				commonArea = FPLibstorage_l.fplib[i].version_lk[0].vPtr->area;

			for(int j = 0 ; j< (int) FPLibstorage_l.fplib[i].version_lk.size() ; j++)
			{
				if(commonArea == FPLibstorage_l.fplib[i].version_lk[j].vPtr->area)
					vth_cnt++;
				else
					break;
			}
			vth_noting = DONE;
		}


		for(int k = 0; k < (int) numVersions /vth_cnt; k++)
		{
			sort (FPLibstorage_l.fplib[i].version_lk.begin()+vth_cnt*k, FPLibstorage_l.fplib[i].version_lk.begin()+vth_cnt*k+vth_cnt,
					CompareByLkg <GateVersionInfo>());

		}

		FPLibstorage_l.fplib[i].version_vth.resize(vth_cnt);


		for(int k = 0; k < (int) numVersions /vth_cnt; k++)
		{
			for(int z = 0; z < vth_cnt; z++)
			{
				FPLibstorage_l.fplib[i].version_vth[z].push_back(FPLibstorage_l.fplib[i].version_lk[k*vth_cnt+z]);
				FPLibstorage_l.fplib[i].version_lk[k*vth_cnt+z].vPtr->idxVth[0] = z;
				FPLibstorage_l.fplib[i].version_lk[k*vth_cnt+z].vPtr->idxVth[1] = FPLibstorage_l.fplib[i].version_vth[z].size()-1;
			}
		}


		sort (FPLibstorage_l.fplib[i].version_lk.begin(), FPLibstorage_l.fplib[i].version_lk.end(), CompareByLkg <GateVersionInfo>());

		for(int z = 0; z < (int) numVersions; z++)
			{
				FPLibstorage_l.fplib[i].version_lk[z].vPtr->idxLk = z;
			}
	}
}

/**
 * \param version Pointer to cell-type
 * \param gate Pointer to instance of a gate
 * \param alpha Max-capacitance scale down factor
 *
 * Finds load and slew violations and return true if a violation is found for the given gate
 */
bool Sizer::FindLoadViolations (LibParserCellInfo* version, Instance* gate, double64 alpha)
{
	LibParserPinInfo* versionOpin = &(version->opins[0]);
	double64 maxCap, output_load;
	maxCap                        = versionOpin->maxCapacitance;
	output_load                   = gate->iopList[0].ceffLate + versionOpin->capacitance - gate->iopList[0].info_late->capacitance;

	if ((alpha * maxCap) > output_load)
		return false;
	else
		return true;
}

/**
 * \param version Pointer to cell-type
 * \param gate Pointer to instance of a gate
 * \param alpha Max-capacitance scale down factor
 *
 * Finds load and slew violations for all
 * the neighbouring gates of the given gate and returns true if a violation is found
 */
bool Sizer::CheckLoadViolations (LibParserCellInfo* version, Instance* gate, double64 alpha)
{
	if(ECM == false)
	{
		if (FindLoadViolations(version, gate, alpha) == true) return true;

		for (int z = 0; z < (int)gate->iipList.size(); z++)
		{
			Instance* prevGate = gate->iipList[z].wire->port.cellPtr;

			if (prevGate->cellLibIndex_l == PI || prevGate->cellLibIndex_l == CLK_SRC)
				continue;
			else if (prevGate->info_late->isSequential)
			{
				if (FindLoadViolations(prevGate->info_late->fp->version_lk[0].vPtr, prevGate, alpha) == true)
					return true;
			}
			else
			{
				if (FindLoadViolations(prevGate->info_late->fp->version_lk[prevGate->info_late->idxLk].vPtr, prevGate, alpha) == true)
					return true;
			}
		}
	}
	else // ECM = true
	{
		if (FindLoadViolations(version, gate, alpha) == true)
			return true;

		// For driving nets
		for(int z = 0 ; z < (int)gate->iipList.size() ; z++)
		{
			Instance* prevGate = gate->iipList[z].wire->port.cellPtr;

			if (prevGate->cellLibIndex_l == PI || prevGate->cellLibIndex_l == CLK_SRC)
					continue;
			else if (prevGate->info_late->isSequential)
			{
				if (FindLoadViolations(prevGate->info_late->fp->version_lk[0].vPtr, prevGate, alpha) == true)
					return true;
			}
			else
			{
				if (FindLoadViolations(prevGate->info_late->fp->version_lk[prevGate->info_late->idxLk].vPtr, prevGate, alpha) == true)
					return true;
			}

			for(int y=0; y< (int)gate->iipList[z].wire->taps.size(); y++)
			{
				prevGate = gate->iipList[z].wire->taps[y].cellPtr;
				if (prevGate->cellLibIndex_l == PO)
						continue;
				else if (prevGate->info_late->isSequential)
				{
					if (FindLoadViolations(prevGate->info_late->fp->version_lk[0].vPtr, prevGate, alpha) == true)
						return true;
				}
				else
				{
					if (FindLoadViolations(prevGate->info_late->fp->version_lk[prevGate->info_late->idxLk].vPtr, prevGate, alpha) == true)
						return true;
				}
			}
		}

		//for drain nets
		for(int m = 0; m < (int)gate->iopList[0].wire->taps.size(); m++)
		{
			if(gate->iopList[0].wire->taps[m].cellPtr->cellLibIndex_l == PO || gate->iopList[0].wire->taps[m].cellPtr->info_late->isSequential == true)
				continue;

			SpefNet* net = gate->iopList[0].wire->taps[m].cellPtr->iopList[0].wire;
			Instance* prevGate = net->port.cellPtr;

			if (prevGate->cellLibIndex_l == PI || prevGate->cellLibIndex_l == CLK_SRC)
				continue;
			else if (prevGate->info_late->isSequential)
			{
				if (FindLoadViolations(prevGate->info_late->fp->version_lk[0].vPtr, prevGate, alpha) == true)
					return true;
			}
			else
			{
				if (FindLoadViolations(prevGate->info_late->fp->version_lk[prevGate->info_late->idxLk].vPtr, prevGate, alpha) == true)
					return true;
			}

			for(int y = 0; y < (int)net->taps.size(); y++)
			{
				prevGate = net->taps[y].cellPtr;
				if (prevGate->cellLibIndex_l == PO)
						continue;
				else if (prevGate->info_late->isSequential)
				{
					if (FindLoadViolations(prevGate->info_late->fp->version_lk[0].vPtr, prevGate, alpha) == true)
						return true;
				}
				else
				{
					if (FindLoadViolations(prevGate->info_late->fp->version_lk[prevGate->info_late->idxLk].vPtr, prevGate, alpha) == true)
						return true;
				}
			}
		}

	}

	return false;
}

/**
 * \param version Pointer to cell-type
 * \param gate Pointer to instance of a gate
 * \param slewMode Slew mode- either DEFAULT_MAX_SLEW or slew of the pin
 * \param alpha Max-capacitance scale down factor
 *
 * Finds load and slew violations and return true if a violation is found for the given gate
 */
bool Sizer::FindLoadSlewViolations (LibParserCellInfo* version, Instance* gate, bool slewMode, double64 alpha)
{
	LibParserPinInfo* versionOpin = &(version->opins[0]);
	double64 maxCap, output_load, output_slew;
	maxCap      = versionOpin->maxCapacitance;
	output_load = gate->iopList[0].ceffLate + versionOpin->capacitance - gate->iopList[0].info_late->capacitance;
	output_slew = _forwardPropagate_ptr->ComputeSlew (gate, version, output_load, versionOpin, slewMode);

	if (output_slew < MaxSlew && (alpha * maxCap) > output_load)
		return false;
	else
		return true;
}

/**
 * Initial sizing algorithm - Initial Sizer first set all gate sizes to lowest leakage version.
 * Then it propagates in Reverse Topological Order (RTO) and removes load & slew violations.
 * Incorporated for multiple output gates as well.
 * Assumed that no sizing needed for clock network member gates
 */
void Sizer::InitialSizer (void)
{
	LibParserFPInfo* fp;

	STA();
    TrackSoln = GetStateParams();

	_sort.clear();
	_coneEndPoints.clear();

	for (int64 i = 0; i < (int64)Sort.size() ; i++)
	{
		_sort.push_back(Sort[i]);
	}

	for (int64 i = 0; i < (int64)ConeEndPoints.size() ; i++)
	{
		if (i!=0 && (ConeEndPoints[i]->hash == ConeEndPoints[i-1]->hash &&
				ConeEndPoints[i]->name.cellid == ConeEndPoints[i-1]->name.cellid))
			continue;

		_coneEndPoints.push_back(ConeEndPoints[i]);
	}

	_currSoln = TrackSoln;

	_dbgFile << -2 << "\t\t" << "\tTNS = " << _currSoln.tns << "\tWNS = " << _currSoln.worstSlack <<
			"\tleakage = " << _currSoln.lkg << "\tLD = " << _currSoln.totalLD << endl;
	cout << -2 << "\t\t" << "\tTNS = " << _currSoln.tns <<
			"\tleakage = " << _currSoln.lkg << "\tLD = " << _currSoln.totalLD << endl;


	// Setting all gates to lowest leakage version
	for (int64 i = 0; i < (int64)InstanceHashTable.size(); i++)
	{
		if (InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PI ||
				InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PO ||
				InstanceList[InstanceHashTable[i].index].cellLibIndex_l == CLK_SRC ||
				InstanceList[InstanceHashTable[i].index].info_late->isSequential == true)
			continue;

		fp = InstanceList[InstanceHashTable[i].index].info_late->fp;
		SetVersion(&(InstanceList[InstanceHashTable[i].index]), &(fp->version_lk[0]));
	}

	STA();
	TrackSoln = GetStateParams();

	_currSoln = TrackSoln;
	_dbgFile << -1 << "\t\t" << "\tTNS = " << _currSoln.tns <<
			"\tleakage = " << _currSoln.lkg << "\tLD = " << _currSoln.totalLD << endl;
	cout     << -1 << "\t\t" << "\tTNS = " << _currSoln.tns <<
			"\tleakage = " << _currSoln.lkg << "\tLD = " << _currSoln.totalLD << endl;

	for(int64 i = (_sort.size()-1); i >= 0; i--)
	{
		Instance* currGate = _sort[i]->port.cellPtr;
		if (currGate->cellLibIndex_l == PI || currGate->cellLibIndex_l == CLK_SRC ||
				currGate->info_late->isSequential == true)
			continue;

		// TODO: Precompute the below vectors apriori before LDP Solver to save time

		vector < SpefNet* > drivingNets;
		vector < std::pair<SpefTap*, int> > sideTaps;
		vector < std::pair<SpefTap*, int> > sinkTaps;

		for (int64 i = 0; i < (int64)currGate->iipList.size(); i++)
		{
			SpefNet* currNet = currGate->iipList[i].wire;
			drivingNets.push_back(currNet);
			for (int j = 0; j < (int)currNet->taps.size(); j++)
			{
				if (currNet->taps[j].cellPtr->hash != currGate->hash)
				{
					if (currNet->taps[j].cellPtr->cellLibIndex_l == PO || currNet->taps[j].cellPtr->info_late->isSequential)
					{
						std::pair <SpefTap*, int> temp;
						temp.first  = &(currNet->taps[j]);
						temp.second = -1;
						sideTaps.push_back(temp);
					}
					else
					{
						InstanceOpin* oPin = &(currNet->taps[j].cellPtr->iopList[0]);
						int idx = 0;
						for (int k = 0; k < (int)oPin->dData.size(); k++)
						{
							if (currNet->taps[j].pinPtr->hash == currNet->taps[j].cellPtr->iipList[oPin->dData[k].index].hash)
							{
								idx = k;
								break;
							}
						}
						std::pair <SpefTap*, int> temp;
						temp.first  = &(currNet->taps[j]);
						temp.second = idx;
						sideTaps.push_back(temp);
					}
				}
			}
		}

		SpefNet* currNet = currGate->iopList[0].wire;
		for (int j = 0; j < (int)currNet->taps.size(); j++)
		{

			if (currNet->taps[j].cellPtr->cellLibIndex_l == PO || currNet->taps[j].cellPtr->info_late->isSequential)
				{
					std::pair <SpefTap*, int> temp;
					temp.first  = &(currNet->taps[j]);
					temp.second = -1;
					sinkTaps.push_back(temp);
				}
				else
				{
					InstanceOpin* oPin = &(currNet->taps[j].cellPtr->iopList[0]);
					int idx = 0;
					for (int k = 0; k < (int)oPin->dData.size(); k++)
					{
						if (currNet->taps[j].pinPtr->hash == currNet->taps[j].cellPtr->iipList[oPin->dData[k].index].hash)
						{
							idx = k;
							break;
						}
					}
					std::pair <SpefTap*, int> temp;
					temp.first  = &(currNet->taps[j]);
					temp.second = idx;
					sinkTaps.push_back(temp);
				}

		}
		sort (drivingNets.begin(), drivingNets.end(), CompareNetLevel2());

		vector < SpefNet* > drainNets;
		for (int64 i = 0; i < (int)sinkTaps.size(); i++)
		{
			if(sinkTaps[i].second != -1)
			{
				SpefNet* dNet = sinkTaps[i].first->cellPtr->iopList[0].wire;
				drainNets.push_back(dNet);
			}
		}
		sort (drainNets.begin(), drainNets.end(), CompareNetLevel2());

		fp = currGate->info_late->fp;
		int64 c1 = 0, idx = -1;

		for (c1 = 0; c1 < (int64)fp->version_lk.size(); c1++)
		{
			if (FindLoadSlewViolations(fp->version_lk[c1].vPtr, currGate, true, ALPHA) == false)
			{
				idx = c1;
				break;
			}
		}
		if (idx != -1)
		{
			if (currGate->info_late != fp->version_lk[idx].vPtr)
			{
				SetVersion(currGate, &(fp->version_lk[idx]));

				if (ECM == true)
					UpdateLocalTiming(currGate, drivingNets, sideTaps, sinkTaps, drainNets);
			}
		}
		else
		{
			cout << "No version without initial load/slew violations found for " << fp->footprint << endl;
			if (currGate->info_late != fp->version_lk[fp->version_lk.size() - 1].vPtr)
			{
				SetVersion(currGate, &(fp->version_lk[fp->version_lk.size() - 1]));

				if (ECM == true)
					UpdateLocalTiming(currGate, drivingNets, sideTaps, sinkTaps, drainNets);
			}
		}

		currGate->bestSoln = currGate->info_late;
	}
}

/**
 * Reads the sizes from given input file- mostly used for debugging purposes where sizes from previous iterations
 * were dumped and they need to be read-in and proceed from there instead of starting the iterations from first
 */
void Sizer::ReadVersion (int numIter)
{
	string verFile = _sizeFile;
	_inpFile.open(verFile.c_str());

	bool valid     = true;
	deque <string> tokens;
	int64 idx1, idx2;
	valid = read_tau2015_as_tokens(_inpFile,tokens);
	while(valid)
	{
		assert(tokens.size() == 2 );

		if(tokens[0] == "iteration" && std::atoi(tokens[1].c_str()) == numIter)
			break;

		valid = read_tau2015_as_tokens(_inpFile, tokens);
	}

	valid = read_tau2015_as_tokens(_inpFile,tokens);

	while(valid)
	{
		if(tokens[0] == "iteration") // Reached next iteration, so break
			break;

		idx1 = Bsearch(InstanceHashTable, ComputeHash(tokens[0]), 0,InstanceHashTable.size()-1);
		assert(idx1 != -1);
		idx2 = Bsearch(celllibstorage_l.cellhashtable, ComputeHash(tokens[1]), 0, celllibstorage_l.cellhashtable.size()-1);
		assert(idx2 != -1);

		assert(InstanceList[InstanceHashTable[idx1].index].instanceName == tokens[0]);
		assert(celllibstorage_l.celllib[celllibstorage_l.cellhashtable[idx2].index].name == tokens[1]);

		int64 idxLk = celllibstorage_l.celllib[celllibstorage_l.cellhashtable[idx2].index].idxLk;

		SetVersion(&(InstanceList[InstanceHashTable[idx1].index]),
				&(celllibstorage_l.celllib[celllibstorage_l.cellhashtable[idx2].index].fp->version_lk[idxLk]));

		valid = read_tau2015_as_tokens(_inpFile, tokens);
	}

	_inpFile.close();
}

/**
 * Dumps the sizes of the current solution to an output file- mostly used for debugging purposes where sizes from current iteration
 * need to be dumped out for future use. This function is also called for final solution dumping
 */
void Sizer:: DumpVersion (int iterCnt)
{
	string verFile = _outFile;
	static int callCnt; // Automatically initialized to 0

	if(callCnt == 0)
		_verFile.open(verFile.append(".version").c_str());
	else
		_verFile.open(verFile.append(".version").c_str(), ios::app);


	_verFile  << endl;
     _verFile << "iteration" << "\t" << iterCnt << endl;

     assert(InstanceList.size() == InstanceHashTable.size());

	for(int64 i = 0; i < (int64)InstanceList.size(); i++)
	{
		if(InstanceList[i].cellLibIndex_l == PO ||
		    InstanceList[i].cellLibIndex_l == PI ||
		    InstanceList[i].cellLibIndex_l == CLK_SRC)
			continue;
		else
		{
			_verFile << InstanceList[i].instanceName << "\t" << InstanceList[i].info_late->name << endl;
		}
 	}

	_verFile << endl;

	_verFile.close();

	callCnt ++;

	return;
}

/**
 * \param gate Gate of the version to be changed
 * \param version Cell-version type to be set for the given gate
 *
 * Sets the gate to a given version, changes all the attributes associated with itself and its pins such as
 * timing arcs, pin capacitance, connection pointers and so on
 */
void Sizer::SetVersion (Instance* gate, GateVersionInfo* version)
{

	TrackSoln.lkg       += version->vPtr->leakage - gate->info_late->leakage;
	gate->info_late      = version->vPtr;
	gate->cellLibIndex_l = version->celllibIdx;

	for (int64 i = 0; i < (int64) gate->iipList.size(); i++)
	{

		InstanceIpin* iPin           = &(gate->iipList[i]);
		SpefNet* net                 = iPin->wire;
		assert(iPin->capacitanceLate == iPin->tap->capacitanceLate);
		iPin->capacitanceLate       -= iPin->info_late->capacitance;
		assert(iPin->name.pinName    == version->vPtr->ipins[version->vPtr->ipinhashtable[i].index].name);
		iPin->info_late              = &(version->vPtr->ipins[version->vPtr->ipinhashtable[i].index]);
		iPin->capacitanceLate       += iPin->info_late->capacitance;
		SpefTap* tap                 = iPin->tap;
		tap->delayLate              += tap->lumpedRes*(iPin->capacitanceLate - tap->capacitanceLate);

		assert(iPin->wire->port.ceffLate == iPin->wire->port.pinPtr->ceffLate);

		iPin->wire->port.ceffLate   += (iPin->capacitanceLate - tap->capacitanceLate);
		iPin->wire->port.pinPtr->ceffLate = iPin->wire->port.ceffLate;

		int idx = -1;
		if(net->otherNode.size() != 0)
		{
			if (ECM == false)
				net->otherNode[net->otherNode.size() - 1].capacitanceLate += (iPin->capacitanceLate - tap->capacitanceLate);
			
			idx = Bsearch(net->otherNode, tap->hash, 0, net->otherNode.size() - 2);
			assert(idx != -1);
			net->otherNode[idx].capacitanceLate += (iPin->capacitanceLate - tap->capacitanceLate);
		}

		iPin->capChange     += (iPin->capacitanceLate - tap->capacitanceLate);
		tap->capacitanceLate = iPin->capacitanceLate;

		assert(iPin->capacitanceLate > -0.001);

		if (SET_BETA == true && net->otherNode.size() != 0 && SAVE_NET == true && ISPD_2012 == false)
			_sp->NFSTapdelaybeta(*net, idx, iPin->capChange);

		iPin->capChange = 0;
	}

	InstanceOpin* oPin           = &(gate->iopList[0]);
	double64 capChange           = 0;
	capChange                   -= oPin->info_late->capacitance;
	oPin->capacitanceLate       -= oPin->info_late->capacitance;
	oPin->ceffLate              -= oPin->info_late->capacitance;
	oPin->port->capacitanceLate -= oPin->info_late->capacitance;
	oPin->port->ceffLate        -= oPin->info_late->capacitance;

	if(oPin->wire->otherNode.size() != 0)
		oPin->wire->otherNode[oPin->wire->otherNode.size() - 1].capacitanceLate -= oPin->info_late->capacitance;

	assert (oPin->name.pinName == version->vPtr->opins[version->vPtr->opinhashtable[0].index].name);

	oPin->info_late              = &(version->vPtr->opins[version->vPtr->opinhashtable[0].index]);
	oPin->capacitanceLate       += oPin->info_late->capacitance;
	oPin->ceffLate              += oPin->info_late->capacitance;
	oPin->port->capacitanceLate += oPin->info_late->capacitance;
	oPin->port->ceffLate        += oPin->info_late->capacitance;
	capChange                   += oPin->info_late->capacitance;

	if(oPin->wire->otherNode.size() != 0)
		oPin->wire->otherNode[oPin->wire->otherNode.size() - 1].capacitanceLate += oPin->info_late->capacitance;

	if (ECM == true && capChange != 0)
	{
		SpefNet& spefNet                = *(oPin->wire);
		int otherNodesz                 = spefNet.otherNode.size();
		spefNet.pi[otherNodesz - 1].y1 += capChange;
		spefNet.c2                      = spefNet.pi[otherNodesz - 1].y1 - spefNet.c1;
		spefNet.port.ceffLate           = spefNet.c1 + spefNet.c2;
		spefNet.port.pinPtr->ceffLate   = spefNet.port.ceffLate;
	}

}

/**
 * \param gate Gate of the version to be changed
 * \param version Cell-version type to be set for the given gate
 *
 * Sets the gate to a given version, changes all the attributes associated with itself and its pins such as
 * timing arcs, pin capacitance, connection pointers and so on. - This function doesn't update moments of the driving nets
 * of the gate like SetVersion does
 */
void Sizer::SetVersion1 (Instance* gate, GateVersionInfo* version)
{
	TrackSoln.lkg       += version->vPtr->leakage - gate->info_late->leakage;
	gate->info_late      = version->vPtr;
	gate->cellLibIndex_l = version->celllibIdx;

	for (int64 i = 0; i < (int64) gate->iipList.size(); i++)
	{
		InstanceIpin* iPin           = &(gate->iipList[i]);
		SpefNet* net                 = iPin->wire;
		assert(iPin->capacitanceLate == iPin->tap->capacitanceLate);
		iPin->capacitanceLate       -= iPin->info_late->capacitance;
		assert(iPin->name.pinName    == version->vPtr->ipins[version->vPtr->ipinhashtable[i].index].name);
		iPin->info_late              = &(version->vPtr->ipins[version->vPtr->ipinhashtable[i].index]);
		iPin->capacitanceLate       += iPin->info_late->capacitance;
		SpefTap* tap                 = iPin->tap;
		tap->delayLate              += tap->lumpedRes*(iPin->capacitanceLate - tap->capacitanceLate);

		assert(iPin->wire->port.ceffLate == iPin->wire->port.pinPtr->ceffLate);

		iPin->wire->port.ceffLate        += (iPin->capacitanceLate - tap->capacitanceLate);
		iPin->wire->port.pinPtr->ceffLate = iPin->wire->port.ceffLate;

		int idx = -1;

		if(net->otherNode.size() != 0)
			//CHECKPOINT: Throughout the code, check if this condition is applied, otherwise gives segmentation fault
		{
			if (ECM == false)
				net->otherNode[net->otherNode.size() - 1].capacitanceLate += (iPin->capacitanceLate - tap->capacitanceLate);

			idx = Bsearch(net->otherNode, tap->hash, 0, net->otherNode.size() - 2);

			assert(idx != -1);

			net->otherNode[idx].capacitanceLate += (iPin->capacitanceLate - tap->capacitanceLate);
		}

		iPin->capChange += iPin->capacitanceLate - tap->capacitanceLate;

		tap->capacitanceLate = iPin->capacitanceLate;

	}

	InstanceOpin* oPin            = &(gate->iopList[0]);
	oPin->capacitanceLate        -= oPin->info_late->capacitance;
	oPin->ceffLate               -= oPin->info_late->capacitance;
	oPin->port->capacitanceLate  -= oPin->info_late->capacitance;
	oPin->port->ceffLate         -= oPin->info_late->capacitance;

	if(oPin->wire->otherNode.size() != 0)
		oPin->wire->otherNode[oPin->wire->otherNode.size() - 1].capacitanceLate -= oPin->info_late->capacitance;

	assert (oPin->name.pinName == version->vPtr->opins[version->vPtr->opinhashtable[0].index].name);

	oPin->info_late                = &(version->vPtr->opins[version->vPtr->opinhashtable[0].index]);
	oPin->capacitanceLate         += oPin->info_late->capacitance;
	oPin->ceffLate                += oPin->info_late->capacitance;
	oPin->port->capacitanceLate   += oPin->info_late->capacitance;
	oPin->port->ceffLate          += oPin->info_late->capacitance;

	if(oPin->wire->otherNode.size() != 0)
		oPin->wire->otherNode[oPin->wire->otherNode.size() - 1].capacitanceLate += oPin->info_late->capacitance;

}

/**
 * \param gate Gate of the version to be changed
 * \param version Cell-version type to be set for the given gate
 *
 * Sets the gate to a given version- This is followed by SetVersion1, performs moment updates for driving nets and other
 * affected nets due to version change of 'gate'
 */
void Sizer::SetVersion2 (Instance* gate, GateVersionInfo* version)
{
	for (int64 i = 0; i < (int64) gate->iipList.size(); i++)
	{
		InstanceIpin* iPin = &(gate->iipList[i]);
		SpefNet* net       = iPin->wire;
		SpefTap* tap       = iPin->tap;

		int idx = -1;
		if(net->otherNode.size() != 0)
		{
			idx = Bsearch(net->otherNode, tap->hash, 0, net->otherNode.size() - 2);
			assert(idx != -1);
		}

		assert(iPin->capacitanceLate > -0.001);

		if (SET_BETA == true && net->otherNode.size() != 0 && SAVE_NET == true && ISPD_2012 == false)
			_sp->NFSTapdelaybeta(*net, idx, iPin->capChange);

		iPin->capChange = 0;
	}
}

/**
 * \param slack Pin-slack
 * \param k Lagrangian-multiplier (LM) scaling exponent
 *
 * Returns the scaling factor for LM update
 */
double64 Sizer::GetScaler  (double64 slack, double64 k)
{
	if (slack < 0)
		return pow((1 - slack/timing.clock.period), (1.0/k)) ;
	else
		return pow((1 + slack/timing.clock.period), (-1.0*k)) ;
}

/**
 * \param k Lagrangian-multiplier (LM) scaling exponent
 *
 * Updates Lagrangian multipliers (of delay) for each timing arc in the design
 */
void Sizer::UpdateLM (double64 powK)
{
	if (_coneEndPoints.size() == 0)
	{
		cout << "No _coneEndPoints, exiting..." << endl;
		assert(0);
	}
	for (int64 i = 0; i < (int64)_coneEndPoints.size(); i++)
	{
		if (i!=0 && (_coneEndPoints[i]->hash == _coneEndPoints[i-1]->hash && _coneEndPoints[i]->name.cellid == _coneEndPoints[i-1]->name.cellid))
			continue;

		Instance* gate  = _coneEndPoints[i]->cellPtr;
		gate->lambdaRL *= GetScaler ((_coneEndPoints[i]->tData.slackRiseLate), powK);
		gate->lambdaFL *= GetScaler ((_coneEndPoints[i]->tData.slackFallLate), powK);
	}

	for (int64 i = 0; i < (int64)InstanceHashTable.size(); i++)
	{
		Instance* gate = &(InstanceList[InstanceHashTable[i].index]);
		if (gate->cellLibIndex_l == PI ||
				 gate->cellLibIndex_l == PO || gate->cellLibIndex_l == CLK_SRC ||
				 gate->info_late->isSequential == true)
			continue;

		for (int64 j = 0; j < (int64)gate->iopList.size(); j++)
		{
			InstanceOpin* oPin = &(gate->iopList[j]);
			for (int64 k = 0; k < (int64)oPin->dData.size(); k++)
			{
				InstanceIpin* iPin = &(gate->iipList[oPin->dData[k].index]);
				if (oPin->dData[k].timeSense == POSITIVE_UNATE)
				{
					oPin->dData[k].lambdaRL *= GetScaler(iPin->tData.slackRiseLate, powK);
					oPin->dData[k].lambdaFL *= GetScaler(iPin->tData.slackFallLate, powK);
				}
				else if (oPin->dData[k].timeSense == NEGATIVE_UNATE)
				{
					oPin->dData[k].lambdaRL *= GetScaler(iPin->tData.slackFallLate, powK);
					oPin->dData[k].lambdaFL *= GetScaler(iPin->tData.slackRiseLate, powK);
				}
				else
				{
					double64 slack = min (iPin->tData.slackRiseLate, iPin->tData.slackFallLate);
					oPin->dData[k].lambdaRL *= GetScaler(slack, powK);
					oPin->dData[k].lambdaFL *= GetScaler(slack, powK);
				}
			}
		}
	}

	for(int64 i = ((int64)_sort.size()-1); i >= 0; i--)
	{
		Instance* currGate = _sort[i]->port.cellPtr;
		if (currGate->cellLibIndex_l == PI || currGate->cellLibIndex_l == CLK_SRC ||
				currGate->info_late->isSequential == true)
			continue;

		double64 lambdaSumRise = 0, lambdaSumFall = 0;

		InstanceOpin* oPin = _sort[i]->port.pinPtr;
		for (int64 j = 0; j < (int64)oPin->dData.size(); j++)
		{
			lambdaSumRise += oPin->dData[j].lambdaRL;
			lambdaSumFall += oPin->dData[j].lambdaFL;
		}

		double64 optSumRise = 0, optSumFall = 0;
		for (int64 j = 0; j < (int64)_sort[i]->taps.size(); j++)
		{
			InstanceIpin* nextIpin = _sort[i]->taps[j].pinPtr;
			Instance* tapGate = nextIpin->cellPtr;

			if (tapGate->cellLibIndex_l == PO || tapGate->info_late->isSequential == true)
			{
				optSumRise += tapGate->lambdaRL;
				optSumFall += tapGate->lambdaFL;
			}
			else
			{
				InstanceOpin* nextOpin = &(tapGate->iopList[0]);
				int64 idx = -1;
				for (int64 k = 0; k < (int64)nextOpin->dData.size(); k++)
				{
					if (tapGate->iipList[nextOpin->dData[k].index].hash == nextIpin->hash)
					{
						idx = k;
						break;
					}
				}

				if (idx == -1)
				{
					assert(0);
				}
				else
				{
					if (nextOpin->dData[idx].timeSense == POSITIVE_UNATE)
					{
						optSumRise += nextOpin->dData[idx].lambdaRL;
						optSumFall += nextOpin->dData[idx].lambdaFL;
					}

					else if (nextOpin->dData[idx].timeSense == NEGATIVE_UNATE)
					{
						optSumRise += nextOpin->dData[idx].lambdaFL;
						optSumFall += nextOpin->dData[idx].lambdaRL;
					}

					else
					{
						if (nextIpin->tData.slackRiseLate < nextIpin->tData.slackFallLate)
						{
							optSumRise += nextOpin->dData[idx].lambdaRL;
							optSumFall += nextOpin->dData[idx].lambdaRL;
						}
						else
						{
							optSumRise += nextOpin->dData[idx].lambdaFL;
							optSumFall += nextOpin->dData[idx].lambdaFL;
						}
					}
				}
			}
		}

		for (int64 j = 0; j < (int64)oPin->dData.size(); j++)
		{

			if (lambdaSumRise == 0)
				oPin->dData[j].lambdaRL = 0;
			else
				oPin->dData[j].lambdaRL *= (optSumRise / lambdaSumRise);

			if (lambdaSumFall == 0)
				oPin->dData[j].lambdaFL = 0;
			else
				oPin->dData[j].lambdaFL *= (optSumFall / lambdaSumFall);

		}
	}
}

/**
 * Updates the best solution of each gate according to the best of all the solutions
 * obtained from LDP solver
 */
void Sizer::UpdateBestSoln (void)
{
	_bestSoln = _currSoln;
	for(int64 i = 0; i< (int64)InstanceHashTable.size(); i++)
	{
		if(InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PO ||
		    InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PI ||
		    InstanceList[InstanceHashTable[i].index].cellLibIndex_l == CLK_SRC
			)
			continue;
		else
			InstanceList[InstanceHashTable[i].index].bestSoln = InstanceList[InstanceHashTable[i].index].info_late;
	}
}

/**
 * By reading the best versions of each gate, it changes the gate version to their respective best versions by iterative
 * calls to SetVersion function
 */
void Sizer::WriteBestSoln (void)
{
	for(int64 i = 0; i< (int64)InstanceHashTable.size(); i++)
	{
		if(InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PO ||
		    InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PI ||
		    InstanceList[InstanceHashTable[i].index].cellLibIndex_l == CLK_SRC
			) continue;
		else
		{
			GateVersionInfo tmp;
			int64 idx      = InstanceList[InstanceHashTable[i].index].bestSoln->idxLk;
			tmp.vPtr       = InstanceList[InstanceHashTable[i].index].bestSoln;
			tmp.celllibIdx = InstanceList[InstanceHashTable[i].index].bestSoln->fp->version_lk[idx].celllibIdx;
			SetVersion    (&(InstanceList[InstanceHashTable[i].index]), &tmp);
		}
	}
}

/**
 * The backbone LDP iterator function- Performs LRS solving iteratively
 */
void Sizer:: LDPSolver (void)
{

	// Run STA- update timing
	STA();

	double64 k1,k2,k3;
	k1 = 1.15;
	k2 = 4;
	k3 = 1.15;

	double64 k = k1;  //TODO: Figure out a way to update k
	int64 count = 0;

	bool exitFlag = false;
	bool k1Flag   = true;

	UpdateLM(k);  // Update lambdas

	TrackSoln = GetStateParams();
	_currSoln = TrackSoln;
	_bestSoln = _currSoln;

	string out;

	_totalViolations = 0;
	out =  !check_violations() ? "violations present" : "no violations";

	cout     << "Solution after IS --> **"<< out<< "**" << "  #Violations = " << _totalViolations << endl << "\tleakage = " << _bestSoln.lkg <<
			"\tTNS = " << _bestSoln.tns << "\tWorst slack = " << _bestSoln.worstSlack
			 << "\tNo.of iterations = " << _numIters << endl;
	_dbgFile << "Solution after IS --> **"<< out<< "**" << "  #Violations = " << _totalViolations << endl << "\tleakage = " << _bestSoln.lkg <<
			"\tTNS = " << _bestSoln.tns << "\tWorst slack = " << _bestSoln.worstSlack
			 << "\tNo.of iterations = " << _numIters << endl;

	_dbgFile << _numIters << "\tk = " << k << "\tTNS = " << _currSoln.tns << "\tWNS = " << _currSoln.worstSlack <<
			"\tleakage = " << _currSoln.lkg << "\tLD = " << _currSoln.totalLD << endl;

	int method;

	 method = UNBIASED;
	 _wtLD  = log(fabs(_currSoln.tns))/log(10);
	_wtLkg = 1.0;

	double64 targetTNS    = pow(10,_targetLRSSlackDir[(int)ceil(_wtLD)]);
	targetTNS             = 2*timing.clock.period;

	string profile = "UNBIASED";

	cout << "\nProfile: " << profile << "\tClock:  " << timing.clock.period << "\tinitial _wtLD: " << _wtLD << " targetTNS: " << targetTNS << endl << endl;
	
	bool firstTime = true;


	while(1 /*condition till convergence*/)
	{
		_numIters++;
		_prevSoln = _currSoln;

		LRSSolver();  // LRS-lambda solver

		STA();

		UpdateLM(k);   // Update lambdas

		TrackSoln = GetStateParams();

		_currSoln = TrackSoln;

		_dbgFile << _numIters << "\tk = " << k << "\tTNS = " << _currSoln.tns <<
				"\tlkg = " << _currSoln.lkg << "\tCost = " << (_currSoln.totalLD+_currSoln.lkg) << "\tLD = " << _currSoln.totalLD << "\tws = " << _currSoln.worstSlack << "\twLD = " << _wtLD << "\twLkg = " << _wtLkg << "\tUp = " << _cellModsUp << "\tDn = " << _cellModsDown << endl;
		cout << _numIters << "\tk = " << k << "\tTNS = " << _currSoln.tns <<
				"\tlkg = " << _currSoln.lkg << "\tCost = " << (_currSoln.totalLD+_currSoln.lkg) << "\tLD = " << _currSoln.totalLD << "\tws = " << _currSoln.worstSlack << "\twLD = " << _wtLD << "\twLkg = " << _wtLkg << "\tUp = " << _cellModsUp << "\tDn = " << _cellModsDown << endl;

		_cellModsUp = 0;
		_cellModsDown = 0;

		if(method == UNBIASED)
		{
			if(firstTime == true)
			{
				if(fabs(_currSoln.tns) <= 10*timing.clock.period)   // Approximately surely recoverable TNS range !
				{
					UpdateBestSoln();
					firstTime = false;
				}
				else if(_numIters > (0.75 * MAX_ITR))
				{
					UpdateBestSoln();
					firstTime = false;
				}
			}

			if(firstTime == false)
			{
				if(fabs(_currSoln.tns) > timing.clock.period/5)   // Approximately surely recoverable TNS range !
				{
					if(_currSoln.tns > _bestSoln.tns)
						UpdateBestSoln();
				}

				else   // Approximately surely recoverable TNS range !
				{
					if(_currSoln.lkg == _bestSoln.lkg)
					{
						if(_currSoln.tns > _bestSoln.tns)
							UpdateBestSoln();
					}
					else if (_currSoln.lkg < _bestSoln.lkg) // Recoverable TNS with smaller leakage, update best solution then
						UpdateBestSoln();
				}
			}

		}

		/* K updates */

		if(method == UNBIASED)
		{
			if(k == k1 && exitFlag == false && k1Flag == true)
			{
				if(fabs(_currSoln.tns) < 2 * timing.clock.period) // TNS has been recovered well.
				{
						k = k2;
						k1Flag = false;
				}
			}
			else if(k == k2 && exitFlag == false && k1Flag == false)
			{
				if (fabs((_prevSoln.lkg - _currSoln.lkg) / _prevSoln.lkg) <= 0.001)
				{
					if(count < CONVERGENCE_COUNT)
						count ++;
					else
					{
						k        = k3;
						exitFlag = true;
						count    = 0;
					}
				}
				else
					count = 0;
			}
			else if(k == k3 && exitFlag == true && k1Flag == false)
			{
				if (fabs((_prevSoln.lkg + _prevSoln.totalLD - _currSoln.lkg - _currSoln.totalLD) / (_prevSoln.lkg + _prevSoln.totalLD)) <= 0.004)
				{
					if(count < CONVERGENCE_COUNT)
						count ++;
					else
					{
						break;
					}
				}
				else
					count = 0;

			}

			if(_numIters > MAX_ITR)
					break;
		}

	}

	WriteBestSoln();

	STA();

	TrackSoln = GetStateParams();

}

/**
 * For a given set of LMs, performs gate version update for each gate to minimize the total cost- This is a
 * standard LRS solving loop
 */
void Sizer::LRSSolver (void)
{

	for(int64 i = 0; i < (int64) _sort.size(); i++)
	{
		Instance* gate = _sort[i]->port.cellPtr;

		if (gate->cellLibIndex_l == PI || gate->cellLibIndex_l == CLK_SRC || gate->info_late->isSequential)
			continue;

		GateVersionSelect(gate);

	}
}

/**
 * \param gate Input gate
 * \return Local slack value
 *
 * Instead of computing TNS, it computes local negative slack around the given gate 'gate'
 */
double64 Sizer::ComputeLocalSlack (Instance* gate)  // Local Negative slack
{
	double64 slack = 0;
	for (int64 i = 0; i < (int64)gate->iipList.size(); i++)
	{
		SpefNet* net = gate->iipList[i].wire;
		if(net->port.cellPtr->cellLibIndex_l != PI && net->port.cellPtr->cellLibIndex_l != CLK_SRC &&
			net->port.cellPtr->info_late->isSequential != true)
		{
			for (int64 j = 0; j < (int64)net->port.cellPtr->iipList.size(); j++)
			{
				InstanceIpin* tapPin = &(net->port.cellPtr->iipList[j]);
				if (tapPin->tData.slackRiseLate < 0)
					slack += tapPin->tData.slackRiseLate;
				if (tapPin->tData.slackFallLate < 0)
					slack += tapPin->tData.slackFallLate;
			}
		}
	}

	SpefNet* net = gate->iopList[0].wire;
	for (int64 j = 0; j < (int64)net->taps.size(); j++)
	{
		InstanceIpin* tapPin = net->taps[j].pinPtr;
		if (tapPin->tData.slackRiseLate < 0)
			slack += tapPin->tData.slackRiseLate;
		if (tapPin->tData.slackFallLate < 0)
			slack += tapPin->tData.slackFallLate;
	}

	return slack;
}

/**
 * \param gate Input gate
 * \param drivingNets Set of driving nets of 'gate'
 * \param drainNets Set of drain nets of 'gate'
 * \return Local slack value
 *
 * Instead of computing TNS, it computes local negative slack around the given gate 'gate'
 */
double64 Sizer::ComputeLocalSlack (Instance* gate, vector < SpefNet* >& drivingNets, vector < SpefNet* >& drainNets)
{

	double64 slack = 0;
	// Driving Nets
	for (int64 i = 0; i < (int64)drivingNets.size(); i++)
	{
		SpefNet* net = drivingNets[i];
		for (int64 j = 0; j < (int64)net->taps.size(); j++)
		{
			InstanceIpin* tapPin = net->taps[j].pinPtr;
			if (tapPin->tData.slackRiseLate < 0)
				slack += tapPin->tData.slackRiseLate;
			if (tapPin->tData.slackFallLate < 0)
				slack += tapPin->tData.slackFallLate;
		}
		
		if(net->port.cellPtr->cellLibIndex_l != PI && net->port.cellPtr->cellLibIndex_l != CLK_SRC &&
			net->port.cellPtr->info_late->isSequential != true)
		{
			for (int64 j = 0; j < (int64)net->port.cellPtr->iipList.size(); j++)
			{
				InstanceIpin* tapPin = &(net->port.cellPtr->iipList[j]);
				if (tapPin->tData.slackRiseLate < 0)
					slack += tapPin->tData.slackRiseLate;
				if (tapPin->tData.slackFallLate < 0)
					slack += tapPin->tData.slackFallLate;
			}
		}
	}

	// Self Nets
	SpefNet* net = gate->iopList[0].wire;
	for (int64 j = 0; j < (int64)net->taps.size(); j++)
	{
		InstanceIpin* tapPin = net->taps[j].pinPtr;
		if (tapPin->tData.slackRiseLate < 0)
			slack += tapPin->tData.slackRiseLate;
		if (tapPin->tData.slackFallLate < 0)
			slack += tapPin->tData.slackFallLate;
	}

	// Drain Nets
	for (int64 i = 0; i < (int64)drainNets.size(); i++)
	{
		SpefNet* net = drainNets[i];
		for (int64 j = 0; j < (int64)net->taps.size(); j++)
		{
			InstanceIpin* tapPin = net->taps[j].pinPtr;
			if (tapPin->tData.slackRiseLate < 0)
				slack += tapPin->tData.slackRiseLate;
			if (tapPin->tData.slackFallLate < 0)
				slack += tapPin->tData.slackFallLate;
		}
	}

	return slack;
}

/**
 * \param gate Input gate
 * \param drivingNets Set of driving nets of 'gate'
 * \param drainNets Set of drain nets of 'gate'
 * \param cutoff Threshold slack value for comparison
 * \return Flag for whether local-slack checking passed or failed
 *
 * Instead of computing TNS, it checks if local slack violates the 'cutoff' limit and returns the result accordingly
 */
bool Sizer::CheckLocalSlack (Instance* gate, vector < SpefNet* >& drivingNets,
        vector < SpefNet* >& drainNets, double64 cutoff)
{

	// Driving Nets
	for (int64 i = 0; i < (int64)drivingNets.size(); i++)
	{
		SpefNet* net = drivingNets[i];
		for (int64 j = 0; j < (int64)net->taps.size(); j++)
		{
			InstanceIpin* tapPin = net->taps[j].pinPtr;
			if (tapPin->tData.slackRiseLate < cutoff)
				return false;
			if (tapPin->tData.slackFallLate < cutoff)
				return false;
		}
	}

	// Self Nets
	SpefNet* net = gate->iopList[0].wire;
	for (int64 j = 0; j < (int64)net->taps.size(); j++)
	{
		InstanceIpin* tapPin = net->taps[j].pinPtr;
		if (tapPin->tData.slackRiseLate < cutoff)
			return false;
		if (tapPin->tData.slackFallLate < cutoff)
			return false;
	}

	// Drain Nets
	for (int64 i = 0; i < (int64)drainNets.size(); i++)
	{
		SpefNet* net = drainNets[i];
		for (int64 j = 0; j < (int64) net->taps.size(); j++)
		{
			InstanceIpin* tapPin = net->taps[j].pinPtr;
			if (tapPin->tData.slackRiseLate < cutoff)
				return false;
			if (tapPin->tData.slackFallLate < cutoff)
				return false;
		}
	}

	return true;
}

/**
 * \param gate Gate version
 * \param drivingNets Drivings nets of the gate
 * \param sideTaps Set of side-taps of the gate
 * \param sinkTaps Set of sink-taps of the gate
 *
 * Computes lambda-delay product for neighbouring gate timing-arcs- Only for self arcs, driving arcs, sink arcs
 */
double64 Sizer::LDCost_1 (Instance* gate, vector < SpefNet* >& drivingNets,
		                            vector < std::pair<SpefTap*, int> >& sideTaps, vector < std::pair<SpefTap*, int> >& sinkTaps)
{
	double64 cost = 0;

	// Self Arcs
	InstanceOpin* oPin = &(gate->iopList[0]);
	for (int i = 0; i < (int)oPin->dData.size(); i++)
	{
		cost += oPin->dData[i].delayRiseLate * oPin->dData[i].lambdaRL;
		cost += oPin->dData[i].delayFallLate * oPin->dData[i].lambdaFL;
	}

	// Driving Arcs
	for (int j = 0; j < (int)drivingNets.size(); j++)
	{
		oPin = drivingNets[j]->port.pinPtr;
		if (oPin->cellPtr->cellLibIndex_l == PI || oPin->cellPtr->cellLibIndex_l == CLK_SRC || oPin->cellPtr->info_late->isSequential) continue;
		for (int i = 0; i < (int)oPin->dData.size(); i++)
		{
			cost += oPin->dData[i].delayRiseLate * oPin->dData[i].lambdaRL;
			cost += oPin->dData[i].delayFallLate * oPin->dData[i].lambdaFL;
		}
	}

	// Sink Arcs
	for (int i = 0; i < (int)sinkTaps.size(); i++)
	{
		int idx = sinkTaps[i].second;
		if (idx != -1)
		{
			oPin = &(sinkTaps[i].first->cellPtr->iopList[0]);
			cost += oPin->dData[idx].delayRiseLate * oPin->dData[idx].lambdaRL;
			cost += oPin->dData[idx].delayFallLate * oPin->dData[idx].lambdaFL;
		}
	}

	return cost;

}

/**
 * \param gate Gate version
 * \param drivingNets Drivings nets of the gate
 * \param sideTaps Set of side-taps of the gate
 * \param sinkTaps Set of sink-taps of the gate
 *
 * Computes lambda-delay product for neighbouring gate timing-arcs- Only for side arcs and drain arcs
 */
double64 Sizer::LDCost_2 (Instance* gate, vector < SpefNet* >& drivingNets,
		                            vector < std::pair<SpefTap*, int> >& sideTaps, vector < std::pair<SpefTap*, int> >& sinkTaps)
{
	double64 cost = 0;
	InstanceOpin* oPin;

	// Side Arcs
	for (int i = 0; i < (int)sideTaps.size(); i++)
	{
		int idx = sideTaps[i].second;
		if (idx != -1)
		{
			oPin = &(sideTaps[i].first->cellPtr->iopList[0]);
			cost += oPin->dData[idx].delayRiseLate * oPin->dData[idx].lambdaRL;
			cost += oPin->dData[idx].delayFallLate * oPin->dData[idx].lambdaFL;
		}
	}

	// Drain Arcs
	for (int i = 0; i < (int)sinkTaps.size(); i++)
	{
		int idx = sinkTaps[i].second;

		if (idx != -1)
		{
			SpefNet* drainNet = sinkTaps[i].first->cellPtr->iopList[0].wire;

			for (int64 j = 0; j < (int64)drainNet->taps.size(); j++)
			{
				Instance* nextCell = drainNet->taps[j].cellPtr;
				if (nextCell->cellLibIndex_l == PO || nextCell->info_late->isSequential)
					continue;
				else
				{
					oPin = &(nextCell->iopList[0]);
					for (int k = 0; k < (int)oPin->dData.size(); k++)
					{
						if (nextCell->iipList[oPin->dData[k].index].hash == drainNet->taps[j].pinPtr->hash)
						{
							cost += oPin->dData[k].delayRiseLate * oPin->dData[k].lambdaRL;
							cost += oPin->dData[k].delayFallLate * oPin->dData[k].lambdaFL;
							break;
						}
					}
				}
			}
		}
	}

	return cost;
}

/**
 * \param currNet Input net
 *
 * Propagates AT from port of a net to tap of that net
 */
void Sizer::SetTapAT (SpefNet* currNet)
{
	for(int k=0;k < (int)currNet->taps.size();k++) // k - tap control
	{
		currNet->taps[k].pinPtr->tData.atFallEarly   = currNet->port.pinPtr->tData.atFallEarly != 987654 ? currNet->port.pinPtr->tData.atFallEarly + currNet->taps[k].delayEarly:987654;
		currNet->taps[k].pinPtr->tData.atFallLate    = currNet->port.pinPtr->tData.atFallLate  != -987654 ? currNet->port.pinPtr->tData.atFallLate + currNet->taps[k].delayLate:-987654;
		currNet->taps[k].pinPtr->tData.atRiseEarly   = currNet->port.pinPtr->tData.atRiseEarly != 987654 ? currNet->port.pinPtr->tData.atRiseEarly + currNet->taps[k].delayEarly:987654;
		currNet->taps[k].pinPtr->tData.atRiseLate    = currNet->port.pinPtr->tData.atRiseLate  != -987654 ? currNet->port.pinPtr->tData.atRiseLate + currNet->taps[k].delayLate:-987654;

		currNet->taps[k].pinPtr->tData.slewFallEarly = currNet->port.pinPtr->tData.slewFallEarly != 987654 ? _forwardPropagate_ptr->ComputeSlew(currNet->port.pinPtr->tData.slewFallEarly,currNet->taps[k].m1_E,currNet->taps[k].m2_E): 987654;
		currNet->taps[k].pinPtr->tData.slewFallLate  = currNet->port.pinPtr->tData.slewFallLate  != -987654 ? _forwardPropagate_ptr->ComputeSlew(currNet->port.pinPtr->tData.slewFallLate,currNet->taps[k].m1_L,currNet->taps[k].m2_L): -987654;
		currNet->taps[k].pinPtr->tData.slewRiseEarly = currNet->port.pinPtr->tData.slewRiseEarly != 987654 ? _forwardPropagate_ptr->ComputeSlew(currNet->port.pinPtr->tData.slewRiseEarly,currNet->taps[k].m1_E,currNet->taps[k].m2_E): 987654;
		currNet->taps[k].pinPtr->tData.slewRiseLate  = currNet->port.pinPtr->tData.slewRiseLate  != -987654 ? _forwardPropagate_ptr->ComputeSlew(currNet->port.pinPtr->tData.slewRiseLate,currNet->taps[k].m1_L,currNet->taps[k].m2_L): -987654;
	}
}

/**
 * \param gate Input gate
 * \param drivingNets Driving nets of 'gate'
 * \param sideTaps Side taps of 'gate'
 * \param sinkTaps Sink taps of 'gate'
 * \param drainNets Drain nets of 'gate'
 *
 * Updates local timing 2 topological levels back and forth from the given gate instead of running full static timing run (STA)
 */
void Sizer::UpdateLocalTiming (Instance* gate, vector < SpefNet* >& drivingNets,
		                     vector < std::pair<SpefTap*, int> >& sideTaps, vector < std::pair<SpefTap*, int> >& sinkTaps, vector < SpefNet* >& drainNets)
{
	SpefNet* sinkNet  = gate->iopList[0].wire;
	InstanceOpin* oPintmp;

	// AT Update Locally
	// AT update of driving arcs

	for (int64 i = 0; i < (int64)drivingNets.size(); i++)
	{
		if (ECM == true)
		{
			oPintmp = drivingNets[i]->port.pinPtr;
			oPintmp->ceffLate = drivingNets[i]->c1 + drivingNets[i]->c2;
			oPintmp->port->ceffLate = oPintmp->ceffLate;
			if (oPintmp->cellPtr->cellLibIndex_l != PI && oPintmp->cellPtr->cellLibIndex_l != CLK_SRC)
			{
				for (int x = 0; x < NUM_CEFF_ITR; x++)
				{
					_forwardPropagate_ptr->SetPortAT(drivingNets[i]);
					_forwardPropagate_ptr->FindcPi(oPintmp);
					oPintmp->ceffLate        = oPintmp->cPi;
					oPintmp->ceffEarly       = oPintmp->cPi;
					oPintmp->port->ceffLate  = oPintmp->cPi;
					oPintmp->port->ceffEarly = oPintmp->cPi;
				}
			}
		}
		if (drivingNets[i]->port.pinPtr->cellPtr->cellLibIndex_l     != PI &&
				drivingNets[i]->port.pinPtr->cellPtr->cellLibIndex_l != CLK_SRC)
			_forwardPropagate_ptr->SetPortAT(drivingNets[i]);
		else
			_forwardPropagate_ptr->SetPI_AT(drivingNets[i]);

		SetTapAT(drivingNets[i]);
	}

	// AT update of self arcs
	if (ECM == true)
	{
		oPintmp = sinkNet->port.pinPtr;
		oPintmp->ceffLate = sinkNet->c1 + sinkNet->c2;
		oPintmp->port->ceffLate = oPintmp->ceffLate;
		for (int x = 0; x < NUM_CEFF_ITR; x++)
		{
			_forwardPropagate_ptr->SetPortAT(sinkNet);
			_forwardPropagate_ptr->FindcPi(oPintmp);
			oPintmp->ceffLate         = oPintmp->cPi;
			oPintmp->ceffEarly        = oPintmp->cPi;
			oPintmp->port->ceffLate   = oPintmp->cPi;
			oPintmp->port->ceffEarly  = oPintmp->cPi;
		}
	}

	_forwardPropagate_ptr->SetPortAT(sinkNet);

	SetTapAT(sinkNet);


	// AT update of side arcs
	for (int64 i = 0; i < (int64)sideTaps.size(); i++)
	{
		if(sideTaps[i].second != -1)
		{
			if (ECM == true)
			{
				oPintmp                 = sideTaps[i].first->cellPtr->iopList[0].wire->port.pinPtr;
				oPintmp->ceffLate       = oPintmp->wire->c1 + oPintmp->wire->c2;
				oPintmp->port->ceffLate = oPintmp->ceffLate;

				for (int x = 0; x < NUM_CEFF_ITR; x++)
				{
					_forwardPropagate_ptr->SetPortAT(sideTaps[i].first->cellPtr->iopList[0].wire);
					_forwardPropagate_ptr->FindcPi(oPintmp);
					oPintmp->ceffLate        = oPintmp->cPi;
					oPintmp->ceffEarly       = oPintmp->cPi;
					oPintmp->port->ceffLate  = oPintmp->cPi;
					oPintmp->port->ceffEarly = oPintmp->cPi;
				}
			}
			_forwardPropagate_ptr->SetPortAT(sideTaps[i].first->cellPtr->iopList[0].wire);
		}
	}

	 // AT update of drain nets
	for (int64 i = 0; i < (int64)drainNets.size(); i++)
	{
		SpefNet* drainNet = drainNets[i];
		if (ECM == true)
		{
			oPintmp                  = drainNet->port.pinPtr;
			oPintmp->ceffLate        = drainNet->c1 + drainNet->c2;
			oPintmp->port->ceffLate  = oPintmp->ceffLate;
			for (int x = 0; x < NUM_CEFF_ITR; x++)
			{
				_forwardPropagate_ptr->SetPortAT(drainNet);
				_forwardPropagate_ptr->FindcPi(oPintmp);
				oPintmp->ceffLate        = oPintmp->cPi;
				oPintmp->ceffEarly       = oPintmp->cPi;
				oPintmp->port->ceffLate  = oPintmp->cPi;
				oPintmp->port->ceffEarly = oPintmp->cPi;
			}
		}
		_forwardPropagate_ptr->SetPortAT(drainNet);

		// Drain Nets AT set
		SetTapAT(drainNet);
		for (int64 j = 0; j < (int64)drainNet->taps.size(); j++)
		{
			Instance* nextCell = drainNet->taps[j].cellPtr;
			if (nextCell->cellLibIndex_l == PO || nextCell->info_late->isSequential)
				continue;
			else
			{
				if (ECM == true)
				{
					oPintmp                 = nextCell->iopList[0].wire->port.pinPtr;
					oPintmp->ceffLate       = oPintmp->wire->c1 + oPintmp->wire->c2;
					oPintmp->port->ceffLate = oPintmp->ceffLate;
					for (int x = 0; x < NUM_CEFF_ITR; x++)
					{
						_forwardPropagate_ptr->SetPortAT(nextCell->iopList[0].wire);
						_forwardPropagate_ptr->FindcPi(oPintmp);
						oPintmp->ceffLate        = oPintmp->cPi;
						oPintmp->ceffEarly       = oPintmp->cPi;
						oPintmp->port->ceffLate  = oPintmp->cPi;
						oPintmp->port->ceffEarly = oPintmp->cPi;
					}
				}
				_forwardPropagate_ptr->SetPortAT(nextCell->iopList[0].wire);
			}
		}
	}

	CRITICALITY ratCrit;

	// 1. Drain Nets (RTO)
	// 2. SinkTaps & SinkNet
	// 3. SideTaps
	// 4. SelfTaps
	// 5. Driving Nets (RTO)

    // Setting RAT taps of drainNets (in RTO)
	NODE_TIMING_DATA inTime;
	for(int ztr = drainNets.size()-1; ztr >= 0 ; ztr--)
	{
		SpefNet * drainNet = drainNets[ztr];

		// clearing RAT of drainNet's port
		_backwardPropagate_ptr->ClearRAT(&drainNet->port.pinPtr->tData,LATE);

		for (int z = 0; z < (int64)drainNet->taps.size(); z++)
		{
			Instance * drainCell = drainNet->taps[z].cellPtr;
			if(drainCell->cellLibIndex_l == PO || drainCell->info_late->isSequential == true)
			{
				// Don't change RAT of these
				// WARNING: Need to Recalculate RAT of D-pin of sequential elements for any changes in clock-tree & if setup-hold
				//          times are NON-ZERO. They change with change in input transition- so mind that.
			}
			else
			{
				InstanceOpin * drainNextOPin = &drainCell->iopList[0];
				for (int y = 0; y < (int)drainNextOPin->dData.size(); y++)
					if(drainNet->taps[z].pinPtr->hash == drainCell->iipList[drainNextOPin->dData[y].index].hash)
					{
						_backwardPropagate_ptr->addIpinRatParam(&drainNet->taps[z].pinPtr->tData, &(drainNextOPin->tData),
								&drainNextOPin->dData[y],ratCrit, LATE);
						drainNet->taps[z].pinPtr->tData.slackRiseLate = drainNet->taps[z].pinPtr->tData.ratRiseLate - drainNet->taps[z].pinPtr->tData.atRiseLate;
						drainNet->taps[z].pinPtr->tData.slackFallLate = drainNet->taps[z].pinPtr->tData.ratFallLate - drainNet->taps[z].pinPtr->tData.atFallLate;

						break;
					}
			}

			_backwardPropagate_ptr->addWireDelayParam    (&inTime, &drainNet->taps[z].pinPtr->tData , &drainNet->taps[z]);
			_backwardPropagate_ptr->UpdateWireDelayParam (&drainNet->port.pinPtr->tData , &inTime, &drainNet->port.pinPtr->tData, ratCrit, ratCrit, LATE );

		}

		drainNet->port.pinPtr->tData.slackRiseLate = drainNet->port.pinPtr->tData.ratRiseLate - drainNet->port.pinPtr->tData.atRiseLate;
		drainNet->port.pinPtr->tData.slackFallLate = drainNet->port.pinPtr->tData.ratFallLate - drainNet->port.pinPtr->tData.atFallLate;
	}



	// Reset RAT of port of the sink net
	InstanceOpin* oPin = sinkNet->port.pinPtr;
	_backwardPropagate_ptr->ClearRAT(&oPin->tData,LATE);

	// Calculating RAT of sink taps
	for (int i = 0; i < (int)sinkTaps.size(); i++)
	{

		if(sinkTaps[i].second != -1)
		// Don't change RAT of these
		// WARNING: Need to Recalculate RAT of D-pin of sequential elements for any changes in clock-tree & if setup-hold
		//          times are NON-ZERO. They change with change in input transition- so mind that.
		{
			_backwardPropagate_ptr->addIpinRatParam(&sinkTaps[i].first->pinPtr->tData, &(sinkTaps[i].first->pinPtr->cellPtr->iopList[0].tData),
			                                &sinkTaps[i].first->pinPtr->cellPtr->iopList[0].dData[sinkTaps[i].second],ratCrit, LATE);
		}
		sinkTaps[i].first->pinPtr->tData.slackRiseLate = sinkTaps[i].first->pinPtr->tData.ratRiseLate - sinkTaps[i].first->pinPtr->tData.atRiseLate;
		sinkTaps[i].first->pinPtr->tData.slackFallLate = sinkTaps[i].first->pinPtr->tData.ratFallLate - sinkTaps[i].first->pinPtr->tData.atFallLate;

	// Calculating RAT of port of the sink net

		_backwardPropagate_ptr->addWireDelayParam    (&inTime, &sinkTaps[i].first->pinPtr->tData , sinkTaps[i].first);
		_backwardPropagate_ptr->UpdateWireDelayParam (&oPin->tData , &inTime, &oPin->tData, ratCrit, ratCrit, LATE );

		oPin->tData.slackRiseLate = oPin->tData.ratRiseLate - oPin->tData.atRiseLate;
		oPin->tData.slackFallLate = oPin->tData.ratFallLate - oPin->tData.atFallLate;

	}

	// Calculating RAT of side taps
	for (int i = 0; i < (int)sideTaps.size(); i++)
	{
		if(sideTaps[i].second != -1) // Not changing RAT of PO/Sequential input pin. They stay unchanged.
		// WARNING: Need to Recalculate RAT of D-pin of sequential elements for any changes in clock-tree & if setup-hold
		//          times are NON-ZERO. They change with change in input transition- so mind that.
		{
			_backwardPropagate_ptr->addIpinRatParam (&sideTaps[i].first->pinPtr->tData, &(sideTaps[i].first->pinPtr->cellPtr->iopList[0].tData),
				                                &sideTaps[i].first->pinPtr->cellPtr->iopList[0].dData[sideTaps[i].second],ratCrit, LATE);
		}
		sideTaps[i].first->pinPtr->tData.slackRiseLate = sideTaps[i].first->pinPtr->tData.ratRiseLate - sideTaps[i].first->pinPtr->tData.atRiseLate;
		sideTaps[i].first->pinPtr->tData.slackFallLate = sideTaps[i].first->pinPtr->tData.ratFallLate - sideTaps[i].first->pinPtr->tData.atFallLate;

	}

	// Calculating RAT at iip's of the gate under version-change test
	for (int i = 0; i < (int)oPin->dData.size(); i++)
	{
		InstanceIpin * iPin = &oPin->cellPtr->iipList[oPin->dData[i].index];
		_backwardPropagate_ptr->addIpinRatParam (&iPin->tData, &(oPin->tData), &oPin->dData[i],ratCrit, LATE);

		iPin->tData.slackRiseLate = iPin->tData.ratRiseLate - iPin->tData.atRiseLate;
		iPin->tData.slackFallLate = iPin->tData.ratFallLate - iPin->tData.atFallLate;

	}

	// For each driving net of current gate, setting RAT at it's respective ports
	// And for each such port, propagating RAT backwards to iip's of gate connected to it.
	// NOTE:Driving nets being chosen in reverse topological order (RTO). This is important
	for (int64 i = drivingNets.size() - 1 ; i >= 0; i--)
	{
		oPin = drivingNets[i]->port.pinPtr;

		// Now update RAT of the port of this net by seeing all of its taps' RAT one by one
		// Before that, clearing RAT of such port.
		_backwardPropagate_ptr->ClearRAT(&oPin->tData,LATE);

		for (int x = 0; x < (int)drivingNets[i]->taps.size() ; x++)
		{
			_backwardPropagate_ptr->addWireDelayParam    (&inTime, &drivingNets[i]->taps[x].pinPtr->tData , &drivingNets[i]->taps[x]);
			_backwardPropagate_ptr->UpdateWireDelayParam (&oPin->tData, &inTime, &oPin->tData, ratCrit, ratCrit, LATE );
		}

		oPin->tData.slackRiseLate = oPin->tData.ratRiseLate - oPin->tData.atRiseLate;
		oPin->tData.slackFallLate = oPin->tData.ratFallLate - oPin->tData.atFallLate;

		// Calculating RAT at iip's of the gate driving the oPin here
		for (int i2 = 0; i2 < (int)oPin->dData.size(); i2++)
		{
			if(oPin->cellPtr->cellLibIndex_l == PI || oPin->cellPtr->cellLibIndex_l == CLK_SRC )  // for PI and CLK source
				break;
			else if(oPin->cellPtr->info_late->isSequential)  // for sequential outputs
			{
				_backwardPropagate_ptr->ComputeRATckPin(oPin->cellPtr);

				oPin->cellPtr->ckList[0].tData.slackRiseLate = oPin->cellPtr->ckList[0].tData.ratRiseLate - oPin->cellPtr->ckList[0].tData.atRiseLate;
				oPin->cellPtr->ckList[0].tData.slackFallLate = oPin->cellPtr->ckList[0].tData.ratFallLate - oPin->cellPtr->ckList[0].tData.atFallLate;

			}
			else   // for combinational outputs
			{
				InstanceIpin * iPin = &oPin->cellPtr->iipList[oPin->dData[i2].index];
				_backwardPropagate_ptr->addIpinRatParam(&iPin->tData, &(oPin->tData), &oPin->dData[i2],ratCrit, LATE);

				iPin->tData.slackRiseLate = iPin->tData.ratRiseLate - iPin->tData.atRiseLate;
				iPin->tData.slackFallLate = iPin->tData.ratFallLate - iPin->tData.atFallLate;


			}
		}

	}

	return;
}

/**
 * \param worstSlack Worst slack in the current design
 *
 * Computes the uphill climbing factor for local TNS to get worse up to certain extent
 * in doing local gate selection
 */
double64 Sizer::FindGamma (double64 worstSlack)
{
	return (1 - (min(0.0, worstSlack) / timing.clock.period));
}

/**
 * \param gate Input gate
 *
 * Selects appropriate gate version for 'gate' so that it minimizes the total cost among costs
 * of its other cell-type variants
 */
void Sizer::GateVersionSelect (Instance* gate)
{
	LibParserFPInfo* fp = gate->info_late->fp;

	// TODO: Precompute the below vectors apriori before LDP Solver to save time
	vector < SpefNet* >                 drivingNets;
	vector < std::pair<SpefTap*, int> > sideTaps;
	vector < std::pair<SpefTap*, int> > sinkTaps;

	for (int64 i = 0; i < (int64)gate->iipList.size(); i++)
	{
		SpefNet* currNet = gate->iipList[i].wire;
		drivingNets.push_back(currNet);
		for (int j = 0; j < (int)currNet->taps.size(); j++)
		{
			if (currNet->taps[j].cellPtr->hash != gate->hash)
			{
				if (currNet->taps[j].cellPtr->cellLibIndex_l == PO || currNet->taps[j].cellPtr->info_late->isSequential)
				{
					std::pair <SpefTap*, int> temp;
					temp.first  = &(currNet->taps[j]);
					temp.second = -1;
					sideTaps.push_back(temp);
				}
				else
				{
					InstanceOpin* oPin = &(currNet->taps[j].cellPtr->iopList[0]);
					int idx = 0;
					for (int k = 0; k < (int)oPin->dData.size(); k++)
					{
						if (currNet->taps[j].pinPtr->hash == currNet->taps[j].cellPtr->iipList[oPin->dData[k].index].hash)
						{
							idx = k;
							break;
						}
					}
					std::pair <SpefTap*, int> temp;
					temp.first   = &(currNet->taps[j]);
					temp.second  = idx;
					sideTaps.push_back(temp);
				}
			}
		}
	}

	SpefNet* currNet = gate->iopList[0].wire;
	for (int j = 0; j < (int)currNet->taps.size(); j++)
	{

		if (currNet->taps[j].cellPtr->cellLibIndex_l == PO || currNet->taps[j].cellPtr->info_late->isSequential)
			{
				std::pair <SpefTap*, int> temp;
				temp.first  = &(currNet->taps[j]);
				temp.second = -1;
				sinkTaps.push_back(temp);
			}
			else
			{
				InstanceOpin* oPin = &(currNet->taps[j].cellPtr->iopList[0]);
				int idx = 0;
				for (int k = 0; k < (int)oPin->dData.size(); k++)
				{
					if (currNet->taps[j].pinPtr->hash == currNet->taps[j].cellPtr->iipList[oPin->dData[k].index].hash)
					{
						idx = k;
						break;
					}
				}
				std::pair <SpefTap*, int> temp;
				temp.first  = &(currNet->taps[j]);
				temp.second = idx;
				sinkTaps.push_back(temp);
			}

	}
	sort (drivingNets.begin(), drivingNets.end(), CompareNetLevel2());

	vector < SpefNet* > drainNets;
	for (int64 i = 0; i < (int64)sinkTaps.size(); i++)
	{
		if(sinkTaps[i].second != -1)
		{
			SpefNet* dNet = sinkTaps[i].first->cellPtr->iopList[0].wire;
			drainNets.push_back(dNet);
		}
	}
	sort (drainNets.begin(), drainNets.end(), CompareNetLevel2());


	int bestCandidate = gate->info_late->idxLk;
	assert(fp->version_lk[gate->info_late->idxLk].vPtr->name == gate->info_late->name);
	int initCandidate = bestCandidate;

	double64 	originalSlack = ComputeLocalSlack(gate, drivingNets, drainNets);
	double64 initLD_2  = LDCost_2(gate, drivingNets, sideTaps, sinkTaps);
	double64 initCost  = LDCost_1(gate, drivingNets, sideTaps, sinkTaps);
	double64 initLkg   = gate->info_late->leakage;
	double64 bestCost  = LDCost_1(gate, drivingNets, sideTaps, sinkTaps) + initLkg + 0
			/* LDCost_2 - LDCost_2*/;

	double64 bestChangeCost = initCost;
	double64 bestChangeLkg  = initLkg;

	int c1 = 0;
	for (c1 = 0; c1 < (int)fp->version_lk.size(); c1++)
	{
		if(fp->version_lk[c1].vPtr->leakage > bestCost) break;

		if (ECM == false)
		{
			SetVersion1(gate, &(fp->version_lk[c1]));
			if (CheckLoadViolations(fp->version_lk[c1].vPtr, gate, 1.0) == true)
			{
				continue;
			}
			SetVersion2(gate, &(fp->version_lk[c1]));
			UpdateLocalTiming(gate, drivingNets, sideTaps, sinkTaps, drainNets);
		}
		else
		{
			SetVersion(gate, &(fp->version_lk[c1]));
			UpdateLocalTiming(gate, drivingNets, sideTaps, sinkTaps, drainNets);

			if (CheckLoadViolations(fp->version_lk[c1].vPtr, gate, LDP_ALPHA) == true)
			{
				continue;
			}
		}

		double64 tempSlack = ComputeLocalSlack(gate, drivingNets, drainNets);

		if (tempSlack < (FindGamma(_currSoln.worstSlack) * originalSlack))
		{
			continue;
		}

		double64 tempLD_2 = LDCost_2(gate, drivingNets, sideTaps, sinkTaps);
		double64 tempCost = LDCost_1(gate, drivingNets, sideTaps, sinkTaps) + tempLD_2 - initLD_2;
		double64 tempLkg  = gate->info_late->leakage;
		double64 cost     = tempCost + tempLkg;

		if (cost < bestCost)
		{
			bestCandidate  = c1;
			bestCost       = cost;
			bestChangeCost = tempCost;
			bestChangeLkg  = tempLkg;
		}
	}

	SetVersion(gate, &(fp->version_lk[bestCandidate]));

	UpdateLocalTiming(gate, drivingNets, sideTaps, sinkTaps, drainNets);

	_currSoln.totalLD  += bestChangeCost - initCost;
	_currSoln.lkg      += bestChangeLkg - initLkg;

	if (bestCandidate > initCandidate)
		_cellModsUp++;
	else if (bestCandidate < initCandidate)
		_cellModsDown++;
}

/**
 * A complete run of static timing- STA
 */
void Sizer:: STA (void)
{
	// TODO: can do TopSort only once, then store the Sort vector somewhere. Note that Sort vector gets changed in IncrementalSTA,
	//       so keep it saved in some other variable and use it instead of calling TopSort again the next time you run STA.

	_isStaticDone = false;
	Sort.clear();

	//WARNING: If you want to remove TopSort from here, make sure to store Sort vector somewhere since it needs to be used in the code
	//         multiple times and TimingRecovery alters it, and PowerReduction needs original Sort (all nets). So, better back it up.

	ClearRATSetFlag();  // It is important to clear these flags if you want to use 'ComputeRATNetBased' Again in STA() next time.

	_forwardPropagate_ptr->  TopSort(); /*WARNING: Read above before removing this*/
	_forwardPropagate_ptr->  ATSlew(LATE);
	_backwardPropagate_ptr-> ComputeRATNetBased();
	_backwardPropagate_ptr-> ComputePreCPPRSlack();

	ClearRATSetFlag();  // It is important to clear these flags if you want to use 'ComputeRATNetBased' Again in STA() next time.

	return;
}

/**
 * Clears the isRatSet flags so that the RAT gets computed again during STA- which
 * it is supposed to do
 */
void Sizer:: ClearRATSetFlag (void)
{
	for(int64 i = 0; i < (int64)InstanceHashTable.size(); i++)
	{
		for(int j = 0; j < (int)InstanceList[InstanceHashTable[i].index].iipList.size(); j++)
		{
			InstanceList[InstanceHashTable[i].index].iipList[j].tData.isRatSet = false;
		}
		for(int j = 0; j < (int)InstanceList[InstanceHashTable[i].index].iopList.size(); j++)
		{
			InstanceList[InstanceHashTable[i].index].iopList[j].tData.isRatSet = false;
		}
		for(int j = 0; j < (int)InstanceList[InstanceHashTable[i].index].ckList.size(); j++)
		{
			InstanceList[InstanceHashTable[i].index].ckList[j].tData.isRatSet = false;
		}

	}

	return;
}

/**
 * \param gate Input gate
 *
 * Runs incremental STA for the gate-change at 'gate'
 */
void Sizer:: IncrementalSTA(Instance* gate)
{

	_operations_ptr-> ColorParentNets(gate);

	 ConeEndPoints.clear();

	 _forwardPropagate_ptr->  InitForwardPropagate(LATE);
	_backwardPropagate_ptr-> ComputeRATInc();  // Slack is also updated in this function itself.

	return;
}

/**
 * \param gate Input gate
 *
 * Runs incremental arrival time update for the gate-change at 'gate'. This is useful in checking
 * violations for end-point TNS. Be noted that RAT won't get updated.
 */
void Sizer:: IncrementalAT(Instance* gate)
{

	_operations_ptr->ColorParentNets(gate);

	ConeEndPoints.clear();
	_forwardPropagate_ptr->  InitForwardPropagate(LATE);

	// Computing slack of cone-end points
	for (int64  i = 0; i < (int64)ConeEndPoints.size(); i++)
	{
		_backwardPropagate_ptr->ComputeSlackPin(ConeEndPoints[i],true);
	}
	/* No need of propagating RAT since we are checking only TNS*/


	return;
}

/**
 * Returns TNS, leakage, worst slack and lambda-delay product of the current circuit state
 * by iterating over each gate
 */
SolutionState Sizer:: GetStateParams (void)
{
	SolutionState tmp;
	double64 tns        = 0;
	double64 lkg        = 0;
	double64 ws         = 0;
	double64 ld         = 0;

	for(int64 i = 0; i< (int64)InstanceHashTable.size(); i++)
	{
		if(InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PO)
		{
			for(int j = 0; j < (int)InstanceList[InstanceHashTable[i].index].iipList.size(); j++)
			{
				tns += std::min <double64> (0.0,InstanceList[InstanceHashTable[i].index].iipList[j].tData.slackRiseLate);
				tns += std::min <double64> (0.0,InstanceList[InstanceHashTable[i].index].iipList[j].tData.slackFallLate);
			}
		}
		else if(InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PI)
		{
			// Do Nothing
		}
		else if(InstanceList[InstanceHashTable[i].index].cellLibIndex_l == CLK_SRC)
		{
			// Do Nothing
		}
		else if(InstanceList[InstanceHashTable[i].index].info_late->isSequential == true)
        {
			for(int j = 0; j < (int)InstanceList[InstanceHashTable[i].index].iipList.size(); j++)
			{
				tns += std::min <double64> (0.0,InstanceList[InstanceHashTable[i].index].iipList[j].tData.slackRiseLate);
				tns += std::min <double64> (0.0,InstanceList[InstanceHashTable[i].index].iipList[j].tData.slackFallLate);
			}

			lkg += InstanceList[InstanceHashTable[i].index].info_late->leakage;
        }
        else
        {
			for(int j = 0; j < (int)InstanceList[InstanceHashTable[i].index].iopList.size(); j++)
			{

				ws = std::min <double64> (ws,InstanceList[InstanceHashTable[i].index].iopList[j].tData.slackRiseLate);
				ws = std::min <double64> (ws,InstanceList[InstanceHashTable[i].index].iopList[j].tData.slackFallLate);

				InstanceOpin* oPin = &(InstanceList[InstanceHashTable[i].index].iopList[j]);

				for (int k = 0; k < (int)oPin->dData.size(); k++)
				{
					ld += oPin->dData[k].delayRiseLate * oPin->dData[k].lambdaRL;
					ld += oPin->dData[k].delayFallLate * oPin->dData[k].lambdaFL;
				}
			}

			lkg += InstanceList[InstanceHashTable[i].index].info_late->leakage;
        }

	}

	tmp.tns        = tns;
	tmp.lkg        = lkg;
	tmp.worstSlack = ws;
	tmp.totalLD    = ld;

	return tmp;
}

/**
 * Dumps gate sizes to a file
 */
void Sizer:: DumpSoln (void)
{
	_dbgFile << "Current solution state ... " << endl;

	for(int i = 0 ; i < (int)InstanceHashTable.size() ; i++)
	{
		if(InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PI ||
				InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PO ||
				InstanceList[InstanceHashTable[i].index].cellLibIndex_l == CLK_SRC) continue;

		_dbgFile << InstanceList[InstanceHashTable[i].index].instanceName << "\t" << InstanceList[InstanceHashTable[i].index].info_late->name << endl;
	}
}

/**
 * \param gate Input gate
 * \param prevVersion Previous version of the gate
 * \param changeCnt Number of updates/changes made to the gate versions overall
 *
 * Checks if the gate under version change for power recovery generates any violations. This is
 * done efficiently by local timing-based filtering to obviate excessive Incremental timing
 * computations
 */
void Sizer:: GatePowerReduction (Instance* gate, GateVersionInfo* prevVersion, int64& changeCnt)
{
	if (APPROX_PR == true)
	{
		vector < SpefNet* > drivingNets;
		vector < std::pair<SpefTap*, int> > sideTaps;
		vector < std::pair<SpefTap*, int> > sinkTaps;

		for (int64 i = 0; i < (int64)gate->iipList.size(); i++)
		{
			SpefNet* currNet = gate->iipList[i].wire;
			drivingNets.push_back(currNet);
			for (int j = 0; j < (int)currNet->taps.size(); j++)
			{
				if (currNet->taps[j].cellPtr->hash != gate->hash)
				{
					if (currNet->taps[j].cellPtr->cellLibIndex_l == PO || currNet->taps[j].cellPtr->info_late->isSequential)
					{
						std::pair <SpefTap*, int> temp;
						temp.first    = &(currNet->taps[j]);
						temp.second   = -1;
						sideTaps.push_back(temp);
					}
					else
					{
						InstanceOpin* oPin = &(currNet->taps[j].cellPtr->iopList[0]);
						int idx = 0;
						for (int k = 0; k < (int)oPin->dData.size(); k++)
						{
							if (currNet->taps[j].pinPtr->hash == currNet->taps[j].cellPtr->iipList[oPin->dData[k].index].hash)
							{
								idx = k;
								break;
							}
						}
						std::pair <SpefTap*, int> temp;
						temp.first  = &(currNet->taps[j]);
						temp.second = idx;
						sideTaps.push_back(temp);
					}
				}
			}
		}

		SpefNet* currNet = gate->iopList[0].wire;
		for (int j = 0; j < (int)currNet->taps.size(); j++)
		{

			if (currNet->taps[j].cellPtr->cellLibIndex_l == PO || currNet->taps[j].cellPtr->info_late->isSequential)
				{
					std::pair <SpefTap*, int> temp;
					temp.first  = &(currNet->taps[j]);
					temp.second = -1;
					sinkTaps.push_back(temp);
				}
				else
				{
					InstanceOpin* oPin = &(currNet->taps[j].cellPtr->iopList[0]);
					int idx = 0;
					for (int k = 0; k < (int)oPin->dData.size(); k++)
					{
						if (currNet->taps[j].pinPtr->hash == currNet->taps[j].cellPtr->iipList[oPin->dData[k].index].hash)
						{
							idx = k;
							break;
						}
					}
					std::pair <SpefTap*, int> temp;
					temp.first  = &(currNet->taps[j]);
					temp.second = idx;
					sinkTaps.push_back(temp);
				}

		}
		sort (drivingNets.begin(), drivingNets.end(), CompareNetLevel2());

		vector < SpefNet* > drainNets;
		for (int64 i = 0; i < (int64)sinkTaps.size(); i++)
		{
			if(sinkTaps[i].second != -1)
			{
				SpefNet* dNet = sinkTaps[i].first->cellPtr->iopList[0].wire;
				drainNets.push_back(dNet);
			}
		}
		sort (drainNets.begin(), drainNets.end(), CompareNetLevel2());

		//Updating timing
		UpdateLocalTiming(gate, drivingNets, sideTaps, sinkTaps, drainNets);

		if (ECM == true)
		{
			if(CheckLoadViolations(gate->info_late, gate, 1.0) == true /* false means no violations*/
					|| CheckLocalSlack(gate, drivingNets, drainNets, ZERO_VAL) == false)
			{
				SetVersion(gate, prevVersion);
				UpdateLocalTiming(gate, drivingNets, sideTaps, sinkTaps, drainNets);
			}
			else
			{
				IncrementalSTA(gate);
				if(TrackSoln.tns > ZERO_VAL && CheckLoadViolations(gate->info_late, gate, 1.0) == false)
					changeCnt ++;
				else /*Violations found, hence undoing the changes */
				{
					SetVersion(gate, prevVersion);
					IncrementalSTA(gate);
				}
			}
		}
		else
		{
			if(CheckLocalSlack(gate, drivingNets, drainNets, ZERO_VAL) == false)
			{
				SetVersion(gate, prevVersion);
				UpdateLocalTiming(gate, drivingNets, sideTaps, sinkTaps, drainNets);
			}
			else
			{
				IncrementalSTA(gate);
				if(TrackSoln.tns > ZERO_VAL)
					changeCnt ++;
				else /*Violations found, hence undoing the changes */
				{
					SetVersion(gate, prevVersion);
					IncrementalSTA(gate);
				}
			}
		}
	}

	else
	{
		//Updating timing
		IncrementalSTA(gate);
		if(TrackSoln.tns > ZERO_VAL && (ECM == false || CheckLoadViolations(gate->info_late, gate, 1.0) == false))
			changeCnt ++;
		else /*Violations found, hence undoing the changes */
		{
			SetVersion(gate, prevVersion);
			IncrementalSTA(gate);
		}
	}
}

/**
 * Runs greedy heuristic for power reduction. It tries downsizing or increasing V-th of every gate
 * without generating any timing and load-slew violations
 */
void Sizer:: PowerReduction(void)
{
	cout << "\nEntering PowerReduction Algo... " << endl;

	STA();
	TrackSoln = GetStateParams();

	cout << flush;
	Sort.clear();
	_forwardPropagate_ptr->TopSort();

	int64 changeCnt = -1;

	// Sorting gates in order of leakage- from highest to lowest
	deque <Instance*> gateList;
	for(int64 i = 0; i < (int64)_sort.size(); i++)
	{
		Instance* gate = _sort[i]->port.cellPtr;
		if(gate->cellLibIndex_l == PI || gate->cellLibIndex_l == CLK_SRC || gate->info_late->isSequential == true)
			continue;
		gateList.push_back(gate);
	}



	do
	{
		// Vth TWEAKING
		sort(gateList.begin(), gateList.end(), CompareLkg());

		changeCnt = 0;
		for(int64 i = 0; i < (int64)gateList.size(); i++)
		{
			Instance* gate = gateList[i];
			if(gate->cellLibIndex_l == PI || gate->cellLibIndex_l == CLK_SRC || gate->info_late->isSequential == true)
				continue;

			GateVersionInfo* prevVersion = &gate->info_late->fp->version_lk[gate->info_late->idxLk];
			if(gate->info_late->idxVth[0] > 0)  // Vth of gate is increasable
			{
				// Setting version to a lower vth version
				SetVersion(gate, &gate->info_late->fp->version_vth[gate->info_late->idxVth[0]-1][gate->info_late->idxVth[1]]);
				/* Gate->info_late is now changed to one-Vth-smaller-than-current-Vth version */

				if (ECM == true)
				{
					GatePowerReduction(gate, prevVersion, changeCnt);
				}
				else
				{
					if(CheckLoadViolations(gate->info_late, gate, 1.0) == false /* false means no violations*/)
						GatePowerReduction(gate, prevVersion, changeCnt);
					else /*Violations found, hence undoing the changes */
						SetVersion(gate, prevVersion);
				}
			}
		}

		sort(gateList.begin(), gateList.end(), CompareLkg());

		// GATE DOWNSIZING
		for(int64 i = 0; i < (int64)gateList.size(); i++)
		{
			Instance* gate = gateList[i];
			if(gate->cellLibIndex_l == PI || gate->cellLibIndex_l == CLK_SRC || gate->info_late->isSequential == true)
				continue;

			GateVersionInfo* prevVersion = &gate->info_late->fp->version_lk[gate->info_late->idxLk];
			if(gate->info_late->idxVth[1] > 0)  // Gate is downsizable
			{
				// Downsizing Gate by setting version to it
				SetVersion(gate, &gate->info_late->fp->version_vth[gate->info_late->idxVth[0]][gate->info_late->idxVth[1]-1]);

				/* Gate->info_late is now changed to one-size-smaller-than-current-size version */
				if (ECM == true)
				{
					GatePowerReduction(gate, prevVersion, changeCnt);
				}
				else
				{
					if(CheckLoadViolations(gate->info_late, gate, 1.0) == false /* false means no violations*/)
						GatePowerReduction(gate, prevVersion, changeCnt);
					else /*Violations found, hence undoing the changes */
						SetVersion(gate, prevVersion);
				}
			}
		}



	} while (changeCnt != 0); /* Until no further changes are possible */

	STA();
    TrackSoln = GetStateParams();

	cout << "Exiting PowerReduction Algo... \n" << endl;

} /* PowerReduction() */

/**
 * Prints attributes like AT/RAT/Slack/Delay/Slew at each input and output
 * pins of every net in the circuit- This is mainly for debugging purposes
 */
void Sizer::PrintNetEstimates(void)
{
	_dbgFile << "\nPrinting Net Estimates\n" << endl;
	for (int64 i = 0; i < (int64)_sort.size(); i++)
	{
		if(_sort[i]->isVirtualNet)continue;
		if(_sort[i]->isIncluded == false)
			{
				_dbgFile << "NetName\t" << _sort[i]->netName << " is not included" << endl;
				continue;
			}

		_dbgFile << "NetName\t" << _sort[i]->netName << "\tNo. of taps = " << _sort[i]->taps.size() << endl;
		InstanceOpin* oPin = _sort[i]->port.pinPtr;

		_dbgFile << endl;
		_dbgFile << "Port -------------------------------------> " << oPin->name.icellName << ":" << oPin->name.pinName << "\tCeff\t" << oPin->ceffLate << endl;
		_dbgFile << "Slew ----->" << "\t" << oPin->tData.slewRiseEarly << "\t\t" << oPin->tData.slewFallEarly << "\t\t" << oPin->tData.slewRiseLate << "\t\t" << oPin->tData.slewFallLate << endl;

		if (oPin->cellPtr->cellLibIndex_l == -2 || oPin->cellPtr->cellLibIndex_l == -4)
		{
			_dbgFile << "Port Pin is PI" << endl;
			_dbgFile << "AT  ------->" << "\t" << oPin->tData.atRiseEarly << "\t\t" << oPin->tData.atFallEarly << "\t\t" << oPin->tData.atRiseLate << "\t\t" << oPin->tData.atFallLate << endl;
			_dbgFile << "RAT ------->" << "\t" << oPin->tData.ratRiseEarly << "\t\t" << oPin->tData.ratFallEarly << "\t\t" << oPin->tData.ratRiseLate << "\t\t" << oPin->tData.ratFallLate << endl;
			_dbgFile << "SLK ------->" << "\t" << oPin->tData.slackRiseEarly << "\t\t" << oPin->tData.slackFallEarly << "\t\t" << oPin->tData.slackRiseLate << "\t\t" << oPin->tData.slackFallLate << endl;
		}
		else if (oPin->cellPtr->info_late->isSequential == true)
		{
			InstanceIpin* iPin = &(oPin->cellPtr->ckList[0]);
			_dbgFile << "Port Pin is FF oPin, ckPin -> " << iPin->name.icellName << ":" << iPin->name.pinName << endl;
			_dbgFile << "AT  ------->" << "\t" << oPin->tData.atRiseEarly << "\t\t" << oPin->tData.atFallEarly << "\t\t" << oPin->tData.atRiseLate << "\t\t" << oPin->tData.atFallLate << endl;
			_dbgFile << "RAT ------->" << "\t" << oPin->tData.ratRiseEarly << "\t\t" << oPin->tData.ratFallEarly << "\t\t" << oPin->tData.ratRiseLate << "\t\t" << oPin->tData.ratFallLate << endl;
			_dbgFile << "SLK ------->" << "\t" << oPin->tData.slackRiseEarly << "\t\t" << oPin->tData.slackFallEarly << "\t\t" << oPin->tData.slackRiseLate << "\t\t" << oPin->tData.slackFallLate << endl;
			_dbgFile << "Delay ---->" << "\t" << oPin->dData[0].delayRiseEarly << "\t\t" << oPin->dData[0].delayFallEarly << "\t\t" << oPin->dData[0].delayRiseLate << "\t\t" << oPin->dData[0].delayFallLate << endl;
		}
		else
		{
			_dbgFile << "No of Related iPins = " << oPin->relPinIndex.size() << endl;
			_dbgFile << endl;
			_dbgFile << "AT  ------->" << "\t" << oPin->tData.atRiseEarly << "\t\t" << oPin->tData.atFallEarly << "\t\t" << oPin->tData.atRiseLate << "\t\t" << oPin->tData.atFallLate << endl;
			_dbgFile << "RAT ------->" << "\t" << oPin->tData.ratRiseEarly << "\t\t" << oPin->tData.ratFallEarly << "\t\t" << oPin->tData.ratRiseLate << "\t\t" << oPin->tData.ratFallLate << endl;
			_dbgFile << "SLK ------->" << "\t" << oPin->tData.slackRiseEarly << "\t\t" << oPin->tData.slackFallEarly << "\t\t" << oPin->tData.slackRiseLate << "\t\t" << oPin->tData.slackFallLate << endl;

			for (int64 j = 0; j < (int64)oPin->dData.size(); j++)
			{
				InstanceIpin* iPin = &(oPin->cellPtr->iipList[oPin->dData[j].index]);
				_dbgFile << "iPin-> " << iPin->name.icellName << ":" << iPin->name.pinName << endl;
				_dbgFile << "Delay ---->" << "\t" << oPin->dData[j].delayRiseEarly << "\t\t" << oPin->dData[j].delayFallEarly << "\t\t" << oPin->dData[j].delayRiseLate << "\t\t" << oPin->dData[j].delayFallLate << endl;
				_dbgFile << "Slew ---->" << "\t" << iPin->tData.slewRiseEarly << "\t\t" << iPin->tData.slewFallEarly << "\t\t" << iPin->tData.slewRiseLate << "\t\t" << iPin->tData.slewFallLate << endl;
			}
		}

		_dbgFile << endl;

		for (int64 k = 0; k < (int64)_sort[i]->taps.size(); k++)
		{
			InstanceIpin* iPin = _sort[i]->taps[k].pinPtr;
			_dbgFile << "Tap ----------------> " << iPin->name.icellName << ":" << iPin->name.pinName <<" "<<" tap cap including pin cap: "<<_sort[i]->taps[k].capacitanceLate<<" "<<"delay: "<<_sort[i]->taps[k].delayLate<< endl;
			_dbgFile << "---------->" << "\tRE\t" << "\tFE\t" << "\tRL\t" << "\tFL\t" << endl;
			_dbgFile << "AT ------->" << "\t" << iPin->tData.atRiseEarly << "\t\t" << iPin->tData.atFallEarly << "\t\t" << iPin->tData.atRiseLate << "\t\t" << iPin->tData.atFallLate << endl;
			_dbgFile << "RAT ------->" << "\t" << iPin->tData.ratRiseEarly << "\t\t" << iPin->tData.ratFallEarly << "\t\t" << iPin->tData.ratRiseLate << "\t\t" << iPin->tData.ratFallLate << endl;
			_dbgFile << "SLK ------->" << "\t" << iPin->tData.slackRiseEarly << "\t\t" << iPin->tData.slackFallEarly << "\t\t" << iPin->tData.slackRiseLate << "\t\t" << iPin->tData.slackFallLate << endl;
			_dbgFile << "Delay ----->" << "\t" << iPin->tap->delayEarly << "\t\t" << iPin->tap->delayEarly << "\t\t" << iPin->tap->delayLate << "\t\t" << iPin->tap->delayLate << endl;
			_dbgFile << "M1    ----->" << "\t" << iPin->tap->m1_E << "\t\t" << iPin->tap->m1_E << "\t\t" << iPin->tap->m1_L << "\t\t" << iPin->tap->m1_L << endl;
			_dbgFile << "M2    ----->" << "\t" << iPin->tap->m2_E << "\t\t" << iPin->tap->m2_E << "\t\t" << iPin->tap->m2_L << "\t\t" << iPin->tap->m2_L << endl;
			_dbgFile << "Slew  ----->" << "\t" << iPin->tData.slewRiseEarly << "\t\t" << iPin->tData.slewFallEarly << "\t\t" << iPin->tData.slewRiseLate << "\t\t" << iPin->tData.slewFallLate << endl;
		}

		_dbgFile << endl;
		_dbgFile << endl;

	}

}

/**
 * A function to check sanity of UpdateLM function- check if KKT conditions are satisfied everywhere
 */
bool Sizer::check_KKTConditions(void)
{
	bool cond = true;
	for(int64 i = 0; i< (int64)InstanceHashTable.size(); i++)
	{
		if(InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PO)
		{
			continue;
		}
		else if (InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PI ||
				InstanceList[InstanceHashTable[i].index].cellLibIndex_l == CLK_SRC)
			continue;
		else if (InstanceList[InstanceHashTable[i].index].info_late->isSequential)
		{
			continue;
		}

		for(int j = 0; j< (int)InstanceList[InstanceHashTable[i].index].iopList.size(); j++)
		{
			double64 inR =0, inF =0, outR =0, outF =0;
			for(int k = 0; k< (int)InstanceList[InstanceHashTable[i].index].iopList[j].dData.size(); k++)
			{
				inR += InstanceList[InstanceHashTable[i].index].iopList[j].dData[k].lambdaRL;
				inF += InstanceList[InstanceHashTable[i].index].iopList[j].dData[k].lambdaFL;
			}
			InstanceOpin * oPin = &InstanceList[InstanceHashTable[i].index].iopList[j];

			Instance* currGate = &(InstanceList[InstanceHashTable[i].index]);

			InstanceOpin * nextoPin;
			int idx = -1;
			for(int k = 0; k < (int)oPin->wire->taps.size(); k++)
			{
				if(oPin->wire->taps[k].pinPtr->cellPtr->cellLibIndex_l == PO || oPin->wire->taps[k].pinPtr->cellPtr->info_late->isSequential)
				{
					outR += oPin->wire->taps[k].pinPtr->cellPtr->lambdaRL;
					outF += oPin->wire->taps[k].pinPtr->cellPtr->lambdaFL;
					continue;
				}
				nextoPin = &oPin->wire->taps[k].pinPtr->cellPtr->iopList[0];
				for(int z = 0; z < (int)nextoPin->dData.size(); z++)
				{
					if(nextoPin->cellPtr->iipList[nextoPin->dData[z].index].hash == oPin->wire->taps[k].pinPtr->hash){
						idx = z;
						break;
					}
				}
				if (idx == -1)
					assert(0);
				if(nextoPin->dData[idx].timeSense == NEGATIVE_UNATE)
				{
					outR += nextoPin->dData[idx].lambdaFL;
					outF += nextoPin->dData[idx].lambdaRL;

				}
				else if (nextoPin->dData[idx].timeSense == POSITIVE_UNATE)
				{
					outR += nextoPin->dData[idx].lambdaRL;
					outF += nextoPin->dData[idx].lambdaFL;
				}
				else
					assert(0 && "non-unate case found !");
			}

			if(fabs(inR - outR) > 0.001 )
			{
				cond = 0;
				cout << "inR " << inR << " outR " <<  outR << endl;
				for (int k = 0; k < (int)oPin->dData.size(); k++)
				{
					cout << currGate->iipList[oPin->dData[k].index].name.icellName << ":" <<
							currGate->iipList[oPin->dData[k].index].name.pinName << "\t" <<
							oPin->dData[k].lambdaRL << "\t" << oPin->dData[k].lambdaFL << endl;
				}

				cout << endl;
				cout << endl;
				InstanceOpin * nextoPin;
				int idx = -1;
				for(int k = 0; k < (int)oPin->wire->taps.size(); k++)
				{
					if(oPin->wire->taps[k].pinPtr->cellPtr->cellLibIndex_l == PO || oPin->wire->taps[k].pinPtr->cellPtr->info_late->isSequential)
					{
						cout << oPin->wire->taps[k].pinPtr->cellPtr->instanceName << "\t" <<
								oPin->wire->taps[k].pinPtr->cellPtr->lambdaRL << "\t" <<
								oPin->wire->taps[k].pinPtr->cellPtr->lambdaFL << endl;
						continue;
					}
					nextoPin = &oPin->wire->taps[k].pinPtr->cellPtr->iopList[0];
					for(int z = 0; z < (int)nextoPin->dData.size(); z++)
					{
						if(nextoPin->cellPtr->iipList[nextoPin->dData[z].index].hash == oPin->wire->taps[k].pinPtr->hash){
							idx = z;
							break;
						}
					}
					if (idx == -1)
						assert(0);

					cout << oPin->wire->taps[k].pinPtr->cellPtr->iipList[nextoPin->dData[idx].index].name.icellName <<
							":" << oPin->wire->taps[k].pinPtr->cellPtr->iipList[nextoPin->dData[idx].index].name.pinName <<
							"\t" <<	nextoPin->dData[idx].lambdaRL <<
							"\t" << nextoPin->dData[idx].lambdaFL <<
							"\t" << "Timesense = " << nextoPin->dData[idx].timeSense << endl;
				}
				assert(0 && "KKT conditions violated ");
			}

			if(fabs(inF - outF) > 0.001)
			{
				cond = 0;
				cout << "inF " << inF << " outF " <<  outF << endl;
				assert(0 && "KKT conditions violated ");

			}

		}

	}
	return cond;
}

/**
 * \return Set of gates with negative output slack
 *
 * Makes list of all the gates with negative output slack (These are the critical gates in timing
 * recovery)
 */
deque <Instance*> Sizer::FindCriticalGates (void)
{
	deque <Instance*> criticalGates;

	for(int64 i = 0; i< (int64)InstanceHashTable.size(); i++)
	{
		if(InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PO ||
				InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PI ||
				InstanceList[InstanceHashTable[i].index].cellLibIndex_l == CLK_SRC ||
				InstanceList[InstanceHashTable[i].index].info_late->isSequential == true) continue;

		double64 slack = min(InstanceList[InstanceHashTable[i].index].iopList[0].tData.slackRiseLate,
				InstanceList[InstanceHashTable[i].index].iopList[0].tData.slackFallLate);

		if (slack < 0)
			criticalGates.push_back(&InstanceList[InstanceHashTable[i].index]);
	}

	return criticalGates;
}

/**
 * \param wGate Input gate
 * \param fp Pointer to footprint of 'wGate' in footprint lib
 * \param idVth Index in version_Vth from fp (first index)
 * \param idArea Index in version_vth from fp (second index- columns)
 * \param criticalGates Set of gates with negative output slack
 * \param idx Index in criticalGates
 * \param prevTNS Previous TNS
 *
 * Performs gate timing recovery- If the gate is upsized and helps in betterment of TNS then the version-change
 * is accepted, else it moves on to next gate version by first resetting the gate to its
 * previous version (one index back)
 */
int Sizer:: GateTimingRecovery(Instance* &wGate, LibParserFPInfo* &fp, int64& idVth, int64& idArea,
								deque <Instance*>& criticalGates, int& idx, double64& prevTNS)
{
	if (APPROX_TR == true)
	{
		vector < SpefNet* > drivingNets;
		vector < std::pair<SpefTap*, int> > sideTaps;
		vector < std::pair<SpefTap*, int> > sinkTaps;

		for (int64 i = 0; i < (int64)wGate->iipList.size(); i++)
		{
			SpefNet* currNet = wGate->iipList[i].wire;
			drivingNets.push_back(currNet);

			for (int j = 0; j < (int)currNet->taps.size(); j++)
			{
				if (currNet->taps[j].cellPtr->hash != wGate->hash)
				{
					if (currNet->taps[j].cellPtr->cellLibIndex_l == PO || currNet->taps[j].cellPtr->info_late->isSequential)
					{
						std::pair <SpefTap*, int> temp;
						temp.first  = &(currNet->taps[j]);
						temp.second = -1;
						sideTaps.push_back(temp);
					}
					else
					{
						InstanceOpin* oPin = &(currNet->taps[j].cellPtr->iopList[0]);
						int idx = 0;
						for (int k = 0; k < (int)oPin->dData.size(); k++)
						{
							if (currNet->taps[j].pinPtr->hash == currNet->taps[j].cellPtr->iipList[oPin->dData[k].index].hash)
							{
								idx = k;
								break;
							}
						}
						std::pair <SpefTap*, int> temp;
						temp.first  = &(currNet->taps[j]);
						temp.second = idx;
						sideTaps.push_back(temp);
					}
				}
			}
		}

		SpefNet* currNet = wGate->iopList[0].wire;

		for (int j = 0; j < (int)currNet->taps.size(); j++)
		{

			if (currNet->taps[j].cellPtr->cellLibIndex_l == PO || currNet->taps[j].cellPtr->info_late->isSequential)
				{
					std::pair <SpefTap*, int> temp;
					temp.first  = &(currNet->taps[j]);
					temp.second = -1;
					sinkTaps.push_back(temp);
				}
				else
				{
					InstanceOpin* oPin = &(currNet->taps[j].cellPtr->iopList[0]);
					int idx            = 0;
					for (int k = 0; k < (int)oPin->dData.size(); k++)
					{
						if (currNet->taps[j].pinPtr->hash == currNet->taps[j].cellPtr->iipList[oPin->dData[k].index].hash)
						{
							idx = k;
							break;
						}
					}
					std::pair <SpefTap*, int> temp;
					temp.first  = &(currNet->taps[j]);
					temp.second = idx;
					sinkTaps.push_back(temp);
				}

		}
		sort (drivingNets.begin(), drivingNets.end(), CompareNetLevel2());

		vector < SpefNet* > drainNets;
		for (int64 i = 0; i < (int64)sinkTaps.size(); i++)
		{
			if(sinkTaps[i].second != -1)
			{
				SpefNet* dNet = sinkTaps[i].first->cellPtr->iopList[0].wire;
				drainNets.push_back(dNet);
			}
		}
		sort (drainNets.begin(), drainNets.end(), CompareNetLevel2());

		double64 originalSlack = ComputeLocalSlack(wGate, drivingNets, drainNets);
		//Updating timing
		UpdateLocalTiming(wGate, drivingNets, sideTaps, sinkTaps, drainNets);

		if (ECM == true)
		{
			if (CheckLoadViolations(fp->version_vth[idVth][idArea + 1].vPtr, wGate, 1.0) == true
					|| ComputeLocalSlack(wGate, drivingNets, drainNets) <= originalSlack)
			{
				SetVersion(wGate, &(fp->version_vth[idVth][idArea]));
				UpdateLocalTiming(wGate, drivingNets, sideTaps, sinkTaps, drainNets);
				if ((int64)criticalGates.size() > (idx + 1))
				{
					wGate = criticalGates[idx + 1];
					idx++;
				}
				else
				{
					if (_bestSoln.tns < ZERO_VAL)
					{
						return 0;
					}
					else
					{
						return 1;
					}
				}
			}
			else
			{
				SolutionState temp = _bestSoln;
				IncrementalSTA(wGate);
				_bestSoln = TrackSoln;
				if (_bestSoln.tns > prevTNS && CheckLoadViolations(fp->version_vth[idVth][idArea + 1].vPtr, wGate, 1.0) == false)
				{
					_dbgFile << "New TNS = " << _bestSoln.tns << endl;
					prevTNS = _bestSoln.tns;
					sort (criticalGates.begin(), criticalGates.end(), CompareGateSlack());
					wGate = criticalGates[0];
					idx = 0;
				}
				else
				{
					SetVersion(wGate, &(fp->version_vth[idVth][idArea]));
					IncrementalSTA(wGate);
					_bestSoln = temp;

					if ((int64)criticalGates.size() > (idx + 1))
					{
						wGate = criticalGates[idx + 1];
						idx++;
					}
					else
					{
						if (_bestSoln.tns < ZERO_VAL)
						{
							return 0;
						}
						else
						{
							return 1;
						}
					}
				}
			}
		}
		else
		{
			if (ComputeLocalSlack(wGate, drivingNets, drainNets) <= originalSlack)
			{
				SetVersion(wGate, &(fp->version_vth[idVth][idArea]));
				UpdateLocalTiming(wGate, drivingNets, sideTaps, sinkTaps, drainNets);
				if ((int64)criticalGates.size() > (idx + 1))
				{
					wGate = criticalGates[idx + 1];
					idx++;
				}
				else
				{
					if (_bestSoln.tns < ZERO_VAL)
					{
						return 0;
					}
					else
					{
						return 1;
					}
				}
			}
			else
			{
				SolutionState temp = _bestSoln;
				IncrementalSTA(wGate);
				_bestSoln = TrackSoln;
				if (_bestSoln.tns > prevTNS)
				{
					_dbgFile << "New TNS = " << _bestSoln.tns << endl;
					prevTNS = _bestSoln.tns;
					sort (criticalGates.begin(), criticalGates.end(), CompareGateSlack());
					wGate = criticalGates[0];
					idx = 0;
				}
				else
				{
					SetVersion(wGate, &(fp->version_vth[idVth][idArea]));
					IncrementalSTA(wGate);
					_bestSoln = temp;

					if ((int64)criticalGates.size() > (idx + 1))
					{
						wGate = criticalGates[idx + 1];
						idx++;
					}
					else
					{
						if (_bestSoln.tns < ZERO_VAL)
						{
							return 0;
						}
						else
						{
							return 1;
						}
					}
				}
			}
		}

	}
	else
	{
		SolutionState temp = _bestSoln;
		IncrementalSTA(wGate);
		_bestSoln = TrackSoln;

		if (_bestSoln.tns > prevTNS && (ECM == false || CheckLoadViolations(fp->version_vth[idVth][idArea + 1].vPtr, wGate, 1.0) == false))
		{
			prevTNS = _bestSoln.tns;
			sort (criticalGates.begin(), criticalGates.end(), CompareGateSlack());
			wGate = criticalGates[0];
			idx = 0;
		}
		else
		{
			SetVersion(wGate, &(fp->version_vth[idVth][idArea]));
			IncrementalSTA(wGate);
			_bestSoln = temp;

			if ((int64)criticalGates.size() > (idx + 1))
			{
				wGate = criticalGates[idx + 1];
				idx++;
			}
			else
			{
				if (_bestSoln.tns < ZERO_VAL)
				{
					return 0;
				}
				else
				{
					return 1;
				}
			}
		}
	}
	return 2;
}

/**
 * \param targetTNS Target TNS for timing recovery - by default it is 0
 *
 * Recovers negative slacks in the design by greedily and iteratively upsizing the gates without generating
 * any load/slew violations
 */
bool Sizer::TimingRecovery (double64 targetTNS)
{
	deque <Instance*> criticalGates = FindCriticalGates();
	if (criticalGates.size() == 0)	return true;

	sort (criticalGates.begin(), criticalGates.end(), CompareGateSlack());

	_bestSoln = TrackSoln;
	Instance* wGate = criticalGates[0];
	int idx = 0;

	double64 prevTNS;
	while (1)
	{
		prevTNS = _bestSoln.tns;
		if (prevTNS >= targetTNS )		break;

		LibParserFPInfo* fp = wGate->info_late->fp;
		int64 idVth         = wGate->info_late->idxVth[0];
		int64 idArea        = wGate->info_late->idxVth[1];
		if (idArea < ((int)fp->version_vth[idVth].size() - 1))	// i.e. if gate is upsizable
		{
			SetVersion(wGate, &(fp->version_vth[idVth][idArea + 1]));

			if (ECM == true)
			{
				int returnVal = GateTimingRecovery(wGate, fp, idVth, idArea, criticalGates, idx, prevTNS);
				if      (returnVal == 0)
					return false;
				else if (returnVal == 1)
					return true;
			}
			else
			{
				if (CheckLoadViolations(fp->version_vth[idVth][idArea + 1].vPtr, wGate, 1.0) == true)
				{
					SetVersion(wGate, &(fp->version_vth[idVth][idArea]));
					if ((int)criticalGates.size() > (idx + 1))
					{
						wGate = criticalGates[idx + 1];
						idx++;
					}
					else
					{
						if (_bestSoln.tns < ZERO_VAL)
						{
							return false;
						}
						else
						{
							return true;
						}
					}
				}
				else
				{
					int returnVal = GateTimingRecovery(wGate, fp, idVth, idArea, criticalGates, idx, prevTNS);
					if      (returnVal == 0)
						return false;
					else if (returnVal == 1)
						return true;
				}
			}
		}
		else
		{
			if ((int)criticalGates.size() > (idx + 1))
			{
				wGate = criticalGates[idx + 1];
				idx++;
			}
			else
			{
				if (_bestSoln.tns < ZERO_VAL)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		}
		if (_bestSoln.tns >= ZERO_VAL)
		{
			return true;
		}
	}

	return true;

}

/**
 * Checks sanity of footprint library by checking properties of each of its variants
 * and checking all the indices that are stored
 */
void Sizer::FPLibSanityCheck(void)
{
	for (int i = 0; i < (int)celllibstorage_l.celllib.size(); i++)
	{
		LibParserCellInfo* cell = &celllibstorage_l.celllib[i];
		if(cell->isSequential)
		{
			//checking its idxLk, idxVth and stuff.
			assert(cell->fp->version_lk.size()     == 1);
			assert(cell->fp->version_vth.size()    == 1);
			assert(cell->fp->version_vth[0].size() == 1);
			assert(cell->idxLk                     == 0);
			assert(cell->idxVth[0]                 ==0);
			assert( cell->idxVth[1]                ==0);
			assert(cell->fp->version_lk[cell->idxLk].celllibIdx                       == i);
			assert(cell->fp->version_vth[cell->idxVth[0]][cell->idxVth[1]].vPtr->name == cell->name);
			assert(cell->fp->version_vth[cell->idxVth[0]][cell->idxVth[1]].celllibIdx == i);

		}
		if(cell->name == "vss" || cell->name == "vcc" || cell->isSequential == true)
			continue;

		assert(cell->idxLk != -1 && cell->idxVth[0] != -1 && cell->idxVth[1] != -1);
		assert(cell->fp->version_lk[cell->idxLk].celllibIdx                       == i);
		assert(cell->fp->version_lk[cell->idxLk].vPtr->name                       == cell->name);
		assert(cell->fp->version_vth[cell->idxVth[0]][cell->idxVth[1]].vPtr->name == cell->name);
		assert(cell->fp->version_vth[cell->idxVth[0]][cell->idxVth[1]].celllibIdx == i);

		for(int j = 1; j < (int)cell->fp->version_lk.size(); j++)
		{
			assert(cell->fp->version_lk[j-1].vPtr->leakage <= cell->fp->version_lk[j].vPtr->leakage);
		}

		double64 area = 0;

		for(int j = 0; j < (int)cell->fp->version_vth.size(); j++)
		{
			for(int k = 1; k < (int)cell->fp->version_vth[j].size(); k++)
			{
				area = cell->fp->version_vth[0][k].vPtr->area;

				assert(cell->fp->version_vth[j][k-1].vPtr->leakage <= cell->fp->version_vth[j][k].vPtr->leakage);

				for(int z = 0; z < (int)cell->fp->version_vth.size(); z++)
				{
					assert(cell->fp->version_vth[z][k].vPtr->area == area);
				}

			}

		}


	}

 cout << " FPLibSanityCheck Passed !... exiting the code" << endl;

return;
}

/**
 * \return Pass/Fail flag
 *
 * Checks globally for Load violations and Slew violations at each pin in the design. Prints
 * all the violations along with their whereabouts in debug file _dbgFile
 */
bool Sizer::check_violations (void)
{
	bool passFail = true;
	for(int64 i = 0; i< (int64)InstanceHashTable.size(); i++)
	{
		if(InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PO ||
				InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PI ||
				InstanceList[InstanceHashTable[i].index].cellLibIndex_l == CLK_SRC) continue;
		for(int j = 0; j< (int)InstanceList[InstanceHashTable[i].index].iopList.size(); j++)
		{
			if(InstanceList[InstanceHashTable[i].index].iopList[j].info_late->maxCapacitance <
			   InstanceList[InstanceHashTable[i].index].iopList[j].ceffLate)
			{
				_dbgFile << "Found load violation at " << InstanceList[InstanceHashTable[i].index].instanceName
						<< " maxCap = " << InstanceList[InstanceHashTable[i].index].iopList[j].info_late->maxCapacitance
						<< " cap = " << InstanceList[InstanceHashTable[i].index].iopList[j].ceffLate << endl;
				passFail = false;
				_totalViolations++;
			}

			if(MaxSlew < std::max(InstanceList[InstanceHashTable[i].index].iopList[j].tData.slewFallLate,
					InstanceList[InstanceHashTable[i].index].iopList[j].tData.slewRiseLate))
			{
				_dbgFile << "Found slew violation at " << InstanceList[InstanceHashTable[i].index].instanceName << endl;
				passFail = false;
				_totalViolations++;
			}

		}
	}
	return passFail;
}

}
