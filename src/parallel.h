/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_PARALLEL_H_
#define IIT_PARALLEL_H_

#include "timing_info.h"
#include "include.h"
#include "classdecs.h"

/* Following data structures are created to parallelize the code on multiple cores */

namespace iit
{

/**
 * Stores timing information for output pin to run multithreaded CPPR
 */
class PARALLEL_DATA_OUT
{

public:
	bool     isRatSet;                                ///< Flag if RAT is set
	int64    flagPath;                                ///< Used in block based CPPR while extracting negPinList

	double64 ratRiseEarly;                            ///<< Rise Early RAT value during block based CPPR
	double64 ratFallEarly;                            ///<< Fall Early RAT value during block based CPPR
	double64 ratRiseLate;                             ///<< Rise Late RAT value during block based CPPR
	double64 ratFallLate;                             ///<< Fall Late RAT value during block based CPPR

	double64 atRiseEarlyCPPR;                         ///<< Rise Early fake AT value during block based CPPR
	double64 atFallEarlyCPPR;                         ///<< Fall Early fake AT value during block based CPPR
	double64 atRiseLateCPPR;                          ///<< Rise Late fake AT value during block based CPPR
	double64 atFallLateCPPR;                          ///<< Fall Late fake AT value during block based CPPR

	CRITICALITY criticalAT;                           ///<< Critical input pin AT information during block based CPPR
	CRITICALITY criticalRAT;                          ///<< Critical tap RAT information during block based CPPR

	PARALLEL_DATA_OUT()
	{
		isRatSet        = false;
		flagPath        = 0;
		ratRiseEarly    = UNDEFINED_RAT_EARLY;
		ratFallEarly    = UNDEFINED_RAT_EARLY;
		ratRiseLate     = UNDEFINED_RAT_LATE;
		ratFallLate     = UNDEFINED_RAT_LATE;

		atRiseEarlyCPPR = UNDEFINED_AT_EARLY;
		atFallEarlyCPPR = UNDEFINED_AT_EARLY;
		atRiseLateCPPR  = UNDEFINED_AT_LATE;
		atFallLateCPPR  = UNDEFINED_AT_LATE;
	}

	void clear()
	{
		isRatSet        = false ;
		flagPath        = 0;
		ratRiseEarly    = UNDEFINED_RAT_EARLY;
		ratFallEarly    = UNDEFINED_RAT_EARLY;
		ratRiseLate     = UNDEFINED_RAT_LATE;
		ratFallLate     = UNDEFINED_RAT_LATE;

		atRiseEarlyCPPR = UNDEFINED_AT_EARLY;
		atFallEarlyCPPR = UNDEFINED_AT_EARLY;
		atRiseLateCPPR  = UNDEFINED_AT_LATE;
		atFallLateCPPR  = UNDEFINED_AT_LATE;
	}
};

/**
 * Stores timing information for input pin to run multithreaded CPPR
 */
class PARALLEL_DATA_IN
{

public:
	bool     isRatSet;                                ///< Flag if RAT is set
	int64    flagPath;                                ///< Used in block based CPPR while extracting negPinList

	double64 ratRiseEarly;                            ///<< Rise Early RAT value during block based CPPR
	double64 ratFallEarly;                            ///<< Fall Early RAT value during block based CPPR
	double64 ratRiseLate;                             ///<< Rise Late RAT value during block based CPPR
	double64 ratFallLate;                             ///<< Fall Late RAT value during block based CPPR

	double64 atRiseEarlyCPPR;                         ///<< Rise Early fake AT value during block based CPPR
	double64 atFallEarlyCPPR;                         ///<< Fall Early fake AT value during block based CPPR
	double64 atRiseLateCPPR;                          ///<< Rise Late fake AT value during block based CPPR
	double64 atFallLateCPPR;                          ///<< Fall Late fake AT value during block based CPPR

	CRITICALITY criticalRAT;                          ///<< Critical output pin RAT information during block based CPPR

	PARALLEL_DATA_IN()
	{
		isRatSet        = false;
		flagPath        = 0;
		ratRiseEarly    = UNDEFINED_RAT_EARLY;
		ratFallEarly    = UNDEFINED_RAT_EARLY;
		ratRiseLate     = UNDEFINED_RAT_LATE;
		ratFallLate     = UNDEFINED_RAT_LATE;

		atRiseEarlyCPPR = UNDEFINED_AT_EARLY;
		atFallEarlyCPPR = UNDEFINED_AT_EARLY;
		atRiseLateCPPR  = UNDEFINED_AT_LATE;
		atFallLateCPPR  = UNDEFINED_AT_LATE;
	}

	void clear(){

		isRatSet        = false;
		flagPath        = 0;
		ratRiseEarly    = UNDEFINED_RAT_EARLY;
		ratFallEarly    = UNDEFINED_RAT_EARLY;
		ratRiseLate     = UNDEFINED_RAT_LATE;
		ratFallLate     = UNDEFINED_RAT_LATE;

		atRiseEarlyCPPR = UNDEFINED_AT_EARLY;
		atFallEarlyCPPR = UNDEFINED_AT_EARLY;
		atRiseLateCPPR  = UNDEFINED_AT_LATE;
		atFallLateCPPR  = UNDEFINED_AT_LATE;
	}
};

};

#endif
