/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_REPORT_H_
#define IIT_REPORT_H_

#include "include.h"
#include "classdecs.h"
#include "cppr.h"

namespace iit
{

/**
 * Class to enable reporting of AT, RAT, Slack, top 'N' worst paths and top 'N' worst paths through a pin
 */
class Report
{

public:
    CPPR*        cppr_ptr;                        ///< Pointer to class CPPR

    void         DumpResults       (ofstream& myfile,deque< string > & reports);
	void         FrontColorPin     (InstanceOpin* oPin, int64 color, deque <TEMP_CEP>& DpinList);
	void         quickReport       (ofstream& myfile);
	void         quickReportPin    (ofstream& myfile, deque < PATH_TABLE_ADV >& pinLUT_Adv, deque < Slack_Instance >& pinLUTSlackTable_Adv);
	void         ReportWorstPaths  (ofstream& myfile);
	double       ReportAT          (void);
	double       ReportRAT         (void);
	double       ReportSlack       (void);
	inline void  RunCPPR_Pin       (deque<std::string>& tokens, deque < TEMP_CEP > PinEndPoints,
							         deque < PATH_TABLE_ADV >& pinLUT_Adv, deque < Slack_Instance >&
							         pinLUTSlackTable_Adv, int MaxNumPaths_pin);

 private:
	string       _pinName;                        ///< Stores pin name of the query
	bool         _transition;                     ///< Stores transition (rise/fall) of the query
	bool         _mode;                           ///< Stores mode (early/late) of the query
	int64        _numPaths;                       ///< Stores # of paths to report i.e. 'N' of the query
	int          _numPathsThreshold;              ///< Stores extra paths threshold for the given # of paths of the query

};

/**
 * Linking function to connect to RunCPPR_Pin in class CPPR
 */
inline  void  Report::RunCPPR_Pin (deque<std::string>& tokens, deque < TEMP_CEP > PinEndPoints,
		                             deque < PATH_TABLE_ADV >& pinLUT_Adv, deque < Slack_Instance >&
									 pinLUTSlackTable_Adv, int MaxNumPaths_pin)
{
	cppr_ptr->RunCPPR_Pin(tokens, PinEndPoints, pinLUT_Adv, pinLUTSlackTable_Adv, MaxNumPaths_pin);

	return;
}

};

#endif
