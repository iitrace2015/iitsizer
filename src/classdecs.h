/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_CLASSDECS_H_
#define IIT_CLASSDECS_H_
/**
 * \file classdecs.h
 *
 * This file contains declaration of all the classes to avoid the forward declaration errors when
 * there are interdependencies between two classes
 */

namespace iit {

/* To avoid just the forward declaration error */
class SpefPort;
class SpefTap ;
class SpefNet ;
class LibParserFPInfo;
class SolutionState;

};

#endif
