/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_OPERATIONS_H_
#define IIT_OPERATIONS_H_

#include "include.h"
#include "classdecs.h"
#include "forward_propagate.h"
#include "container.h"
#include "report.h"
#include "spef.h"

namespace iit {

/**
 * Contains functions for modifying circuit from incremental change commands and few functions for incremental processing for
 * establishing connections and sanity back to new circuit
 */
class Operations {

public:

	ForwardPropagate* forwardPropagate_ptr;         ///< Pointer to an instance of ForwardPropagate class
	SpefParser *      spef_ptr;                     ///< Pointer to an instance of SpefParser class

	void  ClearIncWireListDB         (void);
	void  ComputeDBInc               (void);
	void  ColorParentNets            (Instance* cell);
	void  SetRelatedPins             (Instance* cell);
	void  init_ops_parser            (string filename);
	void  linkerInc                  (SpefNet& spefNet);
	void  FastTapdelaybetaInc        (SpefNet& spefNet);
	void  InplaceMerge               (deque<Hash_Instance>& hashTable);
	bool  read_ops_as_tokens         (istream& is, deque<string>& tokens);
	bool  read_ops_as_tokens_slow    (istream& is, deque<string>& tokens);

	// Incremental design-change commands
	void  DisconnectPin    (string pinName);
	void  RemoveNet        (string netName);
	void  CreateNet        (string netName);
	void  RemoveGate       (string cellInst);
	void  ConnectPin       (string pinName,string netName);
	void  InsertGate       (string cellInst,string cellType);
	void  RepowerGate      (string instName, string cellType);
	void  ReadSpef         (string filename, vector<SpefNet*>& linkWireList);
	void  InitOperations   (deque <string>& commands, ifstream& is, streampos& filePos, string& tempBenchmarkDir, bool& valid);
	void  ReportOperations (Report* report_ptr, deque <string>& reports, ifstream& is, streampos& filePos,ofstream& myfile, bool& valid);

private:
	vector < SpefNet* > IncWireList;                ///< Set of incremental nets
	vector < SpefNet* > ComputeDB;                  ///< Set of nets to be repaired for delay and slew- re-computation

};

};

#endif /* IIT_OPERATIONS_H_*/
