/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_TYPEDEF_H_
#define IIT_TYPEDEF_H_

#include <iostream>
#include <stdlib.h>

/**
 * \file typedef.h
 *
 * This file contains all the derived data-type definitions
 */

typedef long long int  int64;      ///< 64-bit integer type - preferred to run the code on a 64-bit machine
typedef double         double64;   ///< 64-bit double type


#endif /* IIT_TYPEDEF_H_*/
