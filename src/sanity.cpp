/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#include "include.h"
#include "sanity.h"
#include "instance.h"
#include "net.h"
#include "timer.h"
#include "utility.h"
namespace iit{

/* Global variables declaration --------------------------------------- */

//extern CelllibStorage      celllibstorage_e;
//extern CelllibStorage      celllibstorage_l;
//extern LibFileInfo         libinfo_e;
//extern LibFileInfo         libinfo_l;
//extern CompleteTimingInfo  timing;

//extern Instance* CLKSRC;
//extern int       numInc;
//extern int64     MaxNumPaths;
//extern int64     PathFlag_G;
//extern string    BenchmarkDir;
//extern bool     _isStaticDone;

extern deque   < Instance >       InstanceList;
extern vector  < Hash_Instance >  InstanceHashTable;
//extern deque   < SpefNet >        SpefNetList;
//extern vector  < Hash_Instance >  spefNetHashTable;
//extern vector  < SpefNet* >       Sort;
//extern vector  < SpefNet* >       IncNetList;
//extern deque   < SpefNet >        WireList;
//extern queue   < SpefNet* >       InitNetList;
//extern vector  < Instance* >      PIList;
//extern vector  < Instance* >      POList;
//extern vector  < InstanceIpin* >  ConeEndPoints;
//extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
//extern deque   < Slack_Instance > pathLUTSlackTable_Adv;
//extern double64 MaxSlew;
//extern std::tr1::unordered_map <char,bool> specCharLibDir;

/* End - global variables declaration -----------------------------------*/

/**
 * Function for sanity check - runtime for our own derived hash tables
 */
void hash_tables(void)
{
	TimeIt t12;
	timeit_start(&t12);

	for (int i=0; i< (int)InstanceList.size(); i++)
		Bsearch(InstanceHashTable, InstanceList[i].hash,0,InstanceHashTable.size()-1);

	timeit_stop(&t12);
	cout << "hash_tables - time spent -> " << t12.time_spent << endl;

	return;

}

/**
 * Function for sanity check - runtime for unordered_maps from C++ STL
 */
void unordered_maps(void)
{
	tr1::unordered_map <string,int64> instanceMap;

	for (int i=0; i< (int)InstanceList.size(); i++)
	{
		instanceMap[InstanceList[i].instanceName] = i;
	}

	TimeIt t11;
	timeit_start(&t11);

	for (int i=0; i< (int)InstanceList.size(); i++)
		instanceMap[InstanceList[i].instanceName];

	timeit_stop(&t11);
	cout << "unordered maps - time spent -> " << t11.time_spent << endl;

	return;

}

};
