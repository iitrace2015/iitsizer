/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_UTILITY_H_
#define IIT_UTILITY_H_

#include "include.h"
#include "classdecs.h"
#include "net.h"
#include "container.h"

namespace iit{

/* Function declarations */

void               debug_t_start     (int function);
void               debug_t_stop      (int function);
int                Threshold         (int64 numPaths);
inline int64       ComputeHash       (string cellName);
void               timeit_start      (TimeIt *myclock);
void               timeit_stop       (TimeIt *myclock);
void               debug_time        (const string& s);
deque<string>      split             (const string &s, char delim);
deque<string>      &split            (const string &s, char delim, std::deque<string> &elems);
void               PrintNetEstimates (ofstream& _dbgFile, vector <SpefNet*>& _sort);
int64              Bsearch_iPin      (vector <InstanceIpin*> sorted_vec, int64 key, int64 start, int64 end);
template<typename T>
inline int64       Bsearch           (T& sorted_vec, int64 key, int64 start, int64 end);
void               init_enum_to_function_name (void);


/* Function definitions for inline type and template type */

/**
 * Performs binary search (log time) for a key in a given list
 */
template<typename T>
inline int64 Bsearch (T& sorted_vec, int64 key, int64 start, int64 end)
{
	debug_t_start(Bsearch_enum);
	if(sorted_vec.size() == 0)
	{
		debug_t_stop(Bsearch_enum);
		return -1;
	}

	int64 mid, left = start ;
    int64 right     = end + 1; // one position passed the right end

    while (left < right)
    {
	  mid = left + (right - left)/2;
	  if      (key > sorted_vec[mid].hash) left = mid+1;
	  else if (key < sorted_vec[mid].hash) right = mid ;
	  else
		  {
		  	  debug_t_stop(Bsearch_enum);
		  	  return mid;
		  }
   }

       debug_t_stop(Bsearch_enum);
	   return -1;
}

/**
 * Computes hash key for a given string
 */
inline int64 ComputeHash(string cellName)
{
	int64 result = 0;
	for(int64 l = 0; l < (int64)cellName.size(); l++)
	{
		result = result * 31 + cellName[l];
	}
	return (result);
}

/**
 * Returns the element with lower hash among the two
 */
template <typename T> struct CompareByHashnum : binary_function <T,T,bool>
{
  inline bool operator() (const T& x, const T& y) const
  {
	  return x.hash<y.hash;
  }

};

/**
 * Returns the element with lower area among the two
 */
template <typename T> struct CompareByArea : binary_function <T,T,bool>
{
  inline bool operator() (const T& x, const T& y) const
  {
	  return (x.vPtr->area < y.vPtr->area);
  }

};

/**
 * Returns the element with lower leakage among the two
 */
template <typename T> struct CompareByLkg : binary_function <T,T,bool>  // Compare by cell leakage power
{
  inline bool operator() (const T& x, const T& y) const
  {
	  return (x.vPtr->leakage < y.vPtr->leakage);
  }

};

/**
 * Returns the element with minimum slack among the two
 */
struct CompareGateSlack
{
  bool operator() (Instance* x, Instance* y)
  {
	  return (min(x->iopList[0].tData.slackFallLate, x->iopList[0].tData.slackRiseLate) <
			  min(y->iopList[0].tData.slackFallLate, y->iopList[0].tData.slackRiseLate));
  }
};

/**
 * Returns the element with higher leakage among the two
 */
struct CompareLkg
{
  bool operator() (Instance* x, Instance* y)
  {
	  return (x->info_late->leakage > y->info_late->leakage);
  }
};


/**
 * Returns the net with lower topological level among the two
 */
struct CompareNetLevel2
{
  bool operator() (SpefNet* x, SpefNet* y)
  {
	  return (x->level < y->level);
  }
};

/**
 * Returns the net with higher topological level among the two
 */
struct CompareNetLevel3
{
  bool operator() ( SpefNet* x,  SpefNet* y)
  {
	  return (x->level > y->level);
  }
};

/**
 * Returns the element with higher slack among the two
 */
class compareIpinListSlack
{
public:
	inline bool operator() (const TEMP_IPIN_LIST& u, const TEMP_IPIN_LIST& v) const {
		return u.slack > v.slack;
	}
};

/**
 * Returns the end-point with lower hash of the name among the two
 */
struct compareEndPoints2
{
 inline bool operator() (const InstanceIpin* x, const InstanceIpin* y)
  {
	  return ((x->tap->hash) < (y->tap->hash));
  }
};

/**
 * Returns the element with lower index among the two
 */
template <typename T> struct CompareByIndex: binary_function <T,T,bool>
{
  inline bool operator() (const T& x, const T& y) const
  {
	  return x.index < y.index;
  }

};

/**
 * Returns the element with lower hash value among the two
 */
template <typename T> struct CompareByHashnum2: binary_function <T,T,bool>
{
  inline bool operator() (const T* x, const T* y) const
  {
	  return x->hash <y->hash;
  }

};


};

#endif /* IIT_UTILITY_H_*/
