/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_CPPR_H_
#define IIT_CPPR_H_
#include "include.h"
#include "backward_propagate.h"
#include "classdecs.h"
#include "container.h"

namespace iit
{

/**
 * Information relating a cone during block based CPPR
 */
class THREAD_DATA_T
{

public:
	int           threadID ;                              ///< ID of thread for multithreading
	InstanceIpin* currD;                                  ///< CEP of associated cone
	double64 	  CutOffValue;                            ///< Cut off value for pruning a cone
	bool 		  mode;                                   ///< Mode (early/late)

	tbb::concurrent_priority_queue
	        <TEMP_IPIN_LIST, compareIpinListSlack>* negPinList_G;    ///< List to store input pins
	                                                                 ///< with negative slacks for
	                                                                 ///< extracting paths after CPPR

	THREAD_DATA_T()
	{
		threadID     = 0;
		currD        = NULL;
		negPinList_G = NULL;
		CutOffValue  = 0;
		mode         = LATE;
	}
};

/**
 * Element to store a path element during path extraction and reporting
 */
class PATH_ADV
{

public:
	string* icellName;                                     ///< Cell name path node
	string* pinName;                                       ///< Pin name of path node
	char    transition;                                    ///< Associated transition (rise/fall)

	PATH_ADV()
	{
		icellName  = NULL;
		pinName    = NULL;
		transition = 'R';
	}
};

/**
 * Stores a complete path with associated slack / other information
 */
class PATH_TABLE_ADV
{

public:
	vector <PATH_ADV> path;                               ///< Stores successive path elements in order
	InstanceIpin*     Dpin;                               ///< Associated CEP for a path
	int64             DpinHash;                           ///< Hash of CEP
	bool              mode;                               ///< Mode (early/late)
	double64          slack;                              ///< Slack value of path

	PATH_TABLE_ADV()
	{
		Dpin     = NULL;
		mode     = EARLY;
		slack    = FORBIDDEN_SLACK;
		DpinHash = 0;
	}

	void clear()
	{
		Dpin     = NULL;
		mode     = EARLY;
		slack    = FORBIDDEN_SLACK;
		DpinHash = 0;
		path.clear();
	}
};

/**
 * Stores the worst slack failing pin information
 */
class WORST_PIN
{

public:
	int           type;                                 ///< Either a PO_PIN or IPIN
	InstanceIpin* iPin;                                 ///< Worst input pin
	bool          transition;                           ///< Transition (rise/fall) of associated slack
	bool          mode;                                 ///< Mode (early/late) of associated slack
	double64      slack;                                ///< Slack value
	InstanceIpin* Dpin;                                 ///< CEP of associated cone

	WORST_PIN ()
	{
		type       = -1;
		transition = 0;
		mode       = 0;
		slack      = 1;
		Dpin       = NULL;
		iPin       = NULL;
	}
};

/**
 * Stores CPPR credit for a launching FF during block based CPPR
 */
class Credit
{

public:
	double64 creditFallEarly;                            ///< Credit for Fall transition and Early mode
	double64 creditRiseEarly;                            ///< Credit for Rise transition and Early mode
	double64 creditFallLate;                             ///< Credit for Fall transition and Late mode
	double64 creditRiseLate;                             ///< Credit for Rise transition and Late mode

	Credit ()
	{
		creditFallEarly = 0.0;
		creditRiseEarly = 0.0;
		creditFallLate  = 0.0;
		creditRiseLate  = 0.0;
	}
};

/**
 * Stores slack and index for Slack Table (very similar to Hash Table) for sorting according to slack
 */
class Slack_Instance
{

public:
	double64 slack;                                       ///< Slack value
	int64    index;                                       ///< Index of list assocaited with Slack Table

	Slack_Instance()
	{
		index = -1;
		slack =  0;
	}
};

/**
 * Compare function to sort according to slack
 */
struct ComparePathSlack_Adv {
  bool operator() (const Slack_Instance x, const Slack_Instance y)
  {
	  return (x.slack < y.slack);
  }
};

/**
 * Main class for performing CPPR and storing information during CPPR
 */
class CPPR
{

public:
	CPPR();
    BackwardPropagate* backwardPropagate_ptr;                   ///< Pointer to BackwardPropagate class

	void  RunCPPR                    (int numInc);
	void  buildSparseTable           (void);
	void  pinCPPR_Adv                (void* arg, bool thruPin = false);
	void  getCredit_Adv              (SpefPort* cpPort, Credit& credit,
			                           bool ckParity, int timeSense, bool mode);
	void  addCredit_Adv              (Credit currCredit, InstanceOpin* oPin, int pdi, bool mode);
	void  slackUpdateIpin_Adv        (SpefTap* tap, InstanceOpin* oPin, int pdi, bool mode);
	void  slackUpdateOpin_Adv        (InstanceOpin* oPin, int pdi, int64 DpinHash, bool mode);
	void  BackTrace_Adv              (InstanceOpin* oPin, deque <PATH_ADV>& path, bool transition,
			                           bool mode, int64 flagPath, int64 DpinHash, int pdi);
	void  TracePath                  (WORST_PIN& worstPin, int64 flagPath, deque <PATH_ADV>& path,
			                           int64 DpinHash, int pdi);
	bool  searchFlag_Adv             (PATH_FLAG pathFlag, bool mode, bool trans, int threadID, int64 hash);
	void  RunCPPR_Pin                (deque<std::string>& tokens, deque < TEMP_CEP > PinEndPoints,
			                           deque < PATH_TABLE_ADV >& pinLUT_Adv,
									   deque < Slack_Instance >& pinLUTSlackTable_Adv, int MaxNumPaths_pin);
	SpefPort* getLCA_Adv             (SpefPort* p, SpefPort* q);
	double64  getCutOff 	         (InstanceIpin* currD, int threadID, InstanceIpin* currCK, bool mode);
	double64  getCutOff_RE           (InstanceIpin* currD, int threadID, InstanceIpin* currCK);
	double64  getCutOff_FE           (InstanceIpin* currD, int threadID, InstanceIpin* currCK);
	double64  getCutOff_RL           (InstanceIpin* currD, int threadID, InstanceIpin* currCK);
	double64  getCutOff_FL           (InstanceIpin* currD, int threadID, InstanceIpin* currCK);
	InstanceIpin* FrontTrace_Adv     (InstanceOpin* oPin, deque <PATH_ADV>& path, bool transition,
			                           bool mode, int64 flagPath, int64 DpinHash, int pdi);
	inline void ComputeRATCritinCPPR (InstanceIpin* iip, vector< InstanceOpin* >& backPinList,
			                           vector<SpefNet*>& conePinList, int64 flagPath, int pdi,
									   double64 cutOff, bool mode);
	inline void ComputeRATinCPPR     (void);

 private:
	int64	                      _CPPRflag;                    ///< Flag to ensure unique
	                                                            ///< coloring during block based CPPR
	deque < SPARSE_TABLE_NODE >   SparseTable;                  ///< Sparse Table to store logarithmic
	                                                            ///< ancestors in clock tree, used in getLCA

	priority_queue < Slack_Instance, deque < Slack_Instance >,
	                  ComparePathSlack_Adv >                     pathQueue;     ///< Stores Slack Table (like Hash Table)
	                                                                            ///< for paths sorted in worst slack order

};

/**
 * Linking function to connect to ComputeRATCritinCPPR in class BackwardPropagate
 */
inline void CPPR::ComputeRATCritinCPPR  (InstanceIpin* iip, vector< InstanceOpin* >& backPinList,
		                                   vector<SpefNet*>& conePinList, int64 flagPath, int pdi,
										   double64 cutOff, bool mode)
{
	backwardPropagate_ptr->ComputeRATCritinCPPR(iip, backPinList, conePinList,flagPath, pdi, cutOff, mode);

	return;
}

/**
 * Linking function to connect to ComputeRATinCPPR in class BackwardPropagate
 */
inline void CPPR::ComputeRATinCPPR(void)
{

	backwardPropagate_ptr->ComputeRATinCPPR();

	return;
}

};

#endif
