/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/



#ifndef IIT_PATH_H_
#define IIT_PATH_H_

#include "macrodef.h"
#include "instance.h"

namespace iit
{

// Following not needed, hence commented
//class PATH_NODE
//{
//	InstanceIpin* iPin;
//	double64      slack;
//	bool          transition;
//	bool          mode;
//
//	PATH_NODE() {
//
//		iPin       = NULL;
//		slack      = FORBIDDEN_SLACK;
//		transition = RISE;
//		mode       = EARLY;
//	}
//
//	void clear() {
//		iPin       = NULL;
//		slack      = FORBIDDEN_SLACK;
//		transition = RISE;
//		mode       = EARLY;
//	}
//};

};

#endif
