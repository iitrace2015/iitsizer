/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/


#include "include.h"
#include "backward_propagate.h"
#include "utility.h"
#include "timing_info.h"
#include "lib.h"
#include "instance.h"
#include "net.h"
#include "timer.h"
#include "container.h"
#include "globals.h"
namespace iit{

/* Global variables declaration --------------------------------------- */

extern CelllibStorage      celllibstorage_e;
extern CelllibStorage      celllibstorage_l;
extern LibFileInfo         libinfo_e;
extern LibFileInfo         libinfo_l;
extern CompleteTimingInfo  timing;

//extern Instance* CLKSRC;
//extern int       numInc;
//extern int       Verbose;
//extern int64     MaxNumPaths;
//extern int64     PathFlag_G;
//extern string    BenchmarkDir;
//extern bool     _isStaticDone;
extern int64      SkipCnt;
extern int64      NoSkipCnt;


extern deque   < Instance >       InstanceList;
extern vector  < Hash_Instance >  InstanceHashTable;
extern deque   < SpefNet >        SpefNetList;
extern vector  < Hash_Instance >  spefNetHashTable;
extern vector  < SpefNet* >       Sort;
//extern vector  < SpefNet* >       IncNetList;
//extern deque   < SpefNet >        WireList;
//extern queue   < SpefNet* >       InitNetList;
//extern vector  < Instance* >      PIList;
extern vector  < Instance* >      POList;
extern vector  < InstanceIpin* >  ConeEndPoints;
//extern deque   < PATH_TABLE_ADV > pathLUT_Adv;
//extern deque   < Slack_Instance > pathLUTSlackTable_Adv;

extern std::tr1::unordered_map <char,bool> specCharLibDir;

/* End - global variables declaration -----------------------------------*/

/**
 * Returns the tap-pin with lower hash-key among the two
 */
struct CompareEndPoints {
  bool operator() (const InstanceIpin* x, const InstanceIpin* y) {
	  return ((x->tap->hash) < (y->tap->hash));
  }
} compareEndPoints;


/**
 * \param is Input file stream
 * \param tokens Set of tokens read and split from a line
 * \param includeSpecialChars Flag for including special characters/delimiters as a part of the token name
 * \return Flag for line reading was successful or failed
 *
 * Reads a line from the current position of input file stream
 */
bool BackwardPropagate::read_line_as_tokens (istream& is, deque<string>& tokens, bool includeSpecialChars )
{

  tokens.clear() ;

  string line ;
  std::getline (is, line) ;

  while (is && tokens.empty())
  {

		string token = "" ;

		for (int i=0; i < (int)line.size(); ++i)
		{
			char currChar = line[i] ;
			if (currChar=='{')
			{
				bracket_cnt = bracket_cnt+1;
			}
			else if(currChar=='}')
			{
				bracket_cnt = bracket_cnt-1;
			}
			bool isSpecialChar = specCharLibDir[currChar] ;

			if (std::isspace (currChar) || isSpecialChar)
			{

			if (!token.empty())
			{
				// Add the current token to the list of tokens
				tokens.push_back(token) ;
				token.clear() ;
			}

			if (includeSpecialChars && isSpecialChar)
			{
				tokens.push_back(string(1, currChar)) ;
			}

			}
			else
			{
				// Add the char to the current token
				token.push_back(currChar) ;
			}

		}

		if (!token.empty())
			tokens.push_back(token) ;

		if (tokens.empty())
			std::getline (is, line) ; 		// Previous line read was empty. Read the next one.

  }

  return !tokens.empty() ;
}

/**
 * \param c input character
 * \return Return whether the current flag is a special character or not
 *
 * Checks for input character and returns if it belongs to special character set- set of delimiters that are
 * not to be included while splitting a line into meaningful tokens(strings/words)
 */
bool BackwardPropagate::is_special_char2 (char c)
{

  static const char specialChars[] = {'(', ')', ',', ':', ';', '#', '[', ']', '{', '}', '*', '\"', '\\','\''} ;

  for (int i = 0; i < (int)sizeof(specialChars); ++i)
  {
    if (c == specialChars[i])
      return true ;
  }

  return false ;
}


/**
 * \param out Output timing data
 * \param nodeT Timing data to be read
 * \param w Wire tap-pointer
 *
 * Update timing backwards by using tap-delay and given timing data as a starting point
 */
void BackwardPropagate:: addWireDelayParam(NODE_TIMING_DATA *out, NODE_TIMING_DATA *nodeT, SpefTap *w)
{

        (out->ratRiseLate  = nodeT->ratRiseLate  != UNDEFINED_RAT_LATE  ? (nodeT->ratRiseLate  - w->delayLate)  : UNDEFINED_RAT_LATE );
        (out->ratRiseEarly = nodeT->ratRiseEarly != UNDEFINED_RAT_EARLY ? (nodeT->ratRiseEarly - w->delayEarly) : UNDEFINED_RAT_EARLY);
        (out->ratFallLate  = nodeT->ratFallLate  != UNDEFINED_RAT_LATE  ? (nodeT->ratFallLate  - w->delayLate)  : UNDEFINED_RAT_LATE );
        (out->ratFallEarly = nodeT->ratFallEarly != UNDEFINED_RAT_EARLY ? (nodeT->ratFallEarly - w->delayEarly) : UNDEFINED_RAT_EARLY);

        out->isRatSet=true;

        return;
}

/**
 * \param out Output timing data
 * \param ipin Input pin
 * \param pdataIndex Thread-id
 * \param mode Early/Late mode specifier
 *
 * Update timing backwards by using tap-delay and given timing data as a starting point a given thread-id
 */
void BackwardPropagate:: addWireDelayParam_PreCPPR(NODE_TIMING_DATA *out, InstanceIpin* ipin, int pdataIndex, int mode)
{
	if(mode == BOTH)
	{
		(out->ratRiseLate  = ipin->pData[pdataIndex].ratRiseLate  != UNDEFINED_RAT_LATE  ? ( ipin->pData[pdataIndex].ratRiseLate  - ipin->tap->delayLate)  : UNDEFINED_RAT_LATE );
        (out->ratRiseEarly = ipin->pData[pdataIndex].ratRiseEarly != UNDEFINED_RAT_EARLY ? ( ipin->pData[pdataIndex].ratRiseEarly - ipin->tap->delayEarly) : UNDEFINED_RAT_EARLY);
        (out->ratFallLate  = ipin->pData[pdataIndex].ratFallLate  != UNDEFINED_RAT_LATE  ? ( ipin->pData[pdataIndex].ratFallLate  - ipin->tap->delayLate)  : UNDEFINED_RAT_LATE);
        (out->ratFallEarly = ipin->pData[pdataIndex].ratFallEarly != UNDEFINED_RAT_EARLY ? ( ipin->pData[pdataIndex].ratFallEarly - ipin->tap->delayEarly) : UNDEFINED_RAT_EARLY);
        out->isRatSet = true;

	}
	else if(mode == EARLY)
	{
        (out->ratRiseEarly = ipin->pData[pdataIndex].ratRiseEarly != UNDEFINED_RAT_EARLY ? ( ipin->pData[pdataIndex].ratRiseEarly - ipin->tap->delayEarly) : UNDEFINED_RAT_EARLY);
        (out->ratFallEarly = ipin->pData[pdataIndex].ratFallEarly != UNDEFINED_RAT_EARLY ? ( ipin->pData[pdataIndex].ratFallEarly - ipin->tap->delayEarly) : UNDEFINED_RAT_EARLY);
        out->isRatSet = true;

	}
	else if(mode == LATE)
	{
		(out->ratRiseLate  = ipin->pData[pdataIndex].ratRiseLate != UNDEFINED_RAT_LATE ? ( ipin->pData[pdataIndex].ratRiseLate  - ipin->tap->delayLate) : UNDEFINED_RAT_LATE );
        (out->ratFallLate  = ipin->pData[pdataIndex].ratFallLate != UNDEFINED_RAT_LATE ? ( ipin->pData[pdataIndex].ratFallLate  - ipin->tap->delayLate) : UNDEFINED_RAT_LATE);
        out->isRatSet = true;

	}

	return;
}

/**
 * Computes slack at every pin in the design by iterating over them by using AT/RAT information that is
 * already computed in previous steps of AT and RAT propagation
 */
void BackwardPropagate::  ComputePreCPPRSlack()
{
	ConeEndPoints.clear();
	bool isFFflag;
	for (int64 i = 0; i < (int64)InstanceList.size(); i++)
	{
		isFFflag = false;

		PARALLEL_DATA_IN pdataIn;
		if (InstanceList[i].cellLibIndex_l != -2 && InstanceList[i].cellLibIndex_l != -3 && InstanceList[i].cellLibIndex_e != -4)
		{
			if (InstanceList[i].info_late->isSequential == true)
				isFFflag = true;
		}

		for (int64 j = 0; j < (int64)InstanceList[i].iipList.size(); j++)
		{
			if (InstanceList[i].iipList[j].tData.atRiseEarly != UNDEFINED_AT_EARLY && InstanceList[i].iipList[j].tData.ratRiseEarly != UNDEFINED_RAT_EARLY)
				InstanceList[i].iipList[j].tData.slackRiseEarly = InstanceList[i].iipList[j].tData.atRiseEarly - InstanceList[i].iipList[j].tData.ratRiseEarly;
			else
				InstanceList[i].iipList[j].tData.slackRiseEarly = FORBIDDEN_SLACK;

			if (InstanceList[i].iipList[j].tData.atFallEarly != UNDEFINED_AT_EARLY && InstanceList[i].iipList[j].tData.ratFallEarly != UNDEFINED_RAT_EARLY)
				InstanceList[i].iipList[j].tData.slackFallEarly = InstanceList[i].iipList[j].tData.atFallEarly - InstanceList[i].iipList[j].tData.ratFallEarly;
			else
				InstanceList[i].iipList[j].tData.slackFallEarly = FORBIDDEN_SLACK;

			if (InstanceList[i].iipList[j].tData.atRiseLate  != UNDEFINED_AT_LATE && InstanceList[i].iipList[j].tData.ratRiseLate != UNDEFINED_RAT_LATE)
				InstanceList[i].iipList[j].tData.slackRiseLate = InstanceList[i].iipList[j].tData.ratRiseLate - InstanceList[i].iipList[j].tData.atRiseLate;
			else
				InstanceList[i].iipList[j].tData.slackRiseLate = FORBIDDEN_SLACK;

			if (InstanceList[i].iipList[j].tData.atFallLate  != UNDEFINED_AT_LATE && InstanceList[i].iipList[j].tData.ratFallLate != UNDEFINED_RAT_LATE)
				InstanceList[i].iipList[j].tData.slackFallLate = InstanceList[i].iipList[j].tData.ratFallLate - InstanceList[i].iipList[j].tData.atFallLate;
			else
				InstanceList[i].iipList[j].tData.slackFallLate = FORBIDDEN_SLACK;



			for (int k = 0; k < NUM_THREADS; k++)
			{
				InstanceList[i].iipList[j].pData[k].atRiseEarlyCPPR  = InstanceList[i].iipList[j].tData.atRiseEarly;
				InstanceList[i].iipList[j].pData[k].atRiseLateCPPR   = InstanceList[i].iipList[j].tData.atRiseLate;
				InstanceList[i].iipList[j].pData[k].atFallEarlyCPPR  = InstanceList[i].iipList[j].tData.atFallEarly;
				InstanceList[i].iipList[j].pData[k].atFallLateCPPR   = InstanceList[i].iipList[j].tData.atFallLate;
				InstanceList[i].iipList[j].pData[k].flagPath         = 0;

				InstanceList[i].iipList[j].pData[k].ratRiseEarly     = InstanceList[i].iipList[j].tData.ratRiseEarly;
				InstanceList[i].iipList[j].pData[k].ratRiseLate      = InstanceList[i].iipList[j].tData.ratRiseLate;
				InstanceList[i].iipList[j].pData[k].ratFallEarly     = InstanceList[i].iipList[j].tData.ratFallEarly;
				InstanceList[i].iipList[j].pData[k].ratFallLate      = InstanceList[i].iipList[j].tData.ratFallLate;

			}

			if ((isFFflag == true || InstanceList[i].cellLibIndex_e == -3) && InstanceList[i].iipList[j].isConnected)
				ConeEndPoints.push_back(&(InstanceList[i].iipList[j]));
		}
		for (int64 j = 0; j < (int64)InstanceList[i].iopList.size(); j++)
		{
			if (InstanceList[i].iopList[j].tData.atRiseEarly != UNDEFINED_AT_EARLY && InstanceList[i].iopList[j].tData.ratRiseEarly != UNDEFINED_RAT_EARLY)
				InstanceList[i].iopList[j].tData.slackRiseEarly = InstanceList[i].iopList[j].tData.atRiseEarly - InstanceList[i].iopList[j].tData.ratRiseEarly;
			else
				InstanceList[i].iopList[j].tData.slackRiseEarly = FORBIDDEN_SLACK;

			if (InstanceList[i].iopList[j].tData.atFallEarly != UNDEFINED_AT_EARLY && InstanceList[i].iopList[j].tData.ratFallEarly != UNDEFINED_RAT_EARLY)
				InstanceList[i].iopList[j].tData.slackFallEarly = InstanceList[i].iopList[j].tData.atFallEarly - InstanceList[i].iopList[j].tData.ratFallEarly;
			else
				InstanceList[i].iopList[j].tData.slackFallEarly = FORBIDDEN_SLACK;

			if (InstanceList[i].iopList[j].tData.atRiseLate  != UNDEFINED_AT_LATE && InstanceList[i].iopList[j].tData.ratRiseLate != UNDEFINED_RAT_LATE)
				InstanceList[i].iopList[j].tData.slackRiseLate = InstanceList[i].iopList[j].tData.ratRiseLate - InstanceList[i].iopList[j].tData.atRiseLate;
			else
				InstanceList[i].iopList[j].tData.slackRiseLate  = FORBIDDEN_SLACK;

			if (InstanceList[i].iopList[j].tData.atFallLate  != UNDEFINED_AT_LATE && InstanceList[i].iopList[j].tData.ratFallLate != UNDEFINED_RAT_LATE)
				InstanceList[i].iopList[j].tData.slackFallLate = InstanceList[i].iopList[j].tData.ratFallLate - InstanceList[i].iopList[j].tData.atFallLate;
			else
				InstanceList[i].iopList[j].tData.slackFallLate  = FORBIDDEN_SLACK;

			for (int k = 0; k < NUM_THREADS; k++)
			{
				InstanceList[i].iopList[j].pData[k].atRiseEarlyCPPR  = InstanceList[i].iopList[j].tData.atRiseEarly;
				InstanceList[i].iopList[j].pData[k].atRiseLateCPPR   = InstanceList[i].iopList[j].tData.atRiseLate;
				InstanceList[i].iopList[j].pData[k].atFallEarlyCPPR  = InstanceList[i].iopList[j].tData.atFallEarly;
				InstanceList[i].iopList[j].pData[k].atFallLateCPPR   = InstanceList[i].iopList[j].tData.atFallLate;
				InstanceList[i].iopList[j].pData[k].flagPath         = 0;

				InstanceList[i].iopList[j].pData[k].ratRiseEarly     = InstanceList[i].iopList[j].tData.ratRiseEarly;
				InstanceList[i].iopList[j].pData[k].ratRiseLate      = InstanceList[i].iopList[j].tData.ratRiseLate;
				InstanceList[i].iopList[j].pData[k].ratFallEarly     = InstanceList[i].iopList[j].tData.ratFallEarly;
				InstanceList[i].iopList[j].pData[k].ratFallLate      = InstanceList[i].iopList[j].tData.ratFallLate;
			}
		}

		for (int64 j = 0; j < (int64)InstanceList[i].ckList.size(); j++)
		{
			if (InstanceList[i].ckList[j].tData.atRiseEarly != UNDEFINED_AT_EARLY && InstanceList[i].ckList[j].tData.ratRiseEarly != UNDEFINED_RAT_EARLY)
				InstanceList[i].ckList[j].tData.slackRiseEarly = InstanceList[i].ckList[j].tData.atRiseEarly - InstanceList[i].ckList[j].tData.ratRiseEarly;
			else
				InstanceList[i].ckList[j].tData.slackRiseEarly = FORBIDDEN_SLACK;

			if (InstanceList[i].ckList[j].tData.atFallEarly != UNDEFINED_AT_EARLY && InstanceList[i].ckList[j].tData.ratFallEarly != UNDEFINED_RAT_EARLY)
				InstanceList[i].ckList[j].tData.slackFallEarly = InstanceList[i].ckList[j].tData.atFallEarly - InstanceList[i].ckList[j].tData.ratFallEarly;
			else
				InstanceList[i].ckList[j].tData.slackFallEarly = FORBIDDEN_SLACK;

			if (InstanceList[i].ckList[j].tData.atRiseLate  != UNDEFINED_AT_LATE && InstanceList[i].ckList[j].tData.ratRiseLate != UNDEFINED_RAT_LATE)
				InstanceList[i].ckList[j].tData.slackRiseLate  = InstanceList[i].ckList[j].tData.ratRiseLate - InstanceList[i].ckList[j].tData.atRiseLate;
			else
				InstanceList[i].ckList[j].tData.slackRiseLate = FORBIDDEN_SLACK;

			if (InstanceList[i].ckList[j].tData.atFallLate  != UNDEFINED_AT_LATE && InstanceList[i].ckList[j].tData.ratFallLate != UNDEFINED_RAT_LATE)
				InstanceList[i].ckList[j].tData.slackFallLate  = InstanceList[i].ckList[j].tData.ratFallLate - InstanceList[i].ckList[j].tData.atFallLate;
			else
				InstanceList[i].ckList[j].tData.slackFallLate = FORBIDDEN_SLACK;


			for (int k = 0; k < NUM_THREADS; k++)
			{
				InstanceList[i].ckList[j].pData[k].atRiseEarlyCPPR  = InstanceList[i].ckList[j].tData.atRiseEarly;
				InstanceList[i].ckList[j].pData[k].atRiseLateCPPR   = InstanceList[i].ckList[j].tData.atRiseLate;
				InstanceList[i].ckList[j].pData[k].atFallEarlyCPPR  = InstanceList[i].ckList[j].tData.atFallEarly;
				InstanceList[i].ckList[j].pData[k].atFallLateCPPR   = InstanceList[i].ckList[j].tData.atFallLate;

				InstanceList[i].ckList[j].pData[k].ratRiseEarly     = InstanceList[i].ckList[j].tData.ratRiseEarly;
				InstanceList[i].ckList[j].pData[k].ratRiseLate      = InstanceList[i].ckList[j].tData.ratRiseLate;
				InstanceList[i].ckList[j].pData[k].ratFallEarly     = InstanceList[i].ckList[j].tData.ratFallEarly;
				InstanceList[i].ckList[j].pData[k].ratFallLate      = InstanceList[i].ckList[j].tData.ratFallLate;

			}
		}
	}
	
	sort (ConeEndPoints.begin(), ConeEndPoints.end(), compareEndPoints);
}



/**
 * \param out Timing data to be written
 * \param inTime, inTemp Input timing data for comparison
 * \param oPinrat RAT-criticality to be stored in output timing data
 * \param tapCrit RAT-criticality of the tap
 *
 *	Assigns the worst-RAT among inTime and inTemp to out and sets the RAT criticality at out too
 */
template<typename parallel_data>
void BackwardPropagate:: UpdateWireDelayParam_preCPPR(parallel_data *out, NODE_TIMING_DATA *inTime, parallel_data *inTemp,CRITICALITY& oPinrat,CRITICALITY& tapCrit)
{

    	if(inTime->ratFallEarly > inTemp->ratFallEarly )
    	{
			out->ratFallEarly        = inTime->ratFallEarly;
			oPinrat.indexFallEarly   = tapCrit.indexFallEarly;
			oPinrat.transitionFE     = tapCrit.transitionFE;
    	}
    	else
    		out->ratFallEarly        = inTemp->ratFallEarly;

    	if(inTime->ratRiseEarly > inTemp->ratRiseEarly)
    	{
			out->ratRiseEarly        = inTime->ratRiseEarly;
			oPinrat.indexRiseEarly   = tapCrit.indexRiseEarly;
			oPinrat.transitionRE     = tapCrit.transitionRE;
    	}
    	else
    		out->ratRiseEarly        = inTemp->ratRiseEarly;

    	if( inTime->ratFallLate  > inTemp->ratFallLate )
    	{
    		out->ratFallLate         = inTemp->ratFallLate ;

    	}
    	else
    	{
    		out->ratFallLate         = inTime->ratFallLate;
    		oPinrat.indexFallLate    = tapCrit.indexFallLate;
			oPinrat.transitionFL     = tapCrit.transitionFL;
    	}

    	if(inTime->ratRiseLate  > inTemp->ratRiseLate)
    	{
    		out->ratRiseLate         = inTemp->ratRiseLate;

    	}
    	else
    	{
    		out->ratRiseLate         =  inTime->ratRiseLate;
    		oPinrat.indexRiseLate    = tapCrit.indexRiseLate;
			oPinrat.transitionRL     = tapCrit.transitionRL;
    	}

   return;
}

/**
 * \param x Pin timing data 1
 * \param y Pin timing data 2
 *
 * Copies RAT data from y to x
 */
inline void BackwardPropagate:: Copyrat(NODE_TIMING_DATA *x, NODE_TIMING_DATA *y)
{
        (x->ratFallEarly = y->ratFallEarly);
        (x->ratRiseEarly = y->ratRiseEarly);
        (x->ratFallLate  = y->ratFallLate);
		(x->ratRiseLate  = y->ratRiseLate);
        x->isRatSet      = true;

  return;
}


/**
 * \param clkTimingData Clock-pin timing data
 * \param clkPinHash Hash of the clock pin
 * \param iPinHash Hash of input pin-name of FF
 * \param cellIndexEarly Index of cell-type for the given gate in early celllib ( library storage)
 * \param cellIndexLate Index of cell-type for the given gate in late celllib (library storage)
 * \param iPinTimingData Timing data of input pin to be written
 *
 * Computes Setup and Hold timing constraints and RAT for input pin of a sequential cell
 */
std::pair<int,int> BackwardPropagate:: ComputeSetupHoldRat(NODE_TIMING_DATA* clkTimingData, int64 clkPinHash,
        			int64 iPinHash, int64 cellIndexEarly, int64 cellIndexLate, NODE_TIMING_DATA& iPinTimingData)
{
	std::pair<int,int> holdSetupSense;

	vector <LibParserTimingInfo>* tempArcEarly,*tempArcLate;

	tempArcEarly = &celllibstorage_e.celllib[cellIndexEarly].timingArcs;
	tempArcLate  = &celllibstorage_l.celllib[cellIndexLate].timingArcs;

	int arcIndexEarly,arcIndexLate;
	double64 holdRise,holdFall,setupRise,setupFall;

	holdSetupSense.first       = FORBIDDEN_EARLY;
	iPinTimingData.holdSense   = FORBIDDEN_EARLY;
	holdSetupSense.second      = FORBIDDEN_LATE;
	iPinTimingData.setupSense  = FORBIDDEN_LATE;
	arcIndexEarly=arcIndexLate = -1;

	int timingArcSize=(*tempArcLate).size();

	for(int i=0;i<timingArcSize;i++)
	{
		if(clkPinHash==(*tempArcEarly)[i].hashFromPin && iPinHash==(*tempArcEarly)[i].hashToPin)
			{
				arcIndexEarly=i;
			}
		if(clkPinHash==(*tempArcLate)[i].hashFromPin && iPinHash==(*tempArcLate)[i].hashToPin)
			{
				arcIndexLate=i;
			}
		if(arcIndexEarly!=-1 && arcIndexLate!=-1)
			break;
	}
	if(arcIndexEarly==-1 && arcIndexLate ==-1)
	{
		setupFall = FORBIDDEN_LATE;
		setupRise = FORBIDDEN_LATE;
		holdRise  = FORBIDDEN_EARLY;
		holdFall  = FORBIDDEN_EARLY;
	}

	if(arcIndexEarly!=-1)
	{
		if((*tempArcEarly)[arcIndexEarly].timingType=="hold_rising")
		{
			holdSetupSense.first     = HOLD_RISING;
			iPinTimingData.holdSense = HOLD_RISING;

			holdRise = EvaluateLUT(true,clkTimingData->slewRiseEarly,iPinTimingData.slewRiseEarly,(*tempArcEarly)[arcIndexEarly].riseConstraint);
			holdFall = EvaluateLUT(true,clkTimingData->slewRiseEarly,iPinTimingData.slewFallEarly,(*tempArcEarly)[arcIndexEarly].fallConstraint);

			iPinTimingData.ratRiseEarly = holdRise!=FORBIDDEN_EARLY ? clkTimingData->atRiseLate + holdRise : UNDEFINED_RAT_EARLY;
			iPinTimingData.ratFallEarly = holdFall!=FORBIDDEN_EARLY ? clkTimingData->atRiseLate + holdFall : UNDEFINED_RAT_EARLY;

		}

		else if((*tempArcEarly)[arcIndexEarly].timingType=="hold_falling")
		{
			holdSetupSense.first     = HOLD_FALLING;
			iPinTimingData.holdSense = HOLD_FALLING;
			holdRise = EvaluateLUT(true,clkTimingData->slewFallEarly,iPinTimingData.slewRiseEarly,(*tempArcEarly)[arcIndexEarly].riseConstraint);
			holdFall = EvaluateLUT(true,clkTimingData->slewFallEarly,iPinTimingData.slewFallEarly,(*tempArcEarly)[arcIndexEarly].fallConstraint);

			iPinTimingData.ratRiseEarly = holdRise!=FORBIDDEN_EARLY ? clkTimingData->atFallLate + holdRise : UNDEFINED_RAT_EARLY;
			iPinTimingData.ratFallEarly = holdFall!=FORBIDDEN_EARLY ? clkTimingData->atFallLate + holdFall : UNDEFINED_RAT_EARLY;
		}
	}
	if(arcIndexLate!=-1)
	{
		if((*tempArcLate)[arcIndexLate].timingType=="setup_rising")
		{
			holdSetupSense.second     = SETUP_RISING;
			iPinTimingData.setupSense = SETUP_RISING;

			setupRise = EvaluateLUT(false,clkTimingData->slewRiseLate,iPinTimingData.slewRiseLate,(*tempArcLate)[arcIndexLate].riseConstraint);
			setupFall = EvaluateLUT(false,clkTimingData->slewRiseLate,iPinTimingData.slewFallLate,(*tempArcLate)[arcIndexLate].fallConstraint);

			iPinTimingData.ratRiseLate = setupRise!=FORBIDDEN_LATE ? timing.clock.period + clkTimingData->atRiseEarly - setupRise : UNDEFINED_RAT_LATE;
			iPinTimingData.ratFallLate = setupFall!=FORBIDDEN_LATE ? timing.clock.period + clkTimingData->atRiseEarly - setupFall : UNDEFINED_RAT_LATE;
		}
		else if((*tempArcLate)[arcIndexLate].timingType=="setup_falling")
		{
			holdSetupSense.second     = SETUP_FALLING;
			iPinTimingData.setupSense = SETUP_FALLING;

			setupRise = EvaluateLUT(false,clkTimingData->slewFallLate,iPinTimingData.slewRiseLate,(*tempArcLate)[arcIndexLate].riseConstraint);
			setupFall = EvaluateLUT(false,clkTimingData->slewFallLate,iPinTimingData.slewFallLate,(*tempArcLate)[arcIndexLate].fallConstraint);

			iPinTimingData.ratRiseLate = setupRise!=FORBIDDEN_LATE ? timing.clock.period + clkTimingData->atFallEarly - setupRise : UNDEFINED_RAT_LATE;
			iPinTimingData.ratFallLate = setupFall!=FORBIDDEN_LATE ? timing.clock.period + clkTimingData->atFallEarly - setupFall : UNDEFINED_RAT_LATE;

		}
	}

	iPinTimingData.isRatSet = true;

return holdSetupSense;
}

/**
 * \param net Input wire/net
 *
 * Propagates RAT from sink gate of the wire to input of its driving gate
 */
void BackwardPropagate::BackTimingNetBased(SpefNet* net)
{

	Instance* instance = net->port.cellPtr;
    int64 i;

   //NOTE: make sure to report rat for disconnected input pins as undefined, also, at and slack as well.


        // calculate rat at the output based on the rats at the wire ports

	InstanceOpin *iop = net->port.pinPtr;  // choosing a opin for the cell (Assumption is that the iopList is sorted


	SpefNet *         w              = net;   //taking a wire connected to output pin.
	NODE_TIMING_DATA *tempTimingData = &(iop->tData);
	int64             numTaps        = w->taps.size();

	for(int64 j = 0; j < numTaps; j++)
	{

		if(w->taps[j].cellPtr->cellLibIndex_l == -3)
		{
			//Do nothing, RAT of PO would already be set

		}

		// checking for a net if it's ouput is a DFF, then calculate RAT for that input pin first is not already set.
		else if(w->taps[j].cellPtr->info_late->isSequential==true && w->taps[j].pinPtr->tData.isRatSet==false )
		{
			//FIXME:Check if instead of creating these variables, directly give inputs to the functions
			InstanceIpin*      iip             = w->taps[j].pinPtr;
			NODE_TIMING_DATA * ckTimingData    = &(w->taps[j].cellPtr->ckList[0].tData);
			int64              iPinHash        = iip->hash;
			int64              clkPinHash      = w->taps[j].cellPtr->ckList[0].hash;
			int64              cellIndexEarly  = w->taps[j].cellPtr->cellLibIndex_e;
			int64              cellIndexLate   = w->taps[j].cellPtr->cellLibIndex_l;

			//Assuming slew data and at for clock (atleast), has already been filled by AT
			pair<int,int> holdSetupSense;

			holdSetupSense = ComputeSetupHoldRat(ckTimingData,clkPinHash,iPinHash,cellIndexEarly,cellIndexLate,(iip->tData));

		}



		// for each tap find the corresponding delay at the port
		// and then update depending on value set at the taps

		InstanceIpin *iip = w->taps[j].pinPtr;

		NODE_TIMING_DATA inTime, *inTemp = &(iop->tData);

		CRITICALITY criticalRAT;
		if (tempTimingData->isRatSet != true)
		{
			tempTimingData->isRatSet = true;
			addWireDelayParam(tempTimingData, &iip->tData, &w->taps[j]);
		}
		else
		{
			addWireDelayParam(&inTime, &iip->tData, &w->taps[j]);
			CRITICALITY temp;
			temp.indexFallEarly =temp.indexFallLate =
									temp.indexRiseLate = temp.indexRiseEarly=j;
			UpdateWireDelayParam(tempTimingData, &inTime, inTemp,criticalRAT,temp );


		}


   }


    if(instance->cellLibIndex_l == -2 || instance->cellLibIndex_l == -4)
    {
    	return;
    }

    LibParserCellInfo* cellLate;
    cellLate =  instance->info_late;

    //CHECKPOINT: if int fails, then change all variable types to int64 for numipins, numopins etc.

    int64 numRelatedIPins,inPinInd;

    if(!cellLate->isSequential)
    	numRelatedIPins=iop->relPinIndex.size();
	else
	{
	   numRelatedIPins=1;
	   inPinInd=0;
	}
	for(i = 0; i < numRelatedIPins; i++)
	{
	   //index of current IPin in iiPList !
	   InstanceIpin* iip;

	   if(!cellLate->isSequential)
	   {
		   inPinInd = iop->relPinIndex[i];
		   iip     = &(instance->iipList[inPinInd]);
	   }
	   else
			iip=&(instance->ckList[0]);

	    //CHECKPOINT: have to compute RAT and AT for disconnected pins as well !
		//index of current OPin in iopList !
		int64 oPinInd=Bsearch(iop->cellPtr->iopList,iop->hash,0,iop->cellPtr->iopList.size()-1);
		//TODO: make sure to change oPinInd to int64 because Bsearch can also return -1, do this wherver Bsearch is used
		if(oPinInd==-1)
			continue;     //this has to be assert for debugging purposes
		deque<int> relatedPins;

		NODE_TIMING_DATA inTempNodeTiming;

		NODE_TIMING_DATA inTime, *inTemp = &(iip->tData);             /* Stores timing values at output due to current input */
		DELAY_DATA *delayData = NULL;


		int64 sizeTData=iop->dData.size();
		for (int64 itr=0;itr<sizeTData;itr++)
		{
			if(iop->dData[itr].index==inPinInd)
			{
				delayData = &(iop->dData[itr]);
				break;
			}
		}

		//copying delayData into node timing data of output pin
		CRITICALITY iPinrat;
		iPinrat.indexFallEarly = iPinrat.indexFallLate =
								iPinrat.indexRiseLate = iPinrat.indexRiseEarly = oPinInd;


		//TODO:
		// Now update output timing values
		if (iip->tData.isRatSet==false)
		{
			addIpinRatParam (&iip->tData,&(iop->tData),delayData,iPinrat);
			iip->tData.isRatSet = true;
		}
		else
		{
			addIpinRatParam       (&inTime,&(iop->tData),delayData,iPinrat);
			UpdateWireDelayParam  (&iip->tData,&inTime,inTemp,iPinrat,iPinrat);

		}

	}


	if(cellLate->isSequential)    // for sequential cell
	{
		NODE_TIMING_DATA* ckTimingData=&(instance->ckList[0].tData);
		int64 numRelatedIPins=instance->ckList[0].relPinIndex.size();
		for(int64 i = 0; i < numRelatedIPins; i++)
		{

			int64              iPinInd        = instance->ckList[0].relPinIndex[i];
			InstanceIpin*      iip            = &(instance->iipList[iPinInd]);
			NODE_TIMING_DATA*  iPinTData      = &(iip->tData);
			int64              iPinHash       = iip->hash;
			int64              clkPinHash     = instance->ckList[0].hash;
			int64              cellIndexEarly = instance->cellLibIndex_e;
			int64              cellIndexLate  = instance->cellLibIndex_l;  //FIXME: use celllibindex early and late  DB: store them separately in instances.

			std::pair<int,int> holdSetupSense;

			if(iip->tData.isRatSet==false)
				holdSetupSense=ComputeSetupHoldRat(ckTimingData,clkPinHash,iPinHash,cellIndexEarly,cellIndexLate,(iip->tData));
			else
			{

				holdSetupSense.first=iip->tData.holdSense;
				holdSetupSense.second=iip->tData.setupSense;
			}
			if(holdSetupSense.first == HOLD_RISING)
			{

				ckTimingData->ratFallLate = UNDEFINED_RAT_LATE;
				double64 a,b;
				if(iPinTData->atRiseEarly == UNDEFINED_AT_EARLY || iPinTData->ratRiseEarly==UNDEFINED_RAT_EARLY || ckTimingData->atRiseLate == UNDEFINED_AT_LATE)
					a = UNDEFINED_RAT_LATE;
				else
					a = iPinTData->atRiseEarly - iPinTData->ratRiseEarly + ckTimingData->atRiseLate;
				if(iPinTData->atFallEarly == UNDEFINED_AT_EARLY || iPinTData->ratFallEarly==UNDEFINED_RAT_EARLY)
					b = UNDEFINED_RAT_LATE;
				else
					b = iPinTData->atFallEarly - iPinTData->ratFallEarly + ckTimingData->atRiseLate;
				if(ckTimingData->isRatSet==false)
				{
					ckTimingData->isRatSet=true;
					if(a>b)
					{
						ckTimingData->ratRiseLate = b ;
					}
					else
					{
						ckTimingData->ratRiseLate = a ;
					}

				}
				else
				{

					if(a>b)
					{
						if(ckTimingData->ratRiseLate  > b)
						{
							ckTimingData->ratRiseLate = b;

						}
					}
					else
					{
						if(ckTimingData->ratRiseLate  > a)
						{
							ckTimingData->ratRiseLate = a ;
						}
					}

				}
			}
			else   //implies HOLD_FALLING test
			{

				ckTimingData->ratRiseLate=UNDEFINED_RAT_LATE;
				double64 a,b;
				if(iPinTData->atRiseEarly==UNDEFINED_AT_EARLY || iPinTData->ratRiseEarly==UNDEFINED_RAT_EARLY || ckTimingData->atFallLate == UNDEFINED_AT_LATE)
					a =UNDEFINED_RAT_LATE;
				else
					a = iPinTData->atRiseEarly - iPinTData->ratRiseEarly + ckTimingData->atFallLate;
				if(iPinTData->atFallEarly==UNDEFINED_AT_EARLY || iPinTData->ratFallEarly==UNDEFINED_RAT_EARLY)
					b = UNDEFINED_RAT_LATE;
				else
					b = iPinTData->atFallEarly - iPinTData->ratFallEarly + ckTimingData->atFallLate;
				if(ckTimingData->isRatSet==false)
				{
					ckTimingData->ratFallLate = a > b ? b : a ;
					ckTimingData->isRatSet=true;
					if(a>b)
					{
						ckTimingData->ratFallLate = b;
					}
					else
					{
						ckTimingData->ratFallLate = a;
					}
				}
				else
				{
					if(a>b)
					{
						if(ckTimingData->ratFallLate  > b)
						{
							ckTimingData->ratFallLate = b;
						}
					}
					else
					{
						if(ckTimingData->ratFallLate  > a)
						{
							ckTimingData->ratFallLate = a;
						}
					}
				}
			}

			//setup tests
			if(holdSetupSense.second==SETUP_RISING)
			{
				ckTimingData->ratFallEarly=UNDEFINED_RAT_EARLY;
				double64 a,b;
				if(iPinTData->atRiseLate==UNDEFINED_AT_LATE || iPinTData->ratRiseLate==UNDEFINED_RAT_LATE || ckTimingData->atRiseEarly == UNDEFINED_AT_EARLY)
					a =UNDEFINED_RAT_EARLY;
				else
					a = iPinTData->atRiseLate - iPinTData->ratRiseLate + ckTimingData->atRiseEarly;
				if(iPinTData->atFallLate==UNDEFINED_AT_LATE || iPinTData->ratFallLate==UNDEFINED_RAT_LATE)
					b = UNDEFINED_RAT_EARLY;
				else
					b = iPinTData->atFallLate - iPinTData->ratFallLate + ckTimingData->atRiseEarly;
				if(ckTimingData->isRatSet==false)
				{
					ckTimingData->isRatSet=true;
					if(a>b)
					{
						ckTimingData->ratRiseEarly = a;
					}
					else
					{
						ckTimingData->ratRiseEarly = b;
					}
				}

				else
				{
					if(a>b)
					{
						if(ckTimingData->ratRiseEarly  < a)
						{
							ckTimingData->ratRiseEarly = a;
						}
					}
					else
					{
						if(ckTimingData->ratRiseEarly  < b)
						{
							ckTimingData->ratRiseEarly = b;
						}
					}
				}


			}
			else   //implies SETUP_FALLING test
			{

				ckTimingData->ratRiseEarly=UNDEFINED_RAT_EARLY;
				double64 a,b;
				if(iPinTData->atRiseLate==UNDEFINED_AT_LATE || iPinTData->ratRiseLate==UNDEFINED_RAT_LATE || ckTimingData->atFallEarly == UNDEFINED_AT_EARLY)
					a =UNDEFINED_RAT_EARLY;
				else
					a = iPinTData->atRiseLate - iPinTData->ratRiseLate + ckTimingData->atFallEarly;
				if(iPinTData->atFallLate==UNDEFINED_AT_LATE || iPinTData->ratFallLate==UNDEFINED_RAT_LATE)
					b = UNDEFINED_RAT_EARLY;
				else
					b = iPinTData->atFallLate - iPinTData->ratFallLate + ckTimingData->atFallEarly;
				if(ckTimingData->isRatSet==false)
				{
					ckTimingData->isRatSet=true;
					if(a>b)
					{
						ckTimingData->ratFallEarly = a;
					}
					else
					{
						ckTimingData->ratFallEarly = b;
					}
				}
				else
				{
					if(a>b)
					{
						if(ckTimingData->ratFallEarly  < a)
						{
							ckTimingData->ratFallEarly = a;
						}
					}
					else
					{
						if(ckTimingData->ratFallEarly  < b)
						{
							ckTimingData->ratFallEarly = b;

						}
					}
				}
			}
		}
	}

return;
}

/**
 * Propagates RAT backwards during STA- primary RAT propagation function for STA
 */
void BackwardPropagate::ComputeRATNetBased()
{
	int64 sizePOList=POList.size();
	assert(sizePOList != 0);

	for(int64 j=0;j<sizePOList;j++)
	{
		int64 index=Bsearch(timing.rat,POList[j]->hash,0,timing.rat.size()-1);
		if(index != -1)
		{
			POList[j]->iipList[0].tData.ratFallEarly = timing.rat[index].earlyfall;
			POList[j]->iipList[0].tData.ratFallLate  = timing.rat[index].latefall;
			POList[j]->iipList[0].tData.ratRiseEarly = timing.rat[index].earlyrise;
			POList[j]->iipList[0].tData.ratRiseLate  = timing.rat[index].laterise;
			POList[j]->iipList[0].tData.isRatSet     = true;
		}
		else
		{
			assert(0 && "RAT not given for one of the PO");
			POList[j]->iipList[0].tData.ratFallEarly = UNDEFINED_RAT_EARLY;
			POList[j]->iipList[0].tData.ratFallLate  = UNDEFINED_RAT_LATE;
			POList[j]->iipList[0].tData.ratRiseEarly = UNDEFINED_RAT_EARLY;
			POList[j]->iipList[0].tData.ratRiseLate  = UNDEFINED_RAT_LATE;
			POList[j]->iipList[0].tData.isRatSet     = true;
		}
	}



	int64 sizeSort = Sort.size();

	for(int64 i = sizeSort-1 ; i >= 0; i--)
	{
		if(Sort[i]->isVirtualNet) continue;
		if(Sort[i]->isCktreeMember)
		{
			continue;
		}

		BackTimingNetBased(Sort[i]);

	}


	for(int64 i=sizeSort-1;i>=0;i--)
	{
		if(Sort[i]->isVirtualNet)    continue;
		if(!Sort[i]->isCktreeMember) continue;

		BackTimingNetBased(Sort[i]);
	}

}

/**
 * \param cell Input sequential cell
 *
 * Computes RAT of clock pin of a sequential cell using timing data at all of its output and input pins- has to be worst of all
 */
void BackwardPropagate::ComputeRATckPin(Instance* cell)
{

		NODE_TIMING_DATA *ckTimingData =&(cell->ckList[0].tData);;
		NODE_TIMING_DATA * iPinTData;
		ClearRAT(ckTimingData, BOTH);

		std::pair<int,int> holdSetupSense;

		for(int i = 0; i< (int)cell->iipList.size(); i++)
			{
				if(cell->iipList[i].isConnected == false) continue;
				iPinTData = &(cell->iipList[i].tData);

				//Assumption is that postCPPR RAT of D(iPinTData) has been set
				holdSetupSense.first =  iPinTData->holdSense;
				holdSetupSense.second = iPinTData->setupSense;

				if(holdSetupSense.first == HOLD_RISING)
				{

					double64 a,b;

					if(iPinTData->atRiseEarly==UNDEFINED_AT_EARLY || iPinTData->ratRiseEarly==UNDEFINED_RAT_EARLY || ckTimingData->atRiseLate == UNDEFINED_AT_LATE)
						a =UNDEFINED_RAT_LATE;
					else
						a = iPinTData->atRiseEarly - iPinTData->ratRiseEarly + ckTimingData->atRiseLate;
					if(iPinTData->atFallEarly==UNDEFINED_AT_EARLY || iPinTData->ratFallEarly==UNDEFINED_RAT_EARLY)
						b = UNDEFINED_RAT_LATE;
					else
						b = iPinTData->atFallEarly - iPinTData->ratFallEarly + ckTimingData->atRiseLate;

					if(a>b)
					{
						if(ckTimingData->ratRiseLate  > b)
						{
							ckTimingData->ratRiseLate = b;

						}
					}
					else
					{
						if(ckTimingData->ratRiseLate  > a)
						{
							ckTimingData->ratRiseLate = a ;
						}
					}

				}
				else   //implies HOLD_FALLING test
				{

					double64 a,b;

					if(iPinTData->atRiseEarly==UNDEFINED_AT_EARLY || iPinTData->ratRiseEarly==UNDEFINED_RAT_EARLY || ckTimingData->atFallLate == UNDEFINED_AT_LATE)
						a =UNDEFINED_RAT_LATE;
					else
						a = iPinTData->atRiseEarly - iPinTData->ratRiseEarly + ckTimingData->atFallLate;
					if(iPinTData->atFallEarly==UNDEFINED_AT_EARLY || iPinTData->ratFallEarly==UNDEFINED_RAT_EARLY)
						b = UNDEFINED_RAT_LATE;
					else
						b = iPinTData->atFallEarly - iPinTData->ratFallEarly + ckTimingData->atFallLate;

					if(a>b)
					{
						if(ckTimingData->ratFallLate  > b)
						{
							ckTimingData->ratFallLate = b;
						}
					}
					else
					{
						if(ckTimingData->ratFallLate  > a)
						{
							ckTimingData->ratFallLate = a;
						}
					}



				}

				if(holdSetupSense.second == SETUP_RISING)
				{

					double64 a,b;

					if(iPinTData->atRiseLate == UNDEFINED_AT_LATE || iPinTData->ratRiseLate==UNDEFINED_RAT_LATE || ckTimingData->atRiseEarly == UNDEFINED_AT_EARLY)
						a = UNDEFINED_RAT_EARLY;
					else
						a = iPinTData->atRiseLate - iPinTData->ratRiseLate + ckTimingData->atRiseEarly;
					if(iPinTData->atFallLate == UNDEFINED_AT_LATE || iPinTData->ratFallLate==UNDEFINED_RAT_LATE)
						b = UNDEFINED_RAT_EARLY;
					else
						b = iPinTData->atFallLate - iPinTData->ratFallLate + ckTimingData->atRiseEarly;

					if(a>b)
					{
						if(ckTimingData->ratRiseEarly  < a)
						{
							ckTimingData->ratRiseEarly = a;
						}
					}
					else
					{
						if(ckTimingData->ratRiseEarly  < b)
						{
							ckTimingData->ratRiseEarly = b;
						}
					}

				}
				else   //implies SETUP_FALLING test
				{
					double64 a,b;

					if(iPinTData->atRiseLate==UNDEFINED_AT_LATE || iPinTData->ratRiseLate==UNDEFINED_RAT_LATE || ckTimingData->atFallEarly == UNDEFINED_AT_EARLY)
						a = UNDEFINED_RAT_EARLY;
					else
						a = iPinTData->atRiseLate - iPinTData->ratRiseLate + ckTimingData->atFallEarly;
					if(iPinTData->atFallLate==UNDEFINED_AT_LATE || iPinTData->ratFallLate==UNDEFINED_RAT_LATE)
						b = UNDEFINED_RAT_EARLY;
					else
						b = iPinTData->atFallLate - iPinTData->ratFallLate + ckTimingData->atFallEarly;
					if(a>b)
					{
						if(ckTimingData->ratFallEarly  < a)
						{
							ckTimingData->ratFallEarly = a;
						}
					}
					else
					{
						if(ckTimingData->ratFallEarly  < b)
						{
							ckTimingData->ratFallEarly = b;

						}
					}

				}
			}
			for(int i = 0; i < (int)cell->iopList.size(); i++)
			{
				if(cell->iopList[i].isConnected == false) continue;
				InstanceOpin *   iop = &(cell->iopList[i]);
				InstanceIpin*    iip = &cell->ckList[0];
				NODE_TIMING_DATA inTime, *inTemp = &(iip->tData);
				CRITICALITY iPinrat;

				iPinrat.indexFallEarly = iPinrat.indexFallLate = iPinrat.indexRiseLate = iPinrat.indexRiseEarly = i;
					// oPinInd here will be 'i'

				addIpinRatParam      (&inTime,&(iop->tData),&iop->dData[0],iPinrat, BOTH);
				UpdateWireDelayParam (&iip->tData,&inTime,inTemp,iPinrat,iPinrat, BOTH);

			}
}

/**
 * \param prioQueue Priority queue containing set of nets as a starting point for RAT propagation
 * \param clockQueue Priority queue containing set of clock-tree nets as a starting point for RAT propagation
 *
 * Propagates RAT backwards during CPPR
 */
void BackwardPropagate::BackTimingInCPPR (priority_queue < SpefNet*,deque<SpefNet*>, CompareNetLevel2 >& prioQueue,
		priority_queue < SpefNet*,deque<SpefNet*>, CompareNetLevel2 >& clockQueue)
{
	SpefNet*         currNet;
	int64            numTaps;
	InstanceOpin*    iop;
	NODE_TIMING_DATA prevTData;

	while(!prioQueue.empty())
	{

		currNet   = prioQueue.top();
		prioQueue.pop();
		numTaps   = currNet->taps.size();
		iop       = currNet->port.pinPtr;
		prevTData = iop->tData;

		ClearRAT(&iop->tData, BOTH);

		for(int64 i= 0 ; i < numTaps ; i++)
		{

			InstanceIpin *iip  = currNet->taps[i].pinPtr;
			NODE_TIMING_DATA inTime, *inTemp = &(iop->tData);
			addWireDelayParam(&inTime, &iip->tData, &currNet->taps[i]);
			CRITICALITY temp;
			temp.indexFallEarly = temp.indexFallLate = temp.indexRiseLate = temp.indexRiseEarly = i;
			UpdateWireDelayParam(&iop->tData, &inTime, inTemp,temp,temp, BOTH );

		}

		ComputeSlackinCPPRPin(iop);

			if( prevTData.ratFallEarly == iop->tData.ratFallEarly &&
				prevTData.ratFallLate  == iop->tData.ratFallLate   &&
				prevTData.ratRiseEarly == iop->tData.ratRiseEarly &&
				prevTData.ratRiseLate  == iop->tData.ratRiseLate )  continue;

		Instance* portCell = iop->cellPtr;
		if(portCell->cellLibIndex_e == -2 || portCell->cellLibIndex_e == -4  )
		{
			// Means it's a PI
			// Do nothing

		}
		else if (portCell->info_early->isSequential)
		{
			//Means it's a sequential cell
			prevTData= portCell->ckList[0].tData;

			ComputeRATckPin(portCell);

			if(portCell->ckList[0].wire->isIncforRAT==false)
			{

				if( prevTData.ratFallEarly != portCell->ckList[0].tData.ratFallEarly ||
					prevTData.ratFallLate  != portCell->ckList[0].tData.ratFallLate   ||
					prevTData.ratRiseEarly != portCell->ckList[0].tData.ratRiseEarly ||
					prevTData.ratRiseLate  != portCell->ckList[0].tData.ratRiseLate )
					{
						clockQueue.push(portCell->ckList[0].wire);
						portCell->ckList[0].wire->isIncforRAT=true;
					}
			}

			ComputeSlackinCPPRPin(&portCell->ckList[0]);
		}
		else
		{
			//Means it's a combinational cell.

			int numIPins= portCell->iipList.size();

			for(int i=0;i<numIPins;i++)
			{

				InstanceIpin* iip= &(portCell->iipList[i]);

				if(iip->isConnected == false) continue;

				prevTData = iip->tData;

				ClearRAT(&iip->tData, BOTH);

				//Now updating RAT of current input pin based on all of its related output pins

				int numRelOPins = iip->relPinIndex.size();

				for(int j=0;j<numRelOPins;j++)
				{
					InstanceOpin* relOPin = &(portCell->iopList[iip->relPinIndex[j]]);

					if(relOPin->isConnected == false) continue;

					NODE_TIMING_DATA inTime, *inTemp = &(iip->tData);
					CRITICALITY iPinrat;

					int dDataInd=i;

					for(int k=0;k<(int)relOPin->dData.size();k++)
					{
						if(relOPin->dData[k].index == i)
						{
							dDataInd = k; break;
						}
					}

					iPinrat.indexFallEarly=iPinrat.indexFallLate=iPinrat.indexRiseLate=iPinrat.indexRiseEarly= iip->relPinIndex[j];
						// oPinInd here will be 'i'

					addIpinRatParam      (&inTime,&(relOPin->tData),&relOPin->dData[dDataInd],iPinrat, BOTH);
					UpdateWireDelayParam (&iip->tData,&inTime,inTemp,iPinrat,iPinrat, BOTH );

				}

				ComputeSlackinCPPRPin(iip);

				if(iip->wire->isIncforRAT==false)
					if( prevTData.ratFallEarly != iip->tData.ratFallEarly ||
						prevTData.ratFallLate  != iip->tData.ratFallLate   ||
						prevTData.ratRiseEarly != iip->tData.ratRiseEarly ||
						prevTData.ratRiseLate  != iip->tData.ratRiseLate )
					{
						prioQueue.push(iip->wire);
						iip->wire->isIncforRAT=true;
					}
			}
		}
	}
}


/**
 * Propagates PostCPPR-RAT backwards from D pin of the flip-flop or a PO during CPPR
 * and computes the PostCPPR-slacks along
 */
void BackwardPropagate:: ComputeRATinCPPR()
{

	//In this function, dPin means iPin of the instance, it can either be a D pin of sequential cell or a PO.
	int endPoints=ConeEndPoints.size();

	InstanceIpin*dPin;

	priority_queue <SpefNet*,deque<SpefNet*>, CompareNetLevel2> pQueue;
	priority_queue <SpefNet*,deque<SpefNet*>, CompareNetLevel2> ckQueue;

	int count=0;

	pair<int,int> holdSetupSense;
	for(int64 i=0;i<endPoints;i++)
	{
		if (i!=0 && (ConeEndPoints[i]->hash == ConeEndPoints[i-1]->hash && ConeEndPoints[i]->name.cellid == ConeEndPoints[i-1]->name.cellid))
				continue;
		count++;
		dPin = ConeEndPoints[i];

		if(dPin->isConnected==false)continue;

		NODE_TIMING_DATA prevTData;

		if(dPin->cellPtr->cellLibIndex_e != -3)         //Means it's input of sequential cell.
		{
			prevTData = dPin->cellPtr->ckList[0].tData;

			ComputeRATckPin       (dPin->cellPtr);
			ComputeSlackinCPPRPin (&dPin->cellPtr->ckList[0]);

			if(dPin->cellPtr->ckList[0].wire->isIncforRAT==false)
			{

				if( prevTData.ratFallEarly != dPin->cellPtr->ckList[0].tData.ratFallEarly ||
				prevTData.ratFallLate      != dPin->cellPtr->ckList[0].tData.ratFallLate   ||
				prevTData.ratRiseEarly     != dPin->cellPtr->ckList[0].tData.ratRiseEarly ||
				prevTData.ratRiseLate      != dPin->cellPtr->ckList[0].tData.ratRiseLate )
				{
					ckQueue.push(dPin->cellPtr->ckList[0].wire);
					dPin->cellPtr->ckList[0].wire->isIncforRAT=true;
				}
			}

		}
		else       //setting slack if PO (no need to change RAT)
		{
			continue;  //it's a PO, RAT wouldn't have changed, since CPPR won't apply on PO, on applies to D pins

		}
		if(dPin->wire->isIncforRAT==false)
		{
			pQueue.push(dPin->wire);
			dPin->wire->isIncforRAT=true;
			//WARNING: reset all the 'isIncforRAT' flags at the end
		}
	}

		BackTimingInCPPR (pQueue,ckQueue);  // Data-tree
		BackTimingInCPPR (ckQueue,ckQueue); // Clock-tree

		//CHECKPOINT: Now have to clear the 'isIncRAT' flag for the nets.

		int64 spefNetHashTablesz=spefNetHashTable.size();
		for(int64 i=0;i<spefNetHashTablesz;i++)
		{
			SpefNetList[spefNetHashTable[i].index].isIncforRAT = false;
		}

}


/**
 * \param prioQueue Priority queue containing set of nets as a starting point for RAT propagation
 * \param clockQueue Priority queue containing set of clock-tree nets as a starting point for RAT propagation
 *
 * Propagates Incremental-RAT backwards after a set of incremental operations performed
 */
void BackwardPropagate:: BackTimingInc(priority_queue < SpefNet*,deque<SpefNet*>, CompareNetLevel2 >& prioQueue,
		priority_queue < SpefNet*,deque<SpefNet*>, CompareNetLevel2 >& clockQueue)
{
	SpefNet* currNet;
	int64 numTaps;
	InstanceOpin* iop;
	while(!prioQueue.empty())
	{
		currNet = prioQueue.top();
		prioQueue.pop();
		numTaps=currNet->taps.size();
		iop = currNet->port.pinPtr;
		ClearRAT(&iop->tData);
		for(int64 i=0; i<numTaps ; i++)
		{

			InstanceIpin *iip = currNet->taps[i].pinPtr;
			NODE_TIMING_DATA inTime, *inTemp = &(iop->tData);
			addWireDelayParam(&inTime, &iip->tData, &currNet->taps[i] );
			CRITICALITY temp;
			temp.indexFallEarly = temp.indexFallLate = temp.indexRiseLate = temp.indexRiseEarly = i;
			UpdateWireDelayParam(&iop->tData, &inTime, inTemp,temp,temp );

		}
		if(iop->cellPtr->cellLibIndex_l == PI || iop->cellPtr->cellLibIndex_l == CLK_SRC || iop->cellPtr->info_late->isSequential)
			ComputeSlackPin(iop);
		else
			ComputeSlackPin(iop);

		Instance* portCell=iop->cellPtr;

		if(portCell->cellLibIndex_l == -2 || portCell->cellLibIndex_l == -4  )
		{
			// Means it's a PI
			// Do nothing

		}
		else if(portCell->info_late->isSequential)
		{
			//Means it's a sequential cell
			ComputeRATckPin(portCell);
			if(portCell->ckList[0].wire->isIncforRAT==false)
			{
				clockQueue.push(portCell->ckList[0].wire);
				portCell->ckList[0].wire->isIncforRAT=true;
			}

			ComputeSlackPin(&portCell->ckList[0]);
		}
		else
		{
			//Means it's a combinational cell.

			int numIPins= portCell->iipList.size();

			for(int i=0;i<numIPins;i++)
			{

				InstanceIpin* iip= &(portCell->iipList[i]);
				if(iip->isConnected == false) continue;
				ClearRAT(&iip->tData);

				//Now updating RAT of current input pin based on all of its related output pins
				int numRelOPins=iip->relPinIndex.size();
				for(int j=0;j<numRelOPins;j++)
				{
					InstanceOpin* relOPin = &(portCell->iopList[iip->relPinIndex[j]]);
					if(relOPin->isConnected == false) continue;
					NODE_TIMING_DATA inTime, *inTemp = &(iip->tData);
					CRITICALITY iPinrat;
					int dDataInd = i;

					for(int k=0;k<(int)relOPin->dData.size();k++)
					{
						if(relOPin->dData[k].index == i)
						{
							dDataInd = k;
							break;
						}
					}

					iPinrat.indexFallEarly=iPinrat.indexFallLate=iPinrat.indexRiseLate=iPinrat.indexRiseEarly= iip->relPinIndex[j]; // oPinInd here will be 'i'

					addIpinRatParam(&inTime,&(relOPin->tData),&relOPin->dData[dDataInd],iPinrat );
					UpdateWireDelayParam(&iip->tData,&inTime,inTemp,iPinrat,iPinrat );

				}

				ComputeSlackPin(iip);
				if(iip->wire->isIncforRAT==false)
				{
					prioQueue.push(iip->wire);
					iip->wire->isIncforRAT=true;
				}
			}
		}
	}
}

/**
 * \param iip Input D-pin or a PO
 * \param backPinList Set of output pins of launching flip-flops
 * \param conePinList Set of nets discovered in the cone of iip in reverse topological order
 * \param flagPath Coloring flag for the cone
 * \param pdi Thread-id
 * \param cutoff Cutoff slack for pruning
 * \param mode Early/Late mode
 *
 * Propagates RAT criticality backwards during CPPR and colors the cone of current D-pin or a PO. Colors the cone. Also returns
 * the set of nets from the cone in reverse topological order
 */
void BackwardPropagate::ComputeRATCritinCPPR
(
	InstanceIpin* iip,
	vector< InstanceOpin* >& backPinList,
	vector<SpefNet*>& conePinList,
	int64 flagPath,
	int pdi,
	double64 cutOff,
	bool mode
)
{
	assert(pdi<= NUM_THREADS);

	int cpprMode = mode;
	priority_queue <SpefNet*,deque<SpefNet*>, CompareNetLevel2> pQueue;

	if(iip->isConnected==false) return;

	SpefNet* currNet;
	currNet = iip->wire ;
	PARALLEL_DATA_IN pdataIn;
	PARALLEL_DATA_OUT pdataOut;

	iip->pData[pdi].flagPath = flagPath;
	pQueue.push(currNet);
	currNet->port.pinPtr->pData[pdi].flagPath = flagPath;

	NODE_TIMING_DATA inTime;

	while(!pQueue.empty())
	{
		currNet = pQueue.top();
		conePinList.push_back(currNet);
		pQueue.pop();

		ClearRAT(&inTime,cpprMode);

		InstanceOpin *iop=currNet->port.pinPtr;

		ClearRAT(&iop->pData[pdi],cpprMode);

		for(int64 i=0;i< (int64)currNet->taps.size();i++)
		{
			if(currNet->taps[i].pinPtr->pData[pdi].flagPath != flagPath) continue;
			InstanceIpin *iip = currNet->taps[i].pinPtr;

			ClearRAT(&inTime,cpprMode);
			CRITICALITY temp;
			if(mode == EARLY)
			{
				temp.indexFallEarly = temp.indexRiseEarly = i;
			}
			else {
				temp.indexFallLate = temp.indexRiseLate = i;
			}

			addWireDelayParam_PreCPPR (&inTime, iip, pdi,cpprMode);
			UpdateWireDelayParam      (&iop->pData[pdi], &inTime, &iop->pData[pdi],iop->pData[pdi].criticalRAT,
					                    temp,cpprMode);

		}


		double64 maxSlack = FORBIDDEN_SLACK;

		if (mode == EARLY)
		{
			double64 slack_RE                 = iop->tData.atRiseEarly - iop->pData[pdi].ratRiseEarly;
			double64 slack_FE                 = iop->tData.atFallEarly - iop->pData[pdi].ratFallEarly;
			if (maxSlack > slack_RE) maxSlack = slack_RE;
			if (maxSlack > slack_FE) maxSlack = slack_FE;
		}
		else
		{
			double64 slack_RL                 = iop->pData[pdi].ratRiseLate - iop->tData.atRiseLate;
			double64 slack_FL                 = iop->pData[pdi].ratFallLate - iop->tData.atFallLate;
			if (maxSlack > slack_RL) maxSlack = slack_RL;
			if (maxSlack > slack_FL) maxSlack = slack_FL;
		}


		if (maxSlack > (cutOff + FLOATING_ERROR_THRESHOLD))
		{
			SkipCnt++;
				continue;
		}
		else
			NoSkipCnt++;

		if(iop->cellPtr->cellLibIndex_e ==-2 || iop->cellPtr->cellLibIndex_e ==-4 || iop->cellPtr->info_early->isSequential)
		{
			//It's a PI
			//Do nothing.
			backPinList.push_back(iop);
		}

		else
		{
			//Means it's an output of combinational cell
			int numIPins= iop->dData.size();
			for(int i = 0 ; i < numIPins ; i ++)
			{

				InstanceIpin* iip= &(iop->cellPtr->iipList[iop->dData[i].index]);

				if(iip->isConnected == false) continue;

				int oPinInd = 0;
				for(int k = 0; k < (int)iop->cellPtr->iopList.size(); k++)
				{
					if(iop->hash == iop->cellPtr->iopList[k].hash)
					{
						oPinInd = k;
						break;
					}
				}
				CRITICALITY iPinrat;
				if(mode == EARLY)
				{
					iPinrat.indexFallEarly=iPinrat.indexRiseEarly= oPinInd; // oPinInd here will be 'i'

				}
				else
				{
					iPinrat.indexFallLate=iPinrat.indexRiseLate= oPinInd; // oPinInd here will be 'i'

				}
				ClearRAT(&inTime,cpprMode);


				if(iip->pData[pdi].flagPath != flagPath )
				{
					addIpinRatParam(&iip->pData[pdi],&iop->pData[pdi],&(iop->dData[i]),iPinrat,cpprMode);

					if(mode==EARLY)
					{
						iip->pData[pdi].criticalRAT.indexFallEarly = iPinrat.indexFallEarly;
						iip->pData[pdi].criticalRAT.indexRiseEarly = iPinrat.indexRiseEarly;
						iip->pData[pdi].criticalRAT.transitionFE   = iPinrat.transitionFE;
						iip->pData[pdi].criticalRAT.transitionRE   = iPinrat.transitionRE;
					}
					else
					{
						iip->pData[pdi].criticalRAT.indexFallLate  = iPinrat.indexFallLate;
						iip->pData[pdi].criticalRAT.indexRiseLate  = iPinrat.indexRiseLate;
						iip->pData[pdi].criticalRAT.transitionFL   = iPinrat.transitionFL;
						iip->pData[pdi].criticalRAT.transitionRL   = iPinrat.transitionRL;

					}

					iip->pData[pdi].flagPath = flagPath;

					if(iip->wire->port.pinPtr->pData[pdi].flagPath != flagPath)
					{
						iip->wire->port.pinPtr->pData[pdi].flagPath = flagPath;
						pQueue.push(iip->wire);
					}
				}
				else     //Has already been pushed in pQueue, just need to update it's preSlack
				{
					addIpinRatParam(&inTime,&(iop->pData[pdi]),&(iop->dData[i]),iPinrat,cpprMode);
					UpdateWireDelayParam(&(iip->pData[pdi]),&inTime,&(iip->pData[pdi]),
					               		 iip->pData[pdi].criticalRAT,iPinrat, cpprMode);
				}
			}
		}
	}
	return;
}

/**
 * Returns the TNS, leakage, worst slack and total Lambda-delay for the current state of the circuit
 */

SolutionState BackwardPropagate:: GetStateParams (void)
{
	SolutionState tmp;
	double64 tns        = 0;
	double64 lkg        = 0;
	double64 ws         = 0;
	double64 ld         = 0;

	for(int64 i = 0; i< (int64)InstanceHashTable.size(); i++)
	{
		if(InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PO)
		{
			for(int j = 0; j < (int)InstanceList[InstanceHashTable[i].index].iipList.size(); j++)
			{
				tns += std::min <double64> (0.0,InstanceList[InstanceHashTable[i].index].iipList[j].tData.slackRiseLate);
				tns += std::min <double64> (0.0,InstanceList[InstanceHashTable[i].index].iipList[j].tData.slackFallLate);
			}
		}
		else if(InstanceList[InstanceHashTable[i].index].cellLibIndex_l == PI)
		{
			// Do Nothing
		}
		else if(InstanceList[InstanceHashTable[i].index].cellLibIndex_l == CLK_SRC)
		{
			// Do Nothing
		}
		else if(InstanceList[InstanceHashTable[i].index].info_late->isSequential == true)
        {
			for(int j = 0; j < (int)InstanceList[InstanceHashTable[i].index].iipList.size(); j++)
			{
				tns += std::min <double64> (0.0,InstanceList[InstanceHashTable[i].index].iipList[j].tData.slackRiseLate);
				tns += std::min <double64> (0.0,InstanceList[InstanceHashTable[i].index].iipList[j].tData.slackFallLate);
			}

			lkg += InstanceList[InstanceHashTable[i].index].info_late->leakage;
        }
        else
        {
			for(int j = 0; j < (int)InstanceList[InstanceHashTable[i].index].iopList.size(); j++)
			{

				ws = std::min <double64> (ws,InstanceList[InstanceHashTable[i].index].iopList[j].tData.slackRiseLate);
				ws = std::min <double64> (ws,InstanceList[InstanceHashTable[i].index].iopList[j].tData.slackFallLate);

				InstanceOpin* oPin = &(InstanceList[InstanceHashTable[i].index].iopList[j]);

				for (int k = 0; k < (int)oPin->dData.size(); k++)
				{
					ld += oPin->dData[k].delayRiseLate * oPin->dData[k].lambdaRL;
					ld += oPin->dData[k].delayFallLate * oPin->dData[k].lambdaFL;
				}
			}

			lkg += InstanceList[InstanceHashTable[i].index].info_late->leakage;
        }

	}

	tmp.tns        = tns;
	tmp.lkg        = lkg;
	tmp.worstSlack = ws;
	tmp.totalLD    = ld;

	return tmp;
}


/**
 * Computes and porpagates incremental RAT from incremental cone-end points
 */

void BackwardPropagate:: ComputeRATInc()
{
	//In this function, dPin means iPin of the instance, it can either be a D pin of sequential cell or a PO.

	int endPoints=ConeEndPoints.size();
	sort (ConeEndPoints.begin(), ConeEndPoints.end(), compareEndPoints);

	InstanceIpin*dPin;
	priority_queue <SpefNet*,deque<SpefNet*>, CompareNetLevel2> pQueue;
	priority_queue <SpefNet*,deque<SpefNet*>, CompareNetLevel2> ckQueue;

	int count=0;
	pair<int,int> holdSetupSense;

	for(int64 i=0;i<endPoints;i++)
	{
		if (i!=0 && (ConeEndPoints[i]->hash == ConeEndPoints[i-1]->hash &&
				ConeEndPoints[i]->name.cellid == ConeEndPoints[i-1]->name.cellid))
				continue;
		count++;
		dPin=ConeEndPoints[i];

		if(dPin->isConnected==false)continue;

		if(dPin->wire->isIncforRAT==false)
		{
			pQueue.push(dPin->wire);
			dPin->wire->isIncforRAT=true;
			//WARNING: reset all the 'isIncforRAT' flags at the end
		}

		if(dPin->cellPtr->cellLibIndex_l != -3)         //Means it's input of sequential cell.
		{
			//First have to update RAT and slack of D pin , since at of ck has been changed.

			holdSetupSense=ComputeSetupHoldRat(&dPin->cellPtr->ckList[0].tData,dPin->cellPtr->ckList[0].hash,dPin->hash,
					dPin->cellPtr->cellLibIndex_e,dPin->cellPtr->cellLibIndex_l,(dPin->tData));

			ComputeSlackPin (dPin, true);
			ComputeRATckPin (dPin->cellPtr);
			ComputeSlackPin (&dPin->cellPtr->ckList[0]);

			if(dPin->cellPtr->ckList[0].wire->isIncforRAT==false)
			{
				ckQueue.push(dPin->cellPtr->ckList[0].wire);
				dPin->cellPtr->ckList[0].wire->isIncforRAT=true;
			}

		}
		else       //setting slack if PO (no need to change RAT)
			ComputeSlackPin(dPin, true);

		int pdi;
		for( pdi = 0; pdi < NUM_THREADS ; pdi++)
		{
			dPin->pData[pdi].ratFallEarly = dPin->tData.ratFallEarly;
			dPin->pData[pdi].ratFallLate  = dPin->tData.ratFallLate;
			dPin->pData[pdi].ratRiseEarly = dPin->tData.ratRiseEarly;
			dPin->pData[pdi].ratRiseLate  = dPin->tData.ratRiseLate;

		}


	}

		BackTimingInc (pQueue,ckQueue) ;  // Data-tree
		BackTimingInc (ckQueue,ckQueue);  // Clock-tree

		//CHECKPOINT: Now have to clear the 'isIncRAT' flag for the nets.

		int64 spefNetHashTablesz=spefNetHashTable.size();
		for(int64 i=0;i<spefNetHashTablesz;i++)
		{
			SpefNetList[spefNetHashTable[i].index].isIncforRAT = false;
		}

}

/**
 * \param isEarly Flag for the signal mode- Early or late
 * \param slew First index value for interpolation
 * \param cap Second index value for interpolation
 * \param sample LUT for interpolation (2D array)
 *
 * Evaluates a look-up table for delay or transition based on 2 dimensional indexing values
 */

double64 BackwardPropagate:: EvaluateLUT (bool isEarly,double64 slew , double64 cap, LibParserLUT& sample)
{
	if (cap < ZERO_VAL || slew < ZERO_VAL)
	{
		cout << cap << "\t" << slew << endl;
		assert(0);
	}

	//**********************************************************************************************************************************
	// for a sequential element, when computing setup/hold, slew will be nothing but clock slew while cap will be input pin transition.
	//**********************************************************************************************************************************

	if(sample.tableName=="scalar")
	{
			return sample.tableVals[0][0];
	}

	if(!SIZER)
	{
		if(sample.index_1.size()<1 || sample.index_2.size()<1)
			return (isEarly==true ? FORBIDDEN_EARLY : FORBIDDEN_LATE);
		if(fabs(slew)==987654 || fabs(cap)==987654)
			return (isEarly==true ? FORBIDDEN_EARLY : FORBIDDEN_LATE);
	}


	bool isReversed=false;

	if(!SIZER)
	{
		if(isEarly==true)
		{
			int templateListSize=libinfo_e.lut_templates.size();
			for(int i=0;i<templateListSize;i++)
			{
				if(sample.tableName==libinfo_e.lut_templates[i].name)
				{
					if(libinfo_e.lut_templates[i].variable1=="input_net_transition" || libinfo_e.lut_templates[i].variable1=="related_pin_transition")
						{
						    //means index_1 is slew, and index_2 is cap
							isReversed=true;
							break;
						}
				}
			}
		}
		else
		{
			int templateListSize=libinfo_l.lut_templates.size();
			for(int i=0;i<templateListSize;i++)
			{
				if(sample.tableName==libinfo_l.lut_templates[i].name)
				{
					if(libinfo_l.lut_templates[i].variable1=="input_net_transition" || libinfo_l.lut_templates[i].variable1=="related_pin_transition")
						{  //means index_1 is slew, and index_2 is cap
							isReversed=true;
							break;
						}
				}
			}
		}
	}

	int slewi,slewf,capi,capf;
	slewi = capi = slewf= capf = 0 ;
	double64 z_first,z_second,z_final;
	z_final = z_first = z_second = 0;
	int index1sz,index2sz;


	{
		index1sz = sample.index_1.size();
		index2sz = sample.index_2.size();
		if(isReversed == true)
		{

			if(cap<sample.index_2[0]){
				capi = 0;
				capf = 1;
			}
			else if(cap>sample.index_2[index2sz-1]){
				capi = index2sz-2;
				capf = index2sz-1;
			}
			else
			for(int i=0;i<index2sz-1;i++)
			{

				if(cap>sample.index_2[i] && cap<sample.index_2[i+1])
				{
					capi = i;
					capf = i+1;
					break;
				}
				else if(cap==sample.index_2[i])
				{
					capi = i;
					capf = i;
					break;
				}
				else if(cap==sample.index_2[i+1])
				{
					capi = i+1;
					capf = i+1;
					break;
				}

			}
			if(slew<sample.index_1[0]){
				slewi = 0;
				slewf = 1;
			}
			else if(slew>sample.index_1[index1sz-1]){
				slewi = index1sz-2;
				slewf = index1sz-1;
			}
			else
			for(int i=0;i < (int)sample.index_1.size()-1;i++)
			{
				if(slew > sample.index_1[i] && slew<sample.index_1[i+1])
				{
					slewi = i;
					slewf = i+1;
					break;
				}
				else if(slew == sample.index_1[i])
				{
					slewi = i;
					slewf = i;
					break;
				}
				else if(slew == sample.index_1[i+1])
				{
					slewi = i+1;
					slewf = i+1;
					break;
				}
			}

			if(slewi==slewf)
			{
				z_first  = sample.tableVals[slewi][capi];
				z_second = sample.tableVals[slewi][capf];

			}
			else if(slewi!=slewf)
			{
				z_first = (sample.tableVals[slewf][capi]-sample.tableVals[slewi][capi])*(slew-sample.index_1[slewi])/
						  (sample.index_1[slewf]-sample.index_1[slewi])+sample.tableVals[slewi][capi];

				z_second = (sample.tableVals[slewf][capf]-sample.tableVals[slewi][capf])*(slew-sample.index_1[slewi])/
						   (sample.index_1[slewf]-sample.index_1[slewi])+sample.tableVals[slewi][capf];
			}
			if(capi==capf)
				z_final = z_first;
			else if(capi!=capf)
			{
				z_final = ((z_second-z_first)*(cap-sample.index_2[capi])/(sample.index_2[capf]-sample.index_2[capi])+z_first);
			}
		}
		else  // means isReversed=false;
		{

			if(cap<sample.index_1[0]){
				capi = 0;
				capf = 1;
			}
			else if(cap>sample.index_1[index1sz-1]){
				capi = index1sz-2;
				capf = index1sz-1;
			}
			else
			for(int i=0;i < (int)sample.index_1.size()-1;i++)
				{
					if(cap>sample.index_1[i] && cap<sample.index_1[i+1])
					{
						capi = i;
						capf = i+1;
						break;
					}
					else if(cap==sample.index_1[i])
					{
						capi = i;
						capf = i;
						break;
					}
					else if(cap==sample.index_1[i+1])
					{
						capi = i+1;
						capf = i+1;
						break;
					}

				}
			if(slew<sample.index_2[0]){
				slewi = 0;
				slewf = 1;
			}
			else if(slew>sample.index_2[index2sz-1]){
				slewi = index2sz-2;
				slewf = index2sz-1;
			}
			else
				for(int i=0;i < (int)sample.index_2.size()-1;i++)
				{
					if(slew>sample.index_2[i] && slew<sample.index_2[i+1])
					{
						slewi = i;
						slewf = i+1;
						break;
					}
					else if(slew==sample.index_2[i])
					{
						slewi = i;
						slewf = i;
						break;
					}
					else if(slew==sample.index_2[i+1])
					{
						slewi = i+1;
						slewf = i+1;
						break;
					}
				}


				if(slewi==slewf)
					{
						z_first  = sample.tableVals[capi][slewi];
						z_second = sample.tableVals[capf][slewi];

					}
				else if(slewi!=slewf)
					{
						z_first  = (sample.tableVals[capi][slewf]-sample.tableVals[capi][slewi])*(slew-sample.index_2[slewi])/(sample.index_2[slewf]-sample.index_2[slewi])+sample.tableVals[capi][slewi];
						z_second = (sample.tableVals[capf][slewf]-sample.tableVals[capf][slewi])*(slew-sample.index_2[slewi])/(sample.index_2[slewf]-sample.index_2[slewi])+sample.tableVals[capf][slewi];
					}
					if(capi==capf)
						z_final = z_first;
					else if(capi!=capf)
					{
						z_final =((z_second-z_first)*(cap-sample.index_1[capi])/(sample.index_1[capf]-sample.index_1[capi])+z_first);
					}
		}
	}

	return z_final;
}

};

