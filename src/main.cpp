/*************************************************************************************
 * ********************************************************************************* *
 * *                                                                               * *
 * *  Copyright (c) 2016, Indian Institute of Technology Madras, India.            * *
 * *                                                                               * *
 * *  All Rights Reserved.                                                         * *
 * *                                                                               * *
 * *  This program is free software. You can redistribute and/or modify            * *
 * *  it in accordance with the terms of the accompanying license agreement.       * *
 * *  See LICENSE in the top-level directory for details.                          * *
 * *                                                                               * *
 * ********************************************************************************* *
 *************************************************************************************/




#include "include.h"
#include "timer.h"
#include "sizer.h"
#include "utility.h"
/**
 * \file main.h
 *
 * This file is external to the entire project, it uses 'iit' namespace and fires-off timing/sizing engines
 */

namespace iit
{
	extern  TimeIt    g_Timer;
	extern  string    Func_Names[NUM_FUNCTIONS];
	extern  double64  Func_Total_T[NUM_FUNCTIONS];
	extern  int64     Func_Total_C[NUM_FUNCTIONS];
	extern  int       Verbose;
}

/**
 * \param args Number of comand line arguments while executing the binary
 * \param argv Actual command line arguments passed  to the function
 *
 * This function is external to the entire project, it uses 'iit' namespace and fires-off timing/sizing engines
 */
int main(int args, char** argv)
{
	using namespace iit;

#if SIZER==false
	Timer iitRACE;
#endif

#if SIZER==true
	Sizer iitRACE;
#endif

	debug_t_start(Engine_enum);

	iitRACE.Engine(args,argv);

	debug_t_stop(Engine_enum);

	if(Verbose)
	{
		// print total time taken by any function, provided you use debug_t_start and debug_t_stop before and after calling that fn.
		cout << string("Time taken for ").append(Func_Names[Engine_enum]) << " " << Func_Total_T[Engine_enum] << endl;
		cout << string("Time taken for ").append(Func_Names[BuildCircuit_enum]) << " " << Func_Total_T[BuildCircuit_enum] << endl;
		cout << " No. of calls to " << Func_Names[Bsearch_enum] << " " << Func_Total_C[Bsearch_enum] << endl;
		// and so on.
	}

	return 0;
}








